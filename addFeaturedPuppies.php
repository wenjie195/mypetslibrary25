<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Kitten.php';
require_once dirname(__FILE__) . '/classes/Pets.php';
require_once dirname(__FILE__) . '/classes/Puppy.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $puppies = getPuppy($conn, "WHERE status = 'Available' ORDER BY date_created DESC ");
// $featuredPuppies = getPuppy($conn, "WHERE featured_seller = 'Yes' AND status = 'Available' ORDER BY date_created DESC ");

if (isset($_GET['pageno'])) {
      $pageno = $_GET['pageno'];
} else {
      $pageno = 1;
}

$no_of_records_per_page = 500;
$offset = ($pageno-1) * $no_of_records_per_page;

$total_pages_sql = "SELECT COUNT(*) FROM puppy WHERE status != 'Deleted'";
$result = mysqli_query($conn,$total_pages_sql);
$total_rows = mysqli_fetch_array($result)[0];
$total_pages = ceil($total_rows / $no_of_records_per_page);

if (isset($_GET['search']))
{
      $puppies = getPuppy($conn, "WHERE sku= ? ORDER BY date_created DESC ",array("sku"),array($_GET['search']), "s");
      $featuredPuppies = getPuppy($conn, "WHERE featured_seller = 'Yes' AND sku= ? ORDER BY date_created DESC ",array("sku"),array($_GET['search']), "s");
}
else
{
      $puppies = getPuppy($conn, " WHERE status != 'Deleted' ORDER BY date_created DESC LIMIT $offset, $no_of_records_per_page");
      $featuredPuppies = getPuppy($conn, "WHERE featured_seller = 'Yes' AND status != 'Deleted' ORDER BY date_created DESC LIMIT $offset, $no_of_records_per_page");
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add Featured Puppies | Mypetslibrary" />
<title>Add Featured Puppies | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance  padding-bottom30">
	<div class="width100">
		<div class="left-h1-div featured-left">
                  <a href="featuredPets.php"><h1 class="green-text h1-title opacity-hover"><img src="img/back2.png" class="back-png"> Add Featured Puppies</h1></a>
                  <div class="green-border"></div>
		</div>
      </div>

      <div class="clear"></div>

      <div class="width100">
        <?php //echo $_SERVER["PHP_SELF"] ?>
          <form action="search/searchFunction.php" method="post">
                  <?php
                  if (isset($_GET['search'])) {
                    ?>
                    <input class="line-input clean" type="text" value="<?php echo $_GET['search'] ?>" name="search" placeholder="Search">
                    <?php
                  }else {
                    ?>
                    <input class="line-input clean" type="text" name="search" placeholder="SKU">
                    <?php
                  }
                   ?>
                   <!-- SKU and pet name-->
                   <input type="hidden" name="location" value="<?php echo $_SERVER["PHP_SELF"] ?>">
                   <button class="search-btn hover1 clean" type="submit">
                         <img src="img/search.png" class="visible-img hover1a" alt="Search" title="Search">
                         <img src="img/search2.png" class="visible-img hover1b" alt="Search" title="Search">
                   </button>
            </form>
      </div>

	<div class="width103 border-separation" id="app">

      <?php
      if($featuredPuppies)
      {
            $totalFeaturedPuppy = count($featuredPuppies);
      }
      else
      {    $totalFeaturedPuppy = 0;   }
      ?>

      <?php
      if($totalFeaturedPuppy >= 10)
      {
            if($puppies)
            {
                  for($cntAA = 0;$cntAA < count($puppies) ;$cntAA++)
                  {
                  ?>
                  <div class="four-box-size">
                        <div class="shadow-white-box">
                        	<div class="square">
                              <div class="width100 white-bg content progressive">
                              
                                <img data-src="./uploads/<?php echo $puppies[$cntAA]->getDefaultImage();?>" src="img/pet-load300.jpg" class="preview width100 two-border-radius lazy" alt="<?php echo $puppies[$cntAA]->getBreed();?>"
                                    title="<?php echo $puppies[$cntAA]->getBreed();?>" />
                       
                              </div>
                            </div>
                        <!-- Display none or add class hidden if the dog not yet sold -->
                        <!--<div class="sold-label">Sold</div>  -->

                              <?php
                                    $statusA = $puppies[$cntAA]->getStatus();
                                    if($statusA == 'Sold')
                                    {
                                    ?>
                                          <div class="sold-label sold-label3">Sold</div>
                                    <?php
                                    }
                                    else
                                    {}
                              ?>

                        <div class="width100 product-details-div">

                                    <?php 
                                          $dogGender = $puppies[$cntAA]->getGender();
                                          if($dogGender == 'Female')
                                          {
                                                $puppyGender = 'F';
                                          }
                                          elseif($dogGender == 'Male')
                                          {
                                                $puppyGender = 'M';
                                          }
                                    ?>

                              <p class="width100 text-overflow slider-product-name"><?php echo $puppies[$cntAA]->getBreed();?> | <?php echo $puppyGender;?></p>
                              <p class="width100 text-overflow slider-product-name">
                                    <!-- <?php //echo $puppies[$cntAA]->getSku();?> -->
                                    <?php 
                                          $dogSku = $puppies[$cntAA]->getSku();
                                          if($dogSku != "")
                                          {
                                                echo $dogSku;
                                          }
                                          else
                                          {
                                                echo "-";
                                          }
                                    ?>
                              </p>
                        </div>
                  </div>

                  <form method="POST" action="utilities/adminDeleteFeaturedPuppyFunction.php">
                        <button class="clean red-btn featured-same-button open-confirm" type="submit" name="seller_uid" value="<?php echo $puppies[$cntAA]->getUid();?>">
                              Delete
                        </button>
                  </form>

                  </div>
                  <?php
                  }
                  ?>
            <?php
            }


      }
      elseif($totalFeaturedPuppy < 10)
      {

            if($puppies)
            {
                  for($cntAA = 0;$cntAA < count($puppies) ;$cntAA++)
                  {
                  ?>
                        <div class="four-box-size">
                              <div class="shadow-white-box">
                               <div class="square">
                                    <div class="width100 white-bg content progressive">
                                <img data-src="./uploads/<?php echo $puppies[$cntAA]->getDefaultImage();?>" src="img/pet-load300.jpg" class="preview width100 two-border-radius lazy" alt="<?php echo $puppies[$cntAA]->getBreed();?>"
                                    title="<?php echo $puppies[$cntAA]->getBreed();?>" />
                                    </div>
                                 </div>
                                <!-- Display none or add class hidden if the dog not yet sold -->
                                <!--<div class="sold-label">Sold</div>-->

                                    <?php
                                          $statusB = $puppies[$cntAA]->getStatus();
                                          if($statusB == 'Sold')
                                          {
                                          ?>
                                                <div class="sold-label sold-label3">Sold</div>
                                          <?php
                                          }
                                          else
                                          {}
                                    ?>

                              <div class="width100 product-details-div">

                                    <?php 
                                          $dogGender = $puppies[$cntAA]->getGender();
                                          if($dogGender == 'Female')
                                          {
                                                $puppyGender = 'F';
                                          }
                                          elseif($dogGender == 'Male')
                                          {
                                                $puppyGender = 'M';
                                          }
                                    ?>

                                    <p class="width100 text-overflow slider-product-name"><?php echo $puppies[$cntAA]->getBreed();?> | <?php echo $puppyGender;?></p>
                                    <p class="width100 text-overflow slider-product-name">
                                          <!-- <?php //echo $puppies[$cntAA]->getSku();?> -->
                                          <?php 
                                                $dogSku = $puppies[$cntAA]->getSku();
                                                if($dogSku != "")
                                                {
                                                      echo $dogSku;
                                                }
                                                else
                                                {
                                                      echo "-";
                                                }
                                          ?>
                                    </p>
                              </div>
                        </div>

                        <?php
                        $status = $puppies[$cntAA]->getFeaturedSeller();
                        if($status == 'Yes')
                        {
                        ?>
                              <form method="POST" action="utilities/adminDeleteFeaturedPuppyFunction.php">
                                    <button class="clean red-btn featured-same-button open-confirm" type="submit" name="pets_uid" value="<?php echo $puppies[$cntAA]->getUid();?>">
                                          Delete
                                    </button>
                              </form>
                        <?php
                        }
                        else
                        {
                        ?>
                              <form method="POST" action="utilities/adminAddFeaturedPuppyFunction.php">
                                    <button class="clean green-button featured-same-button" type="submit" name="pets_uid" value="<?php echo $puppies[$cntAA]->getUid();?>">
                                          Add
                                    </button>
                              </form>
                        <?php
                        }
                        ?>

                  </div>
                  <?php
                  }
                  ?>
            <?php
            }


      }
      ?>
      <div class="width100 text-center overflow">
            <ul class="pagination page-pagi">
                  <li><a href="?pageno=1">First</a></li>
                  <li class="<?php if($pageno <= 1){ echo 'disabled'; } ?>">
                        <a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>">Prev</a>
                  </li>
                  <li class="<?php if($pageno >= $total_pages){ echo 'disabled'; } ?>">
                        <a href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>">Next</a>
                  </li>
                  <li><a href="?pageno=<?php echo $total_pages; ?>">Last</a></li>
            </ul>
      </div>

      <!-- <?php
      $conn = connDB();
      if($puppies)
      {
            for($cntAA = 0;$cntAA < count($puppies) ;$cntAA++)
            {
            ?>
            <div class="four-box-size">
                  <div class="shadow-white-box featured">
                        <div class="width100 white-bg">
                              <img src="uploads/<?php echo $puppies[$cntAA]->getImageOne();?>" alt="<?php echo $puppies[$cntAA]->getName();?>"
                              title="<?php echo $puppies[$cntAA]->getName();?>" class="width100 two-border-radius">
                        </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name"><?php echo $puppies[$cntAA]->getName();?></p>
                        <p class="width100 text-overflow slider-product-price"><?php echo $puppies[$cntAA]->getSku();?></p>
                  </div>
            </div>

                  <?php
                  $status = $puppies[$cntAA]->getFeaturedSeller();
                  if($status == 'Yes')
                  {
                  ?>
                        <form method="POST" action="utilities/adminDeleteFeaturedPuppyFunction.php">
                              <button class="clean red-btn featured-same-button open-confirm" type="submit" name="pets_uid" value="<?php echo $puppies[$cntAA]->getUid();?>">
                                    Delete
                              </button>
                        </form>
                  <?php
                  }
                  else
                  {
                  ?>
                        <form method="POST" action="utilities/adminAddFeaturedPuppyFunction.php">
                              <button class="clean green-button featured-same-button" type="submit" name="pets_uid" value="<?php echo $puppies[$cntAA]->getUid();?>">
                                    Add
                              </button>
                        </form>
                  <?php
                  }
                  ?>

            </div>
            <?php
            }
            ?>
      <?php
      }
      $conn->close();
      ?> -->

      </div>

</div>
<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
      $messageType = null;

      if($_SESSION['messageType'] == 1)
      {
            if($_GET['type'] == 1)
            {
            $messageType = "Featured Puppy Added !";
            }
            else if($_GET['type'] == 2)
            {
            $messageType = "Fail to add featured puppy !! ";
            }
            else if($_GET['type'] == 3)
            {
            $messageType = "ERROR !! ";
            }
            else if($_GET['type'] == 4)
            {
            $messageType = "Featured Puppy Deleted !";
            }
            else if($_GET['type'] == 5)
            {
            $messageType = "Fail to delete puppy !! ";
            }
            else if($_GET['type'] == 6)
            {
            $messageType = "ERROR <br> Please Retry !! ";
            }
            echo '
            <script>
            putNoticeJavascript("Notice","'.$messageType.'");
            </script>
            ';
            $_SESSION['messageType'] = 0;
      }

}
?>

</body>
</html>
