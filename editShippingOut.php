<?php
// if (session_id() == ""){
//     session_start();
// }
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $id = $_SESSION['order_id'];

$conn = connDB();

$productsOrders =  getProductOrders($conn);

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    if(isset($_POST["shipping_method"])){
        $shipping_method = rewrite($_POST["shipping_method"]);
        $tracking_number = rewrite($_POST["tracking_number"]);
        $shipping_date = rewrite($_POST["shipping_date"]);
        $shipping_status = rewrite($_POST["shipping_status"]);
        $order_id = rewrite($_POST["order_id"]);
    }else{
        $shipping_method = "";
        $tracking_number = "";
        $shipping_date = "";
        $shipping_status = "";
        $order_id = "";
    }
}

$conn->close();
function promptError($msg)
{
    echo '<script>  alert("'.$msg.'");  </script>';
}

function promptSuccess($msg)
{
    echo '<script>  alert("'.$msg.'");   </script>';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Shipping Request | Mypetslibrary" />
<title>Shipping Request | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
 
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
    <h1 class="green-text user-title left-align-title opacity-hover" onclick="goBack()"><img src="img/back2.png" class="back-button" alt="Back" title="Back"> Order Details</h1>
    <div class="clear"></div>
    <?php
        if(isset($_POST['order_id']))
        {
            $conn = connDB();
            $orders = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['order_id']),"s");
        ?>
            <div class="dual-div">
                <p class="green-text top-text">Order by</p>
                <p class="bottom-text"><?php echo $orders[0]->getName();?></p>
            </div>
            <div class="dual-div second-dual-div">
                <p class="green-text top-text">Order No.</p>
                <p class="bottom-text"><?php echo $orders[0]->getId();?></p>
            </div> 
            <div class="clear"></div>
            <div class="dual-div">
                <p class="green-text top-text">Paid On</p>
                <p class="bottom-text"><?php echo $orders[0]->getPaymentDate();?></p>
            </div>
            <div class="dual-div second-dual-div">
                <p class="green-text top-text">Payment Details</p>
                <p class="bottom-text"><a class="green-a">View Here</a></p>
            </div>
            <?php
        }
        ?>     
        <div class="clear"></div>
        <?php
        if(isset($_POST['order_id']))
        {
            $orderId = $_POST['order_id'];
            $conn = connDB();
            //$orders = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"s");
            $orders = getProductOrders($conn);
            $order = getOrders($conn);
            ?>
            <div class="width100 overflow">
                <p class="green-text top-text">Ordered Products</p>
                <div class="table-scroll-div">
                    <table class="order-table">
                        <thead>	
                            <tr>
                                <th>No.</th>
                                <th>Product</th>
                                <th>Qty</th>
                                <th class="price-column">Price per Quantity (RM)</th>
                                <th class="price-column">Total Price (RM)</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                        for($cnt = 1;$cnt < count($orders) ;$cnt++){
                            if($orders[$cnt]->getOrderId() == $orderId)
                            {
                                ?>
                                <tr>
                                    <td><?php echo $cnt; ?></td>
                                    <td><?php echo $orders[$cnt]->getProductName();?></td>
                                    <td><?php echo $orders[$cnt]->getQuantity();?></td>
                                    <td class="price-column"><?php echo $orders[$cnt]->getFinalPrice();?></td>
                                    <td class="price-column"><?php echo $orders[$cnt]->getTotalProductPrice();?></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <table class="price-table">
                    <tbody>
                        <?php 
                            for($cnt = 1;$cnt < count($order) ;$cnt++){
                                if($order[$cnt]->getId() == $orderId)
                                {
                                    ?>
                                    <tr>
                                        <td>Subtotal</td>
                                        <td class="price-padding">RM <?php echo $order[$cnt]->getSubtotal();?>.00</td>
                                    </tr>
                                    <tr>
                                        <td>Shipping</td>
                                        <td class="price-padding">RM30.00</td>
                                    </tr>
                                    <tr>
                                        <td>Discount</td>
                                        <td class="price-padding">RM0.00</td>
                                    </tr>
                                    <tr>
                                        <td class="last-td"><b>Total</b></td>
                                        <td class="last-td price-padding"><b>RM <?php echo $order[$cnt]->getTotal();?>.00</b></td>
                                    </tr>
                                <?php
                                }
                            }
                        ?>                                                
                    </tbody>
                </table>
            </div>
            <?php
            }
            ?>
            <div class="clear"></div>
            <?php
            if(isset($_POST['order_id']))
            {
                $conn = connDB();
                $orders = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['order_id']),"s");
                ?>
                <div class="width100 overflow some-margin-bottom">
                    <p class="green-text top-text">Delivery Address</p>
                    <p class="bottom-text"><?php echo $orders[0]->getName();?></p>  
                    <p class="bottom-text">(+60) <?php echo $orders[0]->getContactNo();?></p>
                    <p class="bottom-text"><?php echo $orders[0]->getAddressLine1();?></p>
                    <p class="bottom-text"><?php echo $orders[0]->getAddressLine2();?></p> 
                    <p class="bottom-text"><?php echo $orders[0]->getCity();?></p> 
                    <p class="bottom-text"><?php echo $orders[0]->getZipcode();?></p>  
                    <p class="bottom-text"><?php echo $orders[0]->getState();?></p> 
                    <p class="bottom-text"><?php echo $orders[0]->getCountry();?></p>        	
                </div>
            <?php
        }
        ?>
        <div class="clear"></div>
        <form action="utilities/updateShippingFunction.php" method="POST">
        <?php
            if(isset($_POST['order_id']))
            {
                $conn = connDB();
                $orders = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['order_id']),"s");
            ?>
            <div class="dual-div">
                    <p class="green-text top-text">Shipping Method</p>
                    <p class="clean tele-input clean-bg"><?php echo $orders[0]->getShippingMethod();?></p> 
                    <!-- <input class="input-name clean input-textarea admin-input date-input" type="text" placeholder="Shipping Method" id="shipping_method" name="shipping_method" required>             -->
                </div>
                <div class="dual-div second-dual-div">
                    <p class="green-text top-text">Tracking No.</p>
                    <p class="clean tele-input clean-bg"><?php echo $orders[0]->getTrackingNumber();?></p> 
                    <!-- <input class="input-name clean input-textarea admin-input date-input" type="text" placeholder="Tracking No." id="tracking_number" name="tracking_number">  -->
                </div>         
                <div class="clear"></div>
                <div class="dual-div">
                    <p class="green-text top-text">Shipping Status</p>
                    <select class="input-name clean admin-input" type="text" id="shipping_status" name="shipping_status" required >
                        <option value="SHIPPING" name="Shipping">Shipping</option>
                        <option value="DELIVERED" name="Delivered">Delivered</option>
                    </select>   
                </div>
                <div class="dual-div second-dual-div">
                    <p class="green-text top-text">Delivered On</p>
                    <p class="clean tele-input clean-bg"><?php echo $orders[0]->getShippingDate();?></p> 
                    <!-- <input class="input-name clean input-textarea admin-input date-input" type="date" placeholder="Delivered Date" id="delivered_on" name="delivered_on">  -->
                </div> 

                <div class="dual-div second-dual-div">
                    <input class="input-name clean input-textarea admin-input date-input" type="hidden" id="order_id" name="order_id" value="<?php echo $_POST['order_id'];?>"> 
                </div> 
                
                <div class="width100 overflow text-center">     
                    <button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
                </div>
                <?php
            }
        ?>
        </form>   
    
</div>
<div class="clear"></div>

<?php include 'js.php'; ?>


</body>
</html>