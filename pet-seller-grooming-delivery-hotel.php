<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon2.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/Services.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pet Seller and Other Services | Mypetslibrary" />
<title>Pet Seller and Other Services | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="pet seller, partner, Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<script src="js/jquery-2.2.0.min.js"></script>
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
      <div class="fix-filter width100 small-padding3 overflow some-margin-top">
            <h1 class="green-text user-title left-align-title">Sellers</h1>
            <div class="filter-div">
            	<a class="open-filter2 filter-a green-a ow-open-filter">Filter</a>
            </div>
      </div>
<!-- Filter Modal -->
<div id="filter-modal2" class="modal-css ow-show-modal">

      <!-- Modal content -->
      <div class="modal-content-css big-filter-margin">

      <span class="close-css close-filter2">&times;</span>

      <div class="clear"></div>

      <h2 class="green-text h2-title">Filter</h2>

      <div class="filter-div-section">
           
            <form action="pet-seller-grooming-delivery-hotel-search.php" method="post"  target="_blank">
              <input class="line-input filter-search-ow clean" type="text" name="seller_name" id="seller_name" placeholder="Search"/>
                                        <button class="search-btn hover1 clean ow-margin-top0" type="submit">
                                              <img src="img/search.png" class="visible-img hover1a" alt="Search" title="Search">
                                              <img src="img/search2.png" class="visible-img hover1b" alt="Search" title="Search">
                                        </button>
            </form>

      </div>
      <div class="filter-div-section">
                        <p class="filter-label filter-label3 green-filter">State</p>
                        <?php
                        $query = "
                        SELECT * FROM states
                        ";
                        $statement = $connect->prepare($query);
                        $statement->execute();
                        $result = $statement->fetchAll();
                        foreach($result as $row)
                        {
                        ?>

                              <div class="filter-option">
                                    <label  class="filter-label filter-label2">
                                          <input type="checkbox" class="common_selector filter-option state" value="<?php echo $row['state_name']; ?>" > <?php echo $row['state_name']; ?><span class="checkmark"></span>
                                    </label>
                              </div>

                        <?php
                        }
                        ?>
                  </div>
	</div>
    <div class="green-button mid-btn-width clean mid-btn-margin close-filter2">Apply</div> 
</div>

    

<div class="width100 small-padding overflow min-height-with-filter filter-distance ow-filter-pet-data">
      <div class="width103">     
            <div class="filter_data"></div>
      </div>
</div>

<div class="clear"></div>


      <style>
            .animated.slideUp{
                  animation:none !important;}
            .animated{
                  animation:none !important;}
            .partner-a .hover1a{
                  display:none !important;}
            .partner-a .hover1b{
                  display:inline-block !important;}
      </style>

      <script>
            $(document).ready(function(){

                  filter_data();

                  function filter_data()
                  {
                  $('.filter_data').html('<div id="loading" style="" ></div>');
                  var action = 'filterdataGrooming.php';
                  var state = get_filter('state');
                  var services = get_filter('services');
                  $.ajax({
                        url:"filterdataGrooming.php",
                        method:"POST",
                        data:{action:action, state:state, services:services},
                        success:function(data){
                              $('.filter_data').html(data);
                        }
                  });
                  }

                  function get_filter(class_name)
                  {
                  var filter = [];
                  $('.'+class_name+':checked').each(function(){
                        filter.push($(this).val());
                  });
                  return filter;
                  }

                  $('.common_selector').click(function(){
                  filter_data();
                  });

            });
      </script>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>
