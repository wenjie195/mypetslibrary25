<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$newProductUid = $_SESSION['product_uid'];

// $newPetsType = $_SESSION['newpets_type'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$sellerDetails = getSeller($conn);

// $uid = md5(uniqid());
$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add Product Image | Mypetslibrary" />
<title>Add Product Image | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<link rel="stylesheet" href="css/dropzone.min.css">
<?php include 'css.php'; ?>

</head>

<body class="body">

<?php include 'header.php'; ?>

<!-- <div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
    <h1 class="green-text h1-title">Upload Product Photo Maximum 6*</h1>
    <div class="green-border"></div>
  </div>
  <div class="border-separation">
    <form action="utilities/addMultiImageProductFunction.php" >
	  <div class="dropzone" id="my-awesome-dropzone">
      <div id="displayTable"></div>
    </div>
		<div class="clear"></div>
    <div class="width100 overflow text-center">
      <button class="green-button white-text clean2 edit-1-btn margin-auto" id="submit" value="submitBtn">Submit</button>
    </div>    
    </form>
  </div>
</div> -->

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
    <h1 class="green-text h1-title">Upload Product Description Photo Maximum 6 (Optional)</h1>
    <div class="green-border"></div>
  </div>
  <div class="border-separation">
    <form action="utilities/addMultiImageProductFunction.php" >
      <!-- <form action="selectDefaultPuppy.php" method="post"> -->
	<div class="dropzone" id="my-awesome-dropzone">
    
    <div id="displayTable"></div>
    </div>
		<div class="clear"></div>
      <div class="width100 overflow text-center">
      	<button class="green-button white-text clean2 edit-1-btn margin-auto" id="submit" value="submitBtn">Next</button>
      </div>    
    
    </form>
  </div>
</div>

<?php include 'js.php'; ?>
<script src="js/dropzone.min.js"></script>
<script>

$(function(){
    var myDropzone = new Dropzone(".dropzone", {
    url: "utilities/addMultiImageProductFunction.php",
    paramName: "file",
    maxFilesize: 5,
	  maxFiles: 6,
    addRemoveLinks: true,
    dictResponseError: 'Server not Configured',
    acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
    // autoProcessQueue: false,
    init:function(){
      var self = this;

      // config
      self.options.addRemoveLinks = true;
      self.options.dictRemoveFile = "Delete";
      
      //New file added
      self.on("addedfile", function (file) {
        console.log('new file added ', file);
      });

      // Send file starts
      self.on("sending", function (file) {
        console.log('upload started', file);
        $('.meter').show();
      });
      
      // File upload Progress
      self.on("totaluploadprogress", function (progress) {
        console.log("progress ", progress);
        $('.roller').width(progress + '%');
      });

      self.on("queuecomplete", function (progress) {
        $('.meter').delay(999).slideUp(999);
      });
      
      // On removing file
      self.on("removedfile", function (file) {
        console.log(file);
      });

	    self.on("maxfilesexceeded", function(file) { 
        this.removeFile(file); 
      });
    }
  });
          });
</script>
</body>
</html>
