<?php
// if (session_id() == "")
// {
//   session_start();
// }
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Cart.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    addToCart();
    createOrder($conn,$uid);
    header('Location: ./checkout.php');
}

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $slider = getSlider($conn," WHERE status = 'Show' ");
$products = getProduct($conn, "WHERE status = 'Available' ");

$productListHtml = "";

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="View Cart | Mypetslibrary" />
<title>View Cart | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
                    
<div class="width100 same-padding menu-distance min-height big-black-text user-dash user-dash2 product-padding">
        <div class="width100 overflow">
			<h1 class="green-text user-title left-align-title">Cart</h1>
                    
            <a href="cartEmpty.php">
                <button class="right-delete clean transparent-button right-align-link">Delete All</button>
                
            </a>
    	</div> 
        <div class="clear"></div>

	<div class="clear"></div>
    <div class="grey-border2"></div>
	<div  id="Cart" class="width100">
        <form method="POST">
            <?php
                $conn = connDB();
                if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'])
                {
                    $productListHtml = getShoppingCart($conn,2);
                    echo $productListHtml;
                }
                else
                {
                    echo " <h3>Your cart is empty.</h3>";
                }


                if(array_key_exists('xclearCart', $_POST))
                {
                    xclearCart();
                }
                else
                {
                // code...
                    unset($productListHtml);
                }

                $conn->close();
            ?>
        </form>
    </div>  
</div>

<style>
.footer-div{
	display:none;}

</style>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>
</body>
</html>
