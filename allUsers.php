<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

//$userRows = getSeller($conn,"WHERE id = ?",array("id"),array($id),"s");
// $userDetails = getUser($conn," WHERE user_type = ? ",array("user_type"),array(1),"s");
$userDetails = getUser($conn," WHERE user_type = ? OR user_type = ? ",array("user_type","user_type"),array(1,5),"ss");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="All Users | Mypetslibrary" />
<title>All Users | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance" id="myTable">
	<div class="width100">
        <div class="left-h1-div left-user-div">
            <h1 class="green-text h1-title">All Users | <a href="bannedUsers.php" class="green-a">Banned</a></h1>
            <div class="green-border"></div>
        </div>
        <div class="mid-search-div">
        	<form>
            <input class="line-input clean" type="text" id="myInput" onkeyup="myFunction()" placeholder="Search">
                <button class="search-btn hover1 clean">
                        <img src="img/search.png" class="visible-img hover1a" alt="Search" title="Search">
                        <img src="img/search2.png" class="visible-img hover1b" alt="Search" title="Search">
                </button>
            </form>
        </div>
        <div class="right-add-div">
        	<a href="addUser.php"><div class="green-button white-text">Add User</div></a>
        </div>
        
    </div>
    <div class="clear"></div>
	<div class="width100 scroll-div border-separation">
    	<table class="green-table width100">
        	<thead>
            	<tr>
                	<th class="first-column">No.</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Contact</th>
                    <th>Facebook ID</th>
                    <th>Joined Date</th>
                    <th>Status</th>
                    <th>Details</th>
                    <th>Profile Picture</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($userDetails)
                    {
                        
                        for($cnt = 0;$cnt < count($userDetails) ;$cnt++)
                        {?>
                            
                            <tr class="link-to-details">
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $userDetails[$cnt]->getName();?></td>
                                <td><?php echo $userDetails[$cnt]->getEmail();?></td>
                                <td><?php echo $userDetails[$cnt]->getPhoneNo();?></td>
                                <td><?php echo $userDetails[$cnt]->getFbId();?></td>
                                <td>
                                    <?php echo $date = date("d-m-Y",strtotime($userDetails[$cnt]->getDateCreated()));?>
                                </td>
                                <td>
                                    <!-- <?php //echo $userDetails[$cnt]->getAccountStatus();?> -->
                                    <?php 
                                        $userType = $userDetails[$cnt]->getUserType();
                                        if($userType == 1)
                                        {
                                            echo "Active";
                                        }
                                        elseif($userType == 5)
                                        {
                                            echo "Banned";
                                        }
                                    ?>
                                </td>
                                <td>
                                    <form action="editUser.php" method="POST" class="hover1">
                                        <button class="clean hover1 img-btn" type="submit" name="user_uid" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                            <img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                                            <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                                        </button>
                                    </form>
                                </td>

                                <td>
                                    <form action="adminUpdateUserProfilePic.php" method="POST" class="hover1">
                                        <button class="clean hover1 transparent-button pointer" type="submit" name="user_uid" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                            <img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                                            <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                                        </button>
                                    </form>                            
                                </td>

                                <td>
                                    <form method="POST" action="utilities/deleteUserFunction.php" class="hover1">
                                        <button class="clean hover1 img-btn" type="submit" name="user_uid" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                            <img src="img/delete1a.png" class="edit-icon1 hover1a" alt="Delete" title="Delete">
                                            <img src="img/delete3a.png" class="edit-icon1 hover1b" alt="Delete" title="Delete">
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>
<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "User Deleted !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to delete user !! ";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "User Account Updated !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update user account !! ";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

}
?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>
</body>
</html>