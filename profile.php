<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Favorite.php';
require_once dirname(__FILE__) . '/classes/Pets.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
$_SESSION['url'] = $_SERVER['REQUEST_URI'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$allFavorite = getFavorite($conn, "WHERE uid =? AND status = 'Yes' ",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Profile | Mypetslibrary" />
<title>Profile | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

	<?php
    if ($_SESSION['fb_login'])
    {
    ?>
      <div class="width100 same-padding overflow min-height menu-distance2">
     		<div class="width100 overflow profile-top-div">
            	<a href="updateProfilePic.php">
                    <div class="profile-pic-div">
                        <div class="update-div-div">
                            <!-- <img src="img/update-profile-pic.jpg" class="profile-pic update-profile-pic" alt="Update Profile Picture" title="Update Profile Picture"> -->
                        </div>
                        <?php
                            if(isset($_SESSION['user_image']))
                            {
                            ?>
                                <img src="<?php echo $_SESSION['user_image'] ?>" class="profile-pic" alt="Profile Picture" title="Profile Picture">
                            <?php
                            }
                            else
                            {
                            ?>
                                <img src="img/profile-pic.jpg" class="profile-pic" alt="Profile Picture" title="Profile Picture">
                            <?php
                            }
                        ?>
                    </div>
                </a>
                <div class="next-to-profile-pic-div">
                    <p class="profile-greeting">Hi, <?php echo $_SESSION['user_name'];?><br>Points: 0</p>
                    <a href="editProfile.php" class="green-a edit-a hover1"><img src="img/edit.png" class="edit-png hover1a" alt="Edit" title="Edit"><img src="img/edit2.png" class="edit-png hover1b" alt="Edit" title="Edit">Edit Profile</a>
                </div>
            </div>

            <div class="clear"></div>

            <a href="editAddress.php">
                <div class="profile-line-divider opacity-hover first-line">
                    <div class="left-icon-div"><img src="img/address.png" class="left-icon-png" alt="Shipping Address" title="Shipping Address"></div>
                    <div class="right-profile-content">Shipping Address</div>
                </div>
            </a>
            <a href="bankDetailsEdit.php">
                <div class="profile-line-divider opacity-hover">
                    <div class="left-icon-div"><img src="img/banking.png" class="left-icon-png" alt="Banking Details" title="Banking Details"></div>
                    <div class="right-profile-content">Banking Details</div>
                </div>
            </a>

            <!-- Testing Only, might be removed -->
            <a href="favouritePets.php">
                <div class="profile-line-divider opacity-hover">
                    <div class="left-icon-div"><img src="img/favourite-green-line.png" class="left-icon-png" alt="Favourite Pets" title="Favourite Pets"></div>
                    <div class="right-profile-content">Favourite Pets</div>
                </div>
            </a> 

            <a href="logout.php">
                <div class="profile-line-divider opacity-hover">
                    <div class="left-icon-div"><img src="img/logout.png" class="left-icon-png" alt="Logout" title="Logout"></div>
                    <div class="right-profile-content">Logout</div>
                </div>
            </a>
    	</div>
    <?php
    }
    else
    {
    ?>
        <div class="width100 same-padding overflow min-height menu-distance2">
     		<div class="width100 overflow profile-top-div">
            	<a href="updateProfilePic.php">
                    <div class="profile-pic-div">
                        <div class="update-div-div">
                            <img src="img/update-profile-pic.jpg" class="profile-pic update-profile-pic" alt="Update Profile Picture" title="Update Profile Picture">
                        </div>
                        <!-- <img src="img/profile-pic.jpg" class="profile-pic" alt="Profile Picture" title="Profile Picture"> -->
                        <?php
                            $proPic = $userData->getProfilePic();
                            if($proPic != "")
                            {
                            ?>
                                <img src="userProfilePic/<?php echo $userData->getProfilePic();?>" class="profile-pic" alt="Profile Picture" title="Profile Picture">
                            <?php
                            }
                            else
                            {
                            ?>
                                <img src="img/profile-pic.jpg" class="profile-pic" alt="Profile Picture" title="Profile Picture">
                            <?php
                            }
                        ?>
                    </div>
                </a>
                <div class="next-to-profile-pic-div">
                	<!-- <p class="profile-greeting">Hi, Username</p> -->
                    <p class="profile-greeting">Hi, <?php echo $userData->getName();?><br>Points: <?php echo $userData->getPoints();?></p>
                    <a href="editProfile.php" class="green-a edit-a hover1"><img src="img/edit.png" class="edit-png hover1a" alt="Edit" title="Edit"><img src="img/edit2.png" class="edit-png hover1b" alt="Edit" title="Edit">Edit Profile</a>
                </div>
            </div>

            <div class="clear"></div>

            <a href="editAddress.php">
                <div class="profile-line-divider opacity-hover first-line">
                    <div class="left-icon-div"><img src="img/address.png" class="left-icon-png" alt="Shipping Address" title="Shipping Address"></div>
                    <div class="right-profile-content">Shipping Address</div>
                </div>
            </a>
            <a href="bankDetailsEdit.php">
                <div class="profile-line-divider opacity-hover">
                    <div class="left-icon-div"><img src="img/banking.png" class="left-icon-png" alt="Banking Details" title="Banking Details"></div>
                    <div class="right-profile-content">Banking Details</div>
                </div>
            </a>

            <a href="favouritePets.php">
                <div class="profile-line-divider opacity-hover">
                    <div class="left-icon-div"><img src="img/favourite-green-line.png" class="left-icon-png" alt="Favourite Pets" title="Favourite Pets"></div>
                    <div class="right-profile-content">Favourite Pets</div>
                </div>
            </a>         

           <a href="favouriteProducts.php">
                <div class="profile-line-divider opacity-hover">
                    <div class="left-icon-div"><img src="img/to-receive-2.png" class="left-icon-png" alt="Favourite Products" title="Favourite Products"></div>
                    <div class="right-profile-content">Favourite Products</div>
                </div>
            </a>
            <a href="viewShoppingCart.php">
                <div class="profile-line-divider opacity-hover">
                    <div class="left-icon-div"><img src="img/cart-2.png" class="left-icon-png" alt="Order History" title="Order History"></div>
                    <div class="right-profile-content">My Cart</div>
                </div>
            </a>   
            <!-- <a href="#"> -->
            <a href="liveChat.php">
                <div class="profile-line-divider opacity-hover">
                    <div class="left-icon-div"><img src="img/chatbox.png" class="left-icon-png" alt="Order History" title="Order History"></div>
                    <div class="right-profile-content">Chat box</div>
                </div>
            </a>            
            <a href="orderHistory.php">
                <div class="profile-line-divider opacity-hover">
                    <div class="left-icon-div"><img src="img/docs.png" class="left-icon-png" alt="Order History" title="Order History"></div>
                    <div class="right-profile-content">Order History</div>
                </div>
            </a>   

            <a href="logout.php">
                <div class="profile-line-divider opacity-hover">
                    <div class="left-icon-div"><img src="img/logout.png" class="left-icon-png" alt="Logout" title="Logout"></div>
                    <div class="right-profile-content">Logout</div>
                </div>
            </a>

            <div class="clear"></div>







    	</div>
    <?php
    }
    ?>

<div class="clear"></div>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Profile Successfully !";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Update Password Successfully !";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Update Shipping Address Successfully !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Profile Picture Updated !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>
