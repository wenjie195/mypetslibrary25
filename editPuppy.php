<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Puppy.php';
require_once dirname(__FILE__) . '/classes/Color.php';
require_once dirname(__FILE__) . '/classes/Breed.php';
require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/Vaccination.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$sellerDetails = getSeller($conn);
$colorDetails = getColor($conn," WHERE type = ? ",array("type"),array(1),"s");
$breedDetails = getBreed($conn," WHERE type = ? ",array("type"),array(1),"s");
$vaccinationDetails = getVaccination($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Puppy Details | Mypetslibrary" />
<title>Edit Puppy Details | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Edit Puppy Details</h1>
            <div class="green-border"></div>
   </div>
   <div class="border-separation">
        <div class="clear"></div>

        <form method="POST" action="utilities/editPuppyFunction.php" enctype="multipart/form-data">
        <?php
            if(isset($_POST['user_id']))
            {
                $conn = connDB();
                $puppyDetails = getPuppy($conn,"WHERE id = ? ", array("id") ,array($_POST['user_id']),"i");
            ?>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Status*</p>
                    <select class="input-name clean admin-input" required name="update_status" id="update_status" value="<?php echo $puppyDetails[0]->getStatus();?>">
                        <!-- <option>Available</option>
                        <option>Sold</option> -->
                        <?php
                            if($puppyDetails[0]->getStatus() == '')
                            {
                            ?>
                                <option value="Available"  name='Available'>For Sale</option>
                                <option value="Sold"  name='Sold'>Sold</option>
                                <option selected value=""  name=''></option>
                            <?php
                            }
                            else if($puppyDetails[0]->getStatus() == 'Sold')
                            {
                            ?>
                                <option value="Available"  name='Available'>For Sale</option>
                                <option selected value="Sold"  name='Sold'>Sold</option>
                            <?php
                            }
                            else if($puppyDetails[0]->getStatus() == 'Available')
                            {
                            ?>
                                <option selected value="Available"  name='Available'>For Sale</option>
                                <option value="Sold"  name='Sold'>Sold</option>
                        <?php
                        }
                        ?>
                    </select>       
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Seller*</p>

                    <?php 
                        $petSellerUid = $puppyDetails[0]->getSeller();
                        $sellDetails = getSeller($conn,"WHERE uid = ? ", array("uid") ,array($petSellerUid),"s");
                        $sellData = $sellDetails[0];
                        $sellUid = $sellData->getCompanyName();
                    ?>

                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $sellUid ;?>" readonly>  
                    <input class="input-name clean input-textarea admin-input" type="hidden" value="<?php echo $petSellerUid;?>" name="update_seller" id="update_seller" readonly> 
                </div>           
                <div class="clear"></div>                
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">SKU*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $puppyDetails[0]->getSKU();?>" required name="update_sku" id="update_sku">       
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Pet Price (RM)*</p>
                    <input class="input-name clean input-textarea admin-input" type="number" value="<?php echo $puppyDetails[0]->getPrice();?>"  required name="update_price" id="update_price">
                </div>        
                <div class="clear"></div>
 
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Pet Age*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $puppyDetails[0]->getAge();?>" required name="update_age" id="update_age">      
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Vaccinated Status*</p>
                    <select class="input-name clean admin-input" name="update_vaccinated" id="update_vaccinated" required>
                        <?php
                            {
                                for ($cntVc=0; $cntVc <count($vaccinationDetails) ; $cntVc++)
                                {
                                    if ($puppyDetails[0]->getVaccinated() == $vaccinationDetails[$cntVc]->getName())
                                    {
                                    ?>
                                        <option selected value="<?php echo $vaccinationDetails[$cntVc]->getName(); ?>"> 
                                            <?php echo $vaccinationDetails[$cntVc]->getName(); ?>
                                        </option>
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                        <option value="<?php echo $vaccinationDetails[$cntVc]->getName(); ?>"> 
                                            <?php echo $vaccinationDetails[$cntVc]->getName(); ?>
                                        </option>
                                    <?php
                                    }
                                }
                            }
                        ?>
                    </select>    
                </div>           
                <div class="clear"></div>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Dewormed Status*</p>
                    <select class="input-name clean admin-input" required name="update_dewormed" id="update_dewormed" value="<?php echo $puppyDetails[0]->getDewormed();?>">
                        <?php
                            if($puppyDetails[0]->getDewormed() == '')
                            {
                            ?>
                                <option value="Yes"  name='Yes'>Yes</option>
                                <option value="No"  name='No'>No</option>
                                <option selected value=""  name=''></option>
                            <?php
                            }
                            else if($puppyDetails[0]->getDewormed() == 'No')
                            {
                            ?>
                                <option value="Yes"  name='Yes'>Yes</option>
                                <option selected value="No"  name='No'>No</option>
                            <?php
                            }
                            else if($puppyDetails[0]->getDewormed() == 'Yes')
                            {
                            ?>
                                <option selected value="Yes"  name='Yes'>Yes</option>
                                <option value="No"  name='No'>No</option>
                        <?php
                        }
                        ?>
                    </select>       
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Gender*</p>
                    <select class="input-name clean admin-input" required name="update_gender" id="update_gender" value="<?php echo $puppyDetails[0]->getGender();?>">
                        <?php
                            if($puppyDetails[0]->getGender() == '')
                            {
                            ?>
                                <option value="Male"  name='Male'>Male</option>
                                <option value="Female"  name='Female'>Female</option>
                                <option selected value=""  name=''></option>
                            <?php
                            }
                            else if($puppyDetails[0]->getGender() == 'Female')
                            {
                            ?>
                                <option value="Male"  name='Male'>Male</option>
                                <option selected value="Female"  name='Female'>Female</option>
                            <?php
                            }
                            else if($puppyDetails[0]->getGender() == 'Male')
                            {
                            ?>
                                <option selected value="Male"  name='Male'>Male</option>
                                <option value="Female"  name='Female'>Female</option>
                        <?php
                        }
                        ?>
                    </select>   
                </div>  
                
                <div class="clear"></div>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Pet Color*</p>
                    <select class="input-name clean admin-input" required name="update_color" id="update_color" value="<?php echo $puppyDetails[0]->getColor();?>">
                        <?php
                        if($puppyDetails[0]->getColor() == ''){
                        ?>
                            <option selected>Please Select a Color</option>
                            <?php
                            for ($cntPro=0; $cntPro <count($colorDetails) ; $cntPro++){
                            ?>
                                <option value="<?php echo $colorDetails[$cntPro]->getName(); ?>"> 
                                    <?php echo $colorDetails[$cntPro]->getName(); ?>
                                </option>
                            <?php
                            }
                        }
                        else{
                            for ($cntPro=0; $cntPro <count($colorDetails) ; $cntPro++){
                                if ($puppyDetails[0]->getColor() == $colorDetails[$cntPro]->getName())
                                {
                                ?>
                                    <option selected value="<?php echo $colorDetails[$cntPro]->getName(); ?>"> 
                                        <?php echo $colorDetails[$cntPro]->getName(); ?>
                                    </option>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <option value="<?php echo $colorDetails[$cntPro]->getName(); ?>"> 
                                        <?php echo $colorDetails[$cntPro]->getName(); ?>
                                    </option>
                                <?php
                                }
                            }
                        }
                        ?>
                    </select>       
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Pet Size*</p>
                    <select class="input-name clean admin-input" required name="update_size" id="update_size" value="<?php echo $puppyDetails[0]->getSize();?>">
                        <?php
                            if($puppyDetails[0]->getSize() == '')
                            {
                            ?>
                                <option value="Small"  name='Small'>Small</option>
                                <option value="Medium"  name='Medium'>Medium</option>
                                <option value="Large"  name='Large'>Large</option>
                                <option selected value="" name=''></option>
                            <?php
                            }
                            else if($puppyDetails[0]->getSize() == 'Large')
                            {
                            ?>
                                <option value="Small"  name='Small'>Small</option>
                                <option value="Medium"  name='Medium'>Medium</option>
                                <option selected value="Large" name='Large'>Large</option>
                            <?php
                            }
                            else if($puppyDetails[0]->getSize() == 'Medium')
                            {
                            ?>
                                <option value="Small"  name='Small'>Small</option>
                                <option selected value="Medium"  name='Medium'>Medium</option>
                                <option value="Large" name='Large'>Large</option>
                            <?php
                            }
                            else if($puppyDetails[0]->getSize() == 'Small')
                            {
                            ?>
                                <option selected value="Small"  name='Small'>Small</option>
                                <option value="Medium" name='Medium'>Medium</option>
                                <option value="Large"  name='Large'>Large</Medium>
                        <?php
                        }
                        ?>
                    </select>   
                </div>  
                <div class="clear"></div>
   
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Pet Breed*</p>
                    <select class="input-name clean admin-input" required  name="update_breed" id="update_breed" value="<?php echo $puppyDetails[0]->getBreed();?>">
                        <?php
                        if($puppyDetails[0]->getBreed() == ''){
                        ?>
                            <option selected>Please Select a Breed</option>
                            <?php
                            for ($cntPro=0; $cntPro <count($breedDetails) ; $cntPro++){
                            ?>
                                <option value="<?php echo $breedDetails[$cntPro]->getName(); ?>"> 
                                    <?php echo $breedDetails[$cntPro]->getName(); ?>
                                </option>
                            <?php
                            }
                        }
                        else{
                            for ($cntPro=0; $cntPro <count($breedDetails) ; $cntPro++){
                                if ($puppyDetails[0]->getBreed() == $breedDetails[$cntPro]->getName())
                                {
                                ?>
                                    <option selected value="<?php echo $breedDetails[$cntPro]->getName(); ?>"> 
                                        <?php echo $breedDetails[$cntPro]->getName(); ?>
                                    </option>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <option value="<?php echo $breedDetails[$cntPro]->getName(); ?>"> 
                                        <?php echo $breedDetails[$cntPro]->getName(); ?>
                                    </option>
                                <?php
                                }
                            }
                        }
                        ?>
                    </select>       
                </div>
  
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">With MKA Cert & Microchip?</p>
                    <select class="input-name clean admin-input" name="update_feature" id="update_feature" value="<?php echo $puppyDetails[0]->getFeature();?>" required>
                        <?php
                            if($puppyDetails[0]->getFeature() == " ")
                            // if(!$puppyDetails[0]->getFeature())
                            {
                            ?>
                                <option value="No"  name='No'>No</option>
                                <option value="Yes"  name='Yes'>Yes</option>
                                <option selected value=""  name=''></option>
                            <?php
                            }
                            else if($puppyDetails[0]->getFeature() == 'No')
                            {
                            ?>
                                <option value="Yes"  name='Yes'>Yes</option>
                                <option selected value="No"  name='No'>No</option>
                            <?php
                            }
                            else if($puppyDetails[0]->getFeature() == 'Yes')
                            {
                            ?>
                                <option selected value="Yes"  name='Yes'>Yes</option>
                                <option value="No"  name='No'>No</option>
                        <?php
                        }
                        ?>
                    </select>     
                </div>       

                <div class="clear"></div>
                
                <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Details</p>                    
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $puppyDetails[0]->getDetails();?>" name="update_details" id="update_details"> 
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $puppyDetails[0]->getDetailsTwo();?>" name="update_details_two" id="update_details_two"> 
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $puppyDetails[0]->getDetailsThree();?>" name="update_details_three" id="update_details_three"> 
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $puppyDetails[0]->getDetailsFour();?>" name="update_details_four" id="update_details_four"> 
                </div>
                
                <div class="clear"></div>     

                <div class="width100 overflow">
                    <img src="img/vimeo-tutorial.png" class="vimeo-tutorial" alt="Tutorial" title="Tutorial">
        			<p class="input-top-p admin-top-p">Vimeo Video Link* (Copy the Highlighted Part Only)</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $puppyDetails[0]->getLink();?>"  required name="update_link" id="update_link">           
                </div>

                <div class="clear"></div>

                <input type="hidden" id="uid" name="uid" value="<?php echo $puppyDetails[0]->getUid() ?>">

                <div class="clear"></div>  
                <div class="width100 overflow text-center">     
                    <button class="green-button white-text clean2 edit-1-btn margin-auto" type="submit" id ="editSubmit" name ="editSubmit">Submit</button>
                </div>
        </form>
	<div class="width100 margin-top50">
            <h1 class="green-text h1-title">Edit Puppy Photo</h1>
            <div class="green-border"></div>
   </div>
   <div class="border-separation"></div>
        <div class="clear"></div>
                <div class="width100 overflow">
         
                    <?php 
                    if($puppyDetails[0]->getDefaultImage()){
                        $_SESSION['defaultImage'] = $puppyDetails[0]->getDefaultImage();
                        $_SESSION['petsType'] = "1";
                    }?>
                    <div class="six-box2">
                    <div class="square">
                    <div class="width100 white-bg content">
                    <?php 
                    if($puppyDetails[0]->getImageOne()){
                        $_SESSION['imageOne'] = $puppyDetails[0]->getImageOne();
                    }?>
                        <img src="uploads/<?php echo $puppyDetails[0]->getImageOne();?>" class="pet-photo-preview">

                    </div>
                    </div>
                         <form method="POST" action="updatePetsImageOne.php" class="hover1" target="_blank">
                            <button class="clean green-button pointer width100 update-photo-btn" type="submit" name="pets_uid" value="<?php echo $puppyDetails[0]->getUid();?>">
                            Update
                            </button>
                        </form>
                    </div>
                    <div class="six-box2">
                    <div class="square">
                    <div class="width100 white-bg content">
                    <?php 
                    if($puppyDetails[0]->getImageTwo()){
                        $_SESSION['imageTwo'] = $puppyDetails[0]->getImageTwo();
                    }?>
                        <img src="uploads/<?php echo $puppyDetails[0]->getImageTwo();?>" class="pet-photo-preview">
                    </div>
                    </div>                        
                        
                        
                        <form method="POST" action="updatePetsImageTwo.php" class="hover1" target="_blank">
                            <button class="clean green-button pointer width100 update-photo-btn" type="submit" name="pets_uid" value="<?php echo $puppyDetails[0]->getUid();?>">
                            Update
                            </button>
                        </form>
                    </div>
                    <div class="six-box2"> 
                    <div class="square">
                    <div class="width100 white-bg content">
                    <?php 
                    if($puppyDetails[0]->getImageThree()){
                        $_SESSION['imageThree'] = $puppyDetails[0]->getImageThree();
                    }?>              
                        <img src="uploads/<?php echo $puppyDetails[0]->getImageThree();?>" class="pet-photo-preview">
                    </div>
                    </div>
                        <form method="POST" action="updatePetsImageThree.php" class="hover1" target="_blank">
                            <button class="clean green-button pointer width100 update-photo-btn" type="submit" name="pets_uid" value="<?php echo $puppyDetails[0]->getUid();?>">
                            Update
                            </button>
                        </form>
                    </div>                    
                    <div class="six-box2"> 
                    <div class="square">
                    <div class="width100 white-bg content">
                    <?php 
                    if($puppyDetails[0]->getImageFour()){
                        $_SESSION['imageFour'] = $puppyDetails[0]->getImageFour();
                    }?>
                        <img src="uploads/<?php echo $puppyDetails[0]->getImageFour();?>" class="pet-photo-preview">
                    </div>
                    </div>
                        <form method="POST" action="updatePetsImageFour.php" class="hover1" target="_blank">
                            <button class="clean green-button pointer width100 update-photo-btn" type="submit" name="pets_uid" value="<?php echo $puppyDetails[0]->getUid();?>">
                            Update
                            </button>
                        </form>
                    </div>
                    <div class="six-box2"> 
                    <div class="square">
                    <div class="width100 white-bg content">
                    <?php 
                    if($puppyDetails[0]->getImageFive()){
                        $_SESSION['imageFive'] = $puppyDetails[0]->getImageFive();
                    }?>
                        <img src="uploads/<?php echo $puppyDetails[0]->getImageFive();?>" class="pet-photo-preview">
                     </div>
                    </div>
                        <form method="POST" action="updatePetsImageFive.php" class="hover1" target="_blank">
                            <button class="clean green-button pointer width100 update-photo-btn" type="submit" name="pets_uid" value="<?php echo $puppyDetails[0]->getUid();?>">
                            Update
                            </button>
                        </form>
                    </div>
                    <div class="six-box2"> 
                    <div class="square">
                    <div class="width100 white-bg content">
                    <?php 
                    if($puppyDetails[0]->getImageSix()){
                        $_SESSION['imageSix'] = $puppyDetails[0]->getImageSix();
                    }?>
                        <img src="uploads/<?php echo $puppyDetails[0]->getImageSix();?>" class="pet-photo-preview">
					</div>
                    </div>                        
                        <form method="POST" action="updatePetsImageSix.php" class="hover1" target="_blank">
                            <button class="clean green-button pointer width100 update-photo-btn" type="submit" name="pets_uid" value="<?php echo $puppyDetails[0]->getUid();?>">
                            Update
                            </button>
                        </form>
                    </div>
                </div> 
                <div class="width100 margin-top50">
                        <h1 class="green-text h1-title">Change Puppy Profile Picture</h1>
                        <div class="green-border"></div>
               </div>
               <div class="border-separation"></div>
                <div class="width100 overflow">
                 
                        <?php
                        if($puppyDetails){
                            $_SESSION['petsType'] = "1";
                        }
                        $imageOne = 'uploads/'.$puppyDetails[0]->getImageOne();
                        $imageTwo = 'uploads/'.$puppyDetails[0]->getImageTwo();
                        $imageThree = 'uploads/'.$puppyDetails[0]->getImageThree();
                        $imageFour = 'uploads/'.$puppyDetails[0]->getImageFour();
                        $imageFive = 'uploads/'.$puppyDetails[0]->getImageFive();
                        $imageSix = 'uploads/'.$puppyDetails[0]->getImageSix();
                        $defaultimage = 'uploads/'.$puppyDetails[0]->getDefaultImage();
                        ?>
                            <form action="utilities/editDefaultFunction.php" method="post">

                                    <?php 
                                        if ($puppyDetails[0]->getImageOne()) {
                                            ?>
                                            
                                    <div class="six-box2"> 
                                    <div class="square">
                                    <div class="width100 white-bg content">
                                        <img src="<?php echo $imageOne; ?>" class="pet-photo-preview">
                                    </div>
                                    </div>                        
                                    <?php
                                    if ($puppyDetails[0]->getImageOne()) {
                                        ?>
                                        <input type="hidden" id="uid" name="uid" value="<?php echo $puppyDetails[0]->getUid() ?>">
                                       <input type="radio" name="default_image" class="pet-photo-preview" id="1" value= "<?php echo $puppyDetails[0]->getImageOne() ?>"
                                            <?php 
                                            if($imageOne==$defaultimage) {
                                                echo "checked";
                                            }
                                            ?>/>
                                    </div>                                            

											<?php
                                        }
                                        if ($puppyDetails[0]->getImageTwo()) {
                                            ?>                                            
                                    <div class="six-box2"> 
                                    <div class="square">
                                    <div class="width100 white-bg content">
                                        <img src="<?php echo $imageTwo; ?>" class="pet-photo-preview">
                                    </div>
                                    </div>                        
                                        <?php
                                    }
                                    if ($puppyDetails[0]->getImageTwo()) {
                                        ?>
                                        <input type="hidden" id="uid" name="uid" value="<?php echo $puppyDetails[0]->getUid() ?>">
                                        <input type="radio" name="default_image" class="pet-photo-preview" id="2" value="<?php echo $puppyDetails[0]->getImageTwo() ?>"
                                            <?php 
                                                if($imageTwo==$defaultimage) {
                                                    echo "checked";
                                                }
                                            ?>/>
                                    </div>                                              
                                            
											
                                            <?php
                                        }
                                        if ($puppyDetails[0]->getImageThree()) {
                                            ?>											
											
                                    <div class="six-box2"> 
                                    <div class="square">
                                    <div class="width100 white-bg content">
                                        <img src="<?php echo $imageThree; ?>" class="pet-photo-preview">
                                    </div>
                                    </div>                        
									<?php
                                    }
                                    if ($puppyDetails[0]->getImageThree()) {
                                        ?>
                                        <input type="hidden" id="uid" name="uid" value="<?php echo $puppyDetails[0]->getUid() ?>">
                                        <input type="radio" name="default_image" class="pet-photo-preview" id="3" value="<?php echo $puppyDetails[0]->getImageThree() ?>"
                                            <?php 
                                                if($imageThree==$defaultimage) {
                                                    echo "checked";
                                                }
                                            ?>/>
                                    </div>											
											
                                            <?php
                                        }
                                        if ($puppyDetails[0]->getImageFour()) {
                                            ?>											
                                    <div class="six-box2"> 
                                    <div class="square">
                                    <div class="width100 white-bg content">
                                        <img src="<?php echo $imageFour; ?>" class="pet-photo-preview">
                                    </div>
                                    </div>                        
									<?php
                                    }
                                    if ($puppyDetails[0]->getImageFour()) {
                                        ?>
                                        <input type="hidden" id="uid" name="uid" value="<?php echo $puppyDetails[0]->getUid() ?>">
                                        <td><input type="radio" name="default_image" class="pet-photo-preview" id="4" value="<?php echo $puppyDetails[0]->getImageFour() ?>"
                                            <?php 
                                                if($imageFour==$defaultimage) {
                                                    echo "checked";
                                                }
                                            ?>/>
                                    </div>		
                                    
                                            <?php
                                        }
                                        if ($puppyDetails[0]->getImageFive()) {
                                            ?>                                    									
                                    <div class="six-box2"> 
                                    <div class="square">
                                    <div class="width100 white-bg content">
                                        <img src="<?php echo $imageFive; ?>" class="pet-photo-preview">
                                    </div>
                                    </div>                        
                                        <?php
                                    }
                                    if ($puppyDetails[0]->getImageFive()) {
                                        ?>
                                        <input type="hidden" id="uid" name="uid" value="<?php echo $puppyDetails[0]->getUid() ?>">
                                        <td><input type="radio" name="default_image" class="pet-photo-preview" id="5" value="<?php echo $puppyDetails[0]->getImageFive() ?>"
                                            <?php 
                                                if($imageFive==$defaultimage) {
                                                    echo "checked";
                                                }
                                            ?>/>
                                    </div>
                                       <?php
                                    }
                                    ?>
										<?php
                                        }
                                        if ($puppyDetails[0]->getImageSix()) {
                                            ?>                                    
                                    <div class="six-box2"> 
                                    <div class="square">
                                    <div class="width100 white-bg content">
                                        <img src="<?php echo $imageSix; ?>" class="pet-photo-preview">
                                    </div>
                                    </div>                        
										<?php
                                    }
                                    if ($puppyDetails[0]->getImageSix()) {
                                        ?>
                                        <input type="hidden" id="uid" name="uid" value="<?php echo $puppyDetails[0]->getUid() ?>">
                                        <input type="radio" name="default_image" class="pet-photo-preview" id="6" value="<?php echo $puppyDetails[0]->getImageSix() ?>"
                                            <?php 
                                                if($imageSix==$defaultimage) {
                                                    echo "checked";
                                                }
                                            ?>/>
                                    </div>                                            


                                  
                                        <?php
                                    }
                                    ?>
                       
                                <div class="clear"></div>  
                                <div class="width100 overflow text-center">  
                                    <button type="submit" class="green-button white-text clean2 edit-1-btn margin-auto" name="defaultImageSubmit" id="defaultImageSubmit">Change Profile Image</button>
                                </div>
                            </form>
                </div>

        <?php
        }
        ?>

	</div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>