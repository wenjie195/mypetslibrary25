<?php
if (session_id() == "")
{
  session_start();
}
 ?>
<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Support Animal Shelter | Mypetslibrary" />
<title>Support Animal Shelter | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="animal adoption, animal rescue, penang, malaysia, adopt, 领养宠物, 爱护动物, 4PAWS (Penang Animal Welfare Society), HOPE (Homeless & Orphan Pet Exist) 希望护生园, Mykebun 菜园.狗, PAWS Animal Welfare Society, Second Chance Animal Society, SPCA Penang (Society for the Prevention of Cruelty to Animals), Jelutong, charity, 义工,照顾狗猫,  Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">

<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'header.php'; ?>

<div class="width100 small-padding overflow min-height menu-distance2">
    <h1 class="green-text user-title left-align-title">Support Animal Shelter</h1>
	<div class="clear"></div>
 	<div class="width103">
	<div class="shadow-white-box four-box-size">
		<div class="width100 white-bg">
			<a href="https://penanganimalwelfaresociety.wordpress.com/" target="_blank" class="opacity-hover"><img src="img/4paws.png" alt="4PAWS (Penang Animal Welfare Society)" title="4PAWS (Penang Animal Welfare Society)" class="width100 two-border-radius"></a>
		</div>
		<div class="width100 product-details-div adopt-details">
        	<p class="width100 slider-product-name text-overflow">4PAWS (Penang Animal Welfare Society)</p>
			<p class="width100 slider-product-name adopt-link"><a href="https://penanganimalwelfaresociety.wordpress.com/" target="_blank" class="green-a"><span class="green-a adopt-span">Visit Their </span>Website</a></p>
            <p class="width100 slider-product-name adopt-link"><a href="https://www.facebook.com/4PAWS.penang/" target="_blank" class="green-a"><span class="green-a adopt-span">Visit Their </span>Facebook</a></p>
		</div>
	</div>

	<div class="shadow-white-box four-box-size">
		<div class="width100 white-bg">
			<a href="http://www.hopejb.org/" target="_blank" class="opacity-hover"><img src="img/hope.png" alt="HOPE (Homeless & Orphan Pet Exist) 希望护生园" title="HOPE (Homeless & Orphan Pet Exist) 希望护生园" class="width100 two-border-radius"></a>
		</div>
		<div class="width100 product-details-div adopt-details">
        	<p class="width100 slider-product-name text-overflow">HOPE (Homeless & Orphan Pet Exist) 希望护生园</p>
			<p class="width100 slider-product-name adopt-link"><a href="http://www.hopejb.org/" target="_blank" class="green-a"><span class="green-a adopt-span">Visit Their </span>Website</a></p>
            <p class="width100 slider-product-name adopt-link"><a href="https://www.facebook.com/hopejb/" target="_blank" class="green-a"><span class="green-a adopt-span">Visit Their </span>Facebook</a></p>
		</div>
	</div>
   
	<div class="shadow-white-box four-box-size">
		<div class="width100 white-bg">
			<a href="https://gogetfunding.com/funding-mykebun-rescued-dogs/" target="_blank" class="opacity-hover"><img src="img/mykebun.png" alt="Mykebun 菜园.狗" title="Mykebun 菜园.狗" class="width100 two-border-radius"></a>
		</div>
		<div class="width100 product-details-div adopt-details">
        	<p class="width100 slider-product-name text-overflow">Mykebun 菜园.狗</p>
			<p class="width100 slider-product-name adopt-link"><a href="https://gogetfunding.com/funding-mykebun-rescued-dogs/" target="_blank" class="green-a"><span class="green-a adopt-span">Visit Their </span>Website</a></p>
            <p class="width100 slider-product-name adopt-link"><a href="https://www.facebook.com/MykebunGrace/" target="_blank" class="green-a"><span class="green-a adopt-span">Visit Their </span>Facebook</a></p>
		</div>
	</div>
   
	<div class="shadow-white-box four-box-size">
		<div class="width100 white-bg">
			<a href="http://www.paws.org.my/" target="_blank" class="opacity-hover"><img src="img/paws.png" alt="PAWS Animal Welfare Society" title="PAWS Animal Welfare Society" class="width100 two-border-radius"></a>
		</div>
		<div class="width100 product-details-div adopt-details">
        	<p class="width100 slider-product-name text-overflow">PAWS Animal Welfare Society</p>
			<p class="width100 slider-product-name adopt-link"><a href="http://www.paws.org.my/" target="_blank" class="green-a"><span class="green-a adopt-span">Visit Their </span>Website</a></p>
            <p class="width100 slider-product-name adopt-link"><a href="https://www.facebook.com/PawsPJ/" target="_blank" class="green-a"><span class="green-a adopt-span">Visit Their </span>Facebook</a></p>
		</div>
	</div>
   
	<div class="shadow-white-box four-box-size">
		<div class="width100 white-bg">
			<a href="https://www.secondchance.com.my/" target="_blank" class="opacity-hover"><img src="img/second-chance.png" alt="Second Chance Animal Society" title="Second Chance Animal Society" class="width100 two-border-radius"></a>
		</div>
		<div class="width100 product-details-div adopt-details">
        	<p class="width100 slider-product-name text-overflow">Second Chance Animal Society</p>
			<p class="width100 slider-product-name adopt-link"><a href="https://www.secondchance.com.my/" target="_blank" class="green-a"><span class="green-a adopt-span">Visit Their </span>Website</a></p>
            <p class="width100 slider-product-name adopt-link"><a href="https://www.facebook.com/scasmalaysia/" target="_blank" class="green-a"><span class="green-a adopt-span">Visit Their </span>Facebook</a></p>
		</div>
	</div>

	<div class="shadow-white-box four-box-size">
		<div class="width100 white-bg">
			<a href="https://spca-penang.net/" target="_blank" class="opacity-hover"><img src="img/spca.png" alt="SPCA Penang" title="SPCA Penang (Society for the Prevention of Cruelty to Animals)" class="width100 two-border-radius"></a>
		</div>
		<div class="width100 product-details-div adopt-details">
        	<p class="width100 slider-product-name text-overflow">SPCA Penang (Society for the Prevention of Cruelty to Animals)</p>
			<p class="width100 slider-product-name adopt-link"><a href="https://spca-penang.net/" target="_blank" class="green-a"><span class="green-a adopt-span">Visit Their </span>Website</a></p>
            <p class="width100 slider-product-name adopt-link"><a href="https://www.facebook.com/SPCAPenang/" target="_blank" class="green-a"><span class="green-a adopt-span">Visit Their </span>Facebook</a></p>
		</div>
	</div>
    </div>
</div>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>
