<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Breed.php';
require_once dirname(__FILE__) . '/../classes/Color.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $id = rewrite($_POST["category_id"]);
    $status = "Delete";
    // $type = "10";

    $categoryName = rewrite($_POST["category_name"]);

    // $previousType = rewrite($_POST["color_type"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    // $color = getColor($conn," id = ?   ",array("id"),array($id),"s");    
    $color = getColor($conn," WHERE id = ? ",array("id"),array($id),"s");    

    // if(!$color)
    if($color)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }

        array_push($tableValue,$id);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"category"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            $_SESSION['messageType'] = 2;
            header('Location: ../category.php?type=1');
        }
        else
        {
            $_SESSION['messageType'] = 2;
            header('Location: ../category.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 2;
        header('Location: ../category.php?type=3');
    }

}
else 
{
    header('Location: ../index.php');
}
?>
