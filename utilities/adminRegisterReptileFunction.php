<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Pets.php';
require_once dirname(__FILE__) . '/../classes/Reptile.php';
require_once dirname(__FILE__) . '/../classes/Seller.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$timestamp = time();

function registerNewReptile($conn,$uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,$breed,$seller,$details,$detailsTwo,
          $detailsThree,$detailsFour,$link,$keywordOne,$sellerState)
{
     if(insertDynamicData($conn,"reptile",array("uid","name","sku","slug","price","age","vaccinated","dewormed","gender","color","size",
     "status","feature","breed","seller","details","details_two","details_three","details_four","link","keyword_one","location"),
          array($uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,$breed,$seller,$details,$detailsTwo,
               $detailsThree,$detailsFour,$link,$keywordOne,$sellerState),"ssssssssssssssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

function registerNewPets($conn,$uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,$breed,$seller,$type,$details,$detailsTwo,
          $detailsThree,$detailsFour,$link,$keywordOne,$sellerState)
{
     if(insertDynamicData($conn,"pet_details",array("uid","name","sku","slug","price","age","vaccinated","dewormed","gender","color","size","status","feature","breed","seller",
                                   "type","details","details_two","details_three","details_four","link","keyword_one","location"),
     array($uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,$breed,$seller,$type,$details,$detailsTwo,
          $detailsThree,$detailsFour,$link,$keywordOne,$sellerState),"sssssssssssssssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     $_SESSION['newPetsUid'] = $uid;

     // $name = rewrite($_POST['register_name']);
     $name = " ";

     // original SKU
     // $sku = rewrite($_POST['register_sku']);

     //new SKU
     // $registerPetType = "reptile";
     $registerPetType = "Reptile";

     // $sku = $registerPetType.$timestamp;
     // $slug = rewrite($_POST['register_slug']);
     $sku = rewrite($_POST['register_sku']);
     // $slash = "-";
     // $slug = $name.$slash.$sku;
     $slug = " ";

     $price = rewrite($_POST['register_price']);
     $age= rewrite($_POST['register_age']);
     $vaccinated = rewrite($_POST['register_vaccinated']);
     $dewormed = rewrite($_POST['register_dewormed']);
     $gender = rewrite($_POST['register_gender']);
     $color = rewrite($_POST['register_color']);
     $size = rewrite($_POST['register_size']);

     $status = "Available";

     // $feature = rewrite($_POST['register_feature']);
     $feature = " ";

     $breed = rewrite($_POST['register_breed']);

     // $seller = rewrite($_POST['register_seller']);
     $sellerName = rewrite($_POST['register_seller']);
     $getSellerDetails = getSeller($conn," WHERE company_name = ? ",array("company_name"),array($sellerName),"s");
     $seller = $getSellerDetails[0]->getUid();
     $sellerState = $getSellerDetails[0]->getState();
     
     $sellerCurrentCredit = $getSellerDetails[0]->getCredit();
     $sellerNewCredit = $sellerCurrentCredit - 1;

     $details = rewrite($_POST['register_details']);

     $detailsTwo = rewrite($_POST['register_details_two']);
     $detailsThree = rewrite($_POST['register_details_three']);
     $detailsFour = rewrite($_POST['register_details_four']);

     $type = "Reptile";
     $link = rewrite($_POST['register_link']);

     // $keywordOne = rewrite($_POST['register_keyword']);
     $keywordOne = " ";


     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $name."<br>";
     // echo $sku."<br>";
     // echo $slug."<br>";
     // echo $price."<br>";
     // echo $age."<br>";
     // echo $vaccinated ."<br>";
     // echo $dewormed."<br>";
     // echo $gender."<br>";
     // echo $color."<br>";
     // echo $size."<br>";
     // echo $status."<br>";
     // echo $feature."<br>";
     // echo $breed."<br>";
     // echo $seller."<br>";
     // echo $details."<br>";
     // echo $link."<br>";

     $reptileNameDetails = getReptile($conn," WHERE name = ? ",array("name"),array($_POST['register_name']),"s");
     $registeredReptileName = $reptileNameDetails[0];

     $reptileSKUDetails = getReptile($conn," WHERE sku = ? ",array("sku"),array($_POST['register_sku']),"s");
     $registeredReptileSKU = $reptileSKUDetails[0];

     if($sellerCurrentCredit > 0)
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($sellerNewCredit)
          {
              array_push($tableName,"credit");
              array_push($tableValue,$sellerNewCredit);
              $stringType .=  "s";
          }    
          if(!$sellerNewCredit)
          {
              array_push($tableName,"credit");
              array_push($tableValue,$sellerNewCredit);
              $stringType .=  "s";
          } 
  
          array_push($tableValue,$seller);
          $stringType .=  "s";
          $updateCredit = updateDynamicData($conn,"seller"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updateCredit)
          {

               if(!$registeredReptileName && !$registeredReptileSKU)
               {
                    if(registerNewReptile($conn,$uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,$breed,$seller,$details,$detailsTwo,
                         $detailsThree,$detailsFour,$link,$keywordOne,$sellerState))
                    {
                         if(registerNewPets($conn,$uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,$breed,$seller,$type,$details,$detailsTwo,
                         $detailsThree,$detailsFour,$link,$keywordOne,$sellerState))
                         {
                              $_SESSION['newpets_uid'] = $uid;
                              $_SESSION['newpets_type'] = $type;
                              header('Location: ../addMultiImageReptile.php');
                         }
                         else
                         { 
                              $_SESSION['messageType'] = 1;
                              header('Location: ../addReptile.php?type=2');
                         } 
          
                    }
                    else
                    {
                         $_SESSION['messageType'] = 1;
                         header('Location: ../addReptile.php?type=3');
                    } 
               }
               else
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../addReptile.php?type=4');
               } 

          }
          else
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../addPuppy.php?type=6');
          }
     }
     else
     { 
          $_SESSION['messageType'] = 1;
          header('Location: ../addPuppy.php?type=5');
     } 

     // if(!$registeredReptileName && !$registeredReptileSKU)
     // {
     //      if(registerNewReptile($conn,$uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,$breed,$seller,$details,$detailsTwo,
     //           $detailsThree,$detailsFour,$link,$keywordOne,$sellerState))
     //      {
     //           if(registerNewPets($conn,$uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,$breed,$seller,$type,$details,$detailsTwo,
     //           $detailsThree,$detailsFour,$link,$keywordOne,$sellerState))
     //           {
     //                $_SESSION['newpets_uid'] = $uid;
     //                $_SESSION['newpets_type'] = $type;
     //                header('Location: ../addMultiImageReptile.php');
     //           }
     //           else
     //           { 
     //                $_SESSION['messageType'] = 1;
     //                header('Location: ../addReptile.php?type=2');
     //           } 

     //      }
     //      else
     //      {
     //           $_SESSION['messageType'] = 1;
     //           header('Location: ../addReptile.php?type=3');
     //      } 
     // }
     // else
     // {
     //      $_SESSION['messageType'] = 1;
     //      header('Location: ../addReptile.php?type=4');
     // }     
}
else 
{
     header('Location: ../index.php');
}

?>