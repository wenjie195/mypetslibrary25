<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Variation.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $imageValue = rewrite($_POST['image_value']);
     $variationUid = rewrite($_POST['variatio_uid']);

     $imageOne = $timestamp.$_FILES['image_one']['name'];
     $target_dir = "../productImage/";
     $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
     }

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $productUid."<br>";

     $product = getVariation($conn," uid = ? ",array("uid"),array($variationUid),"s");    

     if(!$product)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";

          if($imageOne)
          {
               array_push($tableName,"image");
               array_push($tableValue,$imageOne);
               $stringType .=  "s";
          }

          array_push($tableValue,$variationUid);
          $stringType .=  "s";
          $updateImage = updateDynamicData($conn,"variation"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updateImage)
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../allProducts.php?type=1');
          }
          else
          {
               echo "fail";
               // $_SESSION['messageType'] = 1;
               // header('Location: ../allProducts.php?type=2');
          }
     }
     else
     {
          echo "error";
     }
}
else 
{
    header('Location: ../index.php');
}
?>