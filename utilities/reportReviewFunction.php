<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/ReportedReviews.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userID = $_SESSION['uid'];

function reportReview($conn,$uid,$username,$reviewUid,$reason)
{
     if(insertDynamicData($conn,"reported_reviews",array("uid","username","article_uid","reason"),
          array($uid,$username,$reviewUid,$reason),"ssss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $uid = $userID;
     $uid = md5(uniqid());

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userID),"s");
     $username = $userDetails[0]->getName();

     $reviewUid = rewrite($_POST['review_uid']);
     $reason = rewrite($_POST['report_reason']);

     $type = "Reported";

     // $sellerUid = rewrite($_POST['seller_uid']);

     // // FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $username."<br>";
     // echo $reviewUid."<br>";
     // echo $reason."<br>";

     if(reportReview($conn,$uid,$username,$reviewUid,$reason))
     {
          // echo "reported";   
          // echo "<br>";

          if(isset($_POST['review_uid']))
          {   
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
               if($type)
               {
                    array_push($tableName,"type");
                    array_push($tableValue,$type);
                    $stringType .=  "s";
               }    
               if($username)
               {
                    array_push($tableName,"report_ppl");
                    array_push($tableValue,$username);
                    $stringType .=  "s";
               } 
               if($uid)
               {
                    array_push($tableName,"report_id");
                    array_push($tableValue,$uid);
                    $stringType .=  "s";
               } 
               array_push($tableValue,$reviewUid);
               $stringType .=  "s";
               $reportArticle = updateDynamicData($conn,"reviews"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($reportArticle)
               {
                    // echo "reported";
                    // $_SESSION['messageType'] = 2;
                    // header('Location: ../petSellerReview.php?type=1');
                    header('Location: ' . $_SERVER['HTTP_REFERER']);
                    exit;
               }
               else
               {
                    $_SESSION['messageType'] = 2;
                    header('Location: ../petSellerReview.php?type=2');
               }
          }
          else
          {
               $_SESSION['messageType'] = 2;
               header('Location: ../petSellerReview.php?type=3');
          }

     }
     else 
     {
          $_SESSION['messageType'] = 2;
          header('Location: ../petSellerReview.php?type=4');
     }
 
}
else 
{
     header('Location: ../index.php');
}
?>