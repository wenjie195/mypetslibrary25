<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Reviews.php';
require_once dirname(__FILE__) . '/../classes/ReviewsRespond.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

function addLike($conn,$userUid,$username,$reviewUid,$likes)
{
     if(insertDynamicData($conn,"reviews_respond",array("uid","username","review_uid","dislike_amount"),
          array($userUid,$username,$reviewUid,$likes),"sssi") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $userUid = $uid;

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
     $username = $userDetails[0]->getName();

     $reviewUid = rewrite($_POST['review_uid']);

     $reviewDetails = getReviews($conn," WHERE uid = ? ",array("uid"),array($reviewUid),"s");
     $previousLikes = $reviewDetails[0]->getLikes();

     $newLikes = "1";

     $likes = $newLikes;

     $totalLikes = $previousLikes - $newLikes;

     // $sellerUid = rewrite($_POST['seller_uid']);

     // FOR DEBUGGING 
     // echo "<br>";
     // echo $reviewUid."<br>";
     // echo $previousLikes."<br>";
     // echo $newLikes."<br>";
     // echo $totalLikes."<br>";

     if(addLike($conn,$userUid,$username,$reviewUid,$likes))
     {
          echo "success";   
     
          if(isset($_POST['review_uid']))
          {   
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
               if($totalLikes)
               {
                    array_push($tableName,"likes");
                    array_push($tableValue,$totalLikes);
                    $stringType .=  "s";
               }    
               if(!$totalLikes)
               {
                    array_push($tableName,"likes");
                    array_push($tableValue,$totalLikes);
                    $stringType .=  "s";
               } 
               array_push($tableValue,$reviewUid);
               $stringType .=  "s";
               $addLikes = updateDynamicData($conn,"reviews"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($addLikes)
               {
                    header('Location: ' . $_SERVER['HTTP_REFERER']);
                    exit;
               }
               else
               {
                    echo "fail";
                    // $_SESSION['messageType'] = 2;
                    // header('Location: ../petSellerReview.php?type=2');
               }
          }
          else
          {
               echo "error";
               // $_SESSION['messageType'] = 2;
               // header('Location: ../petSellerReview.php?type=3');
          }
     }
     else 
     {
          echo "fail fail";   
     }

 
}
else 
{
     header('Location: ../index.php');
}
?>