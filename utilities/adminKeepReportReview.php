<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Reviews.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST["review_uid"]);
    $display = "Yes";
    $type = "Keep";
    $result = "Keep";

    // //   FOR DEBUGGING
    // echo "<br>";
    // echo $uid."<br>";
    // echo $display."<br>";
    // echo $type."<br>";
    // echo $result."<br>";

    if(isset($_POST['review_uid']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($display)
        {
            array_push($tableName,"display");
            array_push($tableValue,$display);
            $stringType .=  "s";
        }    
        if($type)
        {
            array_push($tableName,"type");
            array_push($tableValue,$type);
            $stringType .=  "s";
        }  

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $approvedArticle = updateDynamicData($conn,"reviews"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($approvedArticle)
        {
            // // echo "success";

            if(isset($_POST['review_uid']))
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($result)
                {
                    array_push($tableName,"result");
                    array_push($tableValue,$result);
                    $stringType .=  "s";
                }            
                array_push($tableValue,$uid);
                $stringType .=  "s";
                // $updateReport = updateDynamicData($conn,"reported_reviews"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                $updateReport = updateDynamicData($conn,"reported_reviews"," WHERE article_uid = ? ",$tableName,$tableValue,$stringType);
                if($updateReport)
                {
                    // echo "success";
                    $_SESSION['messageType'] = 1;
                    header('Location: ../reportedReview.php?type=1');
                }
                else
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../reportedReview.php?type=3');
                }
            }
            else
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../reportedReview.php?type=4');
            }

        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../reportedReview.php?type=5');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../reportedReview.php?type=6');
    }
    
}
else
{
     header('Location: ../index.php');
}
?>