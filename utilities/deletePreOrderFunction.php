<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/PreOrderList.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $preOrderId = rewrite($_POST["preorder_id"]);
    $status = "Delete";

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $preOrderDetails = getPreOrderList($conn," id = ? ",array("id"),array($preOrderId),"s");    

    if(!$preOrderDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }

        array_push($tableValue,$preOrderId);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"preorder_list"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            // $_SESSION['messageType'] = 1;
            header('Location: ../viewShoppingCart.php');
        }
        else
        {
            // $_SESSION['messageType'] = 1;
            // header('Location: ../viewShoppingCart.php?type=2');
            echo "FAIL";
        }
    }
    else
    {
        // $_SESSION['messageType'] = 1;
        // header('Location: ../viewShoppingCart.php?type=2');
        echo "ERROR";
    }

}
else 
{
    header('Location: ../index.php');
}
?>