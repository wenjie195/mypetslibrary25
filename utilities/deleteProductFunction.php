<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $productUid = rewrite($_POST["product_uid"]);

    $status = "Delete";

    $product = getProduct($conn," uid = ? ",array("uid"),array($productUid),"s");    
    if(!$product)
    // if($product)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        array_push($tableValue,$productUid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../allProducts.php?type=1');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../allProducts.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../allProducts.php?type=3');
    }
}
else 
{
    header('Location: ../index.php');
}
?>