<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Pets.php';
require_once dirname(__FILE__) . '/../classes/Puppy.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST['uid']);
    // $name = rewrite($_POST['update_name']);
    $sku = rewrite($_POST['update_sku']);
    // $slug = rewrite($_POST['update_slug']);
    $price = rewrite($_POST['update_price']);
    $age= rewrite($_POST['update_age']);
    $vaccinated = rewrite($_POST['update_vaccinated']);
    $dewormed = rewrite($_POST['update_dewormed']);
    $gender = rewrite($_POST['update_gender']);
    $color = rewrite($_POST['update_color']);
    $size = rewrite($_POST['update_size']);
    $status = rewrite($_POST['update_status']);
    // $feature = rewrite($_POST['update_feature']);
    $breed = rewrite($_POST['update_breed']);
    $seller = rewrite($_POST['update_seller']);

    $details = rewrite($_POST['update_details']);
    $detailsTwo = rewrite($_POST['update_details_two']);
    $detailsThree = rewrite($_POST['update_details_three']);
    $detailsFour = rewrite($_POST['update_details_four']);

    $link = rewrite($_POST['update_link']);
    // $keywordOne = rewrite($_POST['update_keyword']);

    $feature = rewrite($_POST['update_feature']);

    //   FOR DEBUGGING 
    //  echo "<br>";
    //  echo $name."<br>";
    //  echo $sku."<br>";

    if(isset($_POST['editSubmit']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
    
        //echo "save to database";
        // if($name)
        // {
        //     array_push($tableName,"name");
        //     array_push($tableValue,$name);
        //     $stringType .=  "s";
        // }
        if($sku)
        {
            array_push($tableName,"sku");
            array_push($tableValue,$sku);
            $stringType .=  "s";
        }
        // if($slug)
        // {
        //     array_push($tableName,"slug");
        //     array_push($tableValue,$slug);
        //     $stringType .=  "s";
        // }
        if($price)
        {
            array_push($tableName,"price");
            array_push($tableValue,$price);
            $stringType .=  "s";
        }
        if($age)
        {
            array_push($tableName,"age");
            array_push($tableValue,$age);
            $stringType .=  "s";
        }
        if($vaccinated)
        {
            array_push($tableName,"vaccinated");
            array_push($tableValue,$vaccinated);
            $stringType .=  "s";
        }
        if($dewormed)
        {
            array_push($tableName,"dewormed");
            array_push($tableValue,$dewormed);
            $stringType .=  "s";
        }
        if($gender)
        {
            array_push($tableName,"gender");
            array_push($tableValue,$gender);
            $stringType .=  "s";
        }
        if($color)
        {
            array_push($tableName,"color");
            array_push($tableValue,$color);
            $stringType .=  "s";
        }
        if($size)
        {
            array_push($tableName,"size");
            array_push($tableValue,$size);
            $stringType .=  "s";
        }
        if($status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        if($feature)
        {
            array_push($tableName,"feature");
            array_push($tableValue,$feature);
            $stringType .=  "s";
        }
        if($breed)
        {
            array_push($tableName,"breed");
            array_push($tableValue,$breed);
            $stringType .=  "s";
        }
        if($seller)
        {
            array_push($tableName,"seller");
            array_push($tableValue,$seller);
            $stringType .=  "s";
        }
        if($details)
        {
            array_push($tableName,"details");
            array_push($tableValue,$details);
            $stringType .=  "s";
        }
        if($detailsTwo)
        {
            array_push($tableName,"details_two");
            array_push($tableValue,$detailsTwo);
            $stringType .=  "s";
        }
        if($detailsThree)
        {
            array_push($tableName,"details_three");
            array_push($tableValue,$detailsThree);
            $stringType .=  "s";
        }
        if($detailsFour)
        {
            array_push($tableName,"details_four");
            array_push($tableValue,$detailsFour);
            $stringType .=  "s";
        }
        if($link)
        {
            array_push($tableName,"link");
            array_push($tableValue,$link);
            $stringType .=  "s";
        }    
        array_push($tableValue,$uid);
        $stringType .=  "s";
        $updatePuppyDetails = updateDynamicData($conn,"puppy"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($updatePuppyDetails)
        {
            if(isset($_POST['editSubmit']))
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
            
                //echo "save to database";
                if($name)
                {
                    array_push($tableName,"name");
                    array_push($tableValue,$name);
                    $stringType .=  "s";
                }
                if($sku)
                {
                    array_push($tableName,"sku");
                    array_push($tableValue,$sku);
                    $stringType .=  "s";
                }
                if($slug)
                {
                    array_push($tableName,"slug");
                    array_push($tableValue,$slug);
                    $stringType .=  "s";
                }
                if($price)
                {
                    array_push($tableName,"price");
                    array_push($tableValue,$price);
                    $stringType .=  "s";
                }
                if($age)
                {
                    array_push($tableName,"age");
                    array_push($tableValue,$age);
                    $stringType .=  "s";
                }
                if($vaccinated)
                {
                    array_push($tableName,"vaccinated");
                    array_push($tableValue,$vaccinated);
                    $stringType .=  "s";
                }
            
                if($dewormed)
                {
                    array_push($tableName,"dewormed");
                    array_push($tableValue,$dewormed);
                    $stringType .=  "s";
                }
                if($gender)
                {
                    array_push($tableName,"gender");
                    array_push($tableValue,$gender);
                    $stringType .=  "s";
                }
                if($color)
                {
                    array_push($tableName,"color");
                    array_push($tableValue,$color);
                    $stringType .=  "s";
                }
                if($size)
                {
                    array_push($tableName,"size");
                    array_push($tableValue,$size);
                    $stringType .=  "s";
                }
                if($status)
                {
                    array_push($tableName,"status");
                    array_push($tableValue,$status);
                    $stringType .=  "s";
                }
                if($feature)
                {
                    array_push($tableName,"feature");
                    array_push($tableValue,$feature);
                    $stringType .=  "s";
                }
                if($breed)
                {
                    array_push($tableName,"breed");
                    array_push($tableValue,$breed);
                    $stringType .=  "s";
                }
                if($seller)
                {
                    array_push($tableName,"seller");
                    array_push($tableValue,$seller);
                    $stringType .=  "s";
                }
                if($details)
                {
                    array_push($tableName,"details");
                    array_push($tableValue,$details);
                    $stringType .=  "s";
                }
                if($detailsTwo)
                {
                    array_push($tableName,"details_two");
                    array_push($tableValue,$detailsTwo);
                    $stringType .=  "s";
                }
                if($detailsThree)
                {
                    array_push($tableName,"details_three");
                    array_push($tableValue,$detailsThree);
                    $stringType .=  "s";
                }
                if($detailsFour)
                {
                    array_push($tableName,"details_four");
                    array_push($tableValue,$detailsFour);
                    $stringType .=  "s";
                }
                if($link)
                {
                    array_push($tableName,"link");
                    array_push($tableValue,$link);
                    $stringType .=  "s";
                }
                array_push($tableValue,$uid);
                $stringType .=  "s";
                $updatePuppyDetails = updateDynamicData($conn,"pet_details"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($updatePuppyDetails)
                {
                    echo "<script>alert('Data Updated and Stored !');window.location='../allPuppies.php'</script>"; 
                }
                else
                {
                    echo "<script>alert('Fail to Update Data on pets details table!');window.location='../allPuppies.php'</script>"; 
                }
            }
            else
            {
                echo "<script>alert('ERROR 1');window.location='../allPuppies.php'</script>"; 
            }
        }
        else
        {
            echo "<script>alert('Fail to Update Data on puppy table!');window.location='../allPuppies.php'</script>"; 
        }
    }
    else
    {
        echo "<script>alert('ERROR 2');window.location='../allPuppies.php'</script>"; 
    }

}
else
{
    header('Location: ../index.php');
}
?>