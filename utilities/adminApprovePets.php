<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Kitten.php';
require_once dirname(__FILE__) . '/../classes/Puppy.php';
require_once dirname(__FILE__) . '/../classes/Reptile.php';
require_once dirname(__FILE__) . '/../classes/Pets.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST["pets_uid"]);
    $petType = rewrite($_POST["pets_type"]);
    $status = "Available";

    // //   FOR DEBUGGING
    // echo "<br>";
    // echo $bankNumber."<br>";
    // echo $bankUser."<br>";

    if(isset($_POST['pets_uid']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }    

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $approvedPets = updateDynamicData($conn,"pet_details"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($approvedPets)
        {
            // echo "success";

            if($petType == 'Kitten')
            {

                if(isset($_POST['pets_uid']))
                {   
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
                    if($status)
                    {
                        array_push($tableName,"status");
                        array_push($tableValue,$status);
                        $stringType .=  "s";
                    }    
            
                    array_push($tableValue,$uid);
                    $stringType .=  "s";
                    $approvedPets = updateDynamicData($conn,"kitten"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($approvedPets)
                    {
                        // echo "success";
                        $_SESSION['messageType'] = 1;
                        header('Location: ../allPets.php?type=1');
                    }
                    else
                    {
                        $_SESSION['messageType'] = 1;
                        header('Location: ../pendingPetDetails.php?type=2');
                    }

                }
                else
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../pendingPetDetails.php?type=3');
                }

            }
            elseif($petType == 'Puppy')
            {

                if(isset($_POST['pets_uid']))
                {   
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
                    if($status)
                    {
                        array_push($tableName,"status");
                        array_push($tableValue,$status);
                        $stringType .=  "s";
                    }    
            
                    array_push($tableValue,$uid);
                    $stringType .=  "s";
                    $approvedPets = updateDynamicData($conn,"puppy"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($approvedPets)
                    {
                        // echo "success";
                        $_SESSION['messageType'] = 1;
                        header('Location: ../allPets.php?type=1');
                    }
                    else
                    {
                        $_SESSION['messageType'] = 1;
                        header('Location: ../pendingPetDetails.php?type=2');
                    }

                }
                else
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../pendingPetDetails.php?type=3');
                }

            }
            elseif($petType == 'Reptile')
            {

                if(isset($_POST['pets_uid']))
                {   
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
                    if($status)
                    {
                        array_push($tableName,"status");
                        array_push($tableValue,$status);
                        $stringType .=  "s";
                    }    
            
                    array_push($tableValue,$uid);
                    $stringType .=  "s";
                    $approvedPets = updateDynamicData($conn,"reptile"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($approvedPets)
                    {
                        // echo "success";
                        $_SESSION['messageType'] = 1;
                        header('Location: ../allPets.php?type=1');
                    }
                    else
                    {
                        $_SESSION['messageType'] = 1;
                        header('Location: ../pendingPetDetails.php?type=2');
                    }

                }
                else
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../pendingPetDetails.php?type=3');
                }

            }
            else
            {
                echo "unknow species";
            }

        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../pendingPetDetails.php?type=4');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../pendingPetDetails.php?type=5');
    }
    
}
else
{
     header('Location: ../index.php');
}
?>
