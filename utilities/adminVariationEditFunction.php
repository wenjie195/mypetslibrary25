<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/Variation.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $variationUid = rewrite($_POST['variation_uid']);

    $name = rewrite($_POST['update_name']);
    $price = rewrite($_POST['update_price']);
    $amount = rewrite($_POST['update_price']);

    $variationDetails = getVariation($conn, "WHERE uid =?",array("uid"),array($variationUid),"s");
    $mainProductUid = $variationDetails[0]->getProductUid();

    $variationLP = getVariation($conn,"WHERE product_uid = ? AND status = 'Available' ORDER BY amount ASC LIMIT 1", array("product_uid") ,array($mainProductUid),"s");
    $minPrice = $variationLP[0]->getPrice();

    $variationHP = getVariation($conn,"WHERE product_uid = ? AND status = 'Available' ORDER BY amount DESC LIMIT 1", array("product_uid") ,array($mainProductUid),"s");
    $maxPrice = $variationHP[0]->getPrice();

    $variationMP = getVariation($conn,"WHERE product_uid = ? AND status = 'Available' ", array("product_uid") ,array($mainProductUid),"s");
    // $variationMPrice = $variationHP[0]->getAmount();
    if($variationMP)
    {   
        $totalProduct = count($variationMP);
    }
    else
    {   $totalProduct = 0;   }

    if($variationMP)
    {
      $totalVariationPrice = 0;
      for ($cnt=0; $cnt <count($variationMP) ; $cnt++)
      {
        $totalVariationPrice += $variationMP[$cnt]->getPrice();
      }
    }
    else
    {
      $totalVariationPrice = 0 ;
    }

    $midPrice = ($totalVariationPrice / $totalProduct);

    $mainProductDetails = getProduct($conn, "WHERE uid =?",array("uid"),array($mainProductUid),"s");
    $category = $mainProductDetails[0]->getCategory();
    $rating = $mainProductDetails[0]->getRating();


    //   FOR DEBUGGING 
    //  echo "<br>";
    //  echo $category."<br>";

    if(isset($_POST['submit']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
    
        //echo "save to database";
        if($name)
        {
            array_push($tableName,"name");
            array_push($tableValue,$name);
            $stringType .=  "s";
        }

        if($price)
        {
            array_push($tableName,"price");
            array_push($tableValue,$price);
            $stringType .=  "s";
        } 

        if($amount)
        {
            array_push($tableName,"amount");
            array_push($tableValue,$amount);
            $stringType .=  "d";
        } 

        array_push($tableValue,$variationUid);
        $stringType .=  "s";
        $updateProductDetails = updateDynamicData($conn,"variation"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($updateProductDetails)
        {
            // header('Location: ../allProducts.php');

            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
        
            //echo "save to database";
            if($category)
            {
                array_push($tableName,"category");
                array_push($tableValue,$category);
                $stringType .=  "s";
            }
            if($rating)
            {
                array_push($tableName,"rating");
                array_push($tableValue,$rating);
                $stringType .=  "s";
            }     
            array_push($tableValue,$mainProductUid);
            $stringType .=  "s";
            $updateProductDetails = updateDynamicData($conn,"variation"," WHERE product_uid = ? ",$tableName,$tableValue,$stringType);
            if($updateProductDetails)
            {
                // header('Location: ../allProducts.php');

                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
            
                //echo "save to database";
                if($minPrice)
                {
                    array_push($tableName,"minPrice");
                    array_push($tableValue,$minPrice);
                    $stringType .=  "d";
                }
                if($midPrice)
                {
                    array_push($tableName,"midPrice");
                    array_push($tableValue,$midPrice);
                    $stringType .=  "d";
                }   
                if($maxPrice)
                {
                    array_push($tableName,"maxPrice");
                    array_push($tableValue,$maxPrice);
                    $stringType .=  "d";
                }    
                array_push($tableValue,$mainProductUid);
                $stringType .=  "s";
                $updateProductDetails = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($updateProductDetails)
                {
                    header('Location: ../allProducts.php');
                }
                else
                {
                    echo "FAIL !!";
                }

            }
            else
            {
                echo "FAIL !!";
            }

        }
        else
        {
            echo "FAIL !!";
        }
    }
    else
    {
        echo "ERROR !!";
    }

}
else
{
    header('Location: ../index.php');
}
?>