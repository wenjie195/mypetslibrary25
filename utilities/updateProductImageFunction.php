<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $imageValue = rewrite($_POST['image_value']);
     $productUid = rewrite($_POST['product_uid']);

     $imageOne = $timestamp.$_FILES['image_one']['name'];
     $target_dir = "../productImage/";
     $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
     }

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $productUid."<br>";

     $product = getProduct($conn," uid = ? ",array("uid"),array($productUid),"s");    

     if(!$product)
     {   
          if($imageValue == '1')
          {
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
     
               if($imageOne)
               {
                    array_push($tableName,"image_one");
                    array_push($tableValue,$imageOne);
                    $stringType .=  "s";
               }
     
               array_push($tableValue,$productUid);
               $stringType .=  "s";
               $updateImage = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($updateImage)
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../allProducts.php?type=1');
               }
               else
               {
                    echo "fail";
                    // $_SESSION['messageType'] = 1;
                    // header('Location: ../allProducts.php?type=2');
               }
          }
          elseif($imageValue == '2')
          {
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
     
               if($imageOne)
               {
                    array_push($tableName,"image_two");
                    array_push($tableValue,$imageOne);
                    $stringType .=  "s";
               }
     
               array_push($tableValue,$productUid);
               $stringType .=  "s";
               $updateImage = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($updateImage)
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../allProducts.php?type=1');
               }
               else
               {
                    echo "fail";
                    // $_SESSION['messageType'] = 1;
                    // header('Location: ../allProducts.php?type=2');
               }
          }
          elseif($imageValue == '3')
          {
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
     
               if($imageOne)
               {
                    array_push($tableName,"image_three");
                    array_push($tableValue,$imageOne);
                    $stringType .=  "s";
               }
     
               array_push($tableValue,$productUid);
               $stringType .=  "s";
               $updateImage = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($updateImage)
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../allProducts.php?type=1');
               }
               else
               {
                    echo "fail";
                    // $_SESSION['messageType'] = 1;
                    // header('Location: ../allProducts.php?type=2');
               }
          }
          elseif($imageValue == '4')
          {
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
     
               if($imageOne)
               {
                    array_push($tableName,"image_four");
                    array_push($tableValue,$imageOne);
                    $stringType .=  "s";
               }
     
               array_push($tableValue,$productUid);
               $stringType .=  "s";
               $updateImage = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($updateImage)
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../allProducts.php?type=1');
               }
               else
               {
                    echo "fail";
                    // $_SESSION['messageType'] = 1;
                    // header('Location: ../allProducts.php?type=2');
               }
          }
          elseif($imageValue == '5')
          {
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
     
               if($imageOne)
               {
                    array_push($tableName,"image_five");
                    array_push($tableValue,$imageOne);
                    $stringType .=  "s";
               }
     
               array_push($tableValue,$productUid);
               $stringType .=  "s";
               $updateImage = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($updateImage)
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../allProducts.php?type=1');
               }
               else
               {
                    echo "fail";
                    // $_SESSION['messageType'] = 1;
                    // header('Location: ../allProducts.php?type=2');
               }
          }
          elseif($imageValue == '6')
          {
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
     
               if($imageOne)
               {
                    array_push($tableName,"image_six");
                    array_push($tableValue,$imageOne);
                    $stringType .=  "s";
               }
     
               array_push($tableValue,$productUid);
               $stringType .=  "s";
               $updateImage = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($updateImage)
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../allProducts.php?type=1');
               }
               else
               {
                    echo "fail";
                    // $_SESSION['messageType'] = 1;
                    // header('Location: ../allProducts.php?type=2');
               }
          }
          else
          {
               echo "exceed image limit !!";
          }
     }
     else
     {
          echo "error";
          // $_SESSION['messageType'] = 1;
          // header('Location: ../allProducts.php?type=3');
     }

}
else 
{
    header('Location: ../index.php');
}
?>