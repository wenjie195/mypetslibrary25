<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../adminAccess.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Seller.php';
require_once dirname(__FILE__) . '/../classes/Slider.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST["sliderUid"]);

    $link = rewrite($_POST["update_link"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $sliderDetails = getSlider($conn, "WHERE uid = ? ", array("uid"), array($uid), "s");

    // if(!$sliderDetails)
    if($sliderDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($link)
        {
            array_push($tableName,"link");
            array_push($tableValue,$link);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $profileUpdated = updateDynamicData($conn,"slider"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($profileUpdated)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../slider.php?type=6');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../slider.php?type=7');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../slider.php?type=8');
    }

}
else 
{
    header('Location: ../index.php');
}
?>