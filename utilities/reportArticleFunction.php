<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/ReportedArticle.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userID = $_SESSION['uid'];
$userType = $_SESSION['usertype'];

function reportArticle($conn,$uid,$username,$articleUid,$reason)
{
     if(insertDynamicData($conn,"reported_article",array("uid","username","article_uid","reason"),
          array($uid,$username,$articleUid,$reason),"ssss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $uid = $userID;
     $uid = md5(uniqid());

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userID),"s");
     $username = $userDetails[0]->getName();

     $articleUid = rewrite($_POST['article_uid']);
     $reason = rewrite($_POST['report_reason']);

     $type = "Reported";

     // // FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $username."<br>";
     // echo $articleUid."<br>";
     // echo $reason."<br>";

     if(reportArticle($conn,$uid,$username,$articleUid,$reason))
     {
          // echo "reported";   
          // echo "<br>";

          if(isset($_POST['article_uid']))
          {   
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
               if($type)
               {
                    array_push($tableName,"type");
                    array_push($tableValue,$type);
                    $stringType .=  "s";
               }    
               if($username)
               {
                    array_push($tableName,"author");
                    array_push($tableValue,$username);
                    $stringType .=  "s";
               } 
               if($uid)
               {
                    array_push($tableName,"img_five_source");
                    array_push($tableValue,$uid);
                    $stringType .=  "s";
               } 
               array_push($tableValue,$articleUid);
               $stringType .=  "s";
               $reportArticle = updateDynamicData($conn,"articles"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($reportArticle)
               {
                    // $_SESSION['messageType'] = 1;
                    // header('Location: ../malaysia-pet-blog.php?type=1');

                    if($userType == 0)
                    {
                         header('Location: ' . $_SERVER['HTTP_REFERER']);
                         exit;
                    }
                    else
                    {
                         $_SESSION['messageType'] = 1;
                         header('Location: ../malaysia-pet-blog.php?type=1');
                    }

               }
               else
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../malaysia-pet-blog.php?type=2');
               }
          }
          else
          {
              $_SESSION['messageType'] = 1;
              header('Location: ../malaysia-pet-blog.php?type=3');
          }

     }
     else 
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../malaysia-pet-blog.php?type=4');
     }
 
}
else 
{
     header('Location: ../index.php');
}
?>