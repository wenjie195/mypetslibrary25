<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/PreOrderList.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Variation.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userUid = $_SESSION['uid'];

function addPreOrder($conn,$userUid,$productUid,$productName,$originalPrice,$quantity,$totalPrice,$status,$mainProductUid)
{
     if(insertDynamicData($conn,"preorder_list",array("user_uid","product_uid","product_name","original_price","quantity","totalPrice","status","main_product_uid"),
          array($userUid,$productUid,$productName,$originalPrice,$quantity,$totalPrice,$status,$mainProductUid),"ssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $uid = md5(uniqid());

     $productUid = rewrite($_POST['product_uid']);
     // $productUid = $_POST['product_uid'];

     // $productName = rewrite($_POST['product_name']);
     // $originalPrice = rewrite($_POST['product_price']);
     // $mainProductUid = rewrite($_POST['main_product_uid']);

     $productVariation = getVariation($conn,"WHERE uid = ? ", array("uid") ,array($productUid),"s");
     $productName = $productVariation[0]->getName();
     $originalPrice = $productVariation[0]->getPrice();
     $mainProductUid = $productVariation[0]->getProductUid();

     $quantity = rewrite($_POST['product_quantity']);
     $totalPrice = $originalPrice * $quantity;
     $status = 'Pending';

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $name."<br>";

     if($quantity < 1)
     {
          header('Location: ../malaysia-pets-products.php');
     }
     else
     {
          if(addPreOrder($conn,$userUid,$productUid,$productName,$originalPrice,$quantity,$totalPrice,$status,$mainProductUid))
          {
               // echo "product added to cart";
               header('Location: ../viewShoppingCart.php');
          }
          else
          {
               echo "fail to add product into cart";
          }
     }


     // if(addPreOrder($conn,$userUid,$productUid,$productName,$originalPrice,$quantity,$totalPrice,$status))
     // {
     //      // echo "product added to cart";
     //      header('Location: ../viewShoppingCart.php');
     // }
     // else
     // {
     //      echo "fail to add product into cart";
     // }
}
else 
{
     header('Location: ../index.php');
}
?>