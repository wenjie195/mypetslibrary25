<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/OrderList.php';
require_once dirname(__FILE__) . '/../classes/PreOrderList.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];
$timestamp = time();

function addOrderList($conn,$orderUid,$uid,$productUid,$productName,$originalPrice,$quantity,$totalPrice,$status,$mainProductUid)
{
     if(insertDynamicData($conn,"order_list",array("order_id","user_uid","product_uid","product_name","original_price","quantity","totalPrice","status","main_product_uid"),
          array($orderUid,$uid,$productUid,$productName,$originalPrice,$quantity,$totalPrice,$status,$mainProductUid),"sssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

// function orderDetails($conn,$orderUid,$uid)
// {
//      if(insertDynamicData($conn,"orders",array("order_id","uid"),
//           array($orderUid,$uid),"ss") === null)
//      {
//           echo "gg";
//      }
//      else{    }
//      return true;
// }

// function addOrderList($conn,$uid,$productUid,$productName,$originalPrice,$quantity,$totalPrice,$status)
// {
//      if(insertDynamicData($conn,"order_list",array("user_uid","product_uid","product_name","original_price","quantity","totalPrice","status"),
//           array($uid,$productUid,$productName,$originalPrice,$quantity,$totalPrice,$status),"sssssss") === null)
//      {
//           echo "gg";
//      }
//      else{    }
//      return true;
// }

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $preOrderProduct = getPreOrderList($conn, "WHERE user_uid = ? AND status = 'Pending' ",array("user_uid"),array($uid),"s");
     // $preOrderProduct = getPreOrderList($conn);
     if ($preOrderProduct) 
     {
       for ($cnt=0; $cnt < count($preOrderProduct) ; $cnt++)
       {
          // echo $productUid = $preOrderProduct[$cnt]->getProductUid();
          // echo "<br>";
          // echo $productName = $preOrderProduct[$cnt]->getProductName();
          // echo "<br>";
          // echo $originalPrice = $preOrderProduct[$cnt]->getOriginalPrice();
          // echo "<br>";
          // echo $quantity = $preOrderProduct[$cnt]->getQuantity();
          // echo "<br>";
          // echo $totalPrice = $preOrderProduct[$cnt]->getTotalPrice();
          // echo "<br>";
     
          $productUid = $preOrderProduct[$cnt]->getProductUid();
          $productName = $preOrderProduct[$cnt]->getProductName();
          $originalPrice = $preOrderProduct[$cnt]->getOriginalPrice();
          $quantity = $preOrderProduct[$cnt]->getQuantity();
          $totalPrice = $preOrderProduct[$cnt]->getTotalPrice();
          $productOrderId = $preOrderProduct[$cnt]->getId();
          $mainProductUid = $preOrderProduct[$cnt]->getMainProductUid();

          $preOrderStatus = "Sold";
          $orderUid = $uid.$timestamp;
          $_SESSION['order_uid'] = $orderUid;
          $status = "Pending";

          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($preOrderStatus)
          {
              array_push($tableName,"status");
              array_push($tableValue,$preOrderStatus);
              $stringType .=  "s";
          }    
          array_push($tableValue,$productOrderId);
          $stringType .=  "s";
          $updateBonusPool = updateDynamicData($conn,"preorder_list"," WHERE id = ? ",$tableName,$tableValue,$stringType);
          if($updateBonusPool)
          {
               if(addOrderList($conn,$orderUid,$uid,$productUid,$productName,$originalPrice,$quantity,$totalPrice,$status,$mainProductUid))
               {
                    // echo "product added to order list";
                    // $_SESSION['order_uid'] = $orderUid;
                    header('Location: ../viewCheckoutList.php');
               }
               else
               {
                    echo "fail to add product into order list";
               }
          }
          else
          {
               echo "FAIL TO CLEAR  ORDER LIST";
          }

       }
     }

     // $uid = md5(uniqid());

     // if(addOrderList($conn,$userUid,$productUid,$productName,$originalPrice,$quantity,$totalPrice,$status))
     // {
     //      // echo "product added to cart";
     //      header('Location: ../viewShoppingCart.php');
     // }
     // else
     // {
     //      echo "fail to add product into cart";
     // }
}
else 
{
     header('Location: ../index.php');
}
?>