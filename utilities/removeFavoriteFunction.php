<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Favorite.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $itemUid = rewrite($_POST["item_uid"]);
    $uid = rewrite($_POST["user_uid"]);
    $status = "Delete";

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $itemUid."<br>";
    // echo $uid."<br>";

    $favorite = getFavorite($conn," uid = ? AND item_uid = ? ",array("uid, item_uid"),array($uid,$itemUid),"ss");    

    if(!$favorite)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid,$itemUid);
        $stringType .=  "ss";
        $passwordUpdated = updateDynamicData($conn,"favorite"," WHERE uid = ? AND item_uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            // echo "success";
            $url = $_SESSION['url']; 
            header("location: $url");

            // // if($url != '')
            // // {
            // //     header("location: $url");
            // // }
            // // else
            // // {
            // //     header('Location: ../profile.php');
            // // }

        }
        else
        {
            echo "fail to remove favorite";
        }
    }
    else
    {
        echo "error";
    }

}
else 
{
    header('Location: ../index.php');
}
?>
