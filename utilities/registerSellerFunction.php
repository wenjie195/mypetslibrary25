<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Seller.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewSeller($conn,$uid,$companyName,$slug,$registrationNo,$contactNo,$email,$address,$state,$accountStatus,
$contactPerson,$contactPersonNo,$experience,$cert,$services,$breedType,$otherInfo,$infoTwo,$infoThree,$infoFour,$credit)
{
     if(insertDynamicData($conn,"seller",array("uid","company_name","slug","registration_no","contact_no","email","address","state","account_status","contact_person",
     "contact_person_no","experience","cert","services","breed_type","other_info","info_two","info_three","info_four","credit"),
          array($uid,$companyName,$slug,$registrationNo,$contactNo,$email,$address,$state,$accountStatus,
          $contactPerson,$contactPersonNo,$experience,$cert,$services,$breedType,$otherInfo,$infoTwo,$infoThree,$infoFour,$credit),"ssssssssssssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

function registerNewUser($conn,$uid,$companyName,$contactPersonNo,$email,$finalPassword,$salt,$user_type)
{
     if(insertDynamicData($conn,"user",array("uid","name","phone_no","email","password","salt","user_type"),
          array($uid,$companyName,$contactPersonNo,$email,$finalPassword,$salt,$user_type),"ssssssi") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     // $companyName = rewrite($_POST['register_name']);
     $stringOne = ($_POST['register_name']);
     $companyName = preg_replace('/[^\p{L}\p{N}\s][^(\x20-\x7F)]*/u', '', $stringOne);
     // $companyName = str_replace('/[^\p{L}\p{N}\s][^(\x20-\x7F)]*/u', '', $stringOne);

     // $slug = rewrite($_POST['register_slug']);
     $updatedCompName = str_replace(' ', '-', trim($companyName));
     // $slug = $companyName;
     $slug = $updatedCompName;

     $contactPerson = rewrite($_POST['register_contact_person']);
     $contactPersonNo = rewrite($_POST['register_contact_personNo']);
     $state = rewrite($_POST['register_state']);
     $registrationNo = rewrite($_POST['register_registration_no']);

     $sellerContact= rewrite($_POST['register_contact_no']);
     $link = "https://api.whatsapp.com/send?phone=6";
     $contactNo = $link.$sellerContact;

     $address = rewrite($_POST['register_address']);
     $experience = rewrite($_POST['register_experience']);
     $cert = rewrite($_POST['register_cert']);

     $services = $_POST['register_services'];
     $b=implode(", ",$services);

     $accountStatus = "Active";
     $breedType = rewrite($_POST['register_breed']);

     $otherInfo = rewrite($_POST['register_info']);
     $infoTwo = rewrite($_POST['register_info_two']);
     $infoThree = rewrite($_POST['register_info_three']);
     $infoFour = rewrite($_POST['register_info_four']);

     $email = rewrite($_POST['register_email']);

     $credit = rewrite($_POST['register_credit']);

     $register_password = "123321";
     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);
     $user_type = "2";

     // // FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $companyName."<br>";
     // echo $slug."<br>";
     // echo $registrationNo."<br>";
     // echo $contactNo."<br>";
     // echo $address."<br>";
     // echo $state ."<br>";
     // echo $accountStatus."<br>";
     // echo $contactPerson."<br>";
     // echo $contactPersonNo."<br>";
     // echo $experience."<br>";
     // echo $cert."<br>";
     // echo $b."<br>";
     // echo $breedType."<br>";
     // echo $otherInfo."<br>";

     $companyNameRows = getSeller($conn," WHERE company_name = ? ",array("company_name"),array($_POST['register_name']),"s");
     $registeredCompName = $companyNameRows[0];

     // $companyContactRows = getSeller($conn," WHERE contact_no = ? ",array("contact_no"),array($_POST['register_contact_no']),"s");
     // $registeredContactNo = $companyContactRows[0];

     // if(!$registeredCompName && !$registeredContactNo)

     if(!$registeredCompName)
     {
          $services = $b;

          if(registerNewSeller($conn,$uid,$companyName,$slug,$registrationNo,$contactNo,$email,$address,$state,$accountStatus,$contactPerson,$contactPersonNo,
               $experience,$cert,$services,$breedType,$otherInfo,$infoTwo,$infoThree,$infoFour,$credit))
          {

               $usernameRows = getUser($conn," WHERE name = ? ",array("name"),array($_POST['register_name']),"s");
               $usernameDetails = $usernameRows[0];
          
               // $userPhoneRows = getUser($conn," WHERE phone_no = ? ",array("phone_no"),array($_POST['register_contact_no']),"s");
               // $userPhoneDetails = $userPhoneRows[0];
          
               // if(!$usernameDetails && !$userPhoneDetails)
               if(!$usernameDetails)
               {
                    $name = $companyName;
                    
                    if(registerNewUser($conn,$uid,$companyName,$contactPersonNo,$email,$finalPassword,$salt,$user_type))
                    {
                         // $_SESSION['messageType'] = 1;
                         // header('Location: ../addSeller.php?type=1');
                         // echo "register successfully as a seller and an user";

                         // $_SESSION['register_uid'] = $uid;
                         // header('Location: ../cropPhoto.php');

                         // $registerUid = $uid;
                         // $_COOKIE['register_uid'] = $uid;

                         $_SESSION['register_uid'] = $uid;
                         header('Location: ../cropCompanyLogo.php');

                    }
                    else
                    {
                         $_SESSION['messageType'] = 1;
                         header('Location: ../addSeller.php?type=6');
                         // echo "fail to register as a user";
                    }
               }
               else
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../addSeller.php?type=7');
                    // echo "data to register as an user has been taken, pls use a new";
               }

          }
          else
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../addSeller.php?type=5');
               // echo "fail to add seller !!";
          }
     }
     else
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../addSeller.php?type=7');
          // echo "data to register as a seller has been taken, pls use a new";
          // header('Location: ' . $_SERVER['HTTP_REFERER']);
          // exit;
     }

}
else 
{
     header('Location: ../index.php');
}
?>