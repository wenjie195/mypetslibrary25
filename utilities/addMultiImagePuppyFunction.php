<?php 
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Puppy.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$timestamp = time();

$newPetsUid = $_SESSION['newPetsUid'];
$newPetsType = $_SESSION['newpets_type'];

if (isset($_SESSION['image'])) {}else{$_SESSION['image'] = 1;};

if(!empty($_FILES))
{ 
    // File path configuration 
    $uploadDir = "../uploads/"; 
    $fileName = basename($timestamp.$newPetsUid.$_FILES['file']['name']); 
    $uploadFilePath = $uploadDir.$fileName;
    // Upload file to server 

    if(move_uploaded_file($_FILES['file']['tmp_name'], $uploadFilePath))
    {
        if (isset($_SESSION['newPetsUid']))
        {
            if($_SESSION['image'] == 1)
            {
                $upload = updateDynamicData($conn,"puppy","WHERE uid =?",array("image_one"),array($fileName,$_SESSION['newPetsUid']), "ss");
                $uploadPuppy = updateDynamicData($conn,"pet_details","WHERE uid =?",array("image_one"),array($fileName,$_SESSION['newPetsUid']), "ss");
                $upload2A = updateDynamicData($conn,"puppy","WHERE uid =?",array("default_image"),array($fileName,$_SESSION['newPetsUid']), "ss");
                $uploadPuppy2A = updateDynamicData($conn,"pet_details","WHERE uid =?",array("default_image"),array($fileName,$_SESSION['newPetsUid']), "ss");
                $_SESSION['image'] = 2;
            }
            elseif($_SESSION['image'] == 2)
            {
                $upload = updateDynamicData($conn,"puppy","WHERE uid =?",array("image_two"),array($fileName,$_SESSION['newPetsUid']), "ss");
                $uploadPuppy = updateDynamicData($conn,"pet_details","WHERE uid =?",array("image_two"),array($fileName,$_SESSION['newPetsUid']), "ss");
                $_SESSION['image'] = 3;
            }
            elseif($_SESSION['image'] == 3)
            {
                $upload = updateDynamicData($conn,"puppy","WHERE uid =?",array("image_three"),array($fileName,$_SESSION['newPetsUid']), "ss");
                $uploadPuppy = updateDynamicData($conn,"pet_details","WHERE uid =?",array("image_three"),array($fileName,$_SESSION['newPetsUid']), "ss");
                $_SESSION['image'] = 4;
            }
            elseif($_SESSION['image'] == 4)
            {
                $upload = updateDynamicData($conn,"puppy","WHERE uid =?",array("image_four"),array($fileName,$_SESSION['newPetsUid']), "ss");
                $uploadPuppy = updateDynamicData($conn,"pet_details","WHERE uid =?",array("image_four"),array($fileName,$_SESSION['newPetsUid']), "ss");
                $_SESSION['image'] = 5;
            }
            elseif($_SESSION['image'] == 5)
            {
                $upload = updateDynamicData($conn,"puppy","WHERE uid =?",array("image_five"),array($fileName,$_SESSION['newPetsUid']), "ss");
                $uploadPuppy = updateDynamicData($conn,"pet_details","WHERE uid =?",array("image_five"),array($fileName,$_SESSION['newPetsUid']), "ss");
                $_SESSION['image'] = 6;
            }
            elseif($_SESSION['image'] == 6)
            {
                $upload = updateDynamicData($conn,"puppy","WHERE uid =?",array("image_six"),array($fileName,$_SESSION['newPetsUid']), "ss");
                $uploadPuppy = updateDynamicData($conn,"pet_details","WHERE uid =?",array("image_six"),array($fileName,$_SESSION['newPetsUid']), "ss");
                // $_SESSION['image'] = 7;
            }
        }
        else
        {   echo "ERROR";   } 
    } 
    else
    {   echo "ERROR 2";   } 
} 
else
{   
    // echo "ERROR 3";   
    header('Location: ../addPuppy.php');
} 
?>