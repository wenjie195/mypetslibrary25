<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Brand.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $brandName = rewrite($_POST["update_brand_name"]);
    $id = rewrite($_POST["brand_id"]);
    $brandStatus = rewrite($_POST["brand_status"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $allBrand = getBrand($conn," WHERE name = ? AND status = ? ",array("name","status"),array($brandName,$brandStatus),"ss");
    $existingBrand = $allBrand[0];

    $brand = getBrand($conn," WHERE id = ? ",array("id"),array($id),"s");    

    // if(!$existingCategory)
    if(!$existingBrand)
    {
        if($brand)
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($brandName)
            {
                array_push($tableName,"name");
                array_push($tableValue,$brandName);
                $stringType .=  "s";
            }
            array_push($tableValue,$id);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"brand"," WHERE id = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                $_SESSION['messageType'] = 3;
                header('Location: ../brand.php?type=1');
            }
            else
            {
                $_SESSION['messageType'] = 3;
                header('Location: ../brand.php?type=2');
            }
        }
        else
        {
            $_SESSION['messageType'] = 3;
            header('Location: ../brand.php?type=3');
        }
    }
    else
    {
        $_SESSION['messageType'] = 3;
        header('Location: ../brandphp?type=4');
    }
}
else 
{
    header('Location: ../index.php');
}
?>
