<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $productUid = rewrite($_POST['product_uid']);

    $name = rewrite($_POST['update_name']);
    $sku = rewrite($_POST['update_name']);
    $slug = rewrite($_POST['update_name']);
    // $price = rewrite($_POST['update_price']);
    // $diamond = rewrite($_POST['update_price']);

    // $quantity = rewrite($_POST['update_quantity']);
    $category = rewrite($_POST['update_category']);
    $brand = rewrite($_POST['update_brand']);

    $description = rewrite($_POST['update_description']);
    $descriptionTwo = rewrite($_POST['update_description_two']);
    $descriptionThree = rewrite($_POST['update_description_three']);
    $descriptionFour = rewrite($_POST['update_description_four']);
    $descriptionFive = rewrite($_POST['update_description_five']);
    $descriptionSix = rewrite($_POST['update_description_six']);
    $keyword = rewrite($_POST['update_keyword_one']);
    $link = rewrite($_POST['update_link']);

    if($link != '')
    {
        $tubeOne = "Yes";
    }
    else
    {
        $tubeOne = "No";
    }

    //   FOR DEBUGGING 
    //  echo "<br>";
    //  echo $category."<br>";

    if(isset($_POST['submit']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
    
        //echo "save to database";
        if($name)
        {
            array_push($tableName,"name");
            array_push($tableValue,$name);
            $stringType .=  "s";
        }
        if($sku)
        {
            array_push($tableName,"sku");
            array_push($tableValue,$sku);
            $stringType .=  "s";
        }
        if($slug)
        {
            array_push($tableName,"slug");
            array_push($tableValue,$slug);
            $stringType .=  "s";
        }

        if($category)
        {
            array_push($tableName,"category");
            array_push($tableValue,$category);
            $stringType .=  "s";
        }   
        if($brand)
        {
            array_push($tableName,"brand");
            array_push($tableValue,$brand);
            $stringType .=  "s";
        }   

        // if($diamond)
        // {
        //     array_push($tableName,"diamond");
        //     array_push($tableValue,$diamond);
        //     $stringType .=  "s";
        // }   
        // if($price)
        // {
        //     array_push($tableName,"price");
        //     array_push($tableValue,$price);
        //     $stringType .=  "s";
        // } 
        // if($quantity)
        // {
        //     array_push($tableName,"quantity");
        //     array_push($tableValue,$quantity);
        //     $stringType .=  "s";
        // } 

        if($description)
        {
            array_push($tableName,"description");
            array_push($tableValue,$description);
            $stringType .=  "s";
        }
        if($descriptionTwo)
        {
            array_push($tableName,"description_two");
            array_push($tableValue,$descriptionTwo);
            $stringType .=  "s";
        }
        if($descriptionThree)
        {
            array_push($tableName,"description_three");
            array_push($tableValue,$descriptionThree);
            $stringType .=  "s";
        }
        if($descriptionFour)
        {
            array_push($tableName,"description_four");
            array_push($tableValue,$descriptionFour);
            $stringType .=  "s";
        }
        if($descriptionFive)
        {
            array_push($tableName,"description_five");
            array_push($tableValue,$descriptionFive);
            $stringType .=  "s";
        }
        if($descriptionSix)
        {
            array_push($tableName,"description_six");
            array_push($tableValue,$descriptionSix);
            $stringType .=  "s";
        }
        if($link)
        {
            array_push($tableName,"link");
            array_push($tableValue,$link);
            $stringType .=  "s";
        }
        if($keyword)
        {
            array_push($tableName,"keyword_one");
            array_push($tableValue,$keyword);
            $stringType .=  "s";
        }
    
        array_push($tableValue,$productUid);
        $stringType .=  "s";
        $updateProductDetails = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($updateProductDetails)
        {
            header('Location: ../allProducts.php');
        }
        else
        {
            echo "FAIL !!";
        }
    }
    else
    {
        echo "ERROR !!";
    }

}
else
{
    header('Location: ../index.php');
}
?>