<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ProductOrders.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $orderId = rewrite($_POST["order_id"]);

    // $delivered_on = rewrite($_POST["delivered_on"]);

    $tz = 'Asia/Kuala_Lumpur';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $delivered_on = $dt->format('Y-m-d');

    // $orderStatus = "Accepted";
    // $shipping_status = "Delivered";

    $orderStatus = "REJECTED";
    $shipping_status = "REJECTED";

    // // for debugging
    // echo "<br>";
    // echo $orderId."<br>";


    if(isset($_POST['order_id']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($orderStatus)
        {
            array_push($tableName,"payment_status");
            array_push($tableValue,$orderStatus);
            $stringType .=  "s";
        }
        if($shipping_status)
        {
            array_push($tableName,"shipping_status");
            array_push($tableValue,$shipping_status);
            $stringType .=  "s";
        } 
        if($delivered_on)
        {
            array_push($tableName,"shipping_date");
            array_push($tableValue,$delivered_on);
            $stringType .=  "s";
        }
        
        array_push($tableValue,$orderId);
        $stringType .=  "s";
        $orderUpdated = updateDynamicData($conn,"orders"," WHERE order_id = ? ",$tableName,$tableValue,$stringType);
        if($orderUpdated)
        {
            header('Location: ../adminOrdersPending.php');
        }
        else
        {
            echo "fail";
            // $_SESSION['messageType'] = 1;
            // header('Location: ../adminViewRejectedOrders.php?type=2');
        }
    }
    else
    {
        echo "ERROR";
        // $_SESSION['messageType'] = 1;
        // header('Location: ../adminViewRejectedOrders.php?type=3');
    }
}
else 
{
    header('Location: ../index.php');
}
?>