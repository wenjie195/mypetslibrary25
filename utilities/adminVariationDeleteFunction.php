<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Variation.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $variationUid = rewrite($_POST["variation_uid"]);

    $status = "Delete";

    $product = getVariation($conn," uid = ? ",array("uid"),array($variationUid),"s");    
    if(!$product)
    // if($product)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        array_push($tableValue,$variationUid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"variation"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../allProducts.php?type=1');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../allProducts.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../allProducts.php?type=3');
    }
}
else 
{
    header('Location: ../index.php');
}
?>