<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Variation.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$timestamp = time();
// $newProductUid = $_SESSION['product_uid'];

function registerNewVariation($conn,$variationUid,$mainProductUid,$name,$price,$imageOne,$status,$amount,$category,$rating)
{
     if(insertDynamicData($conn,"variation",array("uid","product_uid","name","price","image","status","amount","category","rating"),
          array($variationUid,$mainProductUid,$name,$price,$imageOne,$status,$amount,$category,$rating),"ssssssdss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $variationUid = md5(uniqid());
     $name = rewrite($_POST['register_name']);
     $price = rewrite($_POST['register_price']);
     $amount = rewrite($_POST['register_price']);
     $mainProductUid = rewrite($_POST['product_uid']);

     $minPrice = rewrite($_POST['register_price']);
     $midPrice = rewrite($_POST['register_price']);
     $maxPrice = rewrite($_POST['register_price']);

     $mainProductDetails = getProduct($conn, "WHERE uid =?",array("uid"),array($mainProductUid),"s");
     $preCategory = $mainProductDetails[0]->getCategory();
     $preRating = $mainProductDetails[0]->getRating();
     $mainProductImage = $mainProductDetails[0]->getImageOne();

     if($preCategory != '')
     {
          $category = $preCategory;
     }
     else
     {}

     if($preRating != '')
     {
          $rating = $preRating;
     }
     else
     {}

     // $variationLP = getVariation($conn,"WHERE product_uid = ? AND status = 'Available' ORDER BY amount ASC LIMIT 1", array("product_uid") ,array($mainProductUid),"s");
     // $minPrice = $variationLP[0]->getPrice();
 
     // $variationHP = getVariation($conn,"WHERE product_uid = ? AND status = 'Available' ORDER BY amount DESC LIMIT 1", array("product_uid") ,array($mainProductUid),"s");
     // $maxPrice = $variationHP[0]->getPrice();
 
     // $variationMP = getVariation($conn,"WHERE product_uid = ? AND status = 'Available' ", array("product_uid") ,array($mainProductUid),"s");
     // // $variationMPrice = $variationHP[0]->getAmount();
     // if($variationMP)
     // {   
     //     $totalProduct = count($variationMP);
     // }
     // else
     // {   $totalProduct = 0;   }
 
     // if($variationMP)
     // {
     //   $totalVariationPrice = 0;
     //   for ($cnt=0; $cnt <count($variationMP) ; $cnt++)
     //   {
     //     $totalVariationPrice += $variationMP[$cnt]->getPrice();
     //   }
     // }
     // else
     // {
     //   $totalVariationPrice = 0 ;
     // }

     $imageOne = $timestamp.$_FILES['image_one']['name'];
     $target_dir = "../productImage/";
     $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
     }

     $status = "Available";

     // $category= rewrite($_POST['register_category']);
     // $name = rewrite($_POST['register_name']);
     // $description = rewrite($_POST['register_description']);
     // $descriptionTwo = rewrite($_POST['register_description_two']);
     // $descriptionThree = rewrite($_POST['register_description_three']);
     // $descriptionFour = rewrite($_POST['register_description_four']);
     // $descriptionFive = rewrite($_POST['register_description_five']);
     // $descriptionSix = rewrite($_POST['register_description_six']);
     // $keywordOne = rewrite($_POST['register_keyword']);
     // $link = rewrite($_POST['register_link']);
     // $slug = rewrite($_POST['register_name']);
     // $status = "Available";

     $productNameDetails = getVariation($conn," WHERE name = ? ",array("name"),array($name),"s");
     $registeredVariationName = $productNameDetails[0];

     if(!$registeredVariationName)
     {
          // if(registerNewVariation($conn,$variationUid,$mainProductUid,$name,$price,$imageOne,$status,$amount,$category,$rating))
          // {
          //      $_SESSION['product_uid'] = $mainProductUid;
          //      header('Location: ../addProductVariation2.php');
          //      // echo "success";
          // }
          // else
          // {
          //      echo "fail";
          // }

          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
      
          //echo "save to database";
          if($category)
          {
              array_push($tableName,"category");
              array_push($tableValue,$category);
              $stringType .=  "s";
          }
          if($rating)
          {
              array_push($tableName,"rating");
              array_push($tableValue,$rating);
              $stringType .=  "s";
          }   
          array_push($tableValue,$mainProductUid);
          $stringType .=  "s";
          $updateProductDetails = updateDynamicData($conn,"variation"," WHERE product_uid = ? ",$tableName,$tableValue,$stringType);
          if($updateProductDetails)
          {
               // if(registerNewVariation($conn,$variationUid,$mainProductUid,$name,$price,$imageOne,$status,$amount,$category,$rating))
               // {
               //      $_SESSION['product_uid'] = $mainProductUid;
               //      header('Location: ../addProductVariation2.php');
               //      // echo "success";
               // }
               // else
               // {
               //      echo "fail";
               // }

               if($mainProductImage != "")
               {
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                
                    //echo "save to database";
                    if($minPrice)
                    {
                        array_push($tableName,"minPrice");
                        array_push($tableValue,$minPrice);
                        $stringType .=  "d";
                    }
                    if($midPrice)
                    {
                        array_push($tableName,"midPrice");
                        array_push($tableValue,$midPrice);
                        $stringType .=  "d";
                    }   
                    if($maxPrice)
                    {
                        array_push($tableName,"maxPrice");
                        array_push($tableValue,$maxPrice);
                        $stringType .=  "d";
                    }    
                    array_push($tableValue,$mainProductUid);
                    $stringType .=  "s";
                    $updateProductDetails = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($updateProductDetails)
                    {
                         // header('Location: ../allProducts.php');
                         if(registerNewVariation($conn,$variationUid,$mainProductUid,$name,$price,$imageOne,$status,$amount,$category,$rating))
                         {
                              $_SESSION['product_uid'] = $mainProductUid;
                              header('Location: ../addProductVariation2.php');
                              // echo "success";
                         }
                         else
                         {
                              echo "fail";
                         }
                    }
                    else
                    {
                        echo "FAIL !!";
                    }
               }
               else
               {
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                
                    //echo "save to database";
                    if($minPrice)
                    {
                        array_push($tableName,"minPrice");
                        array_push($tableValue,$minPrice);
                        $stringType .=  "d";
                    }
                    if($midPrice)
                    {
                        array_push($tableName,"midPrice");
                        array_push($tableValue,$midPrice);
                        $stringType .=  "d";
                    }   
                    if($maxPrice)
                    {
                        array_push($tableName,"maxPrice");
                        array_push($tableValue,$maxPrice);
                        $stringType .=  "d";
                    }   
                    if($imageOne)
                    {
                        array_push($tableName,"image_one");
                        array_push($tableValue,$imageOne);
                        $stringType .=  "s";
                    }   
                    array_push($tableValue,$mainProductUid);
                    $stringType .=  "s";
                    $updateProductDetails = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($updateProductDetails)
                    {
                         // header('Location: ../allProducts.php');
                         if(registerNewVariation($conn,$variationUid,$mainProductUid,$name,$price,$imageOne,$status,$amount,$category,$rating))
                         {
                              $_SESSION['product_uid'] = $mainProductUid;
                              header('Location: ../addProductVariation2.php');
                              // echo "success";
                         }
                         else
                         {
                              echo "fail";
                         }
                    }
                    else
                    {
                        echo "FAIL !!";
                    }
               }

               // $tableName = array();
               // $tableValue =  array();
               // $stringType =  "";
           
               // //echo "save to database";
               // if($minPrice)
               // {
               //     array_push($tableName,"minPrice");
               //     array_push($tableValue,$minPrice);
               //     $stringType .=  "d";
               // }
               // if($midPrice)
               // {
               //     array_push($tableName,"midPrice");
               //     array_push($tableValue,$midPrice);
               //     $stringType .=  "d";
               // }   
               // if($maxPrice)
               // {
               //     array_push($tableName,"maxPrice");
               //     array_push($tableValue,$maxPrice);
               //     $stringType .=  "d";
               // }    
               // array_push($tableValue,$mainProductUid);
               // $stringType .=  "s";
               // $updateProductDetails = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               // if($updateProductDetails)
               // {
               //      // header('Location: ../allProducts.php');
               //      if(registerNewVariation($conn,$variationUid,$mainProductUid,$name,$price,$imageOne,$status,$amount,$category,$rating))
               //      {
               //           $_SESSION['product_uid'] = $mainProductUid;
               //           header('Location: ../addProductVariation2.php');
               //           // echo "success";
               //      }
               //      else
               //      {
               //           echo "fail";
               //      }
               // }
               // else
               // {
               //     echo "FAIL !!";
               // }

          }
          else
          {
              echo "FAIL !!";
          }

     }
     else
     {
          echo "product name already register !!";
     }
}
else
{
     header('Location: ../index.php');
}
?>