<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Variation.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Order Details | Mypetslibrary" />
<title>Order Details | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
        <h1 class="green-text h1-title">Order Details</h1>
        <div class="green-border"></div>
   </div>

   <div class="border-separation">
        <div class="clear"></div>

        <?php
        if(isset($_GET['id']))
        {
            $conn = connDB();
            $orderProductArray = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_GET['id']),"s");
            $orderDetails = getOrders($conn,"WHERE id = ? ", array("id") ,array($_GET['id']),"s");
            if($orderProductArray)
            {
            ?>
                <table class="green-table width100">
                        <?php
                            if($orderProductArray)
                            {
                                for($cnt = 0;$cnt < count($orderProductArray) ;$cnt++)
                                {
                                ?>    
                                    <tr>
    
                                        <td> 
                                            
                                            <div class="left-product-check">
                                                <label for="product1" class="filter-label filter-label3">
                                                    <div class="left-cart-img-div">
                                                        <!-- <img src="img/product-2.jpg" class="width100" alt="Product Name" title="Product Name"> -->

                                                        <?php 
                                                            $conn = connDB();
                                                            $productName = $orderProductArray[$cnt]->getProductName();

                                                            $productRows = getVariation($conn,"WHERE variation = ? ", array("variation") ,array($productName),"s");
                                                            $productImage = $productRows[0]->getVariationImage();
                                                        ?> 

                                                        <img src="uploads/<?php echo $productImage;?>" class="width100" alt="<?php echo $orderProductArray[$cnt]->getProductName();?>" title="<?php echo $orderProductArray[$cnt]->getProductName();?>">

                                                    </div>
                                                </label>
                                                    <div class="left-product-details">
                                                        <p class="text-overflow width100 green-text cart-product-title">
                                                            <?php echo $orderProductArray[$cnt]->getProductName();?> 
                                                            (RM <?php echo $orderProductArray[$cnt]->getOriginalPrice();?> / unit)
                                                        </p>

                                                        <p class="left-quantity1">
                                                            Quantity
                                                        </p>  
                                                        <p class="right-rm">
                                                            <?php echo $orderProductArray[$cnt]->getQuantity();?>
                                                        </p>
                                                        
                                                        <div class="clear"></div> 

                                                        <div class="cart-border"></div>

                                                        <p class="left-total">
                                                            Total (Exclude Shipping)
                                                        </p> 
                                                        <p class="right-rm2">
                                                            RM<?php echo $orderProductArray[$cnt]->getTotalProductPrice();?>
                                                        </p>                                                          
                                                    </div>
                                            </div>

                                        </td>  

                                    </tr>
                                <?php
                                }
                            }
                        ?>    
                </table>

                <div class="sticky-bottom-price same-padding3">
                    <div class="grey-border"></div>
                    <p class="left-bottom-price weight900">Grand Total</p>
                    <p class="right-bottom-price weight900">RM<?php echo $orderDetails[0]->getTotal();?></p>   
                    <div class="clear"></div>  
                    <div class="grey-border"></div>
                    <p class="left-bottom-price weight900">Order Date</p>
                    <p class="right-bottom-price weight900"><?php echo $orderDetails[0]->getDateCreated();?></p>   
                </div>
            <?php
            }
        }
        ?>
	</div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>