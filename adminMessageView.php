<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $userSMSDetails = getMessage($conn,"WHERE uid = ? ", array("uid") ,array($_POST['message_uid']),"s");

// $lastSMS = getMessage($conn,"WHERE uid = ? ORDER BY date_created DESC LIMIT 1", array("uid") ,array($_POST['message_uid']),"s");
// $senderUID = $lastSMS[0]->getUid();
// $receiveSMS = $lastSMS[0]->getReceiveSMS();
// $replySMS = $lastSMS[0]->getReplySMS();

$_SESSION['url'] = $_SERVER['REQUEST_URI'];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Message | Mypetslibrary" />
<title>Message | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance" id="myTable">

    <h2 class="h1-title left-title">Chat</h2> 
	
    <div class="clear"></div>

	<div class="chat-section">
	
        <?php 
        // Program to display URL of current page. 
        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
        $link = "https"; 
        else
        $link = "http"; 

        // Here append the common URL characters. 
        $link .= "://"; 

        // Append the host(domain name, ip) to the URL. 
        $link .= $_SERVER['HTTP_HOST']; 

        // Append the requested resource location to the URL 
        $link .= $_SERVER['REQUEST_URI']; 

        if(isset($_GET['id']))
        {
            $referrerUidLink = $_GET['id'];
            // echo $referrerUidLink;
        }
        else 
        {
            $referrerUidLink = "";
            // echo $referrerUidLink;
        }
        ?>

        <?php
        if(isset($_GET['id']))
        {
            $conn = connDB();
            // $puppiesDetails = getPuppy($conn,"WHERE uid = ? ", array("uid") ,array($_GET['id']),"s");

            $userSMSDetails = getMessage($conn,"WHERE uid = ? ", array("uid") ,array($_GET['id']),"s");

            $lastSMS = getMessage($conn,"WHERE uid = ? ORDER BY date_created DESC LIMIT 1", array("uid") ,array($_GET['id']),"s");
            $senderUID = $lastSMS[0]->getUid();
            $receiveSMS = $lastSMS[0]->getReceiveSMS();
            $replySMS = $lastSMS[0]->getReplySMS();
            // $puppiesDetails = $puppiesUid[0];
            if($userSMSDetails)
            {   
                for($cnt = 0;$cnt < count($userSMSDetails) ;$cnt++)
                {
                ?>
                        <div class="chat-bubble-div">

                        <?php 
                            $productQnA = $userSMSDetails[$cnt]->getReplyThree();
                            if($productQnA != '')
                            {
                            ?>
                                <p class="username-date text-center"><b>About : <?php echo $userSMSDetails[$cnt]->getReplyThree();?></b></p>
                            <?php
                            }
                        ?>
                            <p class="chat-date"><?php echo $userSMSDetails[$cnt]->getDateCreated();?></p>
                            <p class="chat-bubble reply-message">
                                <?php echo $userSMSDetails[$cnt]->getReceiveSMS();?>
                            </p>

                            <p class="chat-bubble user-chat">
                                <?php echo $userSMSDetails[$cnt]->getReplySMS();?>
                            </p>
                
                        </div>
                <?php
                }
                ?>
</div>
                <!-- <form action="utilities/replyMessageFunction.php" method="POST">
                    <input class="clean de-input left-msg" type="text" placeholder="Message" id="message_details" name="message_details" required>
                    <input type="hidden" value="<?php //echo $_POST['message_uid'];?>" id="sender_uid" name="sender_uid" readonly>
                    <input type="hidden" value="<?php //echo $lastSMS[0]->getMessageUid();?>" id="message_uid" name="message_uid" readonly>
                    <button class="clean" type="submit" name="reply_sms">
                        REPLY
                    </button>
                </form> -->
    <!-- </div> -->
                <?php
                if($receiveSMS != '' && $replySMS != '')
                {
                ?>
                <div class="width100 bottom-send-div">
                    <form action="utilities/replyNewMessageFunction.php" method="POST">
                        <input class="send-message clean" type="text" placeholder="Message" id="message_details" name="message_details" required>
                        <input type="hidden" value="<?php echo $senderUID;?>" id="sender_uid" name="sender_uid" readonly>
                        <input type="hidden" value="<?php echo $lastSMS[0]->getMessageUid();?>" id="message_uid" name="message_uid" readonly>
                        <button class="clean send-button" type="submit" name="reply_sms">
                            REPLY
                        </button>
                    </form>
                    </div>
                <?php
                }
                elseif($replySMS != '')
                {
                ?>
                <div class="width100 bottom-send-div">
                    <form action="utilities/replyNewMessageFunction.php" method="POST">
                        <input class="send-message clean" type="text" placeholder="Message" id="message_details" name="message_details" required>
                        <input type="hidden" value="<?php echo $senderUID;?>" id="sender_uid" name="sender_uid" readonly>
                        <input type="hidden" value="<?php echo $lastSMS[0]->getMessageUid();?>" id="message_uid" name="message_uid" readonly>
                        <button class="clean send-button" type="submit" name="reply_sms">
                            REPLY
                        </button>
                    </form>
                </div>
                <?php
                }
                else
                {
                ?>
                <div class="width100 bottom-send-div">
                    <form action="utilities/replyMessageFunction.php" method="POST">
                        <input class="send-message clean" type="text" placeholder="Message" id="message_details" name="message_details" required>
                        <input type="hidden" value="<?php echo $_POST['message_uid'];?>" id="sender_uid" name="sender_uid" readonly>
                        <input type="hidden" value="<?php echo $lastSMS[0]->getMessageUid();?>" id="message_uid" name="message_uid" readonly>
                        <button class="clean send-button" type="submit" name="reply_sms">
                            REPLY
                        </button>
                    </form>
                </div>
                <?php
                }
                ?>

            <?php
            }
        }
        ?>

    
  

    <div class="clear"></div>
    <!--<div class="width100 bottom-spacing"></div>
-->
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
$('.chat-section').scrollTop($('.chat-section')[0].scrollHeight);	
</script>	

</body>
</html>