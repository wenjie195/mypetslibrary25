<?php
require_once dirname(__FILE__) . '/sellerAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Puppy.php';
require_once dirname(__FILE__) . '/classes/Color.php';
require_once dirname(__FILE__) . '/classes/Breed.php';
require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$sellerDetails = getSeller($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$sellerData = $sellerDetails[0];

$colorDetails = getColor($conn," WHERE type = ? ",array("type"),array(1),"s");
$breedDetails = getBreed($conn," WHERE type = ? ",array("type"),array(1),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add a Puppy | Mypetslibrary" />
<title>Add a Puppy | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<!-- <?php //include 'userHeaderAfterLogin.php'; ?>
<?php //include 'header.php'; ?> -->

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Add a Puppy</h1>
            <div class="green-border"></div>
   </div>
   <div class="border-separation">
        <div class="clear"></div>
        <form action="utilities/registerPuppyFunction.php" method="POST" enctype="multipart/form-data">
         <div class="dual-input">
        	<p class="input-top-p admin-top-p">Pet Color* <a href="puppyColor.php" class="green-a">(Add New Color Here)</a></p>
            <!-- <select class="input-name clean admin-input" required value="<?php //echo $colorDetails[0]->getName();?>" name="register_color" id="register_color"> -->
            <select class="clean input-name admin-input" required value="<?php echo $colorDetails[0]->getName();?>" name="register_color" id="register_color">
            	<!-- <option>White</option>
                <option>Black</option> -->
                <option value="">Please Select a Color</option>
                <?php
                for ($cntPro=0; $cntPro <count($colorDetails) ; $cntPro++)
                {
                ?>
                    <option value="<?php echo $colorDetails[$cntPro]->getName(); ?>"> 
                        <?php echo $colorDetails[$cntPro]->getName(); ?>
                    </option>
                <?php
                }
                ?>
            </select>       
        </div>        
    
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Pet Breed* <a href="puppyBreed.php" class="green-a">(Add New Breed Here)</a></p>
        	<select class="clean input-name admin-input" value="<?php echo $breedDetails[0]->getName();?>" name="register_breed" id="register_breed" required>
                <option value="">Please Select a Breed</option>
                <?php
                for ($cntPro=0; $cntPro <count($breedDetails) ; $cntPro++)
                {
                ?>
                    <option value="<?php echo $breedDetails[$cntPro]->getName(); ?>"> 
                        <?php echo $breedDetails[$cntPro]->getName(); ?>
                    </option>
                <?php
                }
                ?>
            </select>       
        </div>        
        
        
        
        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Pet Name*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Pet Name" name="register_name" id="register_name" required>      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">SKU*</p>
            <!-- <input class="input-name clean input-textarea admin-input" type="text" placeholder="SKU" name="register_sku" id="register_sku" required>  -->
            <input class="input-name clean input-textarea admin-input" type="text" placeholder="Auto Generate" name="register_sku" id="register_sku" readonly> 
        </div>        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p slug-p">Pet Slug (or URL, can't repeat,  Avoid Spacing and Symbol Specially"',.) Can Use - <img src="img/attention2.png" class="attention-png opacity-hover open-url" alt="Click Me!" title="Click Me!"></p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="puppy-name-on-sale"  name="register_slug" id="register_slug" required>      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p slug-p">Pet Price (RM)*</p>
        	<input class="input-name clean input-textarea admin-input" type="number" placeholder="Pet Price (RM)"  name="register_price" id="register_price" required>    
        </div> 
        <div class="clear"></div>  
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Pet Age*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Pet Age"  name="register_age" id="register_age" required>      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Vaccinated Status*</p>
        	<!-- <select class="input-name clean admin-input" required name="register_vaccinated" id="register_vaccinated">
            	<option>Yes</option>
                <option>No</option>
            </select>    -->

            <select class="clean input-name admin-input" type="text" name="register_vaccinated" id="register_vaccinated" required>
                <option value="" name=" ">Select a status</option>
                <option value="Yes" name="Yes">Yes</option>
                <option value="No" name="No">No</option>
            </select> 

        </div>           
		<div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Dewormed Status*</p>
        	<!-- <select class="input-name clean admin-input" required name="register_dewormed" id="register_dewormed">
            	<option>Yes</option>
                <option>No</option>
            </select>        -->

            <select class="clean input-name admin-input" type="text" name="register_dewormed" id="register_dewormed" required>
                <option value="" name=" ">Select a status</option>
                <option value="Yes" name="Yes">Yes</option>
                <option value="No" name="No">No</option>
            </select> 

        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Gender*</p>
        	<!-- <select class="input-name clean admin-input" required name="register_gender" id="register_gender">
            	<option>Male</option>
                <option>Female</option>
            </select>    -->

            <select class="clean input-name admin-input" type="text" name="register_gender" id="register_gender" required>
                <option value="" name=" ">Select a Gender</option>
                <option value="Male" name="Male">Male</option>
                <option value="Female" name="Female">Female</option>
            </select> 

        </div>  
        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Feature</p>
            <select class="clean input-name admin-input" type="text" name="register_feature" id="register_feature" required>
                <option value="" name=" ">Select a Status</option>
                <option value="Yes" name="Yes">Yes</option>
                <option value="No" name="No">No</option>
            </select>
        </div>          

        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Pet Size</p>
        	<!-- <select class="input-name clean admin-input" required name="register_size" id="register_size">
            	<option>Small</option>
                <option>Middle</option>
                <option>Big</option>
            </select>    -->

            <select class="clean input-name admin-input" type="text" name="register_size" id="register_size" required>
                <option value="" name=" ">Select a Size</option>
                <option value="Small" name="Small">Small</option>
                <option value="Medium" name="Medium">Medium</option>
                <option value="Large" name="Large">Large</option>
            </select> 


        </div>  

        <!-- <div class="clear"></div>

        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Status*</p>
            <select class="clean input-name" type="text" name="register_status" id="register_status" required>
                <option value="" name=" ">Select a Status</option>
                <option value="Available" name="Available">Available</option>
                <option value="Sold" name="Sold">Sold</option>
            </select>
        </div> -->

        <input class="input-name clean input-textarea admin-input" type="hidden" value="Pending"  name="register_status" id="register_status" readonly>     

          



        <!-- <div class="dual-input second-dual-input"> -->
        <!-- <div class="dual-input">
            <p class="input-top-p admin-top-p">Seller*</p> 
            <input class="input-name clean input-textarea admin-input" type="text"   name="register_seller" id="register_seller" readonly>   
        </div>    -->

        <!-- <input class="input-name clean" type="hidden" value="<?php //echo $sellerData->getCompanyName();?>"  name="register_seller" id="register_seller" readonly>  -->
        <input class="input-name clean" type="hidden" value="<?php echo $sellerData->getUid();?>"  name="register_seller" id="register_seller" readonly> 
        <input class="input-name clean" type="hidden" value="<?php echo $sellerData->getState();?>"  name="seller_location" id="seller_location" readonly> 
             
        <div class="clear"></div>

        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Details (Avoid "'')</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Details" name="register_details" id="register_details">           
        </div>
        <div class="clear"></div>
        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Google Search Keyword  (Use Coma , to Separate Each Keyword, Avoid"')</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="dog, pet, cute, for sale, Husky, Penang," name="register_keyword" id="register_keyword" required>
        </div>         
        <div class="clear"></div>
        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Video Link (Avoid"", Only Accept Vimeo, Youtube and FB Link, Please Click the Icon)* <img src="img/attention3.png" class="attention-png opacity-hover open-vimeo" alt="Click Me!" title="Click Me!"></p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="https://player.vimeo.com/video/383039356" name="register_link" id="register_link" required>           
        </div>

        <div class="clear"></div> 
        
        <!-- Photo cropping into square size feature -->
		<!-- <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Upload Pet Photo X4*</p>
            <p><input id="file-upload" type="file" name="image_one" id="image_one" accept="image/*" /></p> 
            
            <p><input id="file-upload" type="file" name="image_two" id="image_two" accept="image/*" /></p> 
            
            <p><input id="file-upload" type="file" name="image_three" id="image_three" accept="image/*" /></p>  
            
            <p><input id="file-upload" type="file" name="image_four" id="image_four" accept="image/*" /></p>  
        </div> -->
    	
        <div class="clear"></div>  
        <div class="width100 overflow text-center">     
            <!-- <button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button> -->
            <button class="green-button white-text clean2 edit-1-btn margin-auto">Next</button>
        </div>
        </form>
	</div>
</div>
<div class="clear"></div>

<?php include 'js.php'; ?>

<?php

if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully registered new Puppy!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to register for Pets !";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Fail to register for Puppy !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Puppy name or SKU has been used !! <br> Please enter a new name or SKU !";
        } 
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>