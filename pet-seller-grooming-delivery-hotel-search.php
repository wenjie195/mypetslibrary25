<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/Services.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
  $sellerName = rewrite($_POST['seller_name']);
  $sellerDetails = getSeller($conn," WHERE company_name = ? ",array("company_name"),array($sellerName),"s");
  // $sellerDetails = getSeller($conn," WHERE company_name = ? AND account_status = Active ",array("company_name"),array($sellerName),"s");
}

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pet Seller and Other Services | Mypetslibrary" />
<title>Pet Seller and Other Services | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="pet seller, partner, Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<script src="js/jquery-2.2.0.min.js"></script>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 small-padding3 overflow menu-distance1">
  <h1 class="green-text user-title left-align-title opacity-hover" onclick="close_window();return false;">
    <img src="img/back2.png" class="back-png2">Search Result
  </h1>
</div>

<div class="clear"></div>

<div class="width100  small-padding3">
  <form action="pet-seller-grooming-delivery-hotel-search.php" method="post">
    <input class="line-input filter-search-ow clean" type="text" name="seller_name" id="seller_name" placeholder="Search"/>
    <button class="search-btn hover1 clean ow-margin-top0" type="submit">
      <img src="img/search.png" class="visible-img hover1a" alt="Search" title="Search">
      <img src="img/search2.png" class="visible-img hover1b" alt="Search" title="Search">
    </button>
  </form>
</div>

<div class="width103 small-padding3 min-height" id="app">   
  <?php
  $conn = connDB();
  if($sellerDetails)
  {
    for($cnt = 0;$cnt < count($sellerDetails) ;$cnt++)
    {
    ?>
      <a href='petSellerDetails.php?id=<?php echo $sellerDetails[$cnt]->getCompanyName();?>'>
        <div class="shadow-white-box four-box-size opacity-hover pointer pointer-div">
          <div class="square">
            <div class="width100 white-bg content progressive">
              <img  src="img/pet-load300.jpg" data-src="sellerProfile/<?php echo $sellerDetails[$cnt]->getCompanyLogo();?>" alt="<?php echo $sellerDetails[$cnt]->getCompanyName();?>" title="<?php echo $sellerDetails[$cnt]->getCompanyName();?>" class="preview width100 two-border-radius opacity-hover pointer lazy">
            </div>
          </div>
          <p align="center" class="width100 text-overflow slider-product-name">
            <?php echo $sellerDetails[$cnt]->getCompanyName();?>
          </p>
          <p align="center" class="width100 text-overflow slider-product-name">
            <?php 
            if ($sellerDetails[$cnt]->getServices()) 
            {
              $petriRateExp = explode(",",$sellerDetails[$cnt]->getServices());
              for ($i=0; $i <count($petriRateExp) ; $i++)
              {
                $petriSelectionDet = getServices($conn," WHERE id =?",array("id"),array($petriRateExp[$i]), "s");
                if ($petriSelectionDet) 
                {
                ?>
                  <?php echo $petriSelectionDet[0]->getServiceType();?>,
                <?php
                }
              }
            }
            ?>
          </p>
        </div>
      </a>
    <?php
    }
    ?>
  <?php
  }
  else
  {
    echo "Seller no found. Please try again with another name.";
  }
  ?>
</div>
  

<div class="clear"></div>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>
<script type="text/javascript">

var totalId = <?php echo $total_row_count + 1 ?>;
// alert(totalId);
for(var i=0; i< totalId; i++){

	$("#"+i+"").click(function(){
					var x = $(this).attr('value');
					location.href = "./petSellerDetails.php?id="+x+"";
				});

}
</script>
  <script src="js/index2.js"></script>
  <script>
    (function(){
      new Progressive({
        el: '#app',
        lazyClass: 'lazy',
        removePreview: true,
        scale: true
      }).fire()
    })()
  </script>
  <script>
 function close_window() {

    close();

} 
  </script>
</body>
</html>
