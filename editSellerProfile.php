<?php
require_once dirname(__FILE__) . '/sellerAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/Services.php';
require_once dirname(__FILE__) . '/classes/States.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];
$sellerName = $userData->getName();

$seller = getSeller($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$sellerDetails = $seller[0];
$service = $sellerDetails->getServices();
$serviceExp = explode(",",$service);

$petriRate = getServices($conn);

$states = getStates($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Seller Profile | Mypetslibrary" />
<title>Edit Seller Profile | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<!-- <?php //include 'sellerHeader.php'; ?> -->
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Edit Profile</h1>
            <div class="green-border"></div>
    </div>
    <div class="border-separation">
        <div class="clear"></div>
        <!-- <form> -->
        <!-- <form method="POST" action="utilities/editSellerProfileFunction.php"> -->
        <form method="POST" action="utilities/editSellerProfileFunction.php" enctype="multipart/form-data">
        <div class="dual-input">
        	<p class="input-top-p admin-top-p slug-p">Company Name*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Company Name" value="<?php echo $sellerDetails->getCompanyName();?>" id="update_name" name="update_name" required>
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p slug-p">Company Slug* (For URL, Avoid Spacing and Symbol Specially"',.) Can Use -  <img src="img/attention2.png" class="attention-png opacity-hover open-url" alt="Click Me!" title="Click Me!"></p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="company-name" value="<?php echo $sellerDetails->getSlug();?>" id="update_slug" name="update_slug" required>
        </div>
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Company Registered No.</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Company Registered No." value="<?php echo $sellerDetails->getRegistrationNo();?>" id="update_regno" name="update_regno" required>
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Company Contact</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Company Contact" value="<?php echo $sellerDetails->getContactNo();?>" id="update_comp_contact" name="update_comp_contact" required>
        </div>
        <div class="clear"></div>
        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Company Address</p>
        	<textarea class="input-name clean input-textarea address-textarea admin-address-textarea" type="text" placeholder="Company Address" id="update_comp_add" name="update_comp_add" required><?php echo $sellerDetails->getAddress();?></textarea>
        </div>
        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p admin-top-p">Company State</p>
            <!-- <input class="input-name clean input-textarea admin-input" type="text" placeholder="Company Contact" value="<?php echo $sellerDetails->getState();?>" id="update_comp_state" name="update_comp_state" required>  -->

            <select class="input-name clean admin-input" type="text" name="update_comp_state" id="update_comp_state" required>
                <?php
                if($sellerDetails->getState() == '')
                {
                ?>
                    <option selected>Select State</option>
                    <?php
                    for ($cnt=0; $cnt <count($states) ; $cnt++)
                    {
                    ?>
                        <option value="<?php echo $states[$cnt]->getStateName(); ?>">
                            <?php echo $states[$cnt]->getStateName(); ?>
                        </option>
                    <?php
                    }
                }
                else
                {
                    for ($cnt=0; $cnt <count($states) ; $cnt++)
                    {
                        if ($sellerDetails->getState() == $states[$cnt]->getStateName())
                        {
                        ?>
                            <option selected value="<?php echo $states[$cnt]->getStateName(); ?>">
                                <?php echo $states[$cnt]->getStateName(); ?>
                            </option>
                        <?php
                        }
                        else
                        {
                        ?>
                            <option value="<?php echo $states[$cnt]->getStateName(); ?>">
                                <?php echo $states[$cnt]->getStateName(); ?>
                            </option>
                        <?php
                        }
                    }
                }
                ?>
            </select>

        </div>

        <div class="clear"></div>

        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Contact Person Name*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Contact Person Name" value="<?php echo $sellerDetails->getContactPerson();?>" id="update_pic" name="update_pic" required>
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Contact Person Contact No.*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Contact No." value="<?php echo $sellerDetails->getContactPersonNo();?>" id="update_pic_hp" name="update_pic_hp" required>
        </div>
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Years of Experience</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Years of Experience" value="<?php echo $sellerDetails->getExperience();?>" id="update_exp" name="update_exp">
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Cert</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Cert"  value="<?php echo $sellerDetails->getCert();?>" id="update_cert" name="update_cert" required>
        </div>

        <div class="clear"></div>

        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Services : <?php //echo $sellerDetails->getServices();?></p>

          <?php
          if ($petriRate) {
            for ($i=0; $i <count($petriRate) ; $i++) {
              if (in_array($petriRate[$i]->getId(),$serviceExp)) {
                ?>
                <div class="filter-option">
                    <label for="<?php echo $petriRate[$i]->getServiceType() ?>" class="filter-label filter-label2"><?php echo $petriRate[$i]->getServiceType() ?>
                        <input checked="checked" type="checkbox" name="petri[]" id="<?php echo $petriRate[$i]->getServiceType() ?>" value="<?php echo $petriRate[$i]->getId() ?>" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <?php
              }else {
                ?>
                <div class="filter-option">
                    <label for="<?php echo $petriRate[$i]->getServiceType() ?>" class="filter-label filter-label2"><?php echo $petriRate[$i]->getServiceType() ?>
                        <input type="checkbox" name="petri[]" id="<?php echo $petriRate[$i]->getServiceType() ?>" value="<?php echo $petriRate[$i]->getId() ?>" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <?php
              }

            }
          }
           ?>
                <!-- <div class="filter-option">
                    <label for="petHotel" class="filter-label filter-label2">Pet Hotel
                        <input type="checkbox" name="petHotel" id="petHotel" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="kittenSellers" class="filter-label filter-label2">Kitten Sellers
                        <input type="checkbox" name="kittenSellers" id="kittenSellers" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="vet" class="filter-label filter-label2">Vet
                        <input type="checkbox" name="vet" id="vet" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="reptileSellers" class="filter-label filter-label2">Reptile Sellers
                        <input type="checkbox" name="reptileSellers" id="reptileSellers" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="grooming" class="filter-label filter-label2">Grooming
                        <input type="checkbox" name="grooming" id="grooming" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="deliveryServices" class="filter-label filter-label2">Delivery Services
                        <input type="checkbox" name="deliveryServices" id="deliveryServices" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="restaurant" class="filter-label filter-label2">Restaurant
                        <input type="checkbox" name="restaurant" id="restaurant" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="others" class="filter-label filter-label2">Others
                        <input type="checkbox" name="others" id="others" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>                                        	 -->
        </div>

        <div class="clear"></div>

        <div class="width100 overflow padding-top30">
        	<p class="input-top-p admin-top-p">Type of Breed</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Type of Breed"  value="<?php echo $sellerDetails->getBreedType();?>" id="update_breed" name="update_breed">
        </div>
        <div class="clear"></div>
        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Other Info</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Other Info Like FB, Insta Link"  value="<?php echo $sellerDetails->getOtherInfo();?>" id="update_info" name="update_info">
        </div>

        <div class="clear"></div>

        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Email</p>
        	<input class="input-name clean input-textarea admin-input" type="email" placeholder="Email"  value="<?php echo $sellerDetails->getEmail();?>" name="update_email" id="update_email" required>
        </div>

        <div class="clear"></div>

        <input class="input-name clean input-textarea admin-input" type="hidden" value="<?php echo $sellerDetails->getUid();?>" id="company_uid" name="company_uid" readonly>

        <div class="clear"></div>

        <div class="width100 overflow text-center">
        	<button class="green-button white-text clean2 edit-1-btn margin-auto" name="submit">Submit</button>
        </div>

        </form>
	</div>

    <!-- <div class="border-separation">

        <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Company Logo</p>
            <div class="four-div-box1">
                <img src="uploads/<?php echo $sellerDetails->getCompanyLogo();?>" class="pet-photo-preview">
            </div>
            <div class="four-div-box1 left-four-div1">
                <img src="uploads/<?php echo $sellerDetails->getCompanyPic();?>" class="pet-photo-preview">
            </div>
        </div>

        <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Upload New Company Logo</p>
            <div class="four-div-box1">
            <p><a href="sellerCropCompanyLogo1.php" class="red-link">Upload New Logo 1</a></p>
            </div>
            <div class="four-div-box1 left-four-div1">
            <p><a href="sellerCropCompanyLogo2.php" class="red-link">Upload New Logo 2</a></p>
            </div>
        </div>

    </div> -->

    <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Seller Company Logo</p>
            <div class="four-div-box1">
                <img src="uploads/<?php echo $sellerDetails->getCompanyLogo();?>" class="pet-photo-preview">
            </div>
            <div class="four-div-box1 left-four-div1">

                <?php 
                if ($sellerDetails->getCompanyPic()) 
                {
                ?>
                    <img src="uploads/<?php echo $sellerDetails->getCompanyPic();?>" class="pet-photo-preview">
                <?php
                }
                else
                {
                ?>
                    <img src="img/ccv.png" class="pet-photo-preview">
                <?php
                }
                ?>
            </div>

            <div class="clear"></div>

            <div class="four-div-box1">
                <form method="POST" action="sellerCropCompanyLogo1.php" class="hover1">
                    <button class="clean green-button pointer width100" type="submit" name="seller_uid" value="<?php echo $sellerDetails->getUid();?>">
                    Update Logo
                    </button>
                </form>
            </div>  

            <div class="four-div-box1 left-four-div1">
                <form method="POST" action="sellerCropCompanyLogo2.php" class="hover1">
                    <button class="clean green-button pointer width100" type="submit" name="seller_uid" value="<?php echo $sellerDetails->getUid();?>">
                    Update Profile
                    </button>
                </form>          
                

                <?php 
                    $compPic = $sellerDetails->getCompanyPic();
                    if($compPic != '')
                    {
                    ?>
                        <button class="clean green-button pointer width100 ow-peach-bg-hover" id="deletePic" type="button" name="button" value="<?php echo $sellerDetails->getuid();?>">
                            Delete
                        </button> 
                    <?php
                    }
                    else
                    {}
                ?>
            
            </div>     
                     
        </div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>


<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Profile Successfully";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Fail to update profile on User !!";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "ERROR 1 !!";
        }
        if($_GET['type'] == 4)
        {
            $messageType = "Fail to update profile on Seller !!";
        }
        if($_GET['type'] == 5)
        {
            $messageType = "ERROR 2 !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>

<script>
  $("#deletePic").click(function(){
    var companyPic = $(this).val();
    // alert(companyPic);
    $.ajax({
      url: 'utilities/deleteCompanyPic.php',
      data: {pic:companyPic},
      type: 'post',
      dataType: 'json',
      success:function(response){
        var success = response[0]['picDelete'];
        alert(success);
        window.location.reload();
      },
      error:function(response){
        alert("Error Occur");
      }
    });
  });
</script>