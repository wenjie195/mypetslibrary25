<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Breed.php';
require_once dirname(__FILE__) . '/classes/Color.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Seller Service | Mypetslibrary" />
<title>Edit Seller Service | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
        <h1 class="green-text h1-title">Edit Seller Service</h1>
        <div class="green-border"></div>
   </div>

   <div class="border-separation">
        <div class="clear"></div>

        <form method="POST" action="utilities/editColorFunction.php">

            <?php
            if(isset($_POST['color_id']))
            {
                $conn = connDB();
                $colorDetails = getColor($conn,"WHERE id = ? ", array("id") ,array($_POST['color_id']),"i");
            ?>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Service*</p>
                    <input class="input-name clean input-textarea admin-input" placeholder="Service" type="text" value="<?php echo $colorDetails[0]->getName();?>" name="update_color_name" id="update_color_name" required>       
                </div>
                
                <div class="clear"></div>

                <input type="hidden" value="<?php echo $colorDetails[0]->getId();?>" name="color_id" id="color_id" required>   
                <input type="hidden" value="<?php echo $colorDetails[0]->getType();?>" name="color_type" id="color_type">

            <?php
            }
            ?>
    	
            <div class="clear"></div>  

            <div class="width100 overflow text-center">     
                <button class="green-button white-text clean2 edit-1-btn margin-auto" type="submit" id ="submit" name ="submit">Submit</button>
            </div>

        </form>

	</div>
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>