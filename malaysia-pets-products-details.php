<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/fblogin.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Favorite.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Variation.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
$_SESSION['url'] = $_SERVER['REQUEST_URI'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$products = getProduct($conn, "WHERE status = 'Available' ORDER BY date_created DESC ");

$favoriteItem = getFavorite($conn, "WHERE uid = ? AND link =? AND status = 'Yes' ",array("uid,link"),array($uid, $_SESSION['url']),"ss");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<?php 
// Program to display URL of current page. 
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
$link = "https"; 
else
$link = "http"; 

// Here append the common URL characters. 
$link .= "://"; 

// Append the host(domain name, ip) to the URL. 
$link .= $_SERVER['HTTP_HOST']; 

// Append the requested resource location to the URL 
$link .= $_SERVER['REQUEST_URI']; 

if(isset($_GET['id']))
{
    $referrerUidLink = $_GET['id'];
    // echo $referrerUidLink;
}
else 
{
    $referrerUidLink = "";
    // echo $referrerUidLink;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>

<?php
if(isset($_GET['id']))
{
$conn = connDB();
// echo $_GET['id'];
$productDetails = getProduct($conn,"WHERE uid = ? AND status = 'Available' ", array("uid") ,array($_GET['id']),"s");
// $productVariation = getVariation($conn,"WHERE product_uid = ? AND status = 'Available' ", array("product_uid") ,array($_GET['id']),"s");
?>
    <?php
    if($productDetails)
    {
        for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
        {
        ?>
            <meta property="og:image" content="https://qlianmeng.asia/mypetslibrary/productImage/<?php echo $productDetails[$cnt]->getImageOne();?>" />        
            <?php include 'meta.php'; ?>
            <meta property="og:url" content="https://qlianmeng.asia/mypetslibrary/malaysia-pets-products-details.php?id=<?php echo $productDetails[$cnt]->getUid();?>" />
            <link rel="canonical" href="https://qlianmeng.asia/mypetslibrary/malaysia-pets-products-details.php?id=<?php echo $productDetails[$cnt]->getUid();?>" />
            <meta property="og:title" content="<?php echo $productDetails[$cnt]->getName();?> | Mypetslibrary" />
            <title><?php echo $productDetails[$cnt]->getName();?> | Mypetslibrary</title>
            <meta property="og:description" content="<?php echo $productDetails[$cnt]->getName();?> for sale. Mypetslibrary online pet store in Malaysia." />
            <meta name="description" content="<?php echo $productDetails[$cnt]->getName();?> for sale. Mypetslibrary online pet store in Malaysia." />
            <meta name="keywords" content="<?php echo $productDetails[$cnt]->getName();?> ,<?php echo $productDetails[$cnt]->getKeywordOne();?>, Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, malaysia, online pet store,马来西亚,上网买宠物, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
            <meta property="og:image" content="https://qlianmeng.asia/mypetslibrary/productImage/<?php echo $productDetails[$cnt]->getImageOne();?>" />
        <?php
        }
        ?>
    <?php
    }
    ?>
<?php
}
?>

<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>
                    
<div class="width100 menu-distance3 same-padding ow-pet-details-div product-details-ow" style="min-height: calc(100vh - 129px);">

    <?php
    if(isset($_GET['id']))
    {
    $conn = connDB();
    // echo $_GET['id'];
    $productDetails = getProduct($conn,"WHERE uid = ? AND status = 'Available' ", array("uid") ,array($_GET['id']),"s");
    $productVariation = getVariation($conn,"WHERE product_uid = ? AND status = 'Available' ", array("product_uid") ,array($_GET['id']),"s");
    ?>

        <div class="left-image-div">
            <div class="item">            
                <div class="clearfix">
                    <ul id="image-gallery" class="gallery list-unstyled cS-hidden pet-ul">

                    <?php
                    if($productDetails)
                    {
                        for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
                        {
                        ?>

                            <?php 
                                $productLink = $productDetails[$cnt]->getLink();
                                if($productLink != '')
                                {
                                ?>
                                    <li data-thumb="img/video.jpg" class="pet-slider-li"> 
                                        <iframe src="https://player.vimeo.com/video/<?php echo $productDetails[$cnt]->getLink();?>" class="video-iframe" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                    </li>
                                <?php
                                }
                            ?>

                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>

                    <?php
                    if($productVariation)
                    {
                        for($cnt = 0;$cnt < count($productVariation) ;$cnt++)
                        {
                        ?>
                            <?php $imgName = $productVariation[$cnt]->getImage();?>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>

                    <?php
                    if($productDetails)
                    {
                        for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
                        {
                        ?>
						
                            <?php $imgOneName = $productDetails[$cnt]->getImageOne();?>

                            <?php 
                                $imgOne = $productDetails[$cnt]->getImageOne();
                                if($imgOne != '')
                                {
                                    if($imgOneName == $imgName)
                                    {}
                                    else
                                    {
                                    ?>
                                        <li data-thumb="productImage/<?php echo $productDetails[$cnt]->getImageOne();?>" class="pet-slider-li"> 
                                            <img src="productImage/<?php echo $productDetails[$cnt]->getImageOne();?>" class="pet-slider-img" alt="<?php echo $productDetails[$cnt]->getName();?>"/>
                                        </li>
                                    <?php
                                    }
                                }
                            ?>

                            <?php 
                                $imgTwo = $productDetails[$cnt]->getImageTwo();
                                if($imgTwo != '')
                                {
                                ?>
                                    <li data-thumb="productImage/<?php echo $productDetails[$cnt]->getImageTwo();?>" class="pet-slider-li"> 
                                        <img src="productImage/<?php echo $productDetails[$cnt]->getImageTwo();?>" class="pet-slider-img" alt="<?php echo $productDetails[$cnt]->getName();?>"/>
                                    </li>
                                <?php
                                }
                            ?>
                            
                            <?php 
                                $imgThree = $productDetails[$cnt]->getImageThree();
                                if($imgThree != '')
                                {
                                ?>
                                    <li data-thumb="productImage/<?php echo $productDetails[$cnt]->getImageThree();?>" class="pet-slider-li"> 
                                        <img src="productImage/<?php echo $productDetails[$cnt]->getImageThree();?>" class="pet-slider-img" alt="<?php echo $productDetails[$cnt]->getName();?>"/>
                                    </li>
                                <?php
                                }
                            ?>

                            <?php 
                                $imgFour = $productDetails[$cnt]->getImageFour();
                                if($imgFour != '')
                                {
                                ?>
                                    <li data-thumb="productImage/<?php echo $productDetails[$cnt]->getImageFour();?>" class="pet-slider-li"> 
                                        <img src="productImage/<?php echo $productDetails[$cnt]->getImageFour();?>" class="pet-slider-img" alt="<?php echo $productDetails[$cnt]->getName();?>"/>
                                    </li>
                                <?php
                                }
                            ?>

                            <?php 
                                $imgFive = $productDetails[$cnt]->getImageFive();
                                if($imgFive != '')
                                {
                                ?>
                                    <li data-thumb="productImage/<?php echo $productDetails[$cnt]->getImageFive();?>" class="pet-slider-li"> 
                                        <img src="productImage/<?php echo $productDetails[$cnt]->getImageFive();?>" class="pet-slider-img" alt="<?php echo $productDetails[$cnt]->getName();?>"/>
                                    </li>
                                <?php
                                }
                            ?>

                            <?php 
                                $imgSix = $productDetails[$cnt]->getImageSix();
                                if($imgSix != '')
                                {
                                ?>
                                    <li data-thumb="productImage/<?php echo $productDetails[$cnt]->getImageSix();?>" class="pet-slider-li"> 
                                        <img src="productImage/<?php echo $productDetails[$cnt]->getImageSix();?>" class="pet-slider-img" alt="<?php echo $productDetails[$cnt]->getName();?>"/>
                                    </li>
                                <?php
                                }
                            ?>



                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>

                    <?php
                    if($productVariation)
                    {
                        for($cnt = 0;$cnt < count($productVariation) ;$cnt++)
                        {
                        ?>
                            <li data-thumb="productImage/<?php echo $productVariation[$cnt]->getImage();?>" class="pet-slider-li"> 
                                <img src="productImage/<?php echo $productVariation[$cnt]->getImage();?>" class="pet-slider-img" alt="<?php echo $productVariation[$cnt]->getName();?>"/>
                            </li>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>

                    </ul>
                </div>
            </div>
        </div>

    <?php
    }
    ?>

    <div class="right-content-div2">
        <?php
        if(isset($_GET['id']))
        {
        $conn = connDB();
        // echo $_GET['id'];
        $productDetails = getProduct($conn,"WHERE uid = ? AND status = 'Available' ", array("uid") ,array($_GET['id']),"s");
        ?>
            <?php
            if($productDetails)
            {
                for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
                {
                ?>
                        <h1 class="green-text pet-name"><?php echo $productDetails[$cnt]->getName();?></h1>

                        <p class="rating-img-p ow-raiting-p">
                            <!-- <img src="img/yellow-star.png" alt="Rating" title="Rating" class="rating-star2"> -->

                            <?php 
                                $display = $productDetails[$cnt]->getRating();
                                // if ($display == 0)
                                // {
                                //     echo '-';
                                // }
                                // elseif ($display > 0)
                                // {
                                //     echo $display ;
                                //     echo " / 5 " ;
                                // }
                            ?>

                            <?php
                                if ($display == 1)
                                {
                                ?>
                                    <a href="productReview.php?id=<?php echo $productDetails[$cnt]->getUid();?>">
                                        <img src="img/yellow-star.png" class="star-img">
                                    </a>
                                <?php
                                }
                                elseif($display == 2)
                                {
                                ?>
                                    <a href="productReview.php?id=<?php echo $productDetails[$cnt]->getUid();?>">
                                        <img src="img/yellow-star.png" class="star-img"><img src="img/yellow-star.png" class="star-img">
                                    </a>
                                <?php
                                }
                                elseif($display == 3)
                                {
                                ?>
                                    <a href="productReview.php?id=<?php echo $productDetails[$cnt]->getUid();?>">
                                        <img src="img/yellow-star.png" class="star-img"><img src="img/yellow-star.png" class="star-img"><img src="img/yellow-star.png" class="star-img">
                                    </a>
                                <?php
                                }
                                elseif($display == 4)
                                {
                                ?>               
                                    <a href="productReview.php?id=<?php echo $productDetails[$cnt]->getUid();?>">
                                        <img src="img/yellow-star.png" class="star-img"><img src="img/yellow-star.png" class="star-img"><img src="img/yellow-star.png" class="star-img"><img src="img/yellow-star.png" class="star-img">
                                    </a>
                                <?php
                                }
                                elseif($display == 5)
                                {
                                ?>               
                                    <a href="productReview.php?id=<?php echo $productDetails[$cnt]->getUid();?>">
                                        <img src="img/yellow-star.png" class="star-img"><img src="img/yellow-star.png" class="star-img"><img src="img/yellow-star.png" class="star-img"><img src="img/yellow-star.png" class="star-img"><img src="img/yellow-star.png" class="star-img">
                                    </a>
                                <?php
                                }
                                elseif($display == 0)
                                { 
                                    echo 'No Rating Yet';  
                                }
                            ?> 

                        </p>

                        <div class="right-info-div fav-div ow-fav-div">
                            <?php
                            if($uid != "")
                            {
                            ?>

                                <?php 
                                    if(!$favoriteItem)
                                    {
                                    ?>
                                        <form action="utilities/addFavoriteFunction.php" method="POST" class="contact-icon"> 
                                                <input type="hidden" value="<?php echo $_SESSION['url'] ;?>" name="link" id="link">   
                                                <input type="hidden" value="Product" name="type" id="type">  
                                                <input type="hidden" value="<?php echo $referrerUidLink ;?>" name="item_uid" id="item_uid">  
                                                <button class="transparent-button clean "><img src="img/heart.png" class="contact-icon" alt="Favourite" title="Favourite"></button>
                                        </form>
                                            <a class="contact-icon hover1 last-contact-icon open-social">
                                                <img src="img/share.png" class="hover1a" alt="Share" title="Share">
                                                <img src="img/share2.png" class="hover1b" alt="Share" title="Share">
                                            </a>
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                        <form action="utilities/removeFavoriteFunction.php" method="POST" class="contact-icon"> 
                                                <input type="hidden" value="<?php echo $referrerUidLink ;?>" name="item_uid" id="item_uid">  
                                                <input type="hidden" value="<?php echo $uid;?>" name="user_uid" id="user_uid"> 
                                                <button class="transparent-button clean"><img src="img/heart2.png" class="contact-icon" alt="Favourite" title="Favourite"></button>
                                        </form>
                                        <a class="contact-icon hover1 last-contact-icon open-social">
                                            <img src="img/share.png" class="hover1a" alt="Share" title="Share">
                                            <img src="img/share2.png" class="hover1b" alt="Share" title="Share">
                                        </a>
                                    <?php
                                    }
                                ?>

                            <?php
                            }
                            else
                            {
                            ?>
                               <!-- <a href='userLogin.php' class="contact-icon">-->
                                        <input type="hidden" value="<?php echo $referrerUidLink ;?>" name="item_uid" id="item_uid">  
                                        <input type="hidden" value="<?php echo $uid;?>" name="user_uid" id="user_uid"> 
                                        <button class="transparent-button contact-icon clean open-login"><img src="img/heart.png" class="contact-icon" alt="Favourite" title="Favourite"></button>
                               <!-- </a>-->
                                <a class="contact-icon hover1 last-contact-icon open-social">
                                    <img src="img/share.png" class="hover1a" alt="Share" title="Share">
                                    <img src="img/share2.png" class="hover1b" alt="Share" title="Share">
                                </a>
                            <?php
                            }
                            ?>
                        </div><div class="clear"></div>
                        <?php
                        if(isset($_GET['id']))
                        {
                        $conn = connDB();
                        $variationLP = getVariation($conn,"WHERE product_uid = ? AND status = 'Available' ORDER BY amount ASC LIMIT 1", array("product_uid") ,array($_GET['id']),"s");
                        $variationLPrice = $variationLP[0]->getAmount();
                        $variationHP = getVariation($conn,"WHERE product_uid = ? AND status = 'Available' ORDER BY amount DESC LIMIT 1", array("product_uid") ,array($_GET['id']),"s");
                        $variationHPrice = $variationHP[0]->getAmount();
                        ?>
                        <?php
                        }
                        ?>

                        <!-- <p class="price-p2 ow-font-weight400">RM 1000 +</p> -->
                        <p class="price-p2 ow-font-weight400">RM <?php echo $variationLPrice;?> - RM <?php echo $variationHPrice;?></p>
                        <!-- <p class="price-p2 ow-font-weight400">RM <?php //echo $productDetails[$cnt]->getPrice();?></p> -->


                        <div class="clear"></div>

                        <div class="pet-details-div" style="border-top: 1px solid #cccccc; border-bottom:1px solid #cccccc; padding-top: 10px;">
                        	<p class="pet-table-p" style="cursor:pointer;" onclick="myShow()">Show Details</p>
                        	<div id="show-hide-p-div" style="display:none;">
                                <p class="pet-table-p"><?php echo $productDetails[$cnt]->getDescription();?></p>
                                <p class="pet-table-p"><?php echo $productDetails[$cnt]->getDescriptionTwo();?></p>
                                <p class="pet-table-p"><?php echo $productDetails[$cnt]->getDescriptionThree();?></p>
                                <p class="pet-table-p"><?php echo $productDetails[$cnt]->getDescriptionFour();?></p>
                                <p class="pet-table-p"><?php echo $productDetails[$cnt]->getDescriptionFive();?></p>
                                <p class="pet-table-p"><?php echo $productDetails[$cnt]->getDescriptionSix();?></p>
                            </div>
                        </div>                
                    
                        <div class="clear"></div>

                        <!--<a href='productReview.php?id=<?php echo $productDetails[$cnt]->getUid();?>'>
                            <div class="view-review-div" style="margin-top:10px !important; ">
                                <p class="light-green-a2 left-review-text">View Reviews</p>
                                <p class="right-arrow2">></p>
                            </div>
                        </a>-->

                        <!--<div class="view-review-div view-review-div2">
                            <p class="light-green-a2 left-review-text">Terms</p>
                            <p class="right-arrow2">></p>
                        </div>-->
                        
                    <?php
                    if($uid != "")
                    {
						
                    ?>
                   
                        <?php
                        if(isset($_GET['id']))
                        {
                        $conn = connDB();
                        // echo $_GET['id'];
                        $productQnA = getProduct($conn,"WHERE uid = ? ", array("uid") ,array($_GET['id']),"s");
                        $productVariation = getVariation($conn,"WHERE product_uid = ? AND status = 'Available' ", array("product_uid") ,array($_GET['id']),"s");
                        ?>
                            <!-- <form method="POST" action="utilities/preOrderTestFunction.php"> -->

                            <!-- <form method="POST" action="utilities/preOrderFunction.php"> -->
                            <form method="POST" id="paymentVerifiedForm" onsubmit="doPreview(this.submited); return false;">
        <?php
        if($productVariation)
        {
            for($cnt = 0;$cnt < count($productVariation) ;$cnt++)
            {
            ?>
                <!-- <input class="input-name" type="hidden" value="<?php echo $productVariation[$cnt]->getName();?>" name="product_name" id="product_name" readonly>      
                <input class="input-name" type="hidden" value="<?php echo $productVariation[$cnt]->getPrice();?>" name="product_price" id="product_price" readonly>    
                <input class="input-name" type="hidden" value="<?php echo $productVariation[$cnt]->getUid();?>" name="product_uid" id="product_uid" readonly> 
                <div class="variation-div">
                <p class="input-top-p admin-top-p">Variation</p>

                <select class="input-name clean border0 margin-bottom10" value="<?php echo $productVariation[$cnt]->getName();?>" name="product_uid" id="product_uid" required>
                    <?php
                    if($productVariation)
                    {
                        for($cnt = 0;$cnt < count($productVariation) ;$cnt++)
                        {
                        ?>
                            <option value="<?php echo $productVariation[$cnt]->getUid();?>">
                                <?php echo $productVariation[$cnt]->getName();?> | RM <?php echo $productVariation[$cnt]->getPrice();?>
                            </option>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>
                </select> -->

            <p class="input-top-p admin-top-p" style="margin-top:10px;">Variation (Please Click 1 Variation to Proceed):</p>
                <?php
                if($productVariation)
                {
                    for($cnt = 0;$cnt < count($productVariation) ;$cnt++)
                    {
                    ?>
                        <input class="checkbox-budget" type="radio" name="product_uid" id="<?php echo $productVariation[$cnt]->getUid();?>" value="<?php echo $productVariation[$cnt]->getUid();?>"  onclick="show1();"  required>
                        
                        <label class="for-checkbox-budget" for="<?php echo $productVariation[$cnt]->getUid();?>">
                            <?php echo $productVariation[$cnt]->getName();?> | RM <?php echo $productVariation[$cnt]->getPrice();?><img src="productImage/<?php echo $productVariation[$cnt]->getImage();?>" alt="<?php echo $productVariation[$cnt]->getName();?>" class="var-img"/> 
                        </label>
                    <?php
                    }
                    ?>
                <?php
                }
                ?>
            

                
            <?php
            }
            ?>
        <?php
        }
        ?>
        <div id="showhide">
        <p class="input-top-p admin-top-p" style="margin-top:10px;">Quantity</p>
        
     
                                
                                <button type="button" class="product-button-minus button-minus math-button math-button2 clean opacity-hover">-</button>
                                    <input readonly type="number" step="1" min=0 max=10 value='0' class="quantity-field math-input clean text-center value-input-div ow-value" name="product_quantity" id="product_quantity" required>
                                <button type="button" class="product-button-plus button-plus math-button math-button2 opacity-hover clean" onclick="show2();">+</button>
		
        <!--<input class="checkbox" type="text" name="product_quantity">-->
        
        <div class="clear"></div>
        <div id="none-first">

        <input onclick="this.form.submited=this.value;"  type="submit" name="Buy Now" value="Buy Now" class="green-button white-text clean2 edit-1-btn margin-auto ow-square-btn ow-width100a ow-dual-button" >
        
        <input onclick="this.form.submited=this.value;"  type="submit" name="Add To Cart" value="Add To Cart" class="green-button white-text clean2 edit-1-btn margin-auto ow-square-btn ow-width100a ow-dual-button second-dual">

        <!-- <button class="green-button white-text clean2 edit-1-btn margin-auto ow-square-btn ow-width100a ow-dual-button" name="submit">Buy Now</button>
        <button class="green-button white-text clean2 edit-1-btn margin-auto ow-square-btn ow-width100a ow-dual-button second-dual"  name="submit">Add To Cart</button></div></div> -->
        
    </form>   

                            <div class="clear"></div>

                            <?php
                            if($uid != "")
                            {
                            ?>
                                <?php
                                if($productQnA)
                                {
                                ?>
                                    <!-- <a href="liveChat.php" class="iframe">
                                    <img src="img/chat.gif" class="absolute-chat opacity-hover" class='iframe' alt="Live Chat" title="Live Chat">
                                    </a> -->
                                    <a href="liveChat.php?id=<?php echo $productQnA[0]->getName();?>">
                                    <div class="peach-button ow-peach-bg ow-square-btn chat-me ow-width100a">Chat Now</div>
                                    </a>
                                <?php
                                }
                                ?>
                            <?php
                            }
                            else
                            {
                                // echo "pls login to chat";
                            }
                            ?>

                        <?php
                        }
                        ?>
                    
                    <?php
                    }
                    else
                    {
                        // login div
                        // echo "pls login";
                    ?>
                       
                            <button class="green-button white-text clean2 edit-1-btn margin-auto ow-square-btn open-login  ow-width100a" name="submit">Add To Cart</button>
                      
               </div>     
                    <?php
                    }
                    ?>
  
                <?php
                }
                ?>
            <?php
            }
            ?>

        <?php
        }
        ?>
    </div>
</div></div></div></div></div></div></div></div></div>
</div>

<div class="clear"></div>
<style>
[type="checkbox"]:checked,
[type="checkbox"]:not(:checked),
[type="radio"]:checked,
[type="radio"]:not(:checked){
	position: absolute;
	left: -9999px;
	width: 0;
	height: 0;
	visibility: hidden;
}
#showhide{
	display:none;}
#none-first{
	display:none;}
</style>
<?php include 'js.php'; ?>

<script>
    $(".button-minus").on("click", function(e)
    {
        e.preventDefault();
        var $this = $(this);
        var $input = $this.closest("div").find("#product_quantity");
        var value = parseInt($input.val());
        if (value > 1)
        {
            value = value - 1;
        } 
        else 
        {
            value = 1;
        }
        $input.val(value);
    });

    $(".button-plus").on("click", function(e)
    {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest("div").find("#product_quantity");
    var value = parseInt($input.val());
    // if (value < 100)
    if (value < 10)
    {
        value = value + 1;
    }
    else
    {
        // value = 100;
        value = 1;
    }
    $input.val(value);
    });
</script>

<script>
function show1()
{
    document.getElementById('showhide').style.display = 'block';
}
function show2()
{
    document.getElementById('none-first').style.display = 'block';
}
</script>

<script type="text/javascript">
    function doPreview(buttonType)
    {
        switch(buttonType){
            case 'Buy Now':
                form=document.getElementById('paymentVerifiedForm');
                form.action='utilities/directOrderFunction.php';
                form.submit();
            break;
            case 'Add To Cart':
                form=document.getElementById('paymentVerifiedForm');
                form.action='utilities/preOrderFunction.php';
                form.submit();
            break;
        }

    }
</script>

<!-- <script>
    function increment() {
        document.getElementById('demoInput').stepUp();
    }
    function decrement() {
        document.getElementById('demoInput').stepDown();
    }
</script> -->

</body>
</html>