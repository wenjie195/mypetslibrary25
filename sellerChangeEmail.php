<?php
require_once dirname(__FILE__) . '/sellerAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Change Email | Mypetslibrary" />
<title>Change Email | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
    	<p class="review-product-name">Change Email</p>
        <!-- <form> -->
        <form action="utilities/sellerEditEmailFunction.php" method="POST">
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">New Email</p>
            <input class="input-name clean input-textarea admin-input" type="text" placeholder="Key in New Email" id="new_email" name="new_email" required> 
            
            <div class="clear"></div>

            <!-- <button class="orange-button white-text width100 clean2 bottom-distance width100 ow-no-margin-top">Verify Email</button>   -->

        	<p class="input-top-p admin-top-p">Password</p>
            <div class="edit-password-input-div admin-edit-password-input-div">
            	<input class="input-name clean input-password edit-password-input"  type="Password" placeholder="Password" id="password" name="password" required>
                <p class="edit-p-password">
                    <img src="img/visible.png" class="hover1a edit-password-img" onclick="myFunctionC()" alt="View Password" title="View Password">
                    <img src="img/visible2.png" class="hover1b edit-password-img" alt="View Password" title="View Password">
                </p>      
            </div>       
        </div>
        <div class="clear"></div>
        	<button class="green-button white-text clean2 edit-1-btn" name="Submit">Submit</button>
        </form>
	</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Email Successfully"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to change email !! ";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Wrong Password !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>