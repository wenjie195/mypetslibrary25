<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $products = getProduct($conn);
// $products = getProduct($conn, "WHERE feature = 'Yes' ORDER BY date_created DESC ");
$products = getProduct($conn, "WHERE feature = 'Yes' AND status = 'Available' ORDER BY date_created DESC ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Featured Products | Mypetslibrary" />
<title>Featured Products | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance">

	<div class="width100">
		<div class="left-h1-div featured-left">
            <h1 class="green-text h1-title">Featured Products</h1>
            <div class="green-border"></div>
		</div>
        <div class="right-add-div featured-right">
        	<a href="addFeaturedProducts.php"><div class="green-button white-text">Add</div></a>
        </div>        
    </div>

    <div class="clear"></div>

	<div class="width103 border-separation">

    <?php
    $conn = connDB();
    if($products)
    {
      for($cnt = 0;$cnt < count($products) ;$cnt++)
      {
      ?>
          <div class="four-box-size"> 	
            <div class="shadow-white-box">
              <div class="width100 white-bg">
                <img src="productImage/<?php echo $products[$cnt]->getImageOne();?>" alt="<?php echo $products[$cnt]->getName();?>" 
                title="<?php echo $products[$cnt]->getName();?>" class="width100 two-border-radius">
              </div>

              <div class="width100 product-details-div">
                <p class="width100 text-overflow slider-product-name"><?php echo $products[$cnt]->getName();?></p>
                <p class="width100 text-overflow slider-product-price"><?php //echo $products[$cnt]->getVariationOneprice();?></p>
              </div>
            </div>        
              
            <form method="POST" action="utilities/adminDeleteFeaturedProductFunction.php">
            <!-- <form method="POST" action="#"> -->
                <button class="clean red-btn featured-same-button open-confirm" type="submit" name="product_uid" value="<?php echo $products[$cnt]->getUid();?>">
                    Delete
                </button> 
            </form>

          </div>
      <?php
      }
      ?>
    <?php
    }
    $conn->close();
    ?>

    </div>

    <div class="clear"></div>

</div>        

<div class="clear"></div>
        
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Product Deleted !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to delete product !! ";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !! ";
        }

        else if($_GET['type'] == 4)
        {
            $messageType = "Featured Product Added !! ";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Fail to add featured product !! ";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "ERROR <br> Please Retry !! ";
        }

        else if($_GET['type'] == 7)
        {
            $messageType = "Featured Product Deleted !! ";
        }
        else if($_GET['type'] == 8)
        {
            $messageType = "Fail to delete featured product !! ";
        }

        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>