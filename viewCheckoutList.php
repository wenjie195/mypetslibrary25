<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/PreOrderList.php';
require_once dirname(__FILE__) . '/classes/States.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
$orderUid = $_SESSION['order_uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

// $products = getPreOrderList($conn, "WHERE status = 'Pending' ");
// $products = getOrderList($conn, "WHERE user_uid = ? AND status = 'Pending' ",array("user_uid"),array($uid),"s");
$products = getOrderList($conn, "WHERE user_uid = ? AND order_id = ? ",array("user_uid","order_id"),array($uid,$orderUid),"ss");

$states = getStates($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Check Out | Mypetslibrary" />
<title>Check Out | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
                    
<div class="width100 same-padding menu-distance min-height big-black-text user-dash user-dash2 product-padding">
  
        <table class="green-table width100">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Product Name</th>
                    <th>Unit Price (RM)</th>
                    <th>Quantity</th>
                    <th>Subtotal (RM)</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($products)
                    {
                        for($cnt = 0;$cnt < count($products) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $products[$cnt]->getProductName();?></td>
                                <td><?php echo $products[$cnt]->getOriginalPrice();?></td>
                                <td><?php echo $products[$cnt]->getQuantity();?></td>
                                <td><?php echo $products[$cnt]->getTotalPrice();?></td>
                            </tr>
                        <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>
   
    
            <!-- <form method="POST" action="shipping.php"> -->

                <?php
                if($products)
                {
                $totalOrderAmount = 0;
                for ($cnt=0; $cnt <count($products) ; $cnt++)
                {
                    $totalOrderAmount += $products[$cnt]->getTotalPrice();
                    // echo "<br>";
                    // echo '123';
                }
                }
                else
                {
                    $totalOrderAmount = 0 ;
                }
                ?>
			<div class="clear"></div>
            <!-- <form method="POST" action="payAndShip.php"> -->
            <form method="POST" action="utilities/createOrderFunction.php">
                <p class="review-product-name" style="margin-top:20px;">Deliver to</p>
                <div class="width100 overflow"> 
                
                    <div class="dual-input">
                        <p class="input-top-p">Name</p>
                        <input class="input-name clean" type="text" id="insert_name" name="insert_name" placeholder="Name" value="<?php echo $userDetails[0]->getName();?>" required>
                    </div>

                    <div class="dual-input second-dual-input">
                        <p class="input-top-p">Phone No.</p>
                        <input class="input-name clean" type="text" id="insert_contact" name="insert_contact" placeholder="Phone No." value="<?php echo $userDetails[0]->getPhoneNo();?>" required> 
                    </div>

                    <div class="clear"></div>

                    <div class="width100">
                        <p class="input-top-p">Address</p>
                        <input class="input-name clean" type="text" id="insert_address" name="insert_address" value="<?php echo $userDetails[0]->getShippingAddress();?>" placeholder="Address" required>   
                    </div>

					<div class="clear"></div> 

                    <div class="dual-input">
                        <p class="input-top-p">Area</p>
                        <input class="input-name clean" type="text" placeholder="Area" value="<?php echo $userDetails[0]->getShippingArea();?>" name="insert_area" id="insert_area" required> 
                    </div>      


                    <div class="dual-input second-dual-input">
                        <p class="input-top-p">Postal Code</p>
                        <input class="input-name clean" type="text" placeholder="Postal Code" value="<?php echo $userDetails[0]->getShippingPostalCode();?>" name="insert_code" id="insert_code" required>     
                    </div> 

                    <div class="clear"></div> 

                    <div class="dual-input">
                        <p class="input-top-p">State</p>
                        <select class="input-name clean" type="text" name="insert_state" id="insert_state" required>
                        <?php
                        // if($userData->getShippingState() == '')
                        if($userDetails[0]->getShippingAddress() == '')
                        {
                        ?>
                            <option selected>Select State</option>
                            <?php
                            for ($cnt=0; $cnt <count($states) ; $cnt++)
                            {
                            ?>
                                <option value="<?php echo $states[$cnt]->getStateName(); ?>"> 
                                    <?php echo $states[$cnt]->getStateName(); ?>
                                </option>
                            <?php
                            }
                        }
                        else
                        {
                            for ($cnt=0; $cnt <count($states) ; $cnt++)
                            {
                                // if ($userData->getShippingState() == $states[$cnt]->getStateName())
                                if ($userDetails[0]->getShippingAddress() == $states[$cnt]->getStateName())
                                {
                                ?>
                                    <option selected value="<?php echo $states[$cnt]->getStateName(); ?>"> 
                                        <?php echo $states[$cnt]->getStateName(); ?>
                                    </option>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <option value="<?php echo $states[$cnt]->getStateName(); ?>"> 
                                        <?php echo $states[$cnt]->getStateName(); ?>
                                    </option>
                                <?php
                                }
                            }
                        }
                        ?>
                        </select>    
                    </div>   

                    <div class="clear"></div> 

                    <input type="hidden" id="order_uid" name="order_uid" value="<?php echo $orderUid ?>" readonly>
                    <input type="hidden" id="subtotal" name="subtotal" value="<?php echo $totalOrderAmount;?>" readonly> 

                    <div class="clear"></div>  

                </div>

                <button class="green-button white-text clean2 width100" type="submit" name="submit" style="margin-bottom:50px;">Continue To Payment</button>

            </form>
    
</div>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>