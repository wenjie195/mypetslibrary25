<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Favorite.php';
require_once dirname(__FILE__) . '/classes/Pets.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
$_SESSION['url'] = $_SERVER['REQUEST_URI'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$allFavorite = getFavorite($conn, "WHERE uid =? AND status = 'Yes' ",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Favourite Pets | Mypetslibrary" />
<title>Favourite Pets | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

        <div class="width100 same-padding overflow min-height menu-distance2">
        
            <h1 class="green-text seller-h1">My Favourite Pets</h1>
        
            <div class="clear"></div>  
     		
            <div class="width103">

            <?php
                if($allFavorite)
                {
                    for($cnt = 0;$cnt < count($allFavorite) ;$cnt++)
                    {
                    ?>
                        <?php $petsUid = $allFavorite[$cnt]->getItemUid();?>

                        <?php
                        $conn = connDB();
                        $favoritePets = getPetsDetails($conn,"WHERE uid = ? ", array("uid") ,array($petsUid),"s");
                        if($favoritePets)
                        {
                            for($cntAA = 0;$cntAA < count($favoritePets) ;$cntAA++)
                            {
                            ?>

                                <!-- <a href='puppyDogForSale.php?id=<?php //echo $petsUid;?>'> -->

                                <?php
                                    $petsType = $allFavorite[$cnt]->getType();
                                    if($petsType == 'Puppy')
                                    {
                                    ?>
                                        <a href='puppyDogForSale.php?id=<?php echo $petsUid;?>'>
                                    <?php
                                    }
                                    elseif($petsType == 'Kitten')
                                    {
                                    ?>
                                        <a href='kittenCatForSale.php?id=<?php echo $petsUid;?>'>
                                    <?php
                                    }
                                    elseif($petsType == 'Reptile')
                                    {
                                    ?>
                                        <a href='reptileForSale.php?id=<?php echo $petsUid;?>'>
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                        <a href=#>
                                    <?php
                                    }
                                ?>

                                    <div class="shadow-white-box four-box-size opacity-hover">
                                    <div class="square">     
                                        <div class="width100 white-bg content">
                                            <img src="uploads/<?php echo $favoritePets[$cntAA]->getDefaultImage();?>" alt="" title="" class="width100 two-border-radius">
                                        </div>
                                    </div> 

                                    <?php 
                                        $status = $favoritePets[$cntAA]->getStatus();
                                        if($status == 'Sold')
                                        {
                                        ?>
                                            <div class="sold-label sold-label3">Sold</div>
                                        <?php
                                        }
                                        else
                                        {}
                                    ?>

                                    <div class="width100 product-details-div">

                                    <?php 
                                        $petGender = $favoritePets[$cntAA]->getGender();
                                        if($petGender == 'Female')
                                        {
                                            $petsGender = 'F';
                                        }
                                        elseif($petGender == 'Male')
                                        {
                                            $petsGender = 'M';
                                        }
                                    ?>

                                    <p class="width100 text-overflow slider-product-name"><?php echo $favoritePets[$cntAA]->getBreed();?> | <?php echo $petsGender ;?></p>
                                    <p class="slider-product-name">
                                        RM<?php $length = strlen($favoritePets[$cntAA]->getPrice());?>
                                        <?php 
                                        if($length == 2)
                                        {
                                        $hiddenPrice = "X";
                                        }
                                        elseif($length == 3)
                                        {
                                        $hiddenPrice = "XX";
                                        }
                                        elseif($length == 4)
                                        {
                                        $hiddenPrice = "XXX";
                                        }
                                        elseif($length == 5)
                                        {
                                        $hiddenPrice = "XXXX";
                                        }
                                        elseif($length == 6)
                                        {
                                        $hiddenPrice = "XXXXX";
                                        }
                                        elseif($length == 7)
                                        {
                                        $hiddenPrice = "XXXXXX";
                                        }
                                        ?>
                                        <?php echo substr($favoritePets[$cntAA]->getPrice(),0,1); echo $hiddenPrice;?>
                                    </p>
                                    </div>

                                    </div>
                                </a> 

                            <?php
                            }
                            ?>
                        <?php
                        }
                        ?>
                        



                    <?php
                    }
                }
            ?>  
            </div>

</div>


<div class="clear"></div>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Profile Successfully !";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Update Password Successfully !";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Update Shipping Address Successfully !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Profile Picture Updated !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>
