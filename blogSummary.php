<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$pendingArticles = getArticles($conn, " WHERE display = 'Pending' ");
$approvedArticles = getArticles($conn, " WHERE display = 'Yes' ");
$rejectedArticles = getArticles($conn, " WHERE display = 'Rejected' ");
$reportedArticles = getArticles($conn, " WHERE display = 'Reported' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Blog Summary | Mypetslibrary" />
<title>Blog Summary | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance">
	<h1 class="green-text h1-title">Blog Summary</h1>
	<div class="green-border"></div>
    <div class="clear"></div>
    <div class="width100 border-separation">
    	<a href="pendingArticle.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box">
                <img src="img/review-pet.png" alt="Pending Article" title="Pending Article" class="four-div-img">
                <p class="four-div-p">Pending Article</p>
                <?php
                if($pendingArticles)
                {   
                    $totalPending = count($pendingArticles);
                }
                else
                {   $totalPending = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalPending;?></b></p>
            </div>
        </a>
        <a href="approvedArticle.php"  class="opacity-hover">
            <div class="white-dropshadow-box four-div-box second-four-div-box left-four-div">
                <img src="img/approve.png" alt="Approved Article" title="Approved Article" class="four-div-img">
                <p class="four-div-p">Approved Article</p>
                <?php
                if($approvedArticles)
                {   
                    $totalApproved = count($approvedArticles);
                }
                else
                {   $totalApproved = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalApproved;?></b></p>
            </div> 
        </a>
        <a  href="rejectedArticle.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box right-four-div">
                <img src="img/reject.png" alt="Rejected Article" title="Rejected Article" class="four-div-img">
                <p class="four-div-p">Rejected Article</p>
                <?php
                if($rejectedArticles)
                {   
                    $totalRejected = count($rejectedArticles);
                }
                else
                {   $totalRejected = 0;   }
                ?>
                <!-- <p class="four-div-amount-p"><b>100</b></p> -->
                <p class="four-div-amount-p"><b><?php echo $totalRejected;?></b></p>
            </div>  
        </a>
        <a href="reportedArticle.php" class="opacity-hover">       
            <div class="white-dropshadow-box four-div-box second-four-div-box forth-div">
                <img src="img/attention.png" alt="Reported Article" title="Reported Article" class="four-div-img">
                <p class="four-div-p">Reported Article</p>
                <?php
                if($reportedArticles)
                {   
                    $totalReported = count($reportedArticles);
                }
                else
                {   $totalReported = 0;   }
                ?>
                <!-- <p class="four-div-amount-p"><b>&nbsp;</b></p> -->
                <p class="four-div-amount-p"><b><?php echo $totalReported;?></b></p>
            </div>  
        </a>
        <a href="addArticle.php" class="opacity-hover">       
            <div class="white-dropshadow-box four-div-box">
                <img src="img/writing.png" alt="Write New Article" title="Write New Article" class="four-div-img">
                <p class="four-div-p">Write New Article</p>
                <p class="four-div-amount-p"><b>&nbsp;</b></p>
            </div> 
        </a>                   
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>
<div class="clear"></div>

<?php include 'js.php'; ?>

<?php

if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Article Published !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to submit article !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Articles's title or link has been used !! <br> Please re-enter a new title or new link !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>