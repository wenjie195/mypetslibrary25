<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/fblogin.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Favorite.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
$_SESSION['url'] = $_SERVER['REQUEST_URI'];

$conn = connDB();

$productDetails = getProduct($conn, "WHERE status = 'Available' ");

$favoriteItem = getFavorite($conn, "WHERE uid = ? AND link =? AND status = 'Yes' ",array("uid,link"),array($uid, $_SESSION['url']),"ss");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>

<?php 
// Program to display URL of current page. 
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
$link = "https"; 
else
$link = "http"; 

// Here append the common URL characters. 
$link .= "://"; 

// Append the host(domain name, ip) to the URL. 
$link .= $_SERVER['HTTP_HOST']; 

// Append the requested resource location to the URL 
$link .= $_SERVER['REQUEST_URI']; 

if(isset($_GET['id']))
{
    $referrerUidLink = $_GET['id'];
    // echo $referrerUidLink;
}
else 
{
    $referrerUidLink = "";
    // echo $referrerUidLink;
}
?>

<!-- <meta property="og:title" content="Pedigree Dentastix Dog Treats | Mypetslibrary" />
<title>Pedigree Dentastix Dog Treats | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary - Pedigree Dentastix Dog Treats - Daily oral care treat for your pet.
Unique tasty X-shaped treat clinically proven to reduce plaque. It has active ingredients zinc sulphate &amp; sodium trio polyphosphate that help in slowly down the rate of tartar build up." />
<meta name="description" content="Mypetslibrary - Pedigree Dentastix Dog Treats - Daily oral care treat for your pet. Unique tasty X-shaped treat clinically proven to reduce plaque. It has active ingredients zinc sulphate &amp; sodium trio polyphosphate that help in slowly down the rate of tartar build up." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
</head> -->

<?php
if(isset($_GET['id']))
{
$conn = connDB();
$productDetails = getProduct($conn,"WHERE uid = ? ", array("uid") ,array($_GET['id']),"s");
// $puppiesDetails = $puppiesUid[0];
?>
<?php
}
?>


<?php
if($productDetails)
{
for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
{
?>
<meta property="og:image" content="https://qlianmeng.asia/mypetslibrary/productImage/<?php echo $productDetails[$cnt]->getImageOne();?>" />        
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://qlianmeng.asia/mypetslibrary/productDetails.php?id=<?php echo $productDetails[$cnt]->getUid();?>" />
<link rel="canonical" href="https://qlianmeng.asia/mypetslibrary/productDetails.php?id=<?php echo $productDetails[$cnt]->getUid();?>" />
<meta property="og:title" content="<?php echo $productDetails[$cnt]->getName();?> Product Details | Mypetslibrary" />
<title><?php echo $productDetails[$cnt]->getName();?> Product Details | Mypetslibrary</title>
<meta property="og:description" content="<?php echo $productDetails[$cnt]->getName();?> for sale. Mypetslibrary online pet store in Malaysia." />
<meta name="description" content="<?php echo $productDetails[$cnt]->getName();?> for sale. Mypetslibrary online pet store in Malaysia." />

<meta name="keywords" content="<?php echo $productDetails[$cnt]->getName();?> ,<?php echo $productDetails[$cnt]->getKeywordOne();?>, Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, malaysia, online pet store,马来西亚,上网买宠物, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<meta property="og:image" content="https://qlianmeng.asia/mypetslibrary/productImage/<?php echo $productDetails[$cnt]->getImageOne();?>" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>
<div class="width100 menu-distance same-padding min-height product-details-all-div">

<?php
}
?>
<?php
}
?>


<?php
if($productDetails)
{
for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
{
?>

    <div class="left-image-div">
       <div class="item">            
            <div class="clearfix">
                <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                    <li data-thumb="productImage/<?php echo $productDetails[$cnt]->getImageOne();?>" class="pet-slider-li"> 
                        <img src="productImage/<?php echo $productDetails[$cnt]->getImageOne();?>" class="pet-slider-img" alt="<?php echo $productDetails[$cnt]->getName();?>" title="<?php echo $productDetails[$cnt]->getName();?>" />
                    </li>

                    <?php 
                        $imageTwo = $productDetails[$cnt]->getImageTwo();
                        if($imageTwo != '')
                        {
                        ?>
                            <li data-thumb="productImage/<?php echo $productDetails[$cnt]->getImageTwo();?>" class="pet-slider-li"> 
                                <img src="productImage/<?php echo $productDetails[$cnt]->getImageTwo();?>" class="pet-slider-img" alt="<?php echo $productDetails[$cnt]->getName();?>"  title="<?php echo $productDetails[$cnt]->getName();?>"  />
                            </li>
                        <?php
                        }
                    ?>

                    <?php 
                        $imageThree = $productDetails[$cnt]->getImageThree();
                        if($imageThree != '')
                        {
                        ?>
                            <li data-thumb="productImage/<?php echo $productDetails[$cnt]->getImageThree();?>" class="pet-slider-li"> 
                                <img src="productImage/<?php echo $productDetails[$cnt]->getImageThree();?>" class="pet-slider-img" alt="<?php echo $productDetails[$cnt]->getName();?>"  title="<?php echo $productDetails[$cnt]->getName();?>"  />
                            </li>
                        <?php
                        }
                    ?>

                    <?php 
                        $imageFour = $productDetails[$cnt]->getImageFour();
                        if($imageFour != '')
                        {
                        ?>
                            <li data-thumb="productImage/<?php echo $productDetails[$cnt]->getImageFour();?>" class="pet-slider-li"> 
                                <img src="productImage/<?php echo $productDetails[$cnt]->getImageFour();?>" class="pet-slider-img" alt="<?php echo $productDetails[$cnt]->getName();?>"  title="<?php echo $productDetails[$cnt]->getName();?>"  />
                            </li>
                        <?php
                        }
                    ?>

                    <?php 
                        $imageFive = $productDetails[$cnt]->getImageFive();
                        if($imageFive != '')
                        {
                        ?>
                            <li data-thumb="productImage/<?php echo $productDetails[$cnt]->getImageFive();?>" class="pet-slider-li"> 
                                <img src="productImage/<?php echo $productDetails[$cnt]->getImageFive();?>" class="pet-slider-img" alt="<?php echo $productDetails[$cnt]->getName();?>"  title="<?php echo $productDetails[$cnt]->getName();?>"  />
                            </li>
                        <?php
                        }
                    ?>

                    <?php 
                        $imageSix = $productDetails[$cnt]->getImageSix();
                        if($imageSix != '')
                        {
                        ?>
                            <li data-thumb="productImage/<?php echo $productDetails[$cnt]->getImageSix();?>" class="pet-slider-li"> 
                                <img src="productImage/<?php echo $productDetails[$cnt]->getImageSix();?>" class="pet-slider-img" alt="<?php echo $productDetails[$cnt]->getName();?>"  title="<?php echo $productDetails[$cnt]->getName();?>"  />
                            </li>
                        <?php
                        }
                    ?>

                    <?php 
                        $productLink = $productDetails[$cnt]->getLink();
                        if($productLink != '')
                        {
                        ?>
                            <li data-thumb="img/video.jpg" class="pet-slider-li"> 
                             <iframe src="https://player.vimeo.com/video/<?php echo $productDetails[$cnt]->getLink();?>" class="video-iframe" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                            </li>
                        <?php
                        }
                    ?>

                </ul>
            </div>
        </div>
    </div>

    <div class="right-content-div2">
    	<!--<p class="green-text breed-p"><?php echo $productDetails[$cnt]->getBrand();?></p>-->
        <h1 class="green-text pet-name"><?php echo $productDetails[$cnt]->getName();?></h1>
        <p class="price-p2 ow-font-weight400">RM<?php echo $productDetails[$cnt]->getPrice();?></p>
        
        <!--<div class="right-info-div">-->
<!--        	<div class="contain1000 three-button-width">
                 	<p class="right-sold product-details-p">1000<br>Sold</p>	
            </div>-->

         	<!-- <a class="contact-icon hover1 three-button-width">
            	<img src="img/favourite-1.png" class="hover1a" alt="Favourite" title="Favourite">
                <img src="img/favourite-2.png" class="hover1b" alt="Favourite" title="Favourite">
            </a>   -->

<!--            <?php 
                if(!$favoriteItem)
                {
                ?>
                    <form action="utilities/addFavoriteFunction.php" method="POST" class="contact-icon"> 
                        
                            <input type="hidden" value="<?php echo $_SESSION['url'] ;?>" name="link" id="link">   
                            <input type="hidden" value="Product" name="type" id="type">  
                            <input type="hidden" value="<?php echo $referrerUidLink ;?>" name="item_uid" id="item_uid">  
                            <button class="transparent-button clean "><img src="img/heart.png" class="contact-icon" alt="Add to Favourite" title="Add to Favourite"></button>
                        
                    </form>
                <?php
                }
                else
                {
                ?>
                    <form action="utilities/removeFavoriteFunction.php" method="POST" class="contact-icon"> 
                            
                            <input type="hidden" value="<?php echo $referrerUidLink ;?>" name="item_uid" id="item_uid">  
                            <input type="hidden" value="<?php echo $uid;?>" name="user_uid" id="user_uid"> 
                            <button class="transparent-button clean"><img src="img/heart2.png" class="contact-icon" alt="Remove Favourite" title="Remove Favourite"></button>
                        
                    </form>
                <?php
                }
            ?>-->

<!--         	<a class="contact-icon hover1 last-contact-icon open-social three-button-width">
                    <img src="img/share.png" class="hover1a" alt="Share" title="Share">
                    <img src="img/share2.png" class="hover1b" alt="Share" title="Share">
            </a>
        </div>-->

        <div class="clear"></div>
		<!--<p class="stock-p">Stocks left: <?php echo $productDetails[$cnt]->getQuantity();?></p>
        <div class="clear"></div>-->
		<div class="pet-details-div">
            <div class="tab">
              <button class="tablinks active" onclick="openTab(event, 'Details')">Details</button>
  			  <button class="tablinks" onclick="openTab(event, 'Terms')">Terms</button>
		</div>     

        <div  id="Details" class="tabcontent block ow-tab-content">
<!--				<table class="pet-table">
                	<tr>
                    	<td class="grey-p">Stock</td>
                        <td class="grey-p">:</td>
						<td><?php echo $productDetails[$cnt]->getQuantity();?></td>
                    </tr>
                	<tr>
                    	<td class="grey-p">For Animal Type</td>
                        <td class="grey-p">:</td>
						<td><?php echo $productDetails[$cnt]->getAnimalType();?></td>
                    </tr>
                	<tr>
                    	<td class="grey-p">Category</td>
                        <td class="grey-p">:</td>
						<td><?php echo $productDetails[$cnt]->getCategory();?></td>
                    </tr>                                                                                                                
                </table>-->
                <p class="pet-table-p">
                    <?php echo $productDetails[$cnt]->getDescription();?>
                </p>
                <p class="pet-table-p">
                    <?php echo $productDetails[$cnt]->getDescriptionTwo();?>
                </p>
                <p class="pet-table-p">
                    <?php echo $productDetails[$cnt]->getDescriptionThree();?>
                </p>
                <p class="pet-table-p">
                    <?php echo $productDetails[$cnt]->getDescriptionFour();?>
                </p>
                <p class="pet-table-p">
                    <?php echo $productDetails[$cnt]->getDescriptionFive();?>
                </p>
                <p class="pet-table-p">
                    <?php echo $productDetails[$cnt]->getDescriptionSix();?>
                </p>
        </div>
        
        <div id="Terms" class="tabcontent">
			<p class="pet-table-p">Terms content</p>
        </div>

            <!-- <a href="malaysia-pet-food-toy-product.php"> -->
            <!--<a href="#">
                <div class="review-div hover1 upper-review-div">
                    <p class="left-review-p grey-p">Brand</p>
                    <p class="left-review-mark brand-p text-overflow"><?php echo $productDetails[$cnt]->getBrand();?></p>

                    <p class="right-arrow">
                        <img src="img/arrow.png" alt="Review" title="Review" class="arrow-img hover1a">
                        <img src="img/arrow2.png" alt="Review" title="Review" class="arrow-img hover1b">
                    </p>
                    <p class="beside-right-arrow grey-to-lightgreen">
                        View All Product
                    </p>                   	
                </div>
            </a>        -->
            <!-- <a href="productReview.php"> -->
            <a href='productReview.php?id=<?php echo $productDetails[$cnt]->getUid();?>'>
                <div class="view-review-div">
                    <p class="light-green-a2 left-review-text">View Reviews</p>
                    <p class="right-arrow2">></p>
                </div>
            </a>
               <div class="view-review-div view-review-div2 open-social">
                    <p class="light-green-a2 left-review-text">Share</p>
                    <p class="right-arrow2">></p>
                </div>
                    <!-- <p class="left-review-mark">4/5</p>
                    <p class="right-review-star">
                        <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                        <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                        <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                        <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                        <img src="img/grey-star.png" alt="Review" title="Review" class="star-img last-star">
                    </p>
                    <p class="right-arrow">
                        <img src="img/arrow.png" alt="Review" title="Review" class="arrow-img hover1a">
                        <img src="img/arrow2.png" alt="Review" title="Review" class="arrow-img hover1b">
                    </p>	 
                </div>
            </a>


            </div>  
                      
    </div>-->
</div>


<!--    <div class="clear"></div>

    <div class="width100 top-divider">-->
        <!-- <h1 class="green-text user-title left-align-title">Products</h1> -->
        <!-- <a class="right-align-link view-a light-green-a hover-a" href="malaysia-pet-food-toy-product.php">View More</a> -->
        <!-- <h1 class="green-text user-title left-align-title">Similar Products</h1>
        <a class="right-align-link view-a light-green-a hover-a" href="#">View More</a> -->
<!--        <h1 class="green-text user-title left-align-title"></h1>
        <a class="right-align-link view-a light-green-a hover-a" href="#"></a>
    </div>
    
    <div class="clear"></div>   -->    
        
	<!-- <div class="width103 product-big-div">
        <a href="productDetails.php">
            <div class="shadow-white-box featured four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/product-img.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Pedigree Dentastix Dog Treats</p>
                        <p class="product-price-width text-overflow slider-product-price left-price">RM2800.00</p>
                        <p class="slider-product-price right-like hover1">
                        	<img src="img/favourite-1.png" class="hover1a like-img width100" alt="Favourite" title="Favourite">
                            <img src="img/favourite-2.png" class="hover1b like-img width100" alt="Favourite" title="Favourite">
                        </p>
                        <div class="clear"></div>
                        <p class="left-rating">5<img src="img/yellow-star.png" class="rating-tiny-star" alt="Rating" title="Rating"></p>
                        <p class="right-sold">1000 <span class="sold-color">Sold</span></p>
                  </div>
            </div>
        </a> 
        <a href="productDetails.php">
            <div class="shadow-white-box featured four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/product-img.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Pedigree Dentastix Dog Treats</p>
                        <p class="product-price-width text-overflow slider-product-price left-price">RM2800.00</p>
                        <p class="slider-product-price right-like hover1">
                        	<img src="img/favourite-1.png" class="hover1a like-img width100" alt="Favourite" title="Favourite">
                            <img src="img/favourite-2.png" class="hover1b like-img width100" alt="Favourite" title="Favourite">
                        </p>
                        <div class="clear"></div>
                        <p class="left-rating">5<img src="img/yellow-star.png" class="rating-tiny-star" alt="Rating" title="Rating"></p>
                        <p class="right-sold">1000 <span class="sold-color">Sold</span></p>
                  </div>
            </div>
        </a>        
        <a href="productDetails.php">
            <div class="shadow-white-box featured four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/product-img.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Pedigree Dentastix Dog Treats</p>
                        <p class="product-price-width text-overflow slider-product-price left-price">RM2800.00</p>
                        <p class="slider-product-price right-like hover1">
                        	<img src="img/favourite-1.png" class="hover1a like-img width100" alt="Favourite" title="Favourite">
                            <img src="img/favourite-2.png" class="hover1b like-img width100" alt="Favourite" title="Favourite">
                        </p>
                        <div class="clear"></div>
                        <p class="left-rating">5<img src="img/yellow-star.png" class="rating-tiny-star" alt="Rating" title="Rating"></p>
                        <p class="right-sold">1000 <span class="sold-color">Sold</span></p>
                  </div>
            </div>
        </a> 
        <a href="productDetails.php">
            <div class="shadow-white-box featured four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/product-img.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Pedigree Dentastix Dog Treats</p>
                        <p class="product-price-width text-overflow slider-product-price left-price">RM2800.00</p>
                        <p class="slider-product-price right-like hover1">
                        	<img src="img/favourite-1.png" class="hover1a like-img width100" alt="Favourite" title="Favourite">
                            <img src="img/favourite-2.png" class="hover1b like-img width100" alt="Favourite" title="Favourite">
                        </p>
                        <div class="clear"></div>
                        <p class="left-rating">5<img src="img/yellow-star.png" class="rating-tiny-star" alt="Rating" title="Rating"></p>
                        <p class="right-sold">1000 <span class="sold-color">Sold</span></p>
                  </div>
            </div>
        </a>         		
	</div>        -->


</div>
</div>
<div class="clear"></div>
<div class="width100 text-center stay-bottom-add"> 
                                <a href="malaysia-pets-products.php#a<?php echo $productDetails[$cnt]->getUid();?>2"><div class="center-div2 clean black-button add-to-cart-btn green-button checkout-btn purchase-btn">Purchase</div></a>
</div>
<div class="stay-bottom-height"></div>
<div class="clear"></div>
<?php
}
?>
<?php
}
?>
<?php include 'js.php'; ?>

<script>
    $(".button-minus").on("click", function(e)
    {
        e.preventDefault();
        var $this = $(this);
        var $input = $this.closest("div").find("input");
        var value = parseInt($input.val());
        if (value > 1)
        {
            value = value - 1;
        } 
        else 
        {
            value = 0;
        }
        $input.val(value);
    });

    $(".button-plus").on("click", function(e)
    {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest("div").find("input");
    var value = parseInt($input.val());
    if (value < 100)
    {
        value = value + 1;
    }
    else
    {
        value = 100;
    }
    $input.val(value);
    });
</script>

</body>
</html>