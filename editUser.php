<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Bank.php';
require_once dirname(__FILE__) . '/classes/CreditCard.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

//$userRows = getUser($conn," WHERE id = ? ",array("id"),array($id),"i");
//$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit User Details | Mypetslibrary" />
<title>Edit User Details | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title opacity-hover pointer" onclick="goBack()"><img src="img/back2.png" class="back-png opacity-hover pointer"> Edit User Details</h1>
            <div class="green-border"></div>
   </div>
   <div class="border-separation">
        <div class="clear"></div>
        
         <!-- <form method="POST" action="utilities/editUserFunction.php" enctype="multipart/form-data"> -->
         <form method="POST" action="utilities/bannedUserFunction.php">
         <!-- <form method="POST" action="#"> -->
            <?php
            if(isset($_POST['user_uid']))
            {
                $conn = connDB();
                // $userDetails = getUser($conn,"WHERE id = ? ", array("id") ,array($_POST['user_id']),"i");
                $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");
            ?>

                <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Profile Picture</p>
                    <div class="profile-pic-div margin-bottom30">
                    <?php 
                        $proPic = $userDetails[0]->getProfilePic();
                        if($proPic != "")
                        {
                        ?>
                            <img src="userProfilePic/<?php echo $userDetails[0]->getProfilePic();?>" class="profile-pic" alt="Profile Picture" title="Profile Picture">
                        <?php
                        }
                        else
                        {
                        ?>
                            <img src="img/profile-pic.jpg" class="profile-pic" alt="Profile Picture" title="Profile Picture">
                        <?php
                        }
                    ?>
                    </div> 
                </div> 

                <div class="clear"></div>
     			<div class="dual-input">
                    <p class="input-top-p admin-top-p">Account Status</p>
                    <select  class="input-name clean input-textarea admin-input" type="text" name="update_status" id="update_status" required>

                        <?php
                            if($userDetails[0]->getUserType() == 1)
                            {
                            ?>
                                <option value="Banned"  name='Banned'>Banned</option>
                                <option selected value="Active"  name='Active'>Active</option>
                            <?php
                            }
                            else if($userDetails[0]->getUserType() == 5)
                            {
                            ?>
                                <option value="Active"  name='Active'>Active</option>
                                <option selected value="Banned"  name='Banned'>Banned</option>
                            <?php
                            }
                        ?>

                    <!-- <option>Active</option>
                    <option>Ban</option> -->
                    </select>
                </div>   
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Username*</p>
                    <input class="input-name clean input-textarea admin-input no-input" type="text" value="<?php echo $userDetails[0]->getName();?>" readonly>      
                </div>



                <div class="clear"></div>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Email*</p>
                    <input class="input-name clean input-textarea admin-input no-input" type="text" value="<?php echo $userDetails[0]->getEmail();?>" readonly>     
                </div>  


                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Contact No.*</p>
                    <input class="input-name clean input-textarea admin-input no-input" type="text" value="<?php echo $userDetails[0]->getPhoneNo();?>" readonly>    
                </div>   
				<div class="clear"></div>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Birthday</p>
                    <input class="input-name clean input-textarea admin-input no-input" type="text"  value="<?php echo $userDetails[0]->getBirthday();?>" readonly>      
                </div>

                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Gender</p>
                    <input class="input-name clean input-textarea admin-input no-input" type="text"  value="<?php echo $userDetails[0]->getGender();?>" readonly>
                </div>    

                <div class="clear"></div>

                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Facebook ID (Optional)</p>
                    <input class="input-name clean input-textarea admin-input no-input" type="text" value="<?php echo $userDetails[0]->getFbId();?>" readonly>      
                </div>           

                <input class="input-name clean input-textarea admin-input no-input" type="hidden"  name="user_uid" id="user_uid" value="<?php echo $userDetails[0]->getUid();?>" readonly>

                <div class="clear"></div>  

                <!-- <div class="clear"></div> -->

                <p class="review-product-name">Shipping Details</p>        
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Receiver Name</p>
                    <input class="input-name clean input-textarea admin-input no-input" type="text" value="<?php echo $userDetails[0]->getReceiverName();?>" readonly>    
                </div>

                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Receiver Contact No.</p>
                    <input class="input-name clean input-textarea admin-input no-input" type="text" value="<?php echo $userDetails[0]->getReceiverContactNo();?>" readonly>    
                </div>        

                <div class="clear"></div>

                <div class="dual-input">
                    <p class="input-top-p admin-top-p">State</p>
                    <input class="input-name clean input-textarea admin-input no-input" type="text" value="<?php echo $userDetails[0]->getShippingState();?>" readonly>
                </div>

                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Area</p>
                    <input class="input-name clean input-textarea admin-input no-input" type="text" value="<?php echo $userDetails[0]->getShippingArea();?>" readonly>
                </div>        

                <div class="clear"></div>

                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Postal Code</p>
                    <input class="input-name clean input-textarea admin-input no-input" type="text"  value="<?php echo $userDetails[0]->getShippingPostalCode();?>" readonly>    
                </div> 
        
                <div class="clear"></div>    

                <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Shipping Address</p>
                    <input class="input-name clean input-textarea admin-input no-input" type="text"  value="<?php echo $userDetails[0]->getShippingAddress();?>" readonly> 
                </div>

                <div class="clear"></div>

                <p class="review-product-name">Bank Account Details </p> 

                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Bank</p>
                    <input class="input-name clean input-textarea admin-input no-input" type="text" value="<?php echo $userDetails[0]->getBankName();?>" readonly>
                </div>        

                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Bank Account Holder Name</p>
                    <input class="input-name clean input-textarea admin-input no-input" type="text"  value="<?php echo $userDetails[0]->getBankAccountHolder();?>" readonly>    
                </div> 
        
                <div class="clear"></div>    

                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Bank Account Number</p>
                    <input class="input-name clean input-textarea admin-input no-input" type="text"  value="<?php echo $userDetails[0]->getBankAccountNo();?>" readonly> 
                </div>
            
            <?php
            }
            ?>

            <div class="clear"></div>  
            
            <div class="width100 text-center margin-top-bottom">
                <button class="green-button mid-btn-width" name ="submit">Submit</button>
            </div>	 

        </form>
      
	</div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>