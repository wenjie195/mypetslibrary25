<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Variation.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Product | Mypetslibrary" />
<title>Edit Product | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<!-- <//?php include 'userHeaderAfterLogin.php'; ?> -->
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">

    <div class="width100">
        <h1 class="green-text h1-title">Edit Product</h1>
        <div class="green-border"></div>
    </div>

    <div class="clear"></div>

    <div class="border-separation">
        <?php
        if(isset($_POST['variation_uid']))
        {
            $conn = connDB();
            $variationDetails = getVariation($conn,"WHERE uid = ? ", array("uid") ,array($_POST['variation_uid']),"s");
        ?>
            <form method="POST" action="utilities/adminVariationEditFunction.php" enctype="multipart/form-data">
                <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Product Name*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $variationDetails[0]->getName();?>" name="update_name" id="update_name" required>      
                </div>

                 <div class="clear"></div>

                <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Product Price (RM)</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $variationDetails[0]->getPrice();?>" name="update_price" id="update_price" required>      
                </div>
               
                <div class="clear"></div>

                <input type="hidden" id="variation_uid" name="variation_uid" value="<?php echo $variationDetails[0]->getUid();?>">

                <div class="clear"></div>  
                
                <div class="width100 overflow text-center">     
                    <button class="green-button white-text clean2 edit-1-btn margin-auto" type="submit" name ="submit">Submit</button>
                </div>

                <div class="clear"></div>
            </form>

            <div class="width100 margin-top50">
                <h1 class="green-text h1-title">Edit Variation Photo</h1>
                <div class="green-border"></div>
            </div>

            <div class="border-separation"></div>
            <div class="clear"></div>

            <div class="width100 overflow">
                <div class="six-box2">
                    <div class="square">
                        <div class="width100 white-bg content">
                            <img src="productImage/<?php echo $variationDetails[0]->getImage();?>" class="pet-photo-preview">
                        </div>
                    </div>
                    <!-- <form action="utilities/updateProductImageFunction.php" method="POST" enctype="multipart/form-data"> -->
                    <form action="utilities/adminUpdateVariationImageFunction.php" method="POST" enctype="multipart/form-data">
                        <p class="input-top-p admin-top-p">Update Image 1</p>
                        <div class="width100 overflow text-center">
                            <input type="file" class="center-input" name="image_one" id="image_one" accept="image/*" required>
                            <!-- <input type="hidden" id="image_value" name="image_value" value="1"> -->
                            <input type="hidden" name="variatio_uid" id="variatio_uid" value="<?php echo $variationDetails[0]->getUid();?>" required>
                        </div>
                        <div class="width100 overflow text-center">  
                        <button type="submit" class="clean green-button pointer width100 update-photo-btn" name="submit" id="submit">Submit</button>
                        </div>
                        <div class="clear"></div> 
                    </form>
                </div>
            </div>

        <?php
        }
        ?>
	</div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>