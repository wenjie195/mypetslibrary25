<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/Brand.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$categoryDetails = getCategory($conn);
$brandDetails = getBrand($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add New Product | Mypetslibrary" />
<title>Add New Product | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<script src="jquery.min.js"></script>
<script src="bootstrap.min.js"></script>
<script src="croppie.js"></script>
<link rel="stylesheet" href="bootstrap.min.css" />
<link rel="stylesheet" href="croppie.css" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">

  <div class="width100">
    <h1 class="green-text h1-title">Add New Product</h1>
    <div class="green-border"></div>
  </div>

  <div class="clear"></div>

  <div class="border-separation">

  <form action="utilities/adminRegisterProductFunction.php" method="POST" enctype="multipart/form-data">
        <div class="width100">
        	<p class="input-top-p admin-top-p">Category* <a href="category.php" class="green-a" target="_blank">(Add New Category Here)</a></p>
        	<select class="input-name clean admin-input" name="register_category" id="register_category" required>
                <option value="">Please Select a Category</option>
                <?php
                for ($cntPro=0; $cntPro <count($categoryDetails) ; $cntPro++)
                {
                ?>
                    <option value="<?php echo $categoryDetails[$cntPro]->getName(); ?>">
                        <?php echo $categoryDetails[$cntPro]->getName(); ?>
                    </option>
                <?php
                }
                ?>
            </select>
        </div>
        <div class="clear"></div>
        <div class="width100">
        	<p class="input-top-p admin-top-p">Brand* <a href="brand.php" class="green-a" target="_blank">(Add New Brand Here)</a></p>
        	<select class="input-name clean admin-input" name="register_brand" id="register_brand" required>
                <option value="">Please Select a Brand</option>
                <?php
                for ($cnt=0; $cnt <count($brandDetails) ; $cnt++)
                {
                ?>
                    <option value="<?php echo $brandDetails[$cnt]->getName(); ?>">
                        <?php echo $brandDetails[$cnt]->getName(); ?>
                    </option>
                <?php
                }
                ?>
            </select>
        </div>
        <div class="clear"></div>        
        
        
        
  <div class="width100">
    <p class="input-top-p admin-top-p">Product Name*</p>
    <input class="input-name clean input-textarea admin-input" type="text" placeholder="Product Name" name="register_name" id="register_name" required>
  </div>
	<div class="clear"></div>
  <!--<div class="width100">
    <p class="input-top-p admin-top-p">Product/Variation 1 Price (RM)</p>
    <input class="input-name clean input-textarea admin-input" type="text" placeholder="Product Price" name="register_price" id="register_price" required>
  </div>
    <div class="clear"></div>
    <div class="width100 overflow">
    <p class="input-top-p admin-top-p">Variation 1 Image*</p>
   	<input type="file"> 
    </div>-->

<!--  <div class="dual-input  second-dual-input">
    <p class="input-top-p admin-top-p">Product Quantity</p>
    <input class="input-name clean input-textarea admin-input" type="text" placeholder="Product Quantity" name="register_quantity" id="register_quantity" required>
  </div>-->

  <!-- <input class="input-name clean input-textarea admin-input" type="hidden" value="1000" name="register_quantity" id="register_quantity" readonly> -->

  <div class="clear"></div>

  <div class="width100 overflow">
    <p class="input-top-p admin-top-p">Product Description* (Avoid Using "'')</p>
    <input class="input-name clean input-textarea admin-input" type="text" placeholder="Product Description 1" name="register_description" id="register_description" required>
  </div>


    <div class="clear"></div>
  <div class="width100 overflow">
    <input class="input-name clean input-textarea admin-input" type="text" placeholder="Product Description 2" name="register_description_two" id="register_description_two">
  </div>

  <div class="clear"></div>

  <div class="width100 overflow">
    <input class="input-name clean input-textarea admin-input" type="text" placeholder="Product Description 3" name="register_description_three" id="register_description_three">
  </div>

  <div class="clear"></div>

  <div class="width100 overflow">
    <input class="input-name clean input-textarea admin-input" type="text" placeholder="Product Description 4" name="register_description_four" id="register_description_four">
  </div>

  <div class="clear"></div>

  <div class="width100 overflow">
    <input class="input-name clean input-textarea admin-input" type="text" placeholder="Product Description 5" name="register_description_five" id="register_description_five">
  </div>

  <div class="clear"></div>

  <div class="width100 overflow">
    <input class="input-name clean input-textarea admin-input" type="text" placeholder="Product Description 6" name="register_description_six" id="register_description_six">
  </div>

  <div class="clear"></div>

  <div class="width100 overflow">
    <p class="input-top-p admin-top-p">Google Search Keyword  (Use Coma , to Separate Each Keyword, Avoid"')</p>
    <input class="input-name clean input-textarea admin-input" type="text" placeholder="dog, pet, cute, for sale, Husky, Penang," name="register_keyword" id="register_keyword">
  </div>      

  <div class="clear"></div>    

  <div class="width100 overflow">
    <img src="img/vimeo-tutorial.png" class="vimeo-tutorial" alt="Tutorial" title="Tutorial">
    <p class="input-top-p admin-top-p">Vimeo Video Link Only (Optional) (Copy the Highlighted Part Only)</p>
    <input class="input-name clean input-textarea admin-input" type="text" placeholder="379922769" name="register_link" id="register_link">
  </div>

  <div class="clear"></div>

  <div class="width100 overflow text-center">
    <button class="green-button white-text clean2 edit-1-btn margin-auto">Next</button>
  </div>

  </form>

  </div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>
<?php unset($_SESSION['product_uid']);unset($_SESSION['image']); ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "New product added!";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to add the new product!";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "This product name is already used!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>