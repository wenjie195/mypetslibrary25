<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/OrderList.php';
// require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Order Review | Mypetslibrary" />
<title>Order Review | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
        <h1 class="green-text h1-title">Order Details</h1>
        <div class="green-border"></div>
    </div>

    <div class="border-separation">

        <div class="clear"></div>

        <?php
            if(isset($_POST['order_id']))
            {
                $conn = connDB();
                // $ordersDetails = getOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"s");
                $ordersDetails = getOrders($conn,"WHERE order_id = ? ORDER BY date_created DESC LIMIT 1  ", array("order_id") ,array($_POST['order_id']),"s");
                $orderId = $ordersDetails[0]->getId();

                // $productOrder = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($orderId),"s");
                $productOrder = getOrderList($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"s");
            ?>

                <div class="dual-input data-text">
                    <p class="input-top-p admin-top-p">Order ID: <b>#<?php echo $orderId;?></b></p>
                </div>

                <div class="dual-input second-dual-input data-text">
                    <p class="input-top-p admin-top-p">Purchaser Name: <b><?php echo $ordersDetails[0]->getName();?></b></p>
                </div>

                <div class="clear"></div>

                <div class="dual-input data-text">
                    <p class="input-top-p admin-top-p">Contact: <b><?php echo $ordersDetails[0]->getContactNo();?></b></p>
                </div>

                <div class="dual-input second-dual-input data-text">
                    <p class="input-top-p admin-top-p">Address: <b><?php echo $ordersDetails[0]->getAddressLine1();?></b></p>
                </div> 

                <div class="clear"></div>

                <div class="dual-input data-text">
                    <p class="input-top-p admin-top-p">Payment References: <b><?php echo $ordersDetails[0]->getPaymentBankReference();?></b></p>
                </div> 
                <div class="dual-input second-dual-input data-text">
                    <p class="input-top-p admin-top-p">Order Date: <b><?php echo $ordersDetails[0]->getDateCreated();?></b></p>
                </div> 

                <div class="clear"></div>

                <?php 
                    // $shippingStatus = $ordersDetails[0]->getShippingStatus();
                    $paymentStatus = $ordersDetails[0]->getPaymentStatus();
                    // if($shippingStatus = 'DELIVERED')
                    if($paymentStatus == 'ACCEPTED')
                    {
                    ?>
                        <div class="dual-input data-text">
                            <p class="input-top-p admin-top-p">Status: <b><?php echo $paymentStatus;?></b></p>
                        </div>

                        <div class="dual-input second-dual-input data-text">
                            <p class="input-top-p admin-top-p">Shipping Method: <b><?php echo $ordersDetails[0]->getShippingMethod();?></b></p>
                        </div>        
                        <div class="clear"></div>
                
                        <div class="dual-input data-text">
                            <p class="input-top-p admin-top-p">Tracking Number: <b><?php echo $ordersDetails[0]->getTrackingNumber();?></b></p>   
                        </div>

                        

                        <div class="dual-input second-dual-input data-text">
                            <p class="input-top-p admin-top-p">Shipped Date: <b><?php echo $ordersDetails[0]->getShippingDate();?></b></p>
                        </div> 
                    <?php
                    }
                    // elseif($shippingStatus = 'REJECTED')
                    elseif($paymentStatus == 'REJECTED')
                    {
                    ?>
                    	<div class="clear"></div>
                        <div class="dual-input data-text">
                            <p class="input-top-p admin-top-p">Status: <b><?php echo $paymentStatus;?></b></p>
                        </div>

                        <div class="dual-input second-dual-input data-text">
                            <p class="input-top-p admin-top-p">Rejected Date: <b><?php echo $ordersDetails[0]->getShippingDate();?></b></p>
                        </div> 
                        <div class="clear"></div>
                    <?php
                    }
                    else
                    {}
                ?>

            <?php
            }
            ?>
        
                <!-- <div class="width100 scroll-div border-separation"> -->
                <div class="width100 border-separation">
                    <table class="green-table width100">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Item</th>
                                <th>Quantity</th>
                                <th>Total Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if($productOrder)
                                {
                                    
                                    for($cnt = 0;$cnt < count($productOrder) ;$cnt++)
                                    {?>
                                        
                                        <tr class="link-to-details">
                                            <td><?php echo ($cnt+1)?></td>
                                            <td><?php echo $productOrder[$cnt]->getProductName();?></td>
                                            <td><?php echo $productOrder[$cnt]->getQuantity();?></td>
                                            <td>RM<?php echo $productOrder[$cnt]->getTotalPrice();?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                            ?>                                 
                        </tbody>
                    </table>
                </div>

	</div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>