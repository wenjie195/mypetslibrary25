<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$articles = getArticles($conn, " WHERE display = 'Yes' ORDER BY date_created DESC");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Approved Articles | Mypetslibrary" />
<title>Approved Articles | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance">
	<div class="width100">
        <div class="left-h1-div">
            <h1 class="green-text h1-title">Approved Articles</h1>
            <div class="green-border"></div>
        </div>
        <div class="mid-search-div">
        	<form>
            <input class="line-input clean" type="text" placeholder="Search" id="myInput" onkeyup="myFunction()">
                <button class="search-btn hover1 clean">
                        <img src="img/search.png" class="visible-img hover1a" alt="Search" title="Search">
                        <img src="img/search2.png" class="visible-img hover1b" alt="Search" title="Search">
                </button>
            </form>
        </div>
        <div class="right-add-div">
        	<a href="addArticle.php"><div class="green-button white-text puppy-button">Add Article</div></a>
        </div>
    
    </div>


    <div class="clear"></div>
	<div class="width100 scroll-div border-separation">
    	<table class="green-table width100" id="myTable">
        	<thead>
            	<tr>
                	<th class="first-column">No.</th>
                    <th>Author</th>
                    <th>Article Title</th>
                    <th>Submit On</th>
                    <th>Approved On</th>
                    <th>Details</th>
                    <th>Delete</th>
                </tr>
            </thead>

            <tbody>
                <?php
                    if($articles)
                    {
                        for($cnt = 0;$cnt < count($articles) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?>.</td>
                                <td><?php echo $articles[$cnt]->getAuthorName();?></td>
                                <td><?php echo $articles[$cnt]->getTitle();?></td>
                                <td><?php echo $date = date("d/m/Y",strtotime($articles[$cnt]->getDateCreated()));?></td>
                                <td><?php echo $date = date("d/m/Y",strtotime($articles[$cnt]->getDateUpdated()));?></td>
          
                                <td>
                                    <form method="POST" action="reviewArticle.php" class="hover1">
                                        <button class="clean hover1 img-btn" type="submit" name="article_uid" value="<?php echo $articles[$cnt]->getUid();?>">
                                            <img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                                            <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                                        </button>
                                    </form>
                                </td>

                                <td>
                                    <!-- <form method="POST" action="#" class="hover1">
                                        <button class="clean hover1 img-btn" type="submit" name="article_uid" value="<?php echo $articles[$cnt]->getUid();?>">
                                            <img src="img/delete1a.png" class="edit-icon1 hover1a" alt="Delete" title="Delete">
                                            <img src="img/delete3a.png" class="edit-icon1 hover1b" alt="Delete" title="Delete">
                                        </button>
                                    </form> -->

                                    <form method="POST" action="utilities/adminDeleteArticleFunction.php" class="hover1">
                                        <button class="clean hover1 img-btn" type="submit" name="article_uid" value="<?php echo $articles[$cnt]->getUid();?>">
                                            <img src="img/delete1a.png" class="edit-icon1 hover1a" alt="Delete" title="Delete">
                                            <img src="img/delete3a.png" class="edit-icon1 hover1b" alt="Delete" title="Delete">
                                        </button>
                                    </form>

                                </td>
                                
                            </tr>
                        <?php
                        }
                    }
                ?>    
            </tbody>

            <!-- <tbody>
            	<tr>
                	<td class="first-column">1.</td>
                    <td>Pet Seller 1</td>
                    <td>XXXXXXXXXXX</td>
                    <td>1/12/2019</td>
                    <td>1/12/2019</td>
                    <td>
                    	<a href="editArticle.php" class="hover1">
                        	<img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                            <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                        </a>                    
                    </td>
                    <td> 
                    	<form>
                            <a class="hover1 open-confirm pointer">
                                <img src="img/delete1a.png" class="edit-icon1 hover1a" alt="Delete" title="Delete">
                                <img src="img/delete3a.png" class="edit-icon1 hover1b" alt="Delete" title="Delete">
                            </a>                   		
                                                                            
                            <div id="confirm-modal" class="modal-css">
                            
                  
                              <div class="modal-content-css confirm-modal-margin">
                                <span class="close-css close-confirm">&times;</span>
                                <div class="clear"></div>
                                <h2 class="green-text h2-title confirm-title">Confirm Delete?</h2>
                                <div class="clean cancel-btn close-confirm">Cancel</div>
                                <button class="clean red-btn delete-btn2">Delete</button>
                                <div class="clear"></div>
                                
                                   
                              </div>
                            
                            </div> 

                        </form>                    	
                    </td>                     
                    
                </tr>
            	<tr>
                	<td class="first-column">2.</td>
                    <td>Pet Seller 2</td>
                    <td>XXXXXXXXXXX</td>
                    <td>1/12/2019</td>
                    <td>1/12/2019</td>
                    <td>
                    	<a href="editArticle.php" class="hover1">
                        	<img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                            <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                        </a>                    
                    </td>
                    <td>
                            <a class="hover1 open-confirm pointer">
                                <img src="img/delete1a.png" class="edit-icon1 hover1a" alt="Delete" title="Delete">
                                <img src="img/delete3a.png" class="edit-icon1 hover1b" alt="Delete" title="Delete">
                            </a>                     
                    </td>                    
                </tr>
            	<tr>
                	<td class="first-column">3.</td>
                    <td>Pet Seller 3</td>
                    <td>XXXXXXXXXXX</td>
                    <td>1/12/2019</td>
                    <td>1/12/2019</td>
                    <td>
                    	<a href="editArticle.php" class="hover1">
                        	<img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                            <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                        </a>                    
                    </td>
                    <td>
                            <a class="hover1 open-confirm pointer">
                                <img src="img/delete1a.png" class="edit-icon1 hover1a" alt="Delete" title="Delete">
                                <img src="img/delete3a.png" class="edit-icon1 hover1b" alt="Delete" title="Delete">
                            </a>                     
                    </td>                    
                </tr>                                
            </tbody> -->
        </table>
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Article Published !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Article Deleted !"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Fail to delete artilce !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "ERROR !!";
        }

        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
    
    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Article Updated !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to edit article !!";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

}
?>

</body>
</html>