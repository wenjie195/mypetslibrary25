<?php
// require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';
  
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

if(isset($_SESSION['defaultImage']) && isset($_SESSION['imageSix']) && isset($_SESSION['petsType'])) {
    $_SESSION['defaultImage'];
	$_SESSION['imageSix'];
	$_SESSION['petsType'];
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$conn = connDB();

	$sellerUid = rewrite($_POST['pets_uid']);
	
	$_SESSION['pets_uid'] = $sellerUid;

	// $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
	// $userData = $userDetails[0];

	$conn->close();
}

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Update Pets Image | Mypetslibrary" />
<title>Update Pets Image | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->

<script src="jquery.min.js"></script>
<script src="bootstrap.min.js"></script>
<script src="croppie.js"></script>
<link rel="stylesheet" href="bootstrap.min.css" />
<link rel="stylesheet" href="croppie.css" />

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
<form action="utilities/updatePetsImageSixFunction.php" method="POST" enctype="multipart/form-data">
	<p class="review-product-name text-center ow-margin-top20">Update Pet Image 6</p>
		<div class="width100 overflow text-center">
			<input type="file" class="center-input" name="image_one" id="image_one" accept="image/*" required>
			<input type="hidden" name="uid" id="uid" value="<?php echo $sellerUid?>" required>
		</div>
		<div class="width100 overflow text-center">  
            <button type="submit" class="green-button white-text clean2 edit-1-btn margin-auto" name="editImage" id="editImage">Confirm</button>
        </div>
        <div class="clear"></div> 
</form>
</div>
	
<div class="clear"></div>

<div class="width100 same-padding green-footer">
	<p class="footer-p white-text">© <?php echo $time;?> Mypetslibrary, All Rights Reserved.</p>
</div>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>

</body>
</html>