<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Puppy.php';
// require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$productDetails = getPuppy($conn, "WHERE status = 'Available' ORDER BY date_created DESC LIMIT 20 ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pet Food, Toys and Etc | Mypetslibrary" />
<title>Pet Food, Toys and Etc | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="fix-filter width100 small-padding overflow some-margin-top">
    <h1 class="green-text user-title left-align-title">Latest Puppies</h1>
    <!-- <div class="filter-div">
        <a class="open-filter2 filter-a green-a">Filter</a>
    </div> -->
</div>

<div class="clear"></div>

<div class="width100 small-padding overflow min-height-with-filter filter-distance">
    <div class="width103" id="app">
        <?php
        $conn = connDB();
        if($productDetails)
        {
            for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
            {
            ?>

                <a href='puppyDogForSale.php?id=<?php echo $productDetails[$cnt]->getUid();?>'  class="opacity-hover pointer">
                    <div class="shadow-white-box four-box-size ow-product-big-div opacity-hover">
                    	<div class="square">
                        <div class="width100 white-bg content progressive">
                            <img src="uploads/<?php echo $productDetails[$cnt]->getImageOne();?>" alt="<?php echo $productDetails[$cnt]->getBreed();?>" title="<?php echo $productDetails[$cnt]->getBreed();?>" class="preview width100 two-border-radius opacity-hover pointer lazy">
                        </div>
                        </div>

                        <?php 
                            $gender = $productDetails[$cnt]->getGender();
                            if($gender == 'Female')
                            {
                                $petsGender = 'F';
                            }
                            elseif($gender == 'Male')
                            {
                                $petsGender = 'M';
                            }
                        ?>

                        <p class="width100 text-overflow slider-product-name">
                            <?php echo $productDetails[$cnt]->getBreed();?> | <?php echo $petsGender;?>
                        </p>
                        <p class="slider-product-name  text-overflow">
                            <?php 
                                $price = $productDetails[$cnt]->getPrice();
                                $length = strlen($price);
                                if($length == 2)
                                {
                                    $hiddenPrice = "X";
                                }
                                elseif($length == 3)
                                {
                                    $hiddenPrice = "XX";
                                }
                                elseif($length == 4)
                                {
                                    $hiddenPrice = "XXX";
                                }
                                elseif($length == 5)
                                {
                                    $hiddenPrice = "XXXX";
                                }
                                elseif($length == 6)
                                {
                                    $hiddenPrice = "XXXXX";
                                }
                                elseif($length == 7)
                                {
                                    $hiddenPrice = "XXXXXX";
                                }
                            ?>
                            RM <?php echo $output = substr($price, 0, 1);  ;?><?php echo $hiddenPrice;?>
						</p>
                    </div>
                </a>

            <?php
            }
            ?>
        <?php
        }
        $conn->close();
        ?>
    </div>

    <div class="clear"></div>

    <!-- <button class="green-button white-text width100 clean2"> -->
        <a href="malaysia-cute-puppy-dog.php">
            <button class="green-button white-text width100 clean2">
            View More Puppies
            </button>
        </a>  
    <!-- </button> -->

    <div class="clear"></div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<style>
	.animated.slideUp{
		animation:none !important;}
	.animated{
		animation:none !important;}
	.product-a .hover1a{
		display:none !important;}
	.product-a .hover1b{
		display:inline-block !important;}	
</style>

<!-- <//?php include 'js.php'; ?> -->
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>
  <script src="js/index2.js"></script>
  <script>
    (function(){
      new Progressive({
        el: '#app',
        lazyClass: 'lazy',
        removePreview: true,
        scale: true
      }).fire()
    })()
  </script>

</body>
</html>