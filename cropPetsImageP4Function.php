<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Kitten.php';
require_once dirname(__FILE__) . '/classes/Pets.php';
require_once dirname(__FILE__) . '/classes/Puppy.php';
require_once dirname(__FILE__) . '/classes/Reptile.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$newPetsUid = $_SESSION['newpets_uid'];
$newPetsType = $_SESSION['newpets_type'];

$userType = $_SESSION['usertype'];

$conn = connDB();
$timestamp = time();

if(isset($_POST["image"]))
{
	$data = $_POST["image"];

	$image_array_1 = explode(";", $data);

	$image_array_2 = explode(",", $image_array_1[1]);

	$data = base64_decode($image_array_2[1]);

	// $imageName = time() . '.png';
	$imageName = $newPetsUid.time() . '.png';

	file_put_contents('uploads/'.$imageName, $data);

	if(isset($_POST["image"]))
	{
		$tableName = array();
		$tableValue =  array();
		$stringType =  "";
		//echo $imageName;

		if($imageName)
		{
		array_push($tableName,"image_four");
		array_push($tableValue,$imageName);
		$stringType .=  "s";
		}

		array_push($tableValue,$newPetsUid);
		$stringType .=  "s";
		$uploadPetsImageOne = updateDynamicData($conn,"pet_details"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
		if($uploadPetsImageOne)
		{

			if(isset($_POST["image"]))
			{
				$tableName = array();
				$tableValue =  array();
				$stringType =  "";
				//echo $imageName;
		
				if($imageName)
				{
				array_push($tableName,"image_four");
				array_push($tableValue,$imageName);
				$stringType .=  "s";
				}
		
				array_push($tableValue,$newPetsUid);
				$stringType .=  "s";

				if($newPetsType == 'Puppy')
				{
					$uploadPetsImageOne = updateDynamicData($conn,"puppy"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
				}
				elseif($newPetsType == 'Kitten')
				{
					$uploadPetsImageOne = updateDynamicData($conn,"kitten"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
				}
				elseif($newPetsType == 'Reptile')
				{
					$uploadPetsImageOne = updateDynamicData($conn,"reptile"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
				}

				unset($_SESSION['newpets_uid']);
				unset($_SESSION['newpets_type']);

				if($uploadPetsImageOne)
				{
				?>
					<div class="preview-img-div">
						<img src=uploads/<?php echo $imageName  ?>  class="width100"/>
					</div>
				<?php
				?>
		
				<h4><?php echo "Image Four"; ?></h4>
			
				<!-- <a href="petSummary.php" class="red-link">
					<button class="green-button white-text clean2 edit-1-btn margin-auto">Finish</button>
				</a> -->

				<?php
				
				if($userType == '0')
				{
					if($newPetsType == 'Puppy')
					{
					?>
						<a href="allPuppies.php" class="red-link">
							<button class="green-button white-text clean2 edit-1-btn margin-auto">Complete</button>
						</a>
					<?php
					}
					elseif($newPetsType == 'Kitten')
					{
					?>
						<a href="allKittens.php" class="red-link">
							<button class="green-button white-text clean2 edit-1-btn margin-auto">Complete</button>
						</a>
					<?php
					}
					elseif($newPetsType == 'Reptile')
					{
					?>
						<a href="allReptiles.php" class="red-link">
							<button class="green-button white-text clean2 edit-1-btn margin-auto">Complete</button>
						</a>
					<?php
					}
				}
				else
				{
					if($newPetsType == 'Puppy')
					{
					?>
						<a href="sellerAllPuppies.php" class="red-link">
							<button class="green-button white-text clean2 edit-1-btn margin-auto">Complete</button>
						</a>
					<?php
					}
					elseif($newPetsType == 'Kitten')
					{
					?>
						<a href="sellerAllKittens.php" class="red-link">
							<button class="green-button white-text clean2 edit-1-btn margin-auto">Complete</button>
						</a>
					<?php
					}
					elseif($newPetsType == 'Reptile')
					{
					?>
						<a href="sellerAllReptiles.php" class="red-link">
							<button class="green-button white-text clean2 edit-1-btn margin-auto">Complete</button>
						</a>
					<?php
					}
				}
				
				?>
		
				<?php
				}
				else
				{
					echo "FAIL Level 2";
				}
		
			}
			
		}
		else
		{
			echo "FAIL Level 2";
		}

	}


}

?>