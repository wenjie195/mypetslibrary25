<?php
// if (session_id() == "")
// {
//     session_start();
// }

include('config.php');

// $_SESSION['url'] = $_SERVER['REQUEST_URI'];

$facebook_output = '';

$facebook_helper = $facebook->getRedirectLoginHelper();

if(isset($_GET['code']))
{
    if(isset($_SESSION['access_token']))
    {
        $access_token = $_SESSION['access_token'];
    }
    else
    {
        $access_token = $facebook_helper->getAccessToken();
        $_SESSION['access_token'] = $access_token;
        $facebook->setDefaultAccessToken($_SESSION['access_token']);
    }

    $_SESSION['uid'] = '';
    $_SESSION['user_name'] = '';
    $_SESSION['user_email_address'] = '';
    $_SESSION['user_image'] = '';
    $_SESSION['fb_login'] = '';

    $graph_response = $facebook->get("/me?fields=name,email", $access_token);

    $facebook_user_info = $graph_response->getGraphUser();

    if(!empty($facebook_user_info['id']))
    {
        $_SESSION['user_image'] = 'http://graph.facebook.com/'.$facebook_user_info['id'].'/picture';
    }

    if(!empty($facebook_user_info['name']))
    {
        $_SESSION['user_name'] = $facebook_user_info['name'];
    }

    if(!empty($facebook_user_info['email']))
    {
        $_SESSION['user_email_address'] = $facebook_user_info['email'];
    }
    if(!empty($facebook_user_info['id']))
    {
        $_SESSION['uid'] = $facebook_user_info['id'];
        $_SESSION['fb_login'] = 1;
    }
}
else
{
    // Get login url
    $facebook_permissions = ['email']; // Optional permissions
    // $facebook_login_url = $facebook_helper->getLoginUrl('https://victory5.co/FBLogin/', $facebook_permissions);
    $facebook_login_url = $facebook_helper->getLoginUrl('https://dxforextrade88.com/mypetslibrary/', $facebook_permissions);
    // Render Facebook login button
}
if(isset($facebook_login_url))
{
    // echo $facebook_login_url;
}
else
{
    //success
    $addUsertoDB = insertDynamicData($conn,"fb_login", array("uid","name","email"), array($_SESSION['uid'],$_SESSION['user_name'],$_SESSION['user_email_address']),"sss");
    $_SESSION['usertype'] = 1;
    header('location: profile.php');

    // if($_SERVER['REQUEST_METHOD'] == 'POST')
    // {
    //     if(isset($_SESSION['url'])) 
    //     {
    //         $url = $_SESSION['url']; 
    //         header("location: $url");
    //     }
    //     else 
    //     {
    //         // header("location: $url");
    //         header('Location: ../profile.php');
    //     }
    // }
}
?>