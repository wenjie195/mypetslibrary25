<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Reviews.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

if (isset($_GET['search'])) {
  $reviews = getReviews($conn, " WHERE type = 'Reported' AND title=? ",array("title"),array($_GET['search']), "s");
}else {
  $reviews = getReviews($conn, " WHERE type = 'Reported' ");
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Reported Review | Mypetslibrary" />
<title>Reported Review | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance">
	<div class="width100">
        <div class="width100 ship-top-div">
            <h1 class="green-text h1-title"><a href="pendingReview.php" class="green-a">Pending Reviews</a> | <a href="approvedReview.php" class="green-a">Approved</a> | <a href="rejectedReview.php" class="green-a">Rejected</a> | Reported</h1>
            <div class="green-border"></div>
        </div>
        <div class="width100 ship-bottom-div">
          <?php //echo $_SERVER["PHP_SELF"] ?>
            <form action="search/searchFunction.php" method="post">
                    <?php
                    if (isset($_GET['search'])) {
                      ?>
                      <input class="line-input clean" type="text" value="<?php echo $_GET['search'] ?>" name="search" placeholder="Search">
                      <?php
                    }else {
                      ?>
                      <input class="line-input clean" type="text" name="search" placeholder="Search">
                      <?php
                    }
                     ?>
                     <!-- SKU and pet name-->
                     <input type="hidden" name="location" value="<?php echo $_SERVER["PHP_SELF"] ?>">
                     <button class="search-btn hover1 clean" type="submit">
                           <img src="img/search.png" class="visible-img hover1a" alt="Search" title="Search">
                           <img src="img/search2.png" class="visible-img hover1b" alt="Search" title="Search">
                     </button>
              </form>

        </div>
    </div>


    <div class="clear"></div>
	<div class="width100 scroll-div small-spacing">
    	<table class="order-table ow-width100">
        	<thead>
            	<tr>
                	<th class="first-column table-green-a">No.</th>
                    <th>For</th>
                    <th>Review</th>
                    <th>Reported by</th>
                    <th>Action</th>
                </tr>
            </thead>

            <tbody>
                <?php
                    if($reviews)
                    {
                        for($cnt = 0;$cnt < count($reviews) ;$cnt++)
                        {
                        ?>

                            <tr>
                                <td class="first-column"><?php echo ($cnt+1)?>.</td>
                                <td><a href="<?php echo "petSellerDetails.php?id=".$reviews[$cnt]->getCompanyUid() ?>" class="green-a table-green-a"><?php echo $reviews[$cnt]->getTitle();?></a></td>
                                <td>

                                        <?php
                                            $compUid = $reviews[$cnt]->getCompanyUid();

                                            $conn = connDB();

                                            $comDetails = getSeller($conn,"WHERE uid = ? ", array("uid") ,array($compUid),"s");
                                            $comData = $comDetails[0];
                                            $compLogo = $comData->getCompanyLogo();
                                            $compName = $comData->getCompanyName();
                                        ?>

                                    <div class="table-left-review-profile">
                                        <img src="uploads/<?php echo $compLogo;?>" class="profile-pic-css">
                                    </div>
                                    <div class="table-left-review-data">
                                        <!-- <p class="table-review-username-p">Jack Lim</p> -->
                                        <p class="table-review-username-p"><?php echo $compName;?></p>
                                        <div class="review-star-div">
                                            <?php $display = $reviews[$cnt]->getRating();?>

                                            <?php
                                                if ($display == 1)
                                                {
                                                    echo '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">';
                                                }
                                                else if ($display == 2)
                                                {
                                                    echo '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">';
                                                }
                                                else if ($display == 3)
                                                {
                                                    echo '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">',
                                                        '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">';
                                                }
                                                else if ($display == 4)
                                                {
                                                    echo '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">',
                                                        '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">';
                                                }
                                                else if ($display == 5)
                                                {
                                                    echo '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">',
                                                    '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">',
                                                    '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">';
                                                }
                                            ?>

                                            <!-- <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img"> -->
                                            <!-- <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                                            <img src="img/grey-star.png" alt="Review" title="Review" class="table-star-img"> -->
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="table-review-comment">
                                        <!-- <a href="./img/product-1.jpg" data-fancybox="images-preview"  >
                                                <img src="img/product-1.jpg" class="table-review-img opacity-hover" >
                                        </a> -->

                                        <a href="reviewImages/<?php echo $reviews[$cnt]->getImage();?>" data-fancybox="images-preview"  >
                                                <img src="reviewImages/<?php echo $reviews[$cnt]->getImage();?>" class="table-review-img opacity-hover"  onerror="this.style.display='none'" >
                                        </a>

                                        <!-- <p class="table-review-p">High quality product with a reasonable price!</p>
                                        <p class="table-date-p">12/12/2019</p> -->
                                        <!-- <p class="table-review-p"><?php //echo $reviews[$cnt]->getTitle();?></p> -->
                                        <p class="table-date-p"></p>
                                    </div>
                                </td>
                                <td>
                                    <!-- <div class="table-left-review-profile">
                                        <img src="img/profile-pic.jpg" class="profile-pic-css">
                                    </div>                     -->

                                    <!-- <div data-fancybox="images-preview" >
                                        <img src="reviewImages/<?php echo $reviews[$cnt]->getImage();?>" class="table-review-img opacity-hover">
                                    </div>  -->

                                    <div class="table-left-review-data">
                                        <p class="table-review-username-p"><?php echo $reviews[$cnt]->getAuthorName();?></p>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="table-review-comment">
                                            <p class="table-review-p"><?php echo $reviews[$cnt]->getParagraphOne();?></p>
                                            <p class="table-date-p"><?php echo $date = date("d/m/Y",strtotime($reviews[$cnt]->getDateCreated()));?></p>
                                    </div>
                                </td>
                                <td>
                                    <form method="POST" action="utilities/adminKeepReportReview.php" class="float-left">
                                        <button class="approve-button right-margin-10px opacity-hover clean transparent-button" name="review_uid" value="<?php echo $reviews[$cnt]->getUid();?>">
                                            <img src="img/approve.png" class="approve-png" alt="Keep the Review" title="Keep the Review">
                                        </button>
                                    </form>

                                    <form method="POST" action="utilities/adminDeleteReportReview.php" class="float-left">
                                        <input class="input-name clean input-textarea" type="hidden" value="<?php echo $reviews[$cnt]->getRating();?>" name="rate" id="rate" readonly>
                                        <input class="input-name clean input-textarea" type="hidden" value="<?php echo $reviews[$cnt]->getCompanyUid();?>" name="issue_uid" id="issue_uid" readonly>
                                        <button class="approve-button opacity-hover clean transparent-button" name="review_uid" value="<?php echo $reviews[$cnt]->getUid();?>">
                                            <img src="img/reject.png" class="approve-png" alt="Remove Review" title="Remove Review">
                                        </button>
                                    </form>
                                </td>
                            </tr>

                        <?php
                        }
                    }
                ?>
            </tbody>

            <!-- <tbody>
            	<tr>
                	<td class="first-column">1.</td>
                    <td><a href="malaysia-pets-products-details.php" class="green-a table-green-a">Pedigree Dentastix Puppy 56g Dog Treats</a></td>
                    <td>
                    <div class="table-left-review-profile">
                        <img src="img/pet-seller2.jpg" class="profile-pic-css">
                    </div>
                    <div class="table-left-review-data">
                        <p class="table-review-username-p">Jack Lim</p>
                        <div class="review-star-div">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/grey-star.png" alt="Review" title="Review" class="table-star-img">
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="table-review-comment">
                           <a href="./img/product-1.jpg" data-fancybox="images-preview"  >
                                <img src="img/product-1.jpg" class="table-review-img opacity-hover" >
                           </a>
                       	   <p class="table-review-p">High quality product with a reasonable price!</p>
                           <p class="table-date-p">12/12/2019</p>
                    </div>
                    </td>
                    <td>
                    <div class="table-left-review-profile">
                        <img src="img/profile-pic.jpg" class="profile-pic-css">
                    </div>
                    <div class="table-left-review-data">
                        <p class="table-review-username-p">User 1</p>
                    </div>
                    <div class="clear"></div>
                    <div class="table-review-comment">
                       	   <p class="table-review-p">Spam good review.</p>
                           <p class="table-date-p">12/12/2019</p>
                    </div>
                    </td>
                    <td>
                    <button class="approve-button right-margin-10px opacity-hover clean transparent-button"><img src="img/approve.png" class="approve-png" alt="Keep the Review" title="Keep the Review"></button>
                    <button class="approve-button opacity-hover clean transparent-button"><img src="img/reject.png" class="approve-png" alt="Remove Review" title="Remove Review"></button>

                    </td>

                </tr>
            	<tr>
                	<td class="first-column table-green-a">2.</td>
                    <td><a href="malaysia-pets-products-details.php" class="green-a table-green-a">Pedigree Dentastix Puppy 56g Dog Treats</a></td>
                    <td>
                        <div class="table-left-review-profile">
                            <img src="img/pet-seller2.jpg" class="profile-pic-css">
                        </div>
                        <div class="table-left-review-data">
                            <p class="table-review-username-p">Jack Lim</p>
                            <div class="review-star-div">
                                <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                                <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                                <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                                <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                                <img src="img/grey-star.png" alt="Review" title="Review" class="table-star-img">
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="table-review-comment">
                               <a href="./img/product-1.jpg" data-fancybox="images-preview"  >
                                    <img src="img/product-1.jpg" class="table-review-img opacity-hover" >
                               </a>
                               <p class="table-review-p">High quality product with a reasonable price!</p>
                               <p class="table-date-p">12/12/2019</p>
                        </div>
                    </td>
                    <td>
                        <div class="table-left-review-profile">
                            <img src="img/profile-pic.jpg" class="profile-pic-css">
                        </div>
                        <div class="table-left-review-data">
                            <p class="table-review-username-p">User 1</p>
                        </div>
                        <div class="clear"></div>
                        <div class="table-review-comment">
                               <p class="table-review-p">Spam good review.</p>
                               <p class="table-date-p">12/12/2019</p>
                        </div>
                    </td>
                    <td>
                    <button class="approve-button right-margin-10px opacity-hover clean transparent-button"><img src="img/approve.png" class="approve-png" alt="Keep the Review" title="Keep the Review"></button>
                    <button class="approve-button opacity-hover clean transparent-button"><img src="img/reject.png" class="approve-png" alt="Remove Review" title="Remove Review"></button>

                    </td>

                </tr>
            	<tr>
                	<td class="first-column table-green-a">3.</td>
                    <td><a href="malaysia-pets-products-details.php" class="green-a table-green-a">Pedigree Dentastix Puppy 56g Dog Treats</a></td>
                    <td>
                    <div class="table-left-review-profile">
                        <img src="img/pet-seller2.jpg" class="profile-pic-css">
                    </div>
                    <div class="table-left-review-data">
                        <p class="table-review-username-p">Jack Lim</p>
                        <div class="review-star-div">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/grey-star.png" alt="Review" title="Review" class="table-star-img">
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="table-review-comment">
                           <a href="./img/product-1.jpg" data-fancybox="images-preview"  >
                                <img src="img/product-1.jpg" class="table-review-img opacity-hover" >
                           </a>
                       	   <p class="table-review-p">High quality product with a reasonable price!</p>
                           <p class="table-date-p">12/12/2019</p>
                    </div>
                    </td>
                    <td>
                        <div class="table-left-review-profile">
                            <img src="img/profile-pic.jpg" class="profile-pic-css">
                        </div>
                        <div class="table-left-review-data">
                            <p class="table-review-username-p">User 1</p>
                        </div>
                        <div class="clear"></div>
                        <div class="table-review-comment">
                               <p class="table-review-p">Spam good review.</p>
                               <p class="table-date-p">12/12/2019</p>
                        </div>
                    </td>
                    <td>
                    <button class="approve-button right-margin-10px opacity-hover clean transparent-button"><img src="img/approve.png" class="approve-png" alt="Keep the Review" title="Keep the Review"></button>
                    <button class="approve-button opacity-hover clean transparent-button"><img src="img/reject.png" class="approve-png" alt="Remove Review" title="Remove Review"></button>

                    </td>

                </tr>
            </tbody> -->
        </table>
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Review remain as the same !!";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Review Deleted !!";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Fail to update reviews status on Reported Review table !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "ERROR 2 !!";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Fail to update review status on Review table !";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "ERROR !!";
        }

        else if($_GET['type'] == 7)
        {
            $messageType = "Fail to update seller rating !!";
        }
        else if($_GET['type'] == 8)
        {
            $messageType = "ERROR on update seller rating !!";
        }

        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>
