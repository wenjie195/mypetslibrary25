<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Reviews.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$reviewsPending = getReviews($conn, " WHERE display = 'Pending' ");
$reviewsApproved = getReviews($conn, " WHERE display = 'Yes' ");
$reviewsRejected = getReviews($conn, " WHERE display = 'Rejected' ");
$reviewsReported = getReviews($conn, " WHERE type = 'Reported' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Review Summary | Mypetslibrary" />
<title>Review Summary | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance">
	<h1 class="green-text h1-title">Review Summary</h1>
	<div class="green-border"></div>
    <div class="clear"></div>
    <div class="width100 border-separation">
    	<a href="pendingReview.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box">
                <img src="img/review-pet.png" alt="Pending Reviews" title="Pending Reviews" class="four-div-img">
                <p class="four-div-p"><b>Pending Reviews</b></p>
                <?php
                if($reviewsPending)
                {   
                    $totalPending = count($reviewsPending);
                }
                else
                {   $totalPending = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalPending;?></b></p>
            </div>
        </a>
        <a href="approvedReview.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box second-four-div-box left-four-div">
                <img src="img/approve.png" alt="Approved Reviews" title="Approved Reviews" class="four-div-img">
                <p class="four-div-p"><b>Approved Reviews</b></p>
                <?php
                if($reviewsApproved)
                {   
                    $totalApproved = count($reviewsApproved);
                }
                else
                {   $totalApproved = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalApproved;?></b></p>
            </div> 
        </a>
        <a href="rejectedReview.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box right-four-div">
                <img src="img/reject.png" alt="Rejected Review" title="Rejected Review" class="four-div-img">
                <p class="four-div-p"><b>Rejected Review</b></p>
                <?php
                if($reviewsRejected)
                {   
                    $totalRejected = count($reviewsRejected);
                }
                else
                {   $totalRejected = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalRejected;?></b></p>
            </div>  
        </a>
<!--        <a href="reportedReview.php" class="opacity-hover">       
            <div class="white-dropshadow-box four-div-box second-four-div-box forth-div">
                <img src="img/attention.png" alt="Reported Review" title="Reported Review" class="four-div-img">
                <p class="four-div-p"><b>Reported Review</b></p>
                <?php
                if($reviewsReported)
                {   
                    $totalReported = count($reviewsReported);
                }
                else
                {   $totalReported = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalReported;?></b></p>
            </div>  
        </a>-->
        </div>
        <div class="clear"></div>      


</div>
<div class="clear"></div>


<?php include 'js.php'; ?>





</body>
</html>