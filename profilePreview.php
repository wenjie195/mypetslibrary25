<?php
require_once dirname(__FILE__) . '/sellerAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Pets.php';
require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';
  
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];
// $sellerName = $userData->getName();
$sellerName = $userData->getUid();
// $sellerUid = $userData->getUid();

$seller = getSeller($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$sellerDetails = $seller[0];

// $sellerPets = getPetsDetails($conn);
// $sellerPets = getPetsDetails($conn, "WHERE seller =? ",array("seller"),array($sellerName),"s");
// $sellerPets = getPetsDetails($conn, "WHERE seller =? AND status = 'Sold' ",array("seller"),array($sellerName),"s");

$sellerPets = getPetsDetails($conn, "WHERE seller =? ",array("seller"),array($sellerName),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Profile Preview | Mypetslibrary" />
<title>Profile Preview | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary - Pet Seller Name, Puppy Seller, Delivery Services, Pet Grooming." />
<meta name="description" content="Mypetslibrary - Pet Seller Name, Puppy Seller, Delivery Services, Pet Grooming." />
<meta name="keywords" content="pet seller name, Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance">
	<div class="big-info-div width100 overflow">

        <div class="left-image-div">
            <div class="item">            
                <div class="clearfix">
                    <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                        <!-- <li data-thumb="img/pet-seller.jpg" class="pet-slider-li"> 
                            <img src="img/pet-seller.jpg" class="pet-slider-img"  alt="Pet Seller Name" title="Pet Seller Name"/>
                        </li>
                        <li data-thumb="img/pet-seller2.jpg" class="pet-slider-li"> 
                            <img src="img/pet-seller2.jpg" class="pet-slider-img" alt="Pet Seller Name" title="Pet Seller Name"  />
                        </li> -->

                        <li data-thumb="sellerProfile/<?php echo $sellerDetails->getCompanyLogo();?>" class="pet-slider-li"> 
                            <img src="sellerProfile/<?php echo $sellerDetails->getCompanyLogo();?>" class="pet-slider-img"  alt="<?php echo $sellerDetails->getCompanyName();?>" title="<?php echo $sellerDetails->getCompanyName();?>"/>
                        </li>
                        <li data-thumb="sellerProfile/<?php echo $sellerDetails->getCompanyPic();?>" class="pet-slider-li"> 
                            <img src="sellerProfile/<?php echo $sellerDetails->getCompanyPic();?>" class="pet-slider-img" alt="<?php echo $sellerDetails->getCompanyName();?>" title="<?php echo $sellerDetails->getCompanyName();?>"  />
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        
        <div class="right-content-div2">
        
            <!-- <div class="left-address-info">
            	<h1 class="green-text pet-name">Fur Fur Fur Fur Fur Fur Fur Fur Fur Fur Fur</h1>
                <p class="green-text breed-p">Street No., Street, City, Postcode, Georgetown, Penang</p>
                <p class="green-text breed-p"><a href="tel:+60383190000" class="contact-icon hover1">010 - 100 1000</a></p>
            </div> -->

            <div class="left-address-info">
            	<h1 class="green-text pet-name"><?php echo $sellerDetails->getCompanyName();?></h1>
                <p class="green-text breed-p"><?php echo $sellerDetails->getAddress();?> <?php echo $sellerDetails->getState();?></p>
                <p class="green-text breed-p"><a href="tel:<?php echo $sellerDetails->getContactNo();?>" class="contact-icon hover1"><?php echo $sellerDetails->getContactNo();?></a></p>
            </div>

            <div class="right-info-div profile-right-info">
            	<a href="editSellerProfile.php"><div class="green-button white-text puppy-button edit-profile-btn">Edit</div></a>
                <a href="tel:+60383190000" class="contact-icon hover1 three-button-width call-button-img">
                    <img src="img/call.png" class="hover1a" alt="Call" title="Call">
                    <img src="img/call2.png" class="hover1b" alt="Call" title="Call">
                </a>
                <!--<a class="contact-icon hover1 three-button-width mid-three-button-width">
                    <img src="img/sms.png" class="hover1a" alt="SMS" title="SMS">
                    <img src="img/sms2.png" class="hover1b" alt="SMS" title="SMS">
                </a> --> 
                <a class="contact-icon hover1 three-button-width second-a profile-whatsapp-a">
                    <img src="img/whatsapp.png" class="hover1a" alt="Whatsapp" title="Whatsapp">
                    <img src="img/whatsapp2.png" class="hover1b" alt="Whatsapp" title="Whatsapp">
                </a>  
            </div>

            <div class="clear"></div>

            <!-- <a href="petSellerReview.php"> -->
            <a href='petSellerReview.php?id=<?php echo $sellerDetails->getUid();?>'>
                <div class="review-div hover1">
                	<div class="seller-review1">
                        <p class="left-review-p grey-p">Reviews</p>
                        <!-- <p class="left-review-mark">4/5</p> -->
                        <p class="left-review-mark"><?php echo $display = $sellerDetails->getRating();?>/5</p>
                        <p class="right-review-star seller-review2 seller-profile-star">

                            <?php
                                if ($display == 1)
                                {
                                    echo '<img src="img/yellow-star.png" class="star-img">';
                                }
                                else if ($display == 2)
                                {
                                    echo '<img src="img/yellow-star.png" class="star-img">','<img src="img/yellow-star.png" class="star-img">';
                                }
                                else if ($display == 3)
                                {
                                    echo '<img src="img/yellow-star.png" class="star-img">','<img src="img/yellow-star.png" class="star-img">',
                                        '<img src="img/yellow-star.png" class="star-img">';
                                }
                                else if ($display == 4)
                                {
                                    echo '<img src="img/yellow-star.png" class="star-img">','<img src="img/yellow-star.png" class="star-img">',
                                        '<img src="img/yellow-star.png" class="star-img">','<img src="img/yellow-star.png" class="star-img">';
                                }
                                else if ($display == 5)
                                {
                                    echo '<img src="img/yellow-star.png" class="star-img">','<img src="img/yellow-star.png" class="star-img">',
                                    '<img src="img/yellow-star.png" class="star-img">','<img src="img/yellow-star.png" class="star-img">',
                                    '<img src="img/yellow-star.png" class="star-img">';
                                }
                            ?> 

                            <!-- <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                            <img src="img/grey-star.png" alt="Review" title="Review" class="star-img last-star"> -->
                        </p>
                    </div>
                    <div class="seller-review3">
                        <p class="right-arrow">
                            <img src="img/arrow.png" alt="Review" title="Review" class="arrow-img hover1a">
                            <img src="img/arrow2.png" alt="Review" title="Review" class="arrow-img hover1b">
                        </p>
                    </div>	
                </div>
            </a>

            <div class="clear"></div>

                <!-- <div class="pet-details-div">
                    <table class="pet-table">
                        <tr>
                            <td class="grey-p">Type of Breed</td>
                            <td class="grey-p">:</td>
                            <td>Pomeranian, Pembroke Welsh Corgi, French Bulldog</td>
                        </tr>
                        <tr>
                            <td class="grey-p">Experience</td>
                            <td class="grey-p">:</td>
                            <td>10 Years</td>
                        </tr>  
                        <tr>
                            <td class="grey-p">Joined Date</td>
                            <td class="grey-p">:</td>
                            <td>1/1/2019</td>
                        </tr>
                        <tr>
                            <td class="grey-p">Services</td>
                            <td class="grey-p">:</td>
                            <td>Puppy Seller, Delivery Services, Pet Grooming</td>
                        </tr>  
                        <tr>
                            <td class="grey-p">Other Info</td>
                            <td class="grey-p">:</td>
                            <td>www.facebook.com/xxxxx</td>
                        </tr>                                                                                                    
                    </table>
                </div>   -->

            <div class="pet-details-div">
                <table class="pet-table">
                    <tr>
                        <td class="grey-p">Type of Breed</td>
                        <td class="grey-p">:</td>
                        <td><?php echo $sellerDetails->getBreedType();?></td>
                    </tr>
                    <tr>
                        <td class="grey-p">Experience</td>
                        <td class="grey-p">:</td>
                        <td><?php echo $sellerDetails->getExperience();?> Years</td>
                    </tr>  
                    <tr>
                        <td class="grey-p">Joined Date</td>
                        <td class="grey-p">:</td>
                        <td>
                            <?php echo $date = date("d-m-Y",strtotime($sellerDetails->getDateCreated()));?>
                        </td>
                    </tr>
                    <tr>
                        <td class="grey-p">Services</td>
                        <td class="grey-p">:</td>
                        <td><?php echo $sellerDetails->getServices();?></td>
                    </tr>  
                    <tr>
                        <td class="grey-p">Other Info</td>
                        <td class="grey-p">:</td>
                        <td><?php echo $sellerDetails->getOtherInfo();?></td>
                    </tr>                                                                                                    
                </table>
            </div>  
                       
        </div>
    </div>

    <div class="clear"></div>

    <h1 class="green-text seller-h1">Seller Listings</h1>

    <div class="clear"></div>    

    <div class="width103">

        <?php
        $conn = connDB();
        if($sellerPets)
        {
            for($cnt = 0;$cnt < count($sellerPets) ;$cnt++)
            {
            ?>
                <a href='petsDetails.php?id=<?php echo $sellerPets[$cnt]->getUid();?>'>
                    <div class="shadow-white-box four-box-size">
                    <!-- <div class="sold-label2">Sold</div> -->
                    <div class="width100 white-bg">
                    <img src="uploads/<?php echo $sellerPets[$cnt]->getImageOne();?>" alt="<?php echo $sellerPets[$cnt]->getBreed();?>" title="<?php echo $sellerPets[$cnt]->getBreed();?>" class="width100 two-border-radius">
                    </div>
					<!-- Display none or add class hidden if the dog not yet sold -->
                    <!--	<div class="sold-label sold-label3">Sold</div>    -->
                       
                    <?php 
                        $status = $sellerPets[$cnt]->getStatus();
                        if($status == 'Sold')
                        {
                        ?>
                            <div class="sold-label sold-label3">Sold</div>
                        <?php
                        }
                        else
                        {}
                    ?>

                    <div class="width100 product-details-div">
                    <p class="width100 text-overflow slider-product-name"><?php echo $sellerPets[$cnt]->getBreed();?> | <?php echo $sellerPets[$cnt]->getGender();?></p>
                    <p class="slider-product-name">RM<?php echo $sellerPets[$cnt]->getPrice();?></p>
                    <!--<p class="width100 text-overflow slider-location"><?php echo $sellerPets[$cnt]->getLocation();?></p>-->
                    </div>
                    </div>
                </a> 
            <?php
            }
            ?>
        <?php
        }
        $conn->close();
        ?>

    </div>

        <!-- Display none or add class hidden if the dog not yet sold -->
        <!-- <div class="width103">
            <a href="puppyDetails.php">
            <div class="shadow-white-box featured four-box-size">
            <div class="sold-label2">Sold</div>
            <div class="width100 white-bg">
            <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
            </div>
            <div class="width100 product-details-div">
            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
            <p class="slider-product-price">RM5XXX</p>
            <p class="width100 text-overflow slider-location">Penang</p>
            </div>
            </div>
            </a> 
            <a href="puppyDetails.php">
            <div class="shadow-white-box featured four-box-size">
            <div class="width100 white-bg">
            <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
            </div>
            <div class="width100 product-details-div">
            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
            <p class="slider-product-price">RM5XXX</p>
            <p class="width100 text-overflow slider-location">Penang</p>
            </div>
            </div>
            </a>        
            <a href="puppyDetails.php">
            <div class="shadow-white-box featured four-box-size">
            <div class="width100 white-bg">
            <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
            </div>
            <div class="width100 product-details-div">
            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
            <p class="slider-product-price">RM5XXX</p>
            <p class="width100 text-overflow slider-location">Penang</p>
            </div>
            </div>
            </a> 
            <a href="puppyDetails.php">
            <div class="shadow-white-box featured four-box-size">
            <div class="width100 white-bg">
            <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
            </div>
            <div class="width100 product-details-div">
            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
            <p class="slider-product-price">RM5XXX</p>
            <p class="width100 text-overflow slider-location">Penang</p>
            </div>
            </div>
            </a>         
            <a href="puppyDetails.php">
            <div class="shadow-white-box four-box-size">
            <div class="width100 white-bg">
            <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
            </div>
            <div class="width100 product-details-div">
            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
            <p class="slider-product-price">RM5XXX</p>
            <p class="width100 text-overflow slider-location">Penang</p>
            </div>
            </div>
            </a> 
            <a href="puppyDetails.php">
            <div class="shadow-white-box four-box-size">
            <div class="width100 white-bg">
            <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
            </div>
            <div class="width100 product-details-div">
            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
            <p class="slider-product-price">RM5XXX</p>
            <p class="width100 text-overflow slider-location">Penang</p>
            </div>
            </div>
            </a>        
            <a href="puppyDetails.php">
            <div class="shadow-white-box four-box-size">
            <div class="width100 white-bg">
            <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
            </div>
            <div class="width100 product-details-div">
            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
            <p class="slider-product-price">RM5XXX</p>
            <p class="width100 text-overflow slider-location">Penang</p>
            </div>
            </div>
            </a> 
            <a href="puppyDetails.php">
            <div class="shadow-white-box four-box-size">
            <div class="width100 white-bg">
            <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
            </div>
            <div class="width100 product-details-div">
            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
            <p class="slider-product-price">RM5XXX</p>
            <p class="width100 text-overflow slider-location">Penang</p>
            </div>
            </div>
            </a>                  	
        </div> -->
                       
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>


<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Profile Successfully"; 
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Update Password Successfully"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>