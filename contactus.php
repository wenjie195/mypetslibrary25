<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Contact Us | Mypetslibrary" />
<title>Contact Us | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Contact Us, Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 menu-distance3 same-padding min-height2">
	<div class="contact-div">
        <h2 class="green-text h2-title">Contact Us</h2>
        <div class="green-border"></div>
    </div>
    <div class="clear"></div>
	<div class="width100 text-center">
   	  <img src="img/mypetslibrary2.png" class="middle-logo" alt="Mypetslibrary" title="Mypetslibrary">
    </div>
  <div class="clear"></div>
    <p class="center-content">
    	Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide.  Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us.
        <br>
        Email: <b>mypetslibrary@gmail.com</b> 
    </p>
    <div class="contact-div">
        <form id="contactform" method="post" action="index.php" class="form-class extra-margin">
                      
                      <input type="text" name="name" placeholder="Name" class="input-name clean" required >
                      
                      <input type="email" name="email" placeholder="Email" class="input-name clean" required >                  
                      
                      <input type="text" name="telephone" placeholder="Contact Number" class="input-name clean" required >
    
    
                                      
                      <textarea name="comments" placeholder="Message" class="input-name input-message clean" ></textarea>
                      <div class="clear"></div>
                      <input type="radio" name="contact-option" value="contact-more-info" class="radio1 float-left clean" required><p class="opt-msg left"> I want to be updated with more information about your company's news and future promotions</p>
                      <div class="clear"></div>
                      <input type="radio" name="contact-option" value="contact-on-request" class="radio1 float-left clean"  required><p class="opt-msg left"> I just want to be contacted based on my request/inquiry</p>
                      <div class="clear"></div>
                       <div class="width100 text-center">
                      <input type="submit" name="send_email_button" value="SEND US A MESSAGE" class="form-submit mid-btn-width green-button white-text clean pointer">
                      </div>
                </form>   
                <div class="width100 bottom-spacing"></div> 
                <div class="clear"></div>
                <h2 class="green-text h2-title">About Us</h2>
                <div class="green-border"></div>                
                <p>
                    As digital shopping trend is getting viral and common nowadays, the objective of Mypetslibrary establishment is to minimize the risk of fraud and to avoid both parties dealing with impostors which commonly encountered by the society today.<br><br>Besides, buyers could be facing the issues such as dealing with irresponsible sellers where the pets sold are unhealthy, problematic or different quality as per promised etc, nevertheless these issues will be avoided when buyers scroll for their favourite pets in Mypetslibrary platform.<br><br>Each of the pets published here underwent quality control and data verified, our breeders and sellers have up to 3 years experiences in this pet industry and they are strictly verified by our professional team before joining into this platform. We serve the best to ensure passionate buyers meet responsible sellers where these lovely healthy pets able to find a forever loving home.
                </p>                
                <div class="width100 bottom-spacing"></div> 
                <div class="clear"></div>                
                <h2 class="green-text h2-title">Our Mission</h2>
                <div class="green-border"></div>                
                <p>
                    <ul>
                    	<li>To protect both sellers and buyer’s interest.</li>
                        <li>To offer variety of pets and breeds.</li>
                        <li>To provide a sense of security and confidence by establishing fair and transparent pets trading across nationwide between buyers and sellers.</li>
                        <li>To ensure Mypetslibrary’s visitors able to bring their preferred breed and favourite pets home by facilitating their purchasing procedure dealing with reputable and verified breeders.</li>
                        <li>To connect with every single top and professional sellers nationwide to enhance their reputation and images in Mypetslibrary.</li>
                    </ul>
                </p>                 
                <div class="width100 bottom-spacing"></div> 
                <div class="clear"></div> 
                <h2 class="green-text h2-title">Our Vision</h2>
                <div class="green-border"></div>                
                <p>
                    To become world leading and fastest-growing online pets store covering every category of pets.
                </p>                
                <div class="width100 bottom-spacing"></div> 
                <div class="clear"></div>
    </div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>
<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['send_email_button'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "mypetslibrary@gmail.com";
    $email_subject = "Contact Form via Mypetslibrary website";
 
    function died($error) 
	{
        // your error code can go here
		echo '<script>alert("We are very sorry, but there were error(s) found with the form you submitted.\n\nThese errors appear below.\n\n';
		echo $error;
        echo '\n\nPlease go back and fix these errors.\n\n")</script>';
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
		!isset($_POST['telephone'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
	
     
 
    $first_name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
	$telephone = $_POST['telephone']; //required
    $comments = $_POST['comments']; 
    $contactOption = $_POST['contact-option']; // required
    $contactMethod = null;
	
	//$error_message = '<script>alert("The name you entered does not appear to be valid.");</script>';
	//if($first_name == ""){
	//	echo $error_message;
	//}

    if($contactOption == null || $contactOption == ""){
        $contactMethod = "don\'t bother me";
    }else if($contactOption == "contact-more-info"){
        $contactMethod = "I want to be contacted with more information about your company's offering marketing services and consulting";
    }else if($contactOption == "contact-on-request"){
        $contactMethod = "I just want to be contacted based on my request/ inquiry";
    }else{
        $contactMethod = "error getting contact options";
		$error_message .="Error getting contact options\n\n";
    }

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The email address you entered does not appear to be valid.\n';
  }
 
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The name you entered does not appear to be valid.\n';
  }
 


 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n";
 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
    $email_message .= "Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\n";   
    $email_message .= "Message : ".clean_string($comments)."\n";
    $email_message .= "Contact Option : ".clean_string($contactMethod)."\n";

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  
echo '<script>alert("Thank you! We will be in contact shortly!")</script>';

?>
<!-- include your own success html here -->

<!--Thank you for contacting us. We will be in touch with you very soon.-->
<?php
 
}
?>

</body>
</html>