<?php
require_once dirname(__FILE__) . '/sellerAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';
require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';
  
require_once dirname(__FILE__) . '/classes/Kitten.php';
require_once dirname(__FILE__) . '/classes/Puppy.php';
require_once dirname(__FILE__) . '/classes/Reptile.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];
// $sellerName = $userData->getName();
$sellerName = $userData->getUid();

$userAmount = getUser($conn," WHERE user_type = 1 ");
$sellerAmount = getSeller($conn);

// $kittenAmount = getKitten($conn);
// $puppyAmount = getPuppy($conn);
// $reptileAmount = getReptile($conn);

$availableKitten = getKitten($conn, "WHERE seller =? AND status = 'Available' ",array("seller"),array($sellerName),"s");
$availablePuppy = getPuppy($conn, "WHERE seller =? AND status = 'Available' ",array("seller"),array($sellerName),"s");
$availableReptile = getReptile($conn, "WHERE seller =? AND status = 'Available' ",array("seller"),array($sellerName),"s");

$soldKitten = getKitten($conn, "WHERE seller =? AND status = 'Sold' ",array("seller"),array($sellerName),"s");
$soldPuppy = getPuppy($conn, "WHERE seller =? AND status = 'Sold' ",array("seller"),array($sellerName),"s");
$soldReptile = getReptile($conn, "WHERE seller =? AND status = 'Sold' ",array("seller"),array($sellerName),"s");

$pendingKitten = getKitten($conn, "WHERE seller =? AND status = 'Pending' ",array("seller"),array($sellerName),"s");
$pendingPuppy = getPuppy($conn, "WHERE seller =? AND status = 'Pending' ",array("seller"),array($sellerName),"s");
$pendingReptile = getReptile($conn, "WHERE seller =? AND status = 'Pending' ",array("seller"),array($sellerName),"s");

$pendingArticle = getArticles($conn, "WHERE author_uid =? AND display = 'Pending' ",array("author_uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Seller Dashboard | Mypetslibrary" />
<title>Seller Dashboard | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance">
	<h1 class="green-text h1-title">Dashboard</h1>
	<div class="green-border"></div>
    <div class="clear"></div>
    <div class="width100 border-separation">
    	<a href="sellerAllPets.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box">
                <img src="img/kitten.png" alt="Total Available Pets" title="Total Available Pets" class="four-div-img">
                <p class="four-div-p">Total Available Pets</p>

                <?php
                if($availableKitten)
                {   
                    $totalAvailableKitten = count($availableKitten);
                }
                else
                {   $totalAvailableKitten = 0;   }
                ?>
                <?php
                if($availablePuppy)
                {   
                    $totalAvailablePuppy = count($availablePuppy);
                }
                else
                {   $totalAvailablePuppy = 0;   }
                ?>
                <?php
                if($availableReptile)
                {   
                    $totalAvailableReptile = count($availableReptile);
                }
                else
                {   $totalAvailableReptile = 0;   }
                ?>

                <?php
                    $totalAvailablePets = $totalAvailableKitten + $totalAvailablePuppy + $totalAvailableReptile ;
                ?>

                <p class="four-div-amount-p"><b><?php echo $totalAvailablePets;?></b></p>

                <!-- <p class="four-div-amount-p"><b>1000</b></p> -->
            </div>
        </a>
		<a href="sellerTotalSoldPets.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box second-four-div-box left-four-div">
                <img src="img/puppy.png" alt="Total Sold Pets" title="Total Sold Pets" class="four-div-img">
                <p class="four-div-p">Total Sold Pets</p>

                <?php
                if($soldKitten)
                {   
                    $totalSoldKitten = count($soldKitten);
                }
                else
                {   $totalSoldKitten = 0;   }
                ?>
                <?php
                if($soldPuppy)
                {   
                    $totalSoldPuppy = count($soldPuppy);
                }
                else
                {   $totalSoldPuppy = 0;   }
                ?>
                <?php
                if($soldReptile)
                {   
                    $totalSoldReptile = count($soldReptile);
                }
                else
                {   $totalSoldReptile = 0;   }
                ?>

                <?php
                    $totalSoldPets = $totalSoldKitten + $totalSoldPuppy + $totalSoldReptile ;
                ?>

                <!-- <p class="four-div-amount-p"><b>50</b></p> -->
                <p class="four-div-amount-p"><b><?php echo $totalSoldPets;?></b></p>
            </div> 
		</a>
        <a href="sellerPendingPets.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box right-four-div">
                <img src="img/pending-pet.png" alt="Pending Pets" title="Pending Pets" class="four-div-img">
                <p class="four-div-p">Pending Pets</p>

                <?php
                if($pendingKitten)
                {   
                    $totalPendingKitten = count($pendingKitten);
                }
                else
                {   $totalPendingKitten = 0;   }
                ?>
                <?php
                if($pendingPuppy)
                {   
                    $totalPendingPuppy = count($pendingPuppy);
                }
                else
                {   $totalPendingPuppy = 0;   }
                ?>
                <?php
                if($pendingReptile)
                {   
                    $totalPendingReptile = count($pendingReptile);
                }
                else
                {   $totalPendingReptile = 0;   }
                ?>

                <?php
                    $totalPendingPets = $totalPendingKitten + $totalPendingPuppy + $totalPendingReptile ;
                ?>

                <!-- <p class="four-div-amount-p"><b>150</b></p> -->
                <p class="four-div-amount-p"><b><?php echo $totalPendingPets;?></b></p>
            </div>  
        </a>
        <a href="sellerPendingArticle.php" class="opacity-hover">       
            <div class="white-dropshadow-box four-div-box second-four-div-box forth-div">
                <img src="img/pet-article.png" alt="Pending Articles" title="Pending Articles" class="four-div-img">
                <p class="four-div-p">Pending Articles</p>
                <?php
                if($pendingArticle)
                {   
                    $totalPendingArticle = count($pendingArticle);
                }
                else
                {   $totalPendingArticle = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalPendingArticle;?></b></p>
                <!-- <p class="four-div-amount-p"><b>3</b></p> -->
            </div>  
        </a>
        </div>
        <div class="clear"></div>

</div>
<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Password Successfully"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update password !! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>