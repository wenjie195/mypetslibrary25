<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/Brand.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$categoryDetails = getCategory($conn);
$brandDetails = getBrand($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add New Product | Mypetslibrary" />
<title>Add New Product | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<script src="jquery.min.js"></script>
<script src="bootstrap.min.js"></script>
<script src="croppie.js"></script>
<link rel="stylesheet" href="bootstrap.min.css" />
<link rel="stylesheet" href="croppie.css" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Add New Product</h1>
            <div class="green-border"></div>
   </div>
   <div class="border-separation">
        <div class="clear"></div>
        <form action="utilities/registerProductFunction.php" method="POST" enctype="multipart/form-data">
  
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Category* <a href="category.php" class="green-a" target="_blank">(Add New Category Here)</a></p>
        	<select class="input-name clean admin-input" value="<?php echo $categoryDetails[0]->getName();?>" name="register_category" id="register_category" required>
                <option value="">Please Select a Category</option>
                <?php
                for ($cntPro=0; $cntPro <count($categoryDetails) ; $cntPro++)
                {
                ?>
                    <option value="<?php echo $categoryDetails[$cntPro]->getName(); ?>">
                        <?php echo $categoryDetails[$cntPro]->getName(); ?>
                    </option>
                <?php
                }
                ?>
            </select>
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Brand* <a href="brand.php" class="green-a" target="_blank">(Add New Brand Here)</a></p>
        	<select class="input-name clean admin-input" value="<?php echo $brandDetails[0]->getName();?>" name="register_brand" id="register_brand" required>
                <option value="">Please Select a Brand</option>
                <?php
                for ($cntPro=0; $cntPro <count($brandDetails) ; $cntPro++)
                {
                ?>
                    <option value="<?php echo $brandDetails[$cntPro]->getName(); ?>">
                        <?php echo $brandDetails[$cntPro]->getName(); ?>
                    </option>
                <?php
                }
                ?>
            </select>
        </div>
        <div class="clear"></div>

        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Product Name*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Product Name" name="register_name" id="register_name" required>
        </div>
<!--
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">SKU*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Auto Generate" name="register_sku" id="register_sku" readonly>
        </div>
        <div class="clear"></div>

        <div class="dual-input">
        	<p class="input-top-p admin-top-p slug-p">Product Slug (or URL, can't repeat,  Avoid Spacing and Symbol Specially"',.) Can Use - <img src="img/attention2.png" class="attention-png opacity-hover open-url" alt="Click Me!" title="Click Me!"></p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="product-name" name="register_slug" id="register_slug" >
        </div>-->

        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p slug-p">For Animal Type*</p>
        	<select class="input-name clean admin-input" name="register_animal_type" id="register_animal_type" required>
            	<option>Puppy</option>
                <option>Kitten</option>
                <option>Reptile</option>
                <option>Other</option>
            </select>
        </div>
        <div class="clear"></div>
		<!--
         <div class="dual-input">
        	<p class="input-top-p admin-top-p">Expiry Date*</p>
        	<input class="input-name clean input-textarea admin-input" type="date" placeholder="Expiry Date" name="register_expiry_date" id="register_expiry_date">
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Status</p>
        	<select class="input-name clean admin-input" name="register_status" id="register_status" required>
            	<option>Available</option>
                <option>Sold</option>
            </select>
        </div>
        <div class="clear"></div>
		-->
         <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Product Description* (Avoid Using "'')</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Product Description" name="register_description" id="register_description" required>
        </div>
        <div class="clear"></div>
        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Google Search Keyword  (Use Coma , to Separate Each Keyword, Avoid"')</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="dog, pet, cute, for sale, Husky, Penang," name="register_keyword" id="register_keyword">
        </div>          
        <div class="clear"></div>        
        <div class="width100 overflow">
        	<img src="img/vimeo-tutorial.png" class="vimeo-tutorial" alt="Tutorial" title="Tutorial">
            <p class="input-top-p admin-top-p">Vimeo Video Link Only (Optional) (Copy the Highlighted Part Only)</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="379922769" name="register_link" id="register_link">
        </div>
		<!-- Video will display as the 1st image -->
        <div class="clear"></div>
		<!-- <div class="width100 overflow margin-bottom10">
        	<p class="input-top-p admin-top-p">Upload Product Photo (Maximum 4)</p>

            <p><input id="file-upload" type="file" name="image_one[]" id="image_one" accept="image/*" class="margin-bottom10" multiple /></p>

            <p><input id="file-upload" type="file" name="image_one[]" id="image_two" accept="image/*" class="margin-bottom10" /></p>

            <p><input id="file-upload" type="file" name="image_one[]" id="image_three" accept="image/*" class="margin-bottom10" /></p>

            <p><input id="file-upload" type="file" name="image_one[]" id="image_four" accept="image/*" class="margin-bottom10" /></p>

        </div>
		<div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Variation 1 Name*</p>
        	<input class="input-name clean input-textarea admin-input" onkeyup="myFunction()" type="text" placeholder="Variation 1 Name" name="register_variation[]" id="register_variation_one" required>
        </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Price*</p>
                <input class="input-name clean input-textarea admin-input" type="number" placeholder="0.00" name="register_variation_price[]" id="register_variation_one_price" required>
            </div>
            <div class="clear"></div>
            <div class="dual-input">
                <p class="input-top-p admin-top-p">Stock*</p>
                <input class="input-name clean input-textarea admin-input" type="number" placeholder="0" name="register_variation_stock[]" id="register_variation_one_stock" required>
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Photo*</p>
                <p class="margin-bottom30"><input id="file-upload" type="file" name="register_variation_one_image[]" id="register_variation_one_image" accept="image/*" class="margin-bottom10" required/></p>
            </div>
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Variation 2 Name (Optional)</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Variation 2 Name" name="register_variation[]" id="register_variation_two">
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Price</p>
        	<input class="input-name clean input-textarea admin-input" type="number" placeholder="0.00" name="register_variation_price[]" id="register_variation_two_price">
        </div>
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Stock</p>
        	<input class="input-name clean input-textarea admin-input" type="number" placeholder="0" name="register_variation_stock[]" id="register_variation_two_stock">
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Photo</p>
        	<p class="margin-bottom30"><input id="file-upload" type="file" name="register_variation_one_image[]" id="register_variation_two_image" accept="image/*" class="margin-bottom10" /></p>
        </div>
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Variation 3 Name (Optional)</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Variation 3 Name" name="register_variation[]" id="register_variation_three">
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Price</p>
        	<input class="input-name clean input-textarea admin-input" type="number" placeholder="0.00" name="register_variation_price[]" id="register_variation_three_price">
        </div>
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Stock</p>
        	<input class="input-name clean input-textarea admin-input" type="number" placeholder="0" name="register_variation_stock[]" id="register_variation_three_stock">
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Photo</p>
        	<p class="margin-bottom30"><input id="file-upload" type="file" name="register_variation_one_image[]" id="register_variation_three_image" accept="image/*"  class="margin-bottom10"/></p>
        </div>
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Variation 4 Name (Optional)</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Variation 4 Name" name="register_variation[]" id="register_variation_four">
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Price</p>
        	<input class="input-name clean input-textarea admin-input" type="number" placeholder="0.00" name="register_variation_price[]" id="register_variation_four_price">
        </div>
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Stock</p>
        	<input class="input-name clean input-textarea admin-input" type="number" placeholder="0" name="register_variation_stock[]" id="register_variation_four_stock">
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Photo</p>
        	<p class="margin-bottom30"><input id="file-upload" type="file" name="register_variation_one_image[]" id="register_variation_four_image" accept="image/*" class="margin-bottom10" /></p>
        </div>
        <div class="clear"></div>
        <div class="dual-input">
            <p class="input-top-p admin-top-p slug-p">Free Gift</p>
            <select class="input-name clean admin-input" name="register_free_gift" id="register_free_gift" onchange='FreeGift(this.value);' required> 
                <option>Free Gift Availability</option>  
                <option value="yes">Yes</option>
                <option value="no">No</option>
            </select>
        </div>
        <div class="dual-input second-dual-input" name="fg_img" id="fg_img" style='display:none;'>
            <p class="input-top-p admin-top-p">Upload Free Gift Image</p>
            <p class="margin-bottom30"><input id="file-upload" type="file" name="register_freeGift_img" id="register_freeGift_img" accept="image/*" class="margin-bottom10" /></p>
        </div>
        <div class="clear"></div>
        <div class="dual-input" name="fg_desc" id="fg_desc" style='display:none;'>
        	<p class="input-top-p admin-top-p">Free Gift Description</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Free Gift Description" name="register_freeGift_desc" id="register_freeGift_desc">
        </div>
        <div class="clear"></div>
        <div class="width100 overflow text-center">
        	<div class="green-button white-text clean2 edit-1-btn margin-auto ow-peach-button" onclick="showVariation();this.style.visibility= 'hidden';">Add More Variation</div>
        </div> -->
        <div class="clear"></div>

        </div>

        <div class="width100 overflow text-center">
        	<button class="green-button white-text clean2 edit-1-btn margin-auto">Next</button>
        </div>
        </form>
	</div>
</div>
<div class="clear"></div>
<!-- <//?php include 'js.php'; ?> -->
<?php
  $tz = 'Asia/Kuala_Lumpur';
  $timestamp = time();
  $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
  $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
  $time = $dt->format('Y');
?>

<div class="width100 same-padding green-footer">
	<p class="footer-p white-text">© <?php echo $time;?> Mypetslibrary, All Rights Reserved.</p>
</div>

<?php

if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully registered new product!";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Registration of new seller failed!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>

<script type="text/javascript">
function FreeGift(val){
 var element=document.getElementById('fg_img');
 var elementA=document.getElementById('fg_desc');
 if(val=='Free Gift Availability'||val=='yes'){
   element.style.display='block';
   elementA.style.display='block';
 }
 else {
   element.style.display='none';
 }
}

</script> 

</body>
</html>

<div id="uploadimageModal" class="modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
        		<h4 class="modal-title">Upload & Crop Image</h4>
      		</div>
      		<div class="modal-body">
        		<div class="row">
  					<div class="col-md-8 text-center">
						  <div id="image_demo" style="width:350px; margin-top:30px"></div>
  					</div>
  					<div class="col-md-4" style="padding-top:30px;">
  						<br />
  						<br />
  						<br/>
						  <button class="btn btn-success crop_image">Crop Image</button>
					</div>
				</div>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      		</div>
    	</div>
    </div>
  </div>
  
<style>
.panel-default>.panel-heading{
	text-align: center;
    font-weight: bold;
    font-size: 18px;	
}
.modal-dialog{
	height:350px;}
</style>
<script>
$(document).ready(function(){

	$image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:200,
      height:200,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  });
  


  $('#image_one').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });

  $('#image_two').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });

  $('#image_three').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });

  $('#image_four').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });

  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
      $.ajax({
        //url:"uploaded.php",
        //type: "POST",
        //data:{"image": response},
        success:function(data)
        {
          $('#uploadimageModal').modal('hide');
        //   $('#uploaded_image').html(data);
        }
      });
    })
  });

});
</script>

