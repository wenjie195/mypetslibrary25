<div class="width103" id="app">
<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon2.php';

if(isset($_POST["action"]))
{
	// $query = "
	// 	SELECT * FROM kitten WHERE status = 'Available' & 'Sold' ORDER BY date_created DESC
	// ";

	// $query = "
	// SELECT * FROM kitten WHERE status = 'Available' & 'Sold'
	// ";

	$query = " 
	SELECT * FROM variation WHERE status = 'Available' 
	";

	$statement = $connect->prepare($query);
	$statement->execute();
	$result = $statement->fetchAll();
	$total_row_count = $statement->rowCount();

	if(isset($_POST["minimum_price"], $_POST["maximum_price"]) && !empty($_POST["minimum_price"]) && !empty($_POST["maximum_price"]))
	{
		$query .= "
		 AND amount BETWEEN '".$_POST["minimum_price"]."' AND '".$_POST["maximum_price"]."'
		";
	}

	if(isset($_POST["category"]))
	{
		$category_filter = implode("','", $_POST["category"]);
		$query .= "
		 AND category IN('".$category_filter."')
		";
	}

	if(isset($_POST["rating"]))
	{
		$rating_filter = implode("','", $_POST["rating"]);
		$query .= "
		 AND rating IN('".$rating_filter."')
		";
	}

	$statement = $connect->prepare($query);
	$statement->execute();
	$result = $statement->fetchAll();
	$total_row = $statement->rowCount();
	$output = '';

	if($total_row > 0)
	{
		foreach($result as $row)
		{
			$uid= $row['product_uid'];

			$imageURL = './productImage/'.$row["image"];
			// elseif ($row['status'] == 'Available')
			// if ($row['status'] == 'Available')
			// {
				$output .= '	
					<a href="malaysia-pets-products-details.php?='.$row['product_uid'].'">
						<div id="'.$row['id'].'" class="shadow-white-box four-box-size opacity-hover pointer-div" value = "'.$row['product_uid'].'">
						 <div class="square">
							<div class="width100 white-bg content progressive">
							<a>
									<img data-src="productImage/'. $row['image'] .'"  src="img/pet-load300.jpg" class="preview width100 two-border-radius lazy" alt="'. $row['name'] .' | " title="'. $row['name'] .' | " />
							</a>                      
							</div>
						  </div>
							<p align="center" class="width100 text-overflow slider-product-name">'. $row['name'] .' </p>
							<p class="slider-product-name text-overflow">
								RM '. $row['price'] .'
							</p>
						</div>
					</a>	
				';
			// }
			
		}
	}
	else
	{
		$output = '<h3>No Data Found</h3>';
	}
	echo $output;
}

?>
</div>
<script type="text/javascript">

var totalId = <?php echo $total_row_count + 1 ?>;
// alert(totalId);
for(var i=0; i< totalId; i++){

	$("#"+i+"").click(function(){
					var x = $(this).attr('value');
					location.href = "./malaysia-pets-products-details.php?id="+x+"";
				});

}
</script>
<script src="js/index2.js"></script>

<script>
(function(){
	new Progressive({
	el: '#app',
	lazyClass: 'lazy',
	removePreview: true,
	scale: true
	}).fire()
})()
</script>