-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 27, 2020 at 08:06 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_mypetslib2`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `author_uid` varchar(255) DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `title` text DEFAULT NULL,
  `seo_title` text DEFAULT NULL,
  `article_link` text DEFAULT NULL,
  `keyword_one` text DEFAULT NULL,
  `keyword_two` text DEFAULT NULL,
  `title_cover` text DEFAULT NULL,
  `paragraph_one` text DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `paragraph_two` text DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `paragraph_three` text DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `paragraph_four` text DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `paragraph_five` text DEFAULT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `img_cover_source` text DEFAULT NULL,
  `img_one_source` text DEFAULT NULL,
  `img_two_source` text DEFAULT NULL,
  `img_three_source` text DEFAULT NULL,
  `img_four_source` text DEFAULT NULL,
  `img_five_source` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `display` varchar(255) DEFAULT 'YES',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `uid`, `author_uid`, `author_name`, `title`, `seo_title`, `article_link`, `keyword_one`, `keyword_two`, `title_cover`, `paragraph_one`, `image_one`, `paragraph_two`, `image_two`, `paragraph_three`, `image_three`, `paragraph_four`, `image_four`, `paragraph_five`, `image_five`, `img_cover_source`, `img_one_source`, `img_two_source`, `img_three_source`, `img_four_source`, `img_five_source`, `author`, `type`, `display`, `date_created`, `date_updated`) VALUES
(1, '9892e3e88f07ccbd84cff19b8a99751e', 'd156938c9b63fd253e9cddf89eba10e7', 'admin', 'Think Thrice Before Getting A Pet', 'ThinkThriceBeforeGettingAPet-think-thrice-before-getting-a-pet', 'think-thrice-before-getting-a-pet', ' Are you googling Puppy For Sale, Puppy Seller or Cat or Dog Breeder to get yourself a puppy or kitten as pet right now? One moment please, why do you want to get a pet at the first place? Because they are fluffy, cuddly and cute? Well, we can’t deny that', 'Puppy For Sale,Puppy Seller,Cat or Dog Breeder,toy poodle puppy,', '9892e3e88f07ccbd84cff19b8a99751eblog-picture.jpg', '<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">Are you googling &ldquo;</span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>Puppy For Sale</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">&rdquo;, &ldquo;</span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>Puppy Seller</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">&rdquo; or &ldquo;</span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>Cat or Dog Breeder</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">&rdquo; to get yourself a puppy or kitten as pet right now? One moment please, why do you want to get a pet at the first place? Because they are fluffy, cuddly and cute? Well, we can&rsquo;t deny that this is normally what we thought as first reason as who can resist a </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>toy poodle puppy</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"> that resembles a tiny teddy bear!</span></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh3.googleusercontent.com/ZNeG7pYfhQLKnh3XqURvYiDm36BO_lemAqauO3Hq12MbcH1slc2hUqIKXN6uwteghzo3s2xAFGxpSCKkybKf6Mglepx6kWEIQRptp6suJWXtdAyunyc1OZ1w67B6UFc799xXtdM1\" style=\"height:531px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">If that&rsquo;s your only reason to own a pet because they are adorable at puppy or kitten stage, do think twice because you need to accept the fact that they will eventually grow up and become mature. Despite your lovely puppy and kitten growing up healthily is a good thing, but some people just feeling &ldquo;disappointed&rdquo; that their pet is no longer the mini fluffy ball like they used to be, the expenses doubled because they eat more and requires regular grooming etc. This is the stage where some of these &ldquo;disappointed&rdquo; owners intend to find a new home for their pet.</span></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh4.googleusercontent.com/uPgP4OHuMNIYmV1t_B0UPsB_-z5d9TcjC2BssuXLiY48fty_3CqE1GW95Aqx3nQn1NKO0aQYmd_XZQybVg72jEHeSxPkkpB56e_M1Jou_a1jqvgDKkbVZWTWW0dEfSUBH9Bgjt_B\" style=\"height:465px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">&nbsp;Before you look for </span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><strong>dog breeders</strong></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"> or hanging around </span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><strong>pet shops in Malaysia</strong></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">, do think thrice can you handle the responsibility of taking care of your pet for the rest of his life?</span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">1)</span></span></span><span style=\"font-size:6.999999999999999pt\"><span style=\"font-family:\'Times New Roman\'\"><span style=\"color:#333333\">&nbsp; &nbsp; &nbsp; </span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">Do you have the patience to train them to pee at the right place without being frustrated?</span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">2)</span></span></span><span style=\"font-size:6.999999999999999pt\"><span style=\"font-family:\'Times New Roman\'\"><span style=\"color:#333333\">&nbsp; &nbsp; &nbsp; </span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">Do you have the time to give them some precious attention and a short walk everyday?</span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">3)</span></span></span><span style=\"font-size:6.999999999999999pt\"><span style=\"font-family:\'Times New Roman\'\"><span style=\"color:#333333\">&nbsp; &nbsp; &nbsp; </span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">Are you willing to spend on his food and medical fees?</span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">4)</span></span></span><span style=\"font-size:6.999999999999999pt\"><span style=\"font-family:\'Times New Roman\'\"><span style=\"color:#333333\">&nbsp; &nbsp; &nbsp; </span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">Do your family members have any objections in getting a pet?</span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">5)</span></span></span><span style=\"font-size:6.999999999999999pt\"><span style=\"font-family:\'Times New Roman\'\"><span style=\"color:#333333\">&nbsp; &nbsp; &nbsp; </span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">Most importantly, whether you can ACCEPT THAT THEY WILL GROW UP EVENTUALLY?</span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh3.googleusercontent.com/QCWDTNDhp7jYedKEnwxbPRJf8qYWtrrv84zKXtPVCxhiI4NolgvGx2QYrGJRv329BkfgRz7ySUTiJz0f9vMNtIgqGFk5Et3vSm0GYQ536JWZXJzqoR4AtIARBnT2b8o1UK54sCIa\" style=\"height:352px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">&nbsp;If you are all set and can carry these huge responsibilities, as well as handling grown-up version of your pet, then CONGRATULATIONS, you are ready for a puppy or kitten!</span></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh3.googleusercontent.com/bR0O2pBc7-qlSQEVzJKMwpHmbEClWm8l8cus534hdRT7qoDuAp4DA1_B-8HDpmZrqAfYcFzyye4wZEfswOKRBdiX4Fh0WSMiz8gmBwfKdVsuDrJM0vrv9WR5ZdTRXDoUUb6enbWq\" style=\"height:351px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">Now, here&rsquo;s the tough part, where to look for reputable </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>kitten and puppy seller</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">, where to search for your preferred breed? For example, </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>British Shorthair Kitten, Persian Kitten, Frenchie Puppy, Corgi Puppy, Toy Poodle Puppy </strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">and others etc. Are you looking for the one that really caught your attention and fall in love with him at the first sight? Wish you have more options? Or you are afraid of dealing with scammers who disappeared after receiving your deposit? Are you worried that the pets you brought home could be problematic and having health issues?&nbsp; Put these worries away, because&hellip;&hellip;.</span></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh4.googleusercontent.com/KipEBPcGjCKhWy4WvTK4h3-YzxcE1aO4SWN87sOHwU_OfWivL7quajv12_WBtV5-hgF8474olC3-CqVdEGEBQebUbF_vrtjHp10bnj6f6RGHvyQ_sBFQ8K1jZl3p8XWaBz_PW55j\" style=\"height:401px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">TA-DAA! The pawsome platform is here &ndash; MYPETSLIBRARY.</span></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh6.googleusercontent.com/ZfSeAHb6tPg_HxU0zIPj9q07XQh2uxdvHhvce2vLaG_frVHxtp9n7Ro6LxWPtgPFoiKY9XiMD-vCsy0PAJzs2aPKuafLyX_l592HWqbNiBqC9AQBdp_6LFAJOtKMI_rRf3ExylAU\" style=\"height:235px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">Mypetslibrary is the 1</span></span></span></span><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">st</span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"> Online Pet Store Platform featuring pets and gathers trusted </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>pet sellers</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"> across Malaysia. In Mypetslibrary, they offer variety breeds of </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>kittens and puppies for sale </strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">by these </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>pet sellers</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"> and </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>pet shops</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">. Rest assured, they are well known and have up to 5 years experiences. You can simply browse for your favourite </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>Pomeranian puppy</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"> in </span></span></span></span><a href=\"http://www.mypetslibrary.com/\" style=\"text-decoration:none\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#2e6f63\"><span style=\"background-color:#ffffff\">www.mypetslibrary.com</span></span></span></span></a><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">, scrolling at their full set of pictures and videos, and contact the seller instantly if you fall in love with the </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>Pomeranian puppy</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"> at first glance and decided to give him a forever loving home. Hence, if you browse for your new pet in Mypetslibrary, there will no worries of dealing with irresponsible sellers, scammers or having limited choice of your favourite breed.</span></span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh4.googleusercontent.com/PwDLiTHQ-pjMAjdr_467rH7x9W9k3sCsgzppYkRCejsOHvWSnSP72q8NzcLpmpt6lopFdtjnixhAOx1NbAudoFoNWiRTXrN3AVY8nUnXaiQbXllTQdMIYsGsRHnolsnY03j6_tcQ\" style=\"height:340px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">If you are not sure on which type of breed suits your lifestyle and home environment, or you are a new paw daddy &amp; mummy, and in need of some advice on how to handle your new family member, you may look for Mypetslibrary&rsquo;s customer service helpline, say HELLO and let&rsquo;s have some Pawtalk. They will answer your inquiries well for sure.&nbsp;</span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In general, looking after a pet may be a big responsibility, but when you consider about their lovely companionship, how they never let you being alone, the fun and laughter they brought to your family, these pets will make all that hard work of yours worthwhile. </span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh4.googleusercontent.com/qD0SOc1YpGWZx9kT37lU_BqiJCDUHKOPy6GVeQ7T5Q_oeNoATi61AUL3vqUr-dgOM934rQIsEImuj5gGgR84wLJWh96aCbeSuU1Ay2QJtdHkaMPQezE20lXd1D-bxPxsvW96DVUM\" style=\"height:172px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh5.googleusercontent.com/GS1fK8WRdJ9wge_GzmdW1TGunhbH38z5EXZCSlm1UTCw1cUAvUyL55Hcl5RPF5hSBktEqp184E6p2rnVbi5EX-rBy53PCUu3DmP0WvLfpGLxxoVBDbm2e81b7yIGdzDG0h4BPPxT\" style=\"height:409px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', '2020-05-12 03:22:25', '2020-07-02 08:31:27'),
(2, '2250064b0ccfe80d005eaac65d3058cd', 'd156938c9b63fd253e9cddf89eba10e7', 'admin', '养宠物前先三思', '养宠物前先三思-养宠物前先三思', '养宠物前先三思', '您是否正在搜寻待售的幼犬，幼犬卖家或猫或狗饲养员，以让自己成为宠物的幼犬或小猫？请等一下，为什么首先要养宠物？是因为它们蓬松，可爱又可爱吗？好吧，我们不能否认', '出售幼犬，幼犬卖方，猫或狗饲养员，玩具贵宾犬幼犬', '2250064b0ccfe80d005eaac65d3058cd9892e3e88f07ccbd84cff19b8a99751eblog-picture.jpg', '<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:16px\"><span style=\"font-family:Times New Roman,Times,serif\">您是否正在搜寻&ldquo;待售幼犬&rdquo;，&ldquo;幼犬卖家&rdquo;或&ldquo;猫或狗饲养员&rdquo;，以让自己成为宠物的幼犬或小猫？请等一下，为什么首先要养宠物？是因为它们蓬松，可爱又可爱吗？好吧，我们不能否认这通常是我们认为的第一个理由，因为谁能抵抗像玩具熊一样的玩具贵宾犬小狗！</span></span></p>\r\n\r\n<p><span style=\"font-size:16px\"><span style=\"font-family:Times New Roman,Times,serif\"><img src=\"https://lh3.googleusercontent.com/ZNeG7pYfhQLKnh3XqURvYiDm36BO_lemAqauO3Hq12MbcH1slc2hUqIKXN6uwteghzo3s2xAFGxpSCKkybKf6Mglepx6kWEIQRptp6suJWXtdAyunyc1OZ1w67B6UFc799xXtdM1\" style=\"height:531px; width:624px\" /></span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:16px\">如果这是您养宠物的唯一原因，因为它们在幼犬或小猫阶段很可爱，请三思而后行，因为您需要接受一个事实，即它们最终会长大并成熟。尽管您可爱的小狗和小猫健康成长是一件好事，但有些人只是感到&ldquo;失望&rdquo;，因为他们的宠物不再像以前那样是迷你蓬松球了，费用翻了一番，因为他们吃得更多并且需要定期梳理等等。在此阶段，其中一些&ldquo;失望的&rdquo;主人打算为他们的宠物寻找新家。</span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><img src=\"https://lh4.googleusercontent.com/uPgP4OHuMNIYmV1t_B0UPsB_-z5d9TcjC2BssuXLiY48fty_3CqE1GW95Aqx3nQn1NKO0aQYmd_XZQybVg72jEHeSxPkkpB56e_M1Jou_a1jqvgDKkbVZWTWW0dEfSUBH9Bgjt_B\" /></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:16px\"><span style=\"font-family:Arial,Helvetica,sans-serif\">在马来西亚寻找养狗人或在宠物商店附近闲逛之前，您认为三次可以承担照顾宠物一生的责任吗？</span></span></p>\r\n\r\n<p style=\"text-align:center\">1）您是否有耐心训练他们在正确的地方撒尿而不会感到沮丧？</p>\r\n\r\n<p style=\"text-align:center\">2）你每天有时间给他们一些宝贵的关注和短暂的步行吗？</p>\r\n\r\n<p style=\"text-align:center\">3）你愿意花他的食物和医疗费用吗？</p>\r\n\r\n<p style=\"text-align:center\">4）您的家人对养宠物有异议吗？</p>\r\n\r\n<p style=\"text-align:center\">5）最重要的是，您是否可以接受他们将长大成人？</p>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', '2020-08-21 03:07:55', '2020-08-21 03:15:08'),
(3, '4758cac1bc20adeda7d9c94a11f05d22', 'd156938c9b63fd253e9cddf89eba10e7', 'admin', '养宠物前先三思(续)', '养宠物前先三思(续)-养宠物前先三思续', '养宠物前先三思续', '您是否正在搜寻待售的幼犬，幼犬卖家或猫或狗饲养员，以让自己成为宠物的幼犬或小猫？', '出售幼犬，幼犬卖方，猫或狗饲养员，玩具贵宾犬幼犬', '4758cac1bc20adeda7d9c94a11f05d229892e3e88f07ccbd84cff19b8a99751eblog-picture.jpg', '<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:16px\"><span style=\"font-family:Times New Roman,Times,serif\">如果您已经准备好并且可以承担这些巨大的责任，并且可以处理成年的宠物，那么恭喜您，您可以准备一只小狗或小猫了!</span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:16px\"><span style=\"font-family:Times New Roman,Times,serif\"><img src=\"https://lh3.googleusercontent.com/bR0O2pBc7-qlSQEVzJKMwpHmbEClWm8l8cus534hdRT7qoDuAp4DA1_B-8HDpmZrqAfYcFzyye4wZEfswOKRBdiX4Fh0WSMiz8gmBwfKdVsuDrJM0vrv9WR5ZdTRXDoUUb6enbWq\" /></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:16px\"><span style=\"font-family:Times New Roman,Times,serif\">现在，这是困难的部分，在哪里寻找有名的小猫和小狗卖家，在哪里寻找您喜欢的品种？例如，英国短毛猫小猫，波斯小猫，弗朗西斯幼犬，柯基犬幼犬，玩具贵宾犬幼犬等。您是否正在寻找真正引起您的注意并一见钟情的人？希望您有更多选择吗？还是您害怕与在收到您的存款后失踪的骗子打交道？您是否担心带回家的宠物会出现问题并出现健康问题？消除这些担忧，因为&hellip;&hellip;</span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:16px\"><span style=\"font-family:Times New Roman,Times,serif\">TA-DAA！令人pawsome的平台在这里&ndash; MYPETSLIBRARY</span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:16px\"><span style=\"font-family:Times New Roman,Times,serif\"><img src=\"https://lh6.googleusercontent.com/ZfSeAHb6tPg_HxU0zIPj9q07XQh2uxdvHhvce2vLaG_frVHxtp9n7Ro6LxWPtgPFoiKY9XiMD-vCsy0PAJzs2aPKuafLyX_l592HWqbNiBqC9AQBdp_6LFAJOtKMI_rRf3ExylAU\" /></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:16px\"><span style=\"font-family:Times New Roman,Times,serif\">Mypetslibrary是第一个以宠物为主题的在线宠物商店平台，并聚集了马来西亚各地值得信赖的宠物卖家。在Mypetslibrary中，他们提供各种品种的小猫和幼犬供这些宠物销售商和宠物店出售。放心，他们是众所周知的，并且具有5年以上的经验。您可以在www.mypetslibrary.com上浏览最喜欢的博美犬幼犬，滚动查看其全套图片和视频，如果您一眼就爱上了博美犬幼犬并决定永久给予他，您可以立即与卖家联系。爱家。因此，如果您在Mypetslibrary中浏览新宠物，就不会担心与不负责任的卖家，骗子或您最喜欢的品种的选择有限。</span></span></p>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', '2020-08-21 03:26:04', '2020-08-25 06:46:46'),
(4, '01c97ae9fb766db1befd4c199fe58eb8', 'd156938c9b63fd253e9cddf89eba10e7', 'admin', 'Think Thrice Before Getting A Pet Part Two', 'ThinkThriceBeforeGettingAPetPartTwo-Think Thrice Before Getting A Pet Part Two', 'Think Thrice Before Getting A Pet Part Two', ' Are you googling Puppy For Sale, Puppy Seller or Cat or Dog Breeder to get yourself a puppy or kitten as pet right now?', 'Puppy For Sale,Puppy Seller,Cat or Dog Breeder,toy poodle puppy', '01c97ae9fb766db1befd4c199fe58eb89892e3e88f07ccbd84cff19b8a99751eblog-picture.jpg', '<p style=\"text-align:center\"><span style=\"font-family:Times New Roman,Times,serif\"><span style=\"font-size:16px\">Now, here&rsquo;s the tough part, where to look for reputable <strong>kitten and puppy seller</strong>, where to search for your preferred breed? For example, <strong>British Shorthair Kitten, Persian Kitten, Frenchie Puppy, Corgi Puppy, Toy Poodle Puppy </strong>and others etc. Are you looking for the one that really caught your attention and fall in love with him at the first sight? Wish you have more options? Or you are afraid of dealing with scammers who disappeared after receiving your deposit? Are you worried that the pets you brought home could be problematic and having health issues?&nbsp; Put these worries away, because&hellip;&hellip;.</span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-family:Times New Roman,Times,serif\"><span style=\"font-size:16px\"><img src=\"https://lh4.googleusercontent.com/KipEBPcGjCKhWy4WvTK4h3-YzxcE1aO4SWN87sOHwU_OfWivL7quajv12_WBtV5-hgF8474olC3-CqVdEGEBQebUbF_vrtjHp10bnj6f6RGHvyQ_sBFQ8K1jZl3p8XWaBz_PW55j\" /></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-family:Times New Roman,Times,serif\"><span style=\"font-size:16px\">TA-DAA! The pawsome platform is here &ndash; MYPETSLIBRARY.</span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-family:Times New Roman,Times,serif\"><span style=\"font-size:16px\"><img src=\"https://lh6.googleusercontent.com/ZfSeAHb6tPg_HxU0zIPj9q07XQh2uxdvHhvce2vLaG_frVHxtp9n7Ro6LxWPtgPFoiKY9XiMD-vCsy0PAJzs2aPKuafLyX_l592HWqbNiBqC9AQBdp_6LFAJOtKMI_rRf3ExylAU\" /></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-family:Times New Roman,Times,serif\"><span style=\"font-size:16px\">Mypetslibrary is the 1st Online Pet Store Platform featuring pets and gathers trusted <strong>pet sellers</strong> across Malaysia. In Mypetslibrary, they offer variety breeds of <strong>kittens and puppies for sale </strong>by these <strong>pet sellers</strong> and <strong>pet shops</strong>. Rest assured, they are well known and have up to 5 years experiences. You can simply browse for your favourite <strong>Pomeranian puppy</strong> in <a href=\"http://www.mypetslibrary.com/\">www.mypetslibrary.com</a>, scrolling at their full set of pictures and videos, and contact the seller instantly if you fall in love with the <strong>Pomeranian puppy</strong> at first glance and decided to give him a forever loving home. Hence, if you browse for your new pet in Mypetslibrary, there will no worries of dealing with irresponsible sellers, scammers or having limited choice of your favourite breed.</span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-family:Times New Roman,Times,serif\"><span style=\"font-size:16px\"><img src=\"https://lh4.googleusercontent.com/PwDLiTHQ-pjMAjdr_467rH7x9W9k3sCsgzppYkRCejsOHvWSnSP72q8NzcLpmpt6lopFdtjnixhAOx1NbAudoFoNWiRTXrN3AVY8nUnXaiQbXllTQdMIYsGsRHnolsnY03j6_tcQ\" /></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-family:Times New Roman,Times,serif\"><span style=\"font-size:16px\">If you are not sure on which type of breed suits your lifestyle and home environment, or you are a new paw daddy &amp; mummy, and in need of some advice on how to handle your new family member, you may look for Mypetslibrary&rsquo;s customer service helpline, say HELLO and let&rsquo;s have some Pawtalk. They will answer your inquiries well for sure.&nbsp;</span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-family:Times New Roman,Times,serif\"><span style=\"font-size:16px\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In general, looking after a pet may be a big responsibility, but when you consider about their lovely companionship, how they never let you being alone, the fun and laughter they brought to your family, these pets will make all that hard work of yours worthwhile.</span></span></p>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', '2020-08-21 03:27:37', '2020-08-21 03:27:37');

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `uid`, `username`, `bank_name`, `bank_account_holder`, `bank_account_no`, `status`, `date_created`, `date_updated`) VALUES
(1, '9349aede685bae49c49b0b895e465f6d', 'user', 'CIMB Bank', 'User', '123321', 'Active', '2020-05-28 05:03:34', '2020-05-28 05:03:34');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `name`, `status`, `date_created`, `date_updated`) VALUES
(1, 'Brand 1', 'Available', '2020-04-21 10:20:15', '2020-04-21 10:20:15'),
(2, 'Brand 2', 'Available', '2020-04-21 10:22:14', '2020-04-21 10:22:14'),
(3, 'Brand 3', 'Available', '2020-04-21 10:22:50', '2020-04-21 10:22:50');

-- --------------------------------------------------------

--
-- Table structure for table `breed`
--

CREATE TABLE `breed` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `breed`
--

INSERT INTO `breed` (`id`, `name`, `status`, `type`, `date_created`, `date_updated`) VALUES
(9, 'Lizard', 'Available', 3, '2020-04-21 09:25:51', '2020-04-21 09:25:51'),
(10, 'Frog', 'Available', 3, '2020-04-21 09:28:28', '2020-04-21 09:28:28'),
(11, 'Akita', 'Available', 1, '2020-06-24 04:10:25', '2020-06-24 04:10:25'),
(12, 'Alaskan Husky', 'Available', 1, '2020-06-24 04:10:35', '2020-06-24 04:10:35'),
(13, 'Alaskan Malamute', 'Available', 1, '2020-06-24 04:11:03', '2020-06-24 04:11:03'),
(14, 'American Bully', 'Available', 1, '2020-06-24 04:11:25', '2020-06-24 04:11:25'),
(15, 'Bassett Hound', 'Available', 1, '2020-06-24 04:11:31', '2020-06-24 04:11:31'),
(16, 'Beagle', 'Available', 1, '2020-06-24 04:11:37', '2020-06-24 04:11:37'),
(17, 'Bernese Mountain Dog', 'Available', 1, '2020-06-24 04:11:45', '2020-06-24 04:11:45'),
(18, 'Bichon Frise', 'Available', 1, '2020-06-24 04:11:50', '2020-06-24 04:11:50'),
(19, 'Border Collie', 'Available', 1, '2020-06-24 04:11:56', '2020-06-24 04:11:56'),
(20, 'Bull Terrier', 'Available', 1, '2020-06-24 04:12:02', '2020-06-24 04:12:02'),
(21, 'Bulldog', 'Available', 1, '2020-06-24 04:12:07', '2020-06-24 04:12:07'),
(22, 'Cane Corso', 'Available', 1, '2020-06-24 04:12:14', '2020-06-24 04:12:14'),
(23, 'Caucasian Shepherd', 'Available', 1, '2020-06-24 04:12:21', '2020-06-24 04:12:21'),
(24, 'Cavalier King Charles Spaniel', 'Available', 1, '2020-06-24 04:12:29', '2020-06-24 04:12:29'),
(25, 'Chihuahua', 'Available', 1, '2020-06-24 04:12:37', '2020-06-24 04:12:37'),
(26, 'Chow Chow', 'Available', 1, '2020-06-24 04:12:42', '2020-06-24 04:12:42'),
(27, 'Cocker Spaniel', 'Available', 1, '2020-06-24 04:12:48', '2020-06-24 04:12:48'),
(28, 'Dachshund', 'Available', 1, '2020-06-24 04:12:55', '2020-06-24 04:12:55'),
(29, 'Dalmatian', 'Available', 1, '2020-06-24 04:13:01', '2020-06-24 04:13:01'),
(30, 'Dobermann', 'Available', 1, '2020-06-24 04:13:09', '2020-06-24 04:13:09'),
(31, 'English Bulldog', 'Available', 1, '2020-06-24 04:13:16', '2020-06-24 04:13:16'),
(32, 'French Bulldog', 'Available', 1, '2020-06-24 04:13:35', '2020-06-24 04:13:35'),
(33, 'German Shepherd', 'Available', 1, '2020-06-24 04:13:42', '2020-06-24 04:13:42'),
(34, 'Giant Poodle', 'Available', 1, '2020-06-24 04:13:50', '2020-06-24 04:13:50'),
(35, 'Golden Retriever', 'Available', 1, '2020-06-24 04:13:56', '2020-06-24 04:13:56'),
(36, 'Great Dane', 'Available', 1, '2020-06-24 04:14:01', '2020-06-24 04:14:01'),
(37, 'Jack Russell', 'Available', 1, '2020-06-24 04:14:07', '2020-06-24 04:14:07'),
(38, 'Labrador', 'Available', 1, '2020-06-24 04:14:13', '2020-06-24 04:14:13'),
(39, 'Maltese', 'Available', 1, '2020-06-24 04:14:18', '2020-06-24 04:14:18'),
(40, 'Maltipoo', 'Available', 1, '2020-06-24 04:14:28', '2020-06-24 04:14:28'),
(41, 'Miniature poodle', 'Available', 1, '2020-06-24 04:14:34', '2020-06-24 04:14:34'),
(42, 'Mix Breed', 'Available', 1, '2020-06-24 04:14:40', '2020-06-24 04:14:40'),
(43, 'Morkie', 'Available', 1, '2020-06-24 04:14:44', '2020-06-24 04:14:44'),
(44, 'Old English Sheepdog', 'Available', 1, '2020-06-24 04:14:50', '2020-06-24 04:14:50'),
(45, 'Papillon', 'Available', 1, '2020-06-24 04:14:55', '2020-06-24 04:14:55'),
(46, 'Pekingese', 'Available', 1, '2020-06-24 04:15:00', '2020-06-24 04:15:00'),
(47, 'Pinscher', 'Available', 1, '2020-06-24 04:15:08', '2020-06-24 04:15:08'),
(48, 'Pitbull', 'Available', 1, '2020-06-24 04:15:14', '2020-06-24 04:15:14'),
(49, 'Pomapoo', 'Available', 1, '2020-06-24 04:15:20', '2020-06-24 04:15:20'),
(50, 'Pomeranian', 'Available', 1, '2020-06-24 04:15:27', '2020-06-24 04:15:27'),
(51, 'Pomsky', 'Available', 1, '2020-06-24 04:15:32', '2020-06-24 04:15:32'),
(52, 'Pug', 'Available', 1, '2020-06-24 04:15:37', '2020-06-24 04:15:37'),
(53, 'Rottweiler', 'Available', 1, '2020-06-24 04:15:42', '2020-06-24 04:15:42'),
(54, 'Saint Bernard', 'Available', 1, '2020-06-24 04:15:47', '2020-06-24 04:15:47'),
(55, 'Samoyed', 'Available', 1, '2020-06-24 04:15:52', '2020-06-24 04:15:52'),
(56, 'Schnauzer', 'Available', 1, '2020-06-24 04:15:59', '2020-06-24 04:15:59'),
(57, 'Shar Pei', 'Available', 1, '2020-06-24 04:16:05', '2020-06-24 04:16:05'),
(58, 'Shetland Sheepdog', 'Available', 1, '2020-06-24 04:16:11', '2020-06-24 04:16:11'),
(59, 'Shiba Inu', 'Available', 1, '2020-06-24 04:16:18', '2020-06-24 04:16:18'),
(60, 'Shih Tzu', 'Available', 1, '2020-06-24 04:16:24', '2020-06-24 04:16:24'),
(61, 'Siberian Husky', 'Available', 1, '2020-06-24 04:16:30', '2020-06-24 04:16:30'),
(62, 'Silky Terrier', 'Available', 1, '2020-06-24 04:16:37', '2020-06-24 04:16:37'),
(63, 'Standard poodle', 'Available', 1, '2020-06-24 04:16:43', '2020-06-24 04:16:43'),
(64, 'Teacup Pomeranian', 'Available', 1, '2020-06-24 04:16:49', '2020-06-24 04:16:49'),
(65, 'Teacup poodle', 'Available', 1, '2020-06-24 04:16:54', '2020-06-24 04:16:54'),
(66, 'Tiny Poodle', 'Available', 1, '2020-06-24 04:16:59', '2020-06-24 04:16:59'),
(67, 'Toy poodle', 'Available', 1, '2020-06-24 04:17:05', '2020-06-24 04:17:05'),
(68, 'Welsh Corgi', 'Available', 1, '2020-06-24 04:17:43', '2020-06-24 04:17:43'),
(69, 'West Highland Terrier', 'Available', 1, '2020-06-24 04:17:49', '2020-06-24 04:17:49'),
(70, 'Wooly Husky', 'Available', 1, '2020-06-24 04:17:56', '2020-06-24 04:17:56'),
(71, 'Wooly Malamute', 'Available', 1, '2020-06-24 04:18:02', '2020-06-24 04:18:02'),
(72, 'Yorkshire Terrier', 'Available', 1, '2020-06-24 04:18:09', '2020-06-24 04:18:09'),
(73, 'American Curl', 'Available', 2, '2020-06-24 04:19:47', '2020-06-24 04:19:47'),
(74, 'American Shorthair', 'Available', 2, '2020-06-24 04:19:52', '2020-06-24 04:19:52'),
(75, 'Bengal', 'Available', 2, '2020-06-24 04:19:58', '2020-06-24 04:19:58'),
(76, 'British Longhair', 'Available', 2, '2020-06-24 04:20:03', '2020-06-24 04:20:03'),
(77, 'British Shorthair', 'Available', 2, '2020-06-24 04:20:09', '2020-06-24 04:20:09'),
(78, 'Domestic Longhair', 'Available', 2, '2020-06-24 04:20:14', '2020-06-24 04:20:14'),
(79, 'Domestic Shorthair', 'Available', 2, '2020-06-24 04:20:20', '2020-06-24 04:20:20'),
(80, 'Exotic Shorthair', 'Available', 2, '2020-06-24 04:20:29', '2020-06-24 04:20:29'),
(81, 'Himalayan', 'Available', 2, '2020-06-24 04:20:33', '2020-06-24 04:20:33'),
(82, 'Mainecoon', 'Available', 2, '2020-06-24 04:20:40', '2020-06-24 04:20:40'),
(83, 'Minuet', 'Available', 2, '2020-06-24 04:20:45', '2020-06-24 04:20:45'),
(84, 'Mix Breed', 'Available', 2, '2020-06-24 04:20:51', '2020-06-24 04:20:51'),
(85, 'Munchkin', 'Available', 2, '2020-06-24 04:20:56', '2020-06-24 04:20:56'),
(86, 'Persian', 'Available', 2, '2020-06-24 04:21:01', '2020-06-24 04:21:01'),
(87, 'Ragdoll', 'Available', 2, '2020-06-24 04:21:06', '2020-06-24 04:21:06'),
(88, 'Scottishfold', 'Available', 2, '2020-06-24 04:21:12', '2020-06-24 04:21:12'),
(89, 'Scottishstraight', 'Available', 2, '2020-06-24 04:21:19', '2020-06-24 04:21:19'),
(90, 'Sphynx', 'Available', 2, '2020-06-24 04:21:24', '2020-06-24 04:21:24');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `status`, `date_created`, `date_updated`) VALUES
(1, 'Grooming', 'Available', '2020-04-21 10:24:39', '2020-04-21 10:24:39'),
(2, 'Accessory', 'Available', '2020-04-21 10:25:19', '2020-04-21 10:25:19'),
(3, 'Food', 'Available', '2020-04-21 10:25:33', '2020-04-21 10:25:33');

-- --------------------------------------------------------

--
-- Table structure for table `color`
--

CREATE TABLE `color` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `color`
--

INSERT INTO `color` (`id`, `name`, `status`, `type`, `date_created`, `date_updated`) VALUES
(4, 'Grey', 'Available', 2, '2020-04-21 08:05:51', '2020-04-21 08:05:51'),
(5, 'White', 'Available', 2, '2020-04-21 09:02:02', '2020-04-21 09:02:02'),
(6, 'Orange', 'Available', 2, '2020-04-21 09:13:20', '2020-04-21 09:13:20'),
(7, 'Black', 'Available', 3, '2020-04-21 09:15:13', '2020-04-21 09:15:13'),
(8, 'Red', 'Available', 2, '2020-04-21 09:19:46', '2020-04-21 09:19:46'),
(9, 'Green', 'Available', 3, '2020-04-21 09:32:48', '2020-04-21 09:32:48'),
(10, 'orange', 'Available', 3, '2020-05-27 07:11:39', '2020-05-27 07:11:39'),
(11, 'Apricot', 'Available', 1, '2020-07-01 08:14:32', '2020-07-01 08:14:32'),
(12, 'Black', 'Available', 1, '2020-07-01 08:14:44', '2020-07-01 08:14:44'),
(13, 'Black &amp; Tan', 'Available', 1, '2020-07-01 08:14:55', '2020-07-01 08:14:55'),
(14, 'Black &amp; White', 'Available', 1, '2020-07-01 08:15:04', '2020-07-01 08:15:04'),
(15, 'Blue Merle', 'Available', 1, '2020-07-01 08:15:13', '2020-07-01 08:15:13'),
(16, 'Brown', 'Available', 1, '2020-07-01 08:15:21', '2020-07-01 08:15:21'),
(17, 'Copper &amp; White', 'Available', 1, '2020-07-01 08:15:29', '2020-07-01 08:15:29'),
(18, 'Cream', 'Available', 1, '2020-07-01 08:15:38', '2020-07-01 08:15:38'),
(19, 'Cream White', 'Available', 1, '2020-07-01 08:15:45', '2020-07-01 08:15:45'),
(20, 'Fawn', 'Available', 1, '2020-07-01 08:15:52', '2020-07-01 08:15:52'),
(21, 'Fawn &amp; White', 'Available', 1, '2020-07-01 08:16:12', '2020-07-01 08:16:12'),
(22, 'Gold', 'Available', 1, '2020-07-01 08:16:20', '2020-07-01 08:16:20'),
(23, 'Latte', 'Available', 1, '2020-07-01 08:16:26', '2020-07-01 08:16:26'),
(24, 'Light Brown', 'Available', 1, '2020-07-01 08:16:32', '2020-07-01 08:16:32'),
(25, 'Liver &amp; Tan', 'Available', 1, '2020-07-01 08:16:38', '2020-07-01 08:16:38'),
(26, 'Orange', 'Available', 1, '2020-07-01 08:16:46', '2020-07-01 08:16:46'),
(27, 'Parti', 'Available', 1, '2020-07-01 08:16:53', '2020-07-01 08:16:53'),
(28, 'Red/Copper', 'Available', 1, '2020-07-01 08:17:00', '2020-07-01 08:17:00'),
(29, 'Red Merle', 'Available', 1, '2020-07-01 08:17:26', '2020-07-01 08:17:26'),
(30, 'Sable Brown', 'Available', 1, '2020-07-01 08:17:39', '2020-07-01 08:17:39'),
(31, 'Sable Gray', 'Available', 1, '2020-07-01 08:17:46', '2020-07-01 08:17:46'),
(32, 'Sable Orange', 'Available', 1, '2020-07-01 08:17:52', '2020-07-01 08:17:52'),
(33, 'Salt &amp; Pepper', 'Available', 1, '2020-07-01 08:17:59', '2020-07-01 08:17:59'),
(34, 'Silver', 'Available', 1, '2020-07-01 08:18:06', '2020-07-01 08:18:06'),
(35, 'Silver &amp; White', 'Available', 1, '2020-07-01 08:18:11', '2020-07-01 08:18:11'),
(36, 'Super Red', 'Available', 1, '2020-07-01 08:18:17', '2020-07-01 08:18:17'),
(37, 'Tri-Colour', 'Available', 1, '2020-07-01 08:18:25', '2020-07-01 08:18:25'),
(38, 'White', 'Available', 1, '2020-07-01 08:18:32', '2020-07-01 08:18:32');

-- --------------------------------------------------------

--
-- Table structure for table `credit_card`
--

CREATE TABLE `credit_card` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `name_on_card` varchar(255) DEFAULT NULL,
  `card_no` int(255) DEFAULT NULL,
  `card_type` varchar(255) DEFAULT NULL,
  `ccv` int(255) DEFAULT NULL,
  `expiry_date` varchar(255) DEFAULT NULL,
  `billing_address` varchar(255) DEFAULT NULL,
  `postal_code` int(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `favorite`
--

CREATE TABLE `favorite` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `item_uid` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `favorite`
--

INSERT INTO `favorite` (`id`, `uid`, `username`, `link`, `item_uid`, `type`, `remark`, `status`, `date_created`, `date_updated`) VALUES
(1, '9349aede685bae49c49b0b895e465f6d', NULL, '/mypetslibrary/puppyDogForSale.php?id=b13a810fdfa89e36da9138f592bf80a0', 'b13a810fdfa89e36da9138f592bf80a0', 'Puppy', NULL, 'Delete', '2020-11-20 10:02:37', '2020-11-20 10:02:51');

-- --------------------------------------------------------

--
-- Table structure for table `fb_login`
--

CREATE TABLE `fb_login` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'facebook_uid',
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'Female', 'Available', 1, '2020-05-29 06:33:21', '2020-05-29 06:33:21'),
(2, 'Male', 'Available', 1, '2020-05-29 06:33:21', '2020-05-29 06:33:21');

-- --------------------------------------------------------

--
-- Table structure for table `kitten`
--

CREATE TABLE `kitten` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `keyword_two` varchar(255) DEFAULT NULL,
  `price` decimal(8,0) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `vaccinated` varchar(255) DEFAULT NULL,
  `dewormed` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `feature` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `seller` varchar(255) DEFAULT NULL,
  `featured_seller` varchar(255) DEFAULT NULL,
  `details` text DEFAULT NULL,
  `details_two` text DEFAULT NULL,
  `details_three` text DEFAULT NULL,
  `details_four` text DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `image_six` varchar(255) DEFAULT NULL,
  `default_image` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kitten`
--

INSERT INTO `kitten` (`id`, `uid`, `name`, `sku`, `slug`, `keyword_one`, `keyword_two`, `price`, `age`, `vaccinated`, `dewormed`, `gender`, `color`, `size`, `status`, `feature`, `breed`, `seller`, `featured_seller`, `details`, `details_two`, `details_three`, `details_four`, `link`, `location`, `image_one`, `image_two`, `image_three`, `image_four`, `image_five`, `image_six`, `default_image`, `date_created`, `date_updated`) VALUES
(1, '1ec3535d4c7c8deaa3c0570e61a88b2f', ' ', 'Mar-LAP-09', ' ', ' ', NULL, '1588', '5.5 months', '1st Vaccination Done', 'Yes', 'Male', 'Grey', 'Medium', 'Available', ' ', 'Persian', 'a55f9ab3c32ebbc3e0baaa00ba07303b', 'Yes', 'DOB on 15th September 2019', NULL, NULL, NULL, '395647814', 'Kuala Lumpur', '16058566401ec3535d4c7c8deaa3c0570e61a88b2fkitten2a.jpg', '16058566401ec3535d4c7c8deaa3c0570e61a88b2fkitten2b.jpg', '16058566401ec3535d4c7c8deaa3c0570e61a88b2fkitten2c.jpg', '16058566401ec3535d4c7c8deaa3c0570e61a88b2fkitten2d.jpg', NULL, NULL, '16058566401ec3535d4c7c8deaa3c0570e61a88b2fkitten2a.jpg', '2020-11-20 07:17:04', '2020-11-20 07:17:04'),
(2, 'dfb7d6ce919317915d6f440dc25d8379', ' ', 'DEC-AZ-01', ' ', ' ', NULL, '888', '7 weeks', '1st Vaccination Done', 'Yes', 'Male', 'Grey', 'Medium', 'Available', ' ', 'Persian', '96efa305ea0de6aad93f3fe561f14bcc', 'Yes', '', NULL, NULL, NULL, '380199673', 'Johor', '1605856756dfb7d6ce919317915d6f440dc25d8379kitten4a.jpg', '1605856756dfb7d6ce919317915d6f440dc25d8379kitten4b.jpg', '1605856756dfb7d6ce919317915d6f440dc25d8379kitten4c.jpg', '1605856756dfb7d6ce919317915d6f440dc25d8379kitten4d.jpg', NULL, NULL, '1605856756dfb7d6ce919317915d6f440dc25d8379kitten4a.jpg', '2020-11-20 07:19:11', '2020-11-20 07:19:11'),
(3, '8ef8bcbcbb7c9949f998d8e4653673d3', ' ', 'NOV-MM-03', ' ', ' ', NULL, '10888', '4 months', '2nd Vaccination Done', 'Yes', 'Female', 'Grey', 'Medium', 'Available', ' ', 'Bengal', '96efa305ea0de6aad93f3fe561f14bcc', 'Yes', 'Bengal mix Scottish Fold', NULL, NULL, NULL, '353747694', 'Johor', '16058568848ef8bcbcbb7c9949f998d8e4653673d3kitten5a.jpg', '16058568848ef8bcbcbb7c9949f998d8e4653673d3kitten5b.jpg', '16058568848ef8bcbcbb7c9949f998d8e4653673d3kitten5c.jpg', '16058568848ef8bcbcbb7c9949f998d8e4653673d3kitten5d.jpg', NULL, NULL, '16058568848ef8bcbcbb7c9949f998d8e4653673d3kitten5a.jpg', '2020-11-20 07:21:11', '2020-11-20 07:21:11'),
(4, '4fef835bcb3e28830f755f960f0c8d7e', ' ', 'NOV-BGL-01', ' ', ' ', NULL, '1088', '4 months', '2nd Vaccination Done', 'Yes', 'Female', 'Grey', 'Medium', 'Available', ' ', 'Bengal', 'c61de2ff2583ec2c9f8ce06db86a7a97', 'Yes', '', NULL, NULL, NULL, '353747635', 'Penang', '16058569614fef835bcb3e28830f755f960f0c8d7ekitten1a.jpg', '16058569614fef835bcb3e28830f755f960f0c8d7ekitten1b.jpg', '16058569614fef835bcb3e28830f755f960f0c8d7ekitten1c.jpg', '16058569614fef835bcb3e28830f755f960f0c8d7ekitten1d.jpg', NULL, NULL, '16058569614fef835bcb3e28830f755f960f0c8d7ekitten1a.jpg', '2020-11-20 07:22:36', '2020-11-20 07:22:36'),
(5, '05cb1ff14b9db3a5e8bdb6750ba95061', ' ', 'Feb-CC-01', ' ', ' ', NULL, '338', '4 months', '2nd Vaccination Done', 'Yes', 'Female', 'White', 'Medium', 'Available', ' ', 'Bengal', 'bd53170fbcc524c73c53d25755f9dbac', 'Yes', 'DOB on 27th October 2019', NULL, NULL, NULL, '393865853', 'Selangor', '160585731405cb1ff14b9db3a5e8bdb6750ba95061kitten3a.jpg', '160585731405cb1ff14b9db3a5e8bdb6750ba95061kitten3b.jpg', '160585731505cb1ff14b9db3a5e8bdb6750ba95061kitten3c.jpg', '160585731505cb1ff14b9db3a5e8bdb6750ba95061kitten3d.jpg', NULL, NULL, '160585731405cb1ff14b9db3a5e8bdb6750ba95061kitten3a.jpg', '2020-11-20 07:28:28', '2020-11-20 07:28:28'),
(6, '0f582bbfbe28d278e9aa81fbb9bddcba', ' ', 'Feb-CC-01aa', ' ', ' ', NULL, '1000', '3 months', '1st Vaccination Done', 'Yes', 'Male', 'Orange', 'Small', 'Deleted', ' ', 'Ragdoll', 'c61de2ff2583ec2c9f8ce06db86a7a97', NULL, '', NULL, NULL, NULL, '414476414', 'Penang', '16058667840f582bbfbe28d278e9aa81fbb9bddcbacat1d.jpg', '16058667840f582bbfbe28d278e9aa81fbb9bddcbacat1a.jpg', '16058667850f582bbfbe28d278e9aa81fbb9bddcbacat1b.jpg', '16058667850f582bbfbe28d278e9aa81fbb9bddcbacat1c.jpg', NULL, NULL, '16058667840f582bbfbe28d278e9aa81fbb9bddcbacat1d.jpg', '2020-11-20 10:06:16', '2020-11-20 10:06:16');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(200) DEFAULT NULL COMMENT 'account that login by user to make order',
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` int(255) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL COMMENT 'person to receive the product for delivery',
  `contactNo` varchar(20) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `address_line_1` varchar(2500) DEFAULT NULL,
  `address_line_2` varchar(2500) DEFAULT NULL,
  `address_line_3` varchar(2500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `zipcode` varchar(100) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `subtotal` decimal(50,0) DEFAULT NULL,
  `total` decimal(50,0) DEFAULT NULL COMMENT 'include postage fees',
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_amount` int(255) DEFAULT NULL,
  `payment_bankreference` varchar(255) DEFAULT NULL,
  `payment_date` varchar(20) DEFAULT NULL,
  `payment_time` varchar(20) DEFAULT NULL,
  `payment_status` varchar(200) DEFAULT NULL COMMENT 'pending, accepted/completed, rejected, NULL = nothing',
  `shipping_status` varchar(200) DEFAULT NULL COMMENT 'pending, shipped, refunded',
  `shipping_method` varchar(200) DEFAULT NULL,
  `shipping_date` date DEFAULT NULL,
  `tracking_number` varchar(200) DEFAULT NULL,
  `reject_reason` varchar(255) DEFAULT NULL,
  `refund_method` varchar(255) DEFAULT NULL,
  `refund_amount` int(255) DEFAULT NULL,
  `refund_note` varchar(255) DEFAULT NULL,
  `refund_reason` varchar(255) DEFAULT NULL,
  `receipt` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `uid`, `username`, `bank_name`, `bank_account_holder`, `bank_account_no`, `name`, `contactNo`, `email`, `address_line_1`, `address_line_2`, `address_line_3`, `city`, `zipcode`, `state`, `country`, `subtotal`, `total`, `payment_method`, `payment_amount`, `payment_bankreference`, `payment_date`, `payment_time`, `payment_status`, `shipping_status`, `shipping_method`, `shipping_date`, `tracking_number`, `reject_reason`, `refund_method`, `refund_amount`, `refund_note`, `refund_reason`, `receipt`, `date_created`, `date_updated`) VALUES
(1, '309e26222d7dd0eb6eec23adddfb8c81', '9349aede685bae49c49b0b895e465f6d', NULL, NULL, NULL, NULL, 'user', '012123123', NULL, '123,asd asd', NULL, NULL, NULL, NULL, NULL, NULL, '6776', NULL, 'BILLPLZ', NULL, 'luq4ft5l', NULL, NULL, 'ACCEPTED', 'DELIVERED', 'Ninja Van', '2020-11-24', 'ASD123', NULL, NULL, NULL, NULL, NULL, '', '2020-11-27 03:21:22', '2020-11-27 06:33:57'),
(2, '73b4596d20fdc1ba29b32589cb1811a4', 'c2e4a20007e868502ea2857e094af93b', NULL, NULL, NULL, NULL, 'user2', '0124564561', NULL, '234, ABC, Jln MNB', NULL, NULL, NULL, NULL, NULL, NULL, '776', NULL, 'BILLPLZ', NULL, 'sa02sxmw', NULL, NULL, 'REJECTED', 'REJECTED', NULL, '2020-11-24', NULL, NULL, NULL, NULL, NULL, NULL, '', '2020-11-27 03:22:36', '2020-11-27 06:34:03'),
(3, '7e11a2d9a3c9537b147b618397efff49', 'c61de2ff2583ec2c9f8ce06db86a7a97', NULL, NULL, NULL, NULL, 'wiskey', '0121234567', NULL, '123, Wiskey Building, Jln Wiskey, 11950 PG.', NULL, NULL, NULL, NULL, NULL, NULL, '3776', NULL, 'BILLPLZ', NULL, 'ky0mue87', NULL, NULL, 'PENDING', 'TBC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2020-11-27 03:40:15', '2020-11-27 03:40:58'),
(4, 'ffec6bdefa6b746c841ed857be8febec', 'a55f9ab3c32ebbc3e0baaa00ba07303b', NULL, NULL, NULL, NULL, 'Durain Sam Gee Kann', '0107419963  Edit', NULL, '345, Durian SGK Building, Jln Win, 88120 KL. Kuala Lumpur', NULL, NULL, NULL, NULL, NULL, NULL, '388', NULL, 'BILLPLZ', NULL, NULL, NULL, NULL, 'WAITING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2020-11-27 03:43:47', '2020-11-27 03:44:19');

-- --------------------------------------------------------

--
-- Table structure for table `pet_details`
--

CREATE TABLE `pet_details` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `keyword_two` varchar(255) DEFAULT NULL,
  `price` decimal(8,0) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `vaccinated` varchar(255) DEFAULT NULL,
  `dewormed` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `feature` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `seller` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `details` text DEFAULT NULL,
  `details_two` text DEFAULT NULL,
  `details_three` text DEFAULT NULL,
  `details_four` text DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `image_six` varchar(255) DEFAULT NULL,
  `default_image` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pet_details`
--

INSERT INTO `pet_details` (`id`, `uid`, `name`, `sku`, `slug`, `keyword_one`, `keyword_two`, `price`, `age`, `vaccinated`, `dewormed`, `gender`, `color`, `size`, `status`, `feature`, `breed`, `seller`, `type`, `details`, `details_two`, `details_three`, `details_four`, `link`, `location`, `image_one`, `image_two`, `image_three`, `image_four`, `image_five`, `image_six`, `default_image`, `date_created`, `date_updated`) VALUES
(1, '17f37cc889a754f723f2eb48adff467f', ' ', 'MAY-PF-04', ' ', ' ', NULL, '3688', '6 weeks', '1st Vaccination Done', 'Yes', 'Male', 'Black', 'Large', 'Available', ' ', 'West Highland Terrier', 'c61de2ff2583ec2c9f8ce06db86a7a97', 'puppy', 'Alaskan Malamute x Wooly Husky  DOB on 20th March 2020', NULL, NULL, NULL, '416349175', 'Penang', '160584626617f37cc889a754f723f2eb48adff467fpuppy1a.jpg', '160584626617f37cc889a754f723f2eb48adff467fpuppy1b.jpg', '160584626617f37cc889a754f723f2eb48adff467fpuppy1c.jpg', '160584626617f37cc889a754f723f2eb48adff467fpuppy1d.jpg', NULL, NULL, '160584626617f37cc889a754f723f2eb48adff467fpuppy1a.jpg', '2020-11-20 04:24:19', '2020-11-20 04:24:19'),
(2, 'ff7d7525abf59f298be972ced3abf088', ' ', 'MAY-RIA-01', ' ', ' ', NULL, '18888', '2 months', '1st Vaccination Done', 'Yes', 'Female', 'Brown', 'Medium', 'Available', ' ', 'Akita', 'c61de2ff2583ec2c9f8ce06db86a7a97', 'puppy', 'DOB on 10th March 2020  With MKA Cert &amp; Microchip', NULL, NULL, NULL, '416350014', 'Penang', '1605846876ff7d7525abf59f298be972ced3abf088puppy2b.jpg', '1605846876ff7d7525abf59f298be972ced3abf088puppy2a.jpg', '1605846876ff7d7525abf59f298be972ced3abf088puppy2c.jpg', '1605846876ff7d7525abf59f298be972ced3abf088puppy2d.jpg', NULL, NULL, '1605846876ff7d7525abf59f298be972ced3abf088puppy2a.jpg', '2020-11-20 04:34:31', '2020-11-20 04:34:31'),
(3, 'd9379a1d674c6ea6722f110f259b1365', ' ', 'MAY-PW-01', ' ', ' ', NULL, '1288', '2 months', '1st Vaccination Done', 'Yes', 'Male', 'Black', 'Medium', 'Available', ' ', 'Tiny Poodle', '96efa305ea0de6aad93f3fe561f14bcc', 'puppy', 'good puppy, nice puppy', NULL, NULL, NULL, '383039356', 'Johor', '1605847072d9379a1d674c6ea6722f110f259b1365puppy6a.jpg', '1605847072d9379a1d674c6ea6722f110f259b1365puppy6c.jpg', '1605847072d9379a1d674c6ea6722f110f259b1365puppy6b.jpg', '1605847072d9379a1d674c6ea6722f110f259b1365puppy6d.jpg', NULL, NULL, '1605847072d9379a1d674c6ea6722f110f259b1365puppy6a.jpg', '2020-11-20 04:37:46', '2020-11-20 04:37:46'),
(4, '4301bfb5e9a13092989c3e952f277e2c', ' ', 'MAY-MP-SI', ' ', ' ', NULL, '3388', '3 months', '1st Vaccination Done', 'Yes', 'Female', 'Brown', 'Medium', 'Available', ' ', 'Yorkshire Terrier', 'bd53170fbcc524c73c53d25755f9dbac', 'puppy', 'DOB on 29th January 2020', NULL, NULL, NULL, '414476414', 'Selangor', '16058472284301bfb5e9a13092989c3e952f277e2cpuppy7b.jpg', '16058472284301bfb5e9a13092989c3e952f277e2cpuppy7a.jpg', '16058472284301bfb5e9a13092989c3e952f277e2cpuppy7c.jpg', '16058472334301bfb5e9a13092989c3e952f277e2cpuppy7d.jpg', NULL, NULL, '16058472284301bfb5e9a13092989c3e952f277e2cpuppy7b.jpg', '2020-11-20 04:40:23', '2020-11-20 04:40:23'),
(5, '52b7eec1156082f8795344a5e1671c43', ' ', 'APR-MP-35', ' ', ' ', NULL, '288', '3 months', '1st Vaccination Done', 'Yes', 'Female', 'Black', 'Medium', 'Available', ' ', 'Akita', 'b0353df94aade4ebd438b30eb3b3415c', 'puppy', 'DOB with 29th January 2020', NULL, NULL, NULL, '412836212', 'Penang', '160584735852b7eec1156082f8795344a5e1671c43puppy4a.jpg', '160584735852b7eec1156082f8795344a5e1671c43puppy4b.jpg', '160584735852b7eec1156082f8795344a5e1671c43puppy4c.jpg', '160584735952b7eec1156082f8795344a5e1671c43puppy4d.jpg', NULL, NULL, '160584735852b7eec1156082f8795344a5e1671c43puppy4a.jpg', '2020-11-20 04:42:32', '2020-11-20 04:42:32'),
(6, 'b13a810fdfa89e36da9138f592bf80a0', ' ', 'JUL-MP-08', ' ', ' ', NULL, '5000', '2 months', '1st Vaccination Done', 'Yes', 'Male', 'Copper &amp; White', 'Medium', 'Available', ' ', 'Welsh Corgi', 'bd53170fbcc524c73c53d25755f9dbac', 'puppy', '', NULL, NULL, NULL, '439881094', 'Selangor', '1605847720b13a810fdfa89e36da9138f592bf80a0puppy5a.jpg', '1605847720b13a810fdfa89e36da9138f592bf80a0puppy5b.jpg', '1605847720b13a810fdfa89e36da9138f592bf80a0puppy5c.jpg', '1605847720b13a810fdfa89e36da9138f592bf80a0puppy5d.jpg', NULL, NULL, '1605847720b13a810fdfa89e36da9138f592bf80a0puppy5a.jpg', '2020-11-20 04:48:34', '2020-11-20 04:48:34'),
(7, '04ed24816dbcb589074732dba38c9d79', ' ', '2142421421', ' ', ' ', NULL, '1000', '2 months', '1st Vaccination Done', 'Yes', 'Male', 'Black', 'Medium', 'Deleted', ' ', 'Bull Terrier', 'b0353df94aade4ebd438b30eb3b3415c', 'puppy', '-', NULL, NULL, NULL, '414476414', 'Penang', '160584807904ed24816dbcb589074732dba38c9d79pup1d.jpg', '160584807904ed24816dbcb589074732dba38c9d79pup1a.jpg', '160584807904ed24816dbcb589074732dba38c9d79pup1b.jpg', '160584807904ed24816dbcb589074732dba38c9d79pup1c.jpg', NULL, NULL, '160584807904ed24816dbcb589074732dba38c9d79pup1d.jpg', '2020-11-20 04:54:24', '2020-11-20 04:54:24'),
(8, '1ec3535d4c7c8deaa3c0570e61a88b2f', ' ', 'Mar-LAP-09', ' ', ' ', NULL, '1588', '5.5 months', '1st Vaccination Done', 'Yes', 'Male', 'Grey', 'Medium', 'Available', ' ', 'Persian', 'a55f9ab3c32ebbc3e0baaa00ba07303b', 'Kitten', 'DOB on 15th September 2019', NULL, NULL, NULL, '395647814', 'Kuala Lumpur', '16058566401ec3535d4c7c8deaa3c0570e61a88b2fkitten2a.jpg', '16058566401ec3535d4c7c8deaa3c0570e61a88b2fkitten2b.jpg', '16058566401ec3535d4c7c8deaa3c0570e61a88b2fkitten2c.jpg', '16058566401ec3535d4c7c8deaa3c0570e61a88b2fkitten2d.jpg', NULL, NULL, '16058566401ec3535d4c7c8deaa3c0570e61a88b2fkitten2a.jpg', '2020-11-20 07:17:04', '2020-11-20 07:17:04'),
(9, 'dfb7d6ce919317915d6f440dc25d8379', ' ', 'DEC-AZ-01', ' ', ' ', NULL, '888', '7 weeks', '1st Vaccination Done', 'Yes', 'Male', 'Grey', 'Medium', 'Available', ' ', 'Persian', '96efa305ea0de6aad93f3fe561f14bcc', 'Kitten', '', NULL, NULL, NULL, '380199673', 'Johor', '1605856756dfb7d6ce919317915d6f440dc25d8379kitten4a.jpg', '1605856756dfb7d6ce919317915d6f440dc25d8379kitten4b.jpg', '1605856756dfb7d6ce919317915d6f440dc25d8379kitten4c.jpg', '1605856756dfb7d6ce919317915d6f440dc25d8379kitten4d.jpg', NULL, NULL, '1605856756dfb7d6ce919317915d6f440dc25d8379kitten4a.jpg', '2020-11-20 07:19:11', '2020-11-20 07:19:11'),
(10, '8ef8bcbcbb7c9949f998d8e4653673d3', ' ', 'NOV-MM-03', ' ', ' ', NULL, '10888', '4 months', '2nd Vaccination Done', 'Yes', 'Female', 'Grey', 'Medium', 'Available', ' ', 'Bengal', '96efa305ea0de6aad93f3fe561f14bcc', 'Kitten', 'Bengal mix Scottish Fold', NULL, NULL, NULL, '353747694', 'Johor', '16058568848ef8bcbcbb7c9949f998d8e4653673d3kitten5a.jpg', '16058568848ef8bcbcbb7c9949f998d8e4653673d3kitten5b.jpg', '16058568848ef8bcbcbb7c9949f998d8e4653673d3kitten5c.jpg', '16058568848ef8bcbcbb7c9949f998d8e4653673d3kitten5d.jpg', NULL, NULL, '16058568848ef8bcbcbb7c9949f998d8e4653673d3kitten5a.jpg', '2020-11-20 07:21:11', '2020-11-20 07:21:11'),
(11, '4fef835bcb3e28830f755f960f0c8d7e', ' ', 'NOV-BGL-01', ' ', ' ', NULL, '1088', '4 months', '2nd Vaccination Done', 'Yes', 'Female', 'Grey', 'Medium', 'Available', ' ', 'Bengal', 'c61de2ff2583ec2c9f8ce06db86a7a97', 'Kitten', '', NULL, NULL, NULL, '353747635', 'Penang', '16058569614fef835bcb3e28830f755f960f0c8d7ekitten1a.jpg', '16058569614fef835bcb3e28830f755f960f0c8d7ekitten1b.jpg', '16058569614fef835bcb3e28830f755f960f0c8d7ekitten1c.jpg', '16058569614fef835bcb3e28830f755f960f0c8d7ekitten1d.jpg', NULL, NULL, '16058569614fef835bcb3e28830f755f960f0c8d7ekitten1a.jpg', '2020-11-20 07:22:36', '2020-11-20 07:22:36'),
(12, '05cb1ff14b9db3a5e8bdb6750ba95061', ' ', 'Feb-CC-01', ' ', ' ', NULL, '338', '4 months', '2nd Vaccination Done', 'Yes', 'Female', 'White', 'Medium', 'Available', ' ', 'Bengal', 'bd53170fbcc524c73c53d25755f9dbac', 'Kitten', 'DOB on 27th October 2019', NULL, NULL, NULL, '393865853', 'Selangor', '160585731405cb1ff14b9db3a5e8bdb6750ba95061kitten3a.jpg', '160585731405cb1ff14b9db3a5e8bdb6750ba95061kitten3b.jpg', '160585731505cb1ff14b9db3a5e8bdb6750ba95061kitten3c.jpg', '160585731505cb1ff14b9db3a5e8bdb6750ba95061kitten3d.jpg', NULL, NULL, '160585731405cb1ff14b9db3a5e8bdb6750ba95061kitten3a.jpg', '2020-11-20 07:28:28', '2020-11-20 07:28:28'),
(13, '3383fce517d26daa4ced4ebc4e2c8c13', ' ', 'rr', ' ', ' ', NULL, '1000', '3 months', '1st Vaccination Done', 'Yes', 'Female', 'Apricot', 'Small', 'Deleted', ' ', 'Bulldog', 'c61de2ff2583ec2c9f8ce06db86a7a97', 'puppy', '', NULL, NULL, NULL, '414476414', 'Penang', '16058667233383fce517d26daa4ced4ebc4e2c8c13puppy4.jpg', '16058667243383fce517d26daa4ced4ebc4e2c8c13puppy3.jpg', NULL, NULL, NULL, NULL, '16058667233383fce517d26daa4ced4ebc4e2c8c13puppy4.jpg', '2020-11-20 10:05:06', '2020-11-20 10:05:06'),
(14, '0f582bbfbe28d278e9aa81fbb9bddcba', ' ', 'Feb-CC-01aa', ' ', ' ', NULL, '1000', '3 months', '1st Vaccination Done', 'Yes', 'Male', 'Orange', 'Small', 'Deleted', ' ', 'Ragdoll', 'c61de2ff2583ec2c9f8ce06db86a7a97', 'Kitten', '', NULL, NULL, NULL, '414476414', 'Penang', '16058667840f582bbfbe28d278e9aa81fbb9bddcbacat1d.jpg', '16058667840f582bbfbe28d278e9aa81fbb9bddcbacat1a.jpg', '16058667850f582bbfbe28d278e9aa81fbb9bddcbacat1b.jpg', '16058667850f582bbfbe28d278e9aa81fbb9bddcbacat1c.jpg', NULL, NULL, '16058667840f582bbfbe28d278e9aa81fbb9bddcbacat1d.jpg', '2020-11-20 10:06:16', '2020-11-20 10:06:16');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `diamond` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `description_two` text DEFAULT NULL,
  `description_three` text DEFAULT NULL,
  `description_four` text DEFAULT NULL,
  `animal_type` varchar(255) DEFAULT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `image_six` varchar(255) DEFAULT NULL,
  `default_image` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `uid`, `category`, `brand`, `name`, `sku`, `slug`, `price`, `diamond`, `status`, `quantity`, `description`, `description_two`, `description_three`, `description_four`, `animal_type`, `keyword_one`, `link`, `image_one`, `image_two`, `image_three`, `image_four`, `image_five`, `image_six`, `default_image`, `date_created`, `date_updated`) VALUES
(1, 'e7ff1b7e0e2558ea87319ba24c0ce0f2', 'Accessory', 'Brand 1', 'SmartPhone', NULL, 'SmartPhone', '3388', '3388', 'Available', '125', 'Apple, Huawei, Samsung', NULL, NULL, NULL, 'Other', 'Apple, Huawei, Samsung, Smartphoe', '123456', '1606298576e7ff1b7e0e2558ea87319ba24c0ce0f21594603920iPhone11.jpg', '1606298576e7ff1b7e0e2558ea87319ba24c0ce0f21594604021iPhone11-Pro.jpg', '1606298576e7ff1b7e0e2558ea87319ba24c0ce0f21594604081iPhone11-Pro-Max.png', '1606298576e7ff1b7e0e2558ea87319ba24c0ce0f21594604659HuaweiP30Pro.png', '1606298576e7ff1b7e0e2558ea87319ba24c0ce0f21594604709HuaweiMate10.jpg', NULL, NULL, '2020-11-25 10:02:38', '2020-11-26 07:07:50'),
(2, '290cca1c94657733359dbdefaf512507', 'Accessory', 'Brand 2', 'Jersey', NULL, 'Jersey', '388', '388', 'Available', '550', 'Grade AAA Jersey', NULL, NULL, NULL, 'Other', 'Grade AAA Jersey, Good Quality', '12345123', '1606373752290cca1c94657733359dbdefaf5125071594605186FC-Barcelona.jpg', '1606373752290cca1c94657733359dbdefaf5125071594605129Real-Madrid.jpg', '1606373752290cca1c94657733359dbdefaf5125071594605336Manchester-City.jpg', '1606373752290cca1c94657733359dbdefaf5125071594605406Liverpool.jpg', NULL, NULL, NULL, '2020-11-26 06:55:37', '2020-11-26 07:09:33');

-- --------------------------------------------------------

--
-- Table structure for table `product_orders`
--

CREATE TABLE `product_orders` (
  `id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `quantity` int(10) NOT NULL,
  `final_price` decimal(50,0) NOT NULL COMMENT 'In points',
  `original_price` decimal(50,0) NOT NULL COMMENT 'In points',
  `discount_given` decimal(50,0) NOT NULL COMMENT 'in points',
  `totalProductPrice` decimal(50,0) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_orders`
--

INSERT INTO `product_orders` (`id`, `product_id`, `product_name`, `order_id`, `quantity`, `final_price`, `original_price`, `discount_given`, `totalProductPrice`, `date_created`, `date_updated`) VALUES
(1, 1, 'SmartPhone', 1, 2, '3388', '3388', '0', '6776', '2020-11-27 03:21:22', '2020-11-27 03:21:22'),
(2, 2, 'Jersey', 2, 2, '388', '388', '0', '776', '2020-11-27 03:22:36', '2020-11-27 03:22:36'),
(3, 1, 'SmartPhone', 3, 1, '3388', '3388', '0', '3388', '2020-11-27 03:40:15', '2020-11-27 03:40:15'),
(4, 2, 'Jersey', 3, 1, '388', '388', '0', '388', '2020-11-27 03:40:15', '2020-11-27 03:40:15'),
(5, 2, 'Jersey', 4, 1, '388', '388', '0', '388', '2020-11-27 03:43:47', '2020-11-27 03:43:47');

-- --------------------------------------------------------

--
-- Table structure for table `puppy`
--

CREATE TABLE `puppy` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `keyword_two` varchar(255) DEFAULT NULL,
  `price` decimal(8,0) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `vaccinated` varchar(255) DEFAULT NULL,
  `dewormed` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `feature` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `seller` varchar(255) DEFAULT NULL,
  `featured_seller` varchar(255) DEFAULT NULL,
  `details` text DEFAULT NULL,
  `details_two` text DEFAULT NULL,
  `details_three` text DEFAULT NULL,
  `details_four` text DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `image_six` varchar(255) DEFAULT NULL,
  `default_image` varchar(255) NOT NULL,
  `img_name` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puppy`
--

INSERT INTO `puppy` (`id`, `uid`, `name`, `sku`, `slug`, `keyword_one`, `keyword_two`, `price`, `age`, `vaccinated`, `dewormed`, `gender`, `color`, `size`, `status`, `feature`, `breed`, `seller`, `featured_seller`, `details`, `details_two`, `details_three`, `details_four`, `link`, `location`, `image_one`, `image_two`, `image_three`, `image_four`, `image_five`, `image_six`, `default_image`, `img_name`, `date_created`, `date_updated`) VALUES
(1, '17f37cc889a754f723f2eb48adff467f', ' ', 'MAY-PF-04', ' ', ' ', NULL, '3688', '6 weeks', '1st Vaccination Done', 'Yes', 'Male', 'Black', 'Large', 'Available', ' ', 'West Highland Terrier', 'c61de2ff2583ec2c9f8ce06db86a7a97', 'Yes', 'Alaskan Malamute x Wooly Husky  DOB on 20th March 2020', NULL, NULL, NULL, '416349175', 'Penang', '160584626617f37cc889a754f723f2eb48adff467fpuppy1a.jpg', '160584626617f37cc889a754f723f2eb48adff467fpuppy1b.jpg', '160584626617f37cc889a754f723f2eb48adff467fpuppy1c.jpg', '160584626617f37cc889a754f723f2eb48adff467fpuppy1d.jpg', NULL, NULL, '160584626617f37cc889a754f723f2eb48adff467fpuppy1a.jpg', NULL, '2020-11-20 04:24:19', '2020-11-20 04:24:19'),
(2, 'ff7d7525abf59f298be972ced3abf088', ' ', 'MAY-RIA-01', ' ', ' ', NULL, '18888', '2 months', '1st Vaccination Done', 'Yes', 'Female', 'Brown', 'Medium', 'Available', ' ', 'Akita', 'c61de2ff2583ec2c9f8ce06db86a7a97', 'Yes', 'DOB on 10th March 2020  With MKA Cert &amp; Microchip', NULL, NULL, NULL, '416350014', 'Penang', '1605846876ff7d7525abf59f298be972ced3abf088puppy2b.jpg', '1605846876ff7d7525abf59f298be972ced3abf088puppy2a.jpg', '1605846876ff7d7525abf59f298be972ced3abf088puppy2c.jpg', '1605846876ff7d7525abf59f298be972ced3abf088puppy2d.jpg', NULL, NULL, '1605846876ff7d7525abf59f298be972ced3abf088puppy2a.jpg', NULL, '2020-11-20 04:34:31', '2020-11-20 04:34:31'),
(3, 'd9379a1d674c6ea6722f110f259b1365', ' ', 'MAY-PW-01', ' ', ' ', NULL, '1288', '2 months', '1st Vaccination Done', 'Yes', 'Male', 'Black', 'Medium', 'Available', ' ', 'Tiny Poodle', '96efa305ea0de6aad93f3fe561f14bcc', 'Yes', 'good puppy, nice puppy', NULL, NULL, NULL, '383039356', 'Johor', '1605847072d9379a1d674c6ea6722f110f259b1365puppy6a.jpg', '1605847072d9379a1d674c6ea6722f110f259b1365puppy6c.jpg', '1605847072d9379a1d674c6ea6722f110f259b1365puppy6b.jpg', '1605847072d9379a1d674c6ea6722f110f259b1365puppy6d.jpg', NULL, NULL, '1605847072d9379a1d674c6ea6722f110f259b1365puppy6a.jpg', NULL, '2020-11-20 04:37:46', '2020-11-20 04:37:46'),
(4, '4301bfb5e9a13092989c3e952f277e2c', ' ', 'MAY-MP-SI', ' ', ' ', NULL, '3388', '3 months', '1st Vaccination Done', 'Yes', 'Female', 'Brown', 'Medium', 'Available', ' ', 'Yorkshire Terrier', 'bd53170fbcc524c73c53d25755f9dbac', 'Yes', 'DOB on 29th January 2020', NULL, NULL, NULL, '414476414', 'Selangor', '16058472284301bfb5e9a13092989c3e952f277e2cpuppy7b.jpg', '16058472284301bfb5e9a13092989c3e952f277e2cpuppy7a.jpg', '16058472284301bfb5e9a13092989c3e952f277e2cpuppy7c.jpg', '16058472334301bfb5e9a13092989c3e952f277e2cpuppy7d.jpg', NULL, NULL, '16058472284301bfb5e9a13092989c3e952f277e2cpuppy7b.jpg', NULL, '2020-11-20 04:40:23', '2020-11-20 04:40:23'),
(5, '52b7eec1156082f8795344a5e1671c43', ' ', 'APR-MP-35', ' ', ' ', NULL, '288', '3 months', '1st Vaccination Done', 'Yes', 'Female', 'Black', 'Medium', 'Available', ' ', 'Akita', 'b0353df94aade4ebd438b30eb3b3415c', 'Yes', 'DOB with 29th January 2020', NULL, NULL, NULL, '412836212', 'Penang', '160584735852b7eec1156082f8795344a5e1671c43puppy4a.jpg', '160584735852b7eec1156082f8795344a5e1671c43puppy4b.jpg', '160584735852b7eec1156082f8795344a5e1671c43puppy4c.jpg', '160584735952b7eec1156082f8795344a5e1671c43puppy4d.jpg', NULL, NULL, '160584735852b7eec1156082f8795344a5e1671c43puppy4a.jpg', NULL, '2020-11-20 04:42:32', '2020-11-20 04:42:32'),
(6, 'b13a810fdfa89e36da9138f592bf80a0', ' ', 'JUL-MP-08', ' ', ' ', NULL, '5000', '2 months', '1st Vaccination Done', 'Yes', 'Male', 'Copper &amp; White', 'Medium', 'Available', ' ', 'Welsh Corgi', 'bd53170fbcc524c73c53d25755f9dbac', 'Yes', '', NULL, NULL, NULL, '439881094', 'Selangor', '1605847720b13a810fdfa89e36da9138f592bf80a0puppy5a.jpg', '1605847720b13a810fdfa89e36da9138f592bf80a0puppy5b.jpg', '1605847720b13a810fdfa89e36da9138f592bf80a0puppy5c.jpg', '1605847720b13a810fdfa89e36da9138f592bf80a0puppy5d.jpg', NULL, NULL, '1605847720b13a810fdfa89e36da9138f592bf80a0puppy5a.jpg', NULL, '2020-11-20 04:48:34', '2020-11-20 04:48:34'),
(7, '04ed24816dbcb589074732dba38c9d79', ' ', '2142421421', ' ', ' ', NULL, '1000', '2 months', '1st Vaccination Done', 'Yes', 'Male', 'Black', 'Medium', 'Deleted', ' ', 'Bull Terrier', 'b0353df94aade4ebd438b30eb3b3415c', NULL, '-', NULL, NULL, NULL, '414476414', 'Penang', '160584807904ed24816dbcb589074732dba38c9d79pup1d.jpg', '160584807904ed24816dbcb589074732dba38c9d79pup1a.jpg', '160584807904ed24816dbcb589074732dba38c9d79pup1b.jpg', '160584807904ed24816dbcb589074732dba38c9d79pup1c.jpg', NULL, NULL, '160584807904ed24816dbcb589074732dba38c9d79pup1d.jpg', NULL, '2020-11-20 04:54:24', '2020-11-20 04:54:24'),
(8, '3383fce517d26daa4ced4ebc4e2c8c13', ' ', 'rr', ' ', ' ', NULL, '1000', '3 months', '1st Vaccination Done', 'Yes', 'Female', 'Apricot', 'Small', 'Deleted', ' ', 'Bulldog', 'c61de2ff2583ec2c9f8ce06db86a7a97', NULL, '', NULL, NULL, NULL, '414476414', 'Penang', '16058667233383fce517d26daa4ced4ebc4e2c8c13puppy4.jpg', '16058667243383fce517d26daa4ced4ebc4e2c8c13puppy3.jpg', NULL, NULL, NULL, NULL, '16058667233383fce517d26daa4ced4ebc4e2c8c13puppy4.jpg', NULL, '2020-11-20 10:05:06', '2020-11-20 10:05:06');

-- --------------------------------------------------------

--
-- Table structure for table `reported_article`
--

CREATE TABLE `reported_article` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `article_uid` varchar(255) DEFAULT NULL,
  `reason` text DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reported_article`
--

INSERT INTO `reported_article` (`id`, `uid`, `username`, `article_uid`, `reason`, `result`, `date_created`, `date_updated`) VALUES
(1, '2255a46dc84bcb6dc0ef8309a71e5ce0', 'user', 'b6514af54ff5209a1bc8b9e0ae1a6ce6', 'asd asd asd', NULL, '2020-05-13 04:24:36', '2020-05-13 04:24:36');

-- --------------------------------------------------------

--
-- Table structure for table `reported_reviews`
--

CREATE TABLE `reported_reviews` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `article_uid` varchar(255) DEFAULT NULL,
  `reason` text DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reported_reviews`
--

INSERT INTO `reported_reviews` (`id`, `uid`, `username`, `article_uid`, `reason`, `result`, `date_created`, `date_updated`) VALUES
(1, '87fe7166a03c03a394405881c8929e4f', 'user', 'c2df33a07c1570f52333555c813bbab5', 'asd', NULL, '2020-05-13 02:31:14', '2020-05-13 02:31:14');

-- --------------------------------------------------------

--
-- Table structure for table `reptile`
--

CREATE TABLE `reptile` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `keyword_two` varchar(255) DEFAULT NULL,
  `price` decimal(8,0) NOT NULL,
  `age` varchar(255) NOT NULL,
  `vaccinated` varchar(255) NOT NULL,
  `dewormed` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `feature` varchar(255) NOT NULL,
  `breed` varchar(255) NOT NULL,
  `seller` varchar(255) NOT NULL,
  `featured_seller` varchar(255) DEFAULT NULL,
  `details` text DEFAULT NULL,
  `details_two` text DEFAULT NULL,
  `details_three` text DEFAULT NULL,
  `details_four` text DEFAULT NULL,
  `link` varchar(255) NOT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) NOT NULL,
  `image_two` varchar(255) NOT NULL,
  `image_three` varchar(255) NOT NULL,
  `image_four` varchar(255) NOT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `image_six` varchar(255) DEFAULT NULL,
  `default_image` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `company_uid` varchar(255) DEFAULT NULL,
  `author_uid` varchar(255) DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `paragraph_one` text DEFAULT NULL,
  `paragraph_two` text DEFAULT NULL,
  `paragraph_three` text DEFAULT NULL,
  `rating` text DEFAULT NULL,
  `image` text DEFAULT NULL,
  `likes` varchar(255) DEFAULT '0',
  `report_ppl` varchar(255) DEFAULT NULL,
  `report_id` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `display` varchar(255) DEFAULT 'YES',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `uid`, `company_uid`, `author_uid`, `author_name`, `title`, `paragraph_one`, `paragraph_two`, `paragraph_three`, `rating`, `image`, `likes`, `report_ppl`, `report_id`, `type`, `display`, `date_created`, `date_updated`) VALUES
(1, '0fa9a146ad4eb0501f735e6b74f046cb', 'b312b2a08c82e29a07d4e54daf2b765c', '9349aede685bae49c49b0b895e465f6d', 'user', 'Wiskey', 'review test one', NULL, NULL, '1', '0fa9a146ad4eb0501f735e6b74f046cbFacebookBanner-like.png', '0', NULL, NULL, NULL, 'Rejected', '2020-05-11 01:46:58', '2020-05-15 10:00:37'),
(2, 'c2df33a07c1570f52333555c813bbab5', 'b312b2a08c82e29a07d4e54daf2b765c', '9349aede685bae49c49b0b895e465f6d', 'user', 'Wiskey', 'review test two', NULL, NULL, '2', 'c2df33a07c1570f52333555c813bbab5image-01.png', '6', 'user', '87fe7166a03c03a394405881c8929e4f', 'Reported', 'Yes', '2020-05-11 01:49:14', '2020-05-18 07:42:40'),
(3, '09d79c335af30a8f3ec949d9d1670b0b', 'b312b2a08c82e29a07d4e54daf2b765c', '9349aede685bae49c49b0b895e465f6d', 'user', 'Wiskey', 'review test three', NULL, NULL, '3', '09d79c335af30a8f3ec949d9d1670b0bimage-02.jpg', '4', NULL, NULL, NULL, 'Yes', '2020-05-11 01:50:31', '2020-05-18 07:19:05'),
(4, 'a672bbbbd21bf0d5fd918549600b842a', 'b312b2a08c82e29a07d4e54daf2b765c', '9349aede685bae49c49b0b895e465f6d', 'user', 'Wiskey', 'review test four', NULL, NULL, '4', 'a672bbbbd21bf0d5fd918549600b842aimage-03.jpg', '0', NULL, NULL, NULL, 'Pending', '2020-05-11 01:50:58', '2020-05-15 10:00:42'),
(5, 'f43e93914a35b0796266a626178fda52', 'b312b2a08c82e29a07d4e54daf2b765c', '9349aede685bae49c49b0b895e465f6d', 'user', 'Wiskey', 'review test five', NULL, NULL, '5', 'f43e93914a35b0796266a626178fda52image-04.png', '2', NULL, NULL, NULL, 'Yes', '2020-05-11 01:51:10', '2020-05-18 07:19:07'),
(6, '901d3e7f615b01156b0ef5be37c31d13', 'b312b2a08c82e29a07d4e54daf2b765c', '9349aede685bae49c49b0b895e465f6d', 'user', 'Wiskey', 'review test six', NULL, NULL, '1', '901d3e7f615b01156b0ef5be37c31d13image-05.png', '0', NULL, NULL, NULL, 'Pending', '2020-05-11 01:51:42', '2020-05-15 10:00:47'),
(7, 'fb724c31955b569b77437b11f8e6a10b', 'b312b2a08c82e29a07d4e54daf2b765c', '9349aede685bae49c49b0b895e465f6d', 'user', 'Wiskey', 'review test seven', NULL, NULL, '2', 'fb724c31955b569b77437b11f8e6a10bimage-06.png', '0', NULL, NULL, NULL, 'Pending', '2020-05-11 01:51:53', '2020-05-15 10:00:49');

-- --------------------------------------------------------

--
-- Table structure for table `reviews_respond`
--

CREATE TABLE `reviews_respond` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `review_uid` varchar(255) DEFAULT NULL,
  `like_amount` int(255) DEFAULT NULL,
  `dislike_amount` int(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `seller`
--

CREATE TABLE `seller` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'random user id',
  `company_name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `registration_no` varchar(255) DEFAULT NULL,
  `contact_no` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `account_status` varchar(255) DEFAULT NULL,
  `featured_seller` varchar(255) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `contact_person_no` varchar(255) DEFAULT NULL,
  `sec_contact_person` varchar(255) DEFAULT NULL,
  `sec_contact_person_no` varchar(255) DEFAULT NULL,
  `experience` varchar(255) DEFAULT NULL,
  `cert` varchar(255) DEFAULT NULL,
  `services` varchar(255) DEFAULT NULL,
  `breed_type` varchar(255) DEFAULT NULL,
  `other_info` text DEFAULT NULL,
  `info_two` text DEFAULT NULL,
  `info_three` text DEFAULT NULL,
  `info_four` text DEFAULT NULL,
  `company_logo` varchar(255) DEFAULT NULL,
  `company_pic` varchar(255) DEFAULT NULL,
  `rating` varchar(255) DEFAULT NULL,
  `credit` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seller`
--

INSERT INTO `seller` (`id`, `uid`, `company_name`, `slug`, `registration_no`, `contact_no`, `email`, `address`, `state`, `country`, `account_status`, `featured_seller`, `contact_person`, `contact_person_no`, `sec_contact_person`, `sec_contact_person_no`, `experience`, `cert`, `services`, `breed_type`, `other_info`, `info_two`, `info_three`, `info_four`, `company_logo`, `company_pic`, `rating`, `credit`, `date_created`, `date_updated`) VALUES
(1, 'c61de2ff2583ec2c9f8ce06db86a7a97', 'Wiskey', 'Wiskey', 'wiskeypg', 'https://api.whatsapp.com/send?phone=60121234567', 'wiskey100@gmail.com', '123, Wiskey Building, Jln Wiskey, 11950 PG.', 'Penang', NULL, 'Active', NULL, 'Wiskey Admin', '0123456789', NULL, NULL, '10', 'QWERTY, ASD', '1,2,3', 'Breed 1, Breed 2, Breed 3', 'Other Info 1', 'Other Info 2', 'Other Info 3', 'Other Info 4', 'c61de2ff2583ec2c9f8ce06db86a7a971605842716.png', 'c61de2ff2583ec2c9f8ce06db86a7a971605842878.png', NULL, '95', '2020-11-20 03:20:39', '2020-11-20 03:20:39'),
(2, '96efa305ea0de6aad93f3fe561f14bcc', 'Win Win Pet House', 'Win-Win-Pet-House', 'winwinpethousejb', 'https://api.whatsapp.com/send?phone=60107418520', 'win125@gmail.com', '234, Win Win Building, Jln Win, 00100 JB.', 'Johor', NULL, 'Active', NULL, 'WWPH Admin', '0127418520', NULL, NULL, '10', 'QWERTY, ZXC', '1, 4', 'Breed 1, Breed 2', 'Other Info 1', 'Other Info 12', 'Other Info 123', '', '96efa305ea0de6aad93f3fe561f14bcc1605843836.png', '96efa305ea0de6aad93f3fe561f14bcc1605843843.png', NULL, '122', '2020-11-20 03:43:46', '2020-11-20 03:43:46'),
(3, 'a55f9ab3c32ebbc3e0baaa00ba07303b', 'Durain Sam Gee Kann', 'Durain-Sam-Gee-Kann', 'duriansamgeekannkl', 'https://api.whatsapp.com/send?phone=60107419963', 'duriansgk150@gmail.com', '345, Durian SGK Building, Jln Win, 88120 KL.', 'Kuala Lumpur', NULL, 'Active', NULL, 'Durain Sam Gee Kann Admin', '0127419963', NULL, NULL, '5', 'QWERT, QWERTY', '1, 4, 5', 'Breed 1, Breed 3', 'Other Info 1', 'Other Info 12', 'Other Info 123', 'Other Info 1234', 'a55f9ab3c32ebbc3e0baaa00ba07303b1605844161.png', 'a55f9ab3c32ebbc3e0baaa00ba07303b1605844167.png', NULL, '149', '2020-11-20 03:49:14', '2020-11-20 03:49:14'),
(4, 'bd53170fbcc524c73c53d25755f9dbac', 'Happy Paws Paradise Trading', 'Happy-Paws-Paradise-Trading', 'happyppts', 'https://api.whatsapp.com/send?phone=60108520885', 'happyppt175@gmail.com', '44, Happy PP Building, Jln Win, 01120 Selangor.', 'Selangor', NULL, 'Active', NULL, 'Happy Paws Paradise Trading Admin', '0128520885', NULL, NULL, '6', 'ASD, ZXC', '1, 2, 3', 'Breed 01, Breed 04', 'Other Info 1', 'Other Info 1, Other Info 2', '', '', 'bd53170fbcc524c73c53d25755f9dbac1605844314.png', 'bd53170fbcc524c73c53d25755f9dbac1605844320.png', NULL, '172', '2020-11-20 03:51:48', '2020-11-20 03:51:48'),
(5, 'b0353df94aade4ebd438b30eb3b3415c', 'JPuppies Corner', 'JPuppies-Corner', 'jpuppiescornerpg', 'https://api.whatsapp.com/send?phone=60107788997', 'jpuppiescorner@gmail.com', '34, JPC Building, Jln JPC, 12345 PG.', 'Penang', NULL, 'Active', NULL, 'JPuppies Corner Admin', '0127788997', NULL, NULL, '8', 'Cert 1, Cert 2, Cert 3', '2,5', 'Breed 1, Breed 3, Breed 5', 'Other Info 111', 'Other Info 222', 'Other Info 333', '', 'b0353df94aade4ebd438b30eb3b3415c1605844812.png', NULL, NULL, '0', '2020-11-20 04:00:06', '2020-11-20 04:00:06');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) NOT NULL,
  `service` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'Grooming Services', 'Available', 1, '2020-07-03 01:45:10', '2020-07-03 02:07:36'),
(2, 'Pets Hotel / Boarding Services', 'Available', 1, '2020-07-03 01:45:34', '2020-07-03 02:15:12'),
(3, 'Stud Services', 'Available', 1, '2020-07-03 01:45:44', '2020-07-03 01:45:44'),
(4, 'Delivery Services / Pet Taxi', 'Available', 1, '2020-07-03 01:46:08', '2020-07-03 01:46:08'),
(5, 'Selling Pet Food &amp; Pet Accessories', 'Available', 1, '2020-07-03 01:46:28', '2020-07-03 01:46:28');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `img_name` text DEFAULT NULL,
  `link` text DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `uid`, `img_name`, `link`, `status`, `date_created`, `date_updated`) VALUES
(1, '052f3bd0866f1f6847471f054e005fd7', '1595496209052f3bd0866f1f6847471f054e005fd7.png', 'https://pariepets.com/dog.html', 'Show', '2020-05-15 02:23:59', '2020-07-23 09:23:29'),
(2, '27c4b80568e129104977f628cf1f33bf', '159549622827c4b80568e129104977f628cf1f33bf.png', 'https://pariepets.com/cat.html', 'Show', '2020-05-15 02:24:20', '2020-07-23 09:23:48'),
(3, '650fbc9ff8a916fcc1920fca983fa937', '650fbc9ff8a916fcc1920fca983fa9371589510512poster3.jpg', 'https://mypetslibrary.com/', 'Delete', '2020-05-15 02:41:52', '2020-07-08 01:14:17');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` bigint(20) NOT NULL,
  `state_name` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `state_name`, `date_created`, `date_updated`) VALUES
(1, 'Johor', '2020-05-22 06:42:19', '2020-05-29 01:44:12'),
(2, 'Kedah', '2020-05-22 06:42:48', '2020-05-29 01:44:08'),
(3, 'Kelantan', '2020-05-22 06:42:48', '2020-05-29 01:44:03'),
(4, 'Negeri Sembilan', '2020-05-22 06:43:46', '2020-05-29 01:43:59'),
(5, 'Pahang', '2020-05-22 06:43:46', '2020-05-29 01:43:55'),
(6, 'Perak', '2020-05-22 06:44:16', '2020-05-29 01:43:52'),
(7, 'Perlis', '2020-05-22 06:44:16', '2020-05-29 01:43:50'),
(8, 'Selangor', '2020-05-22 06:44:37', '2020-05-29 01:43:45'),
(9, 'Terengannu', '2020-05-22 06:44:37', '2020-05-29 01:43:40'),
(10, 'Malacca', '2020-05-22 06:44:48', '2020-05-22 06:44:48'),
(11, 'Penang', '2020-05-22 06:44:48', '2020-05-22 06:44:48'),
(12, 'Sabah ', '2020-05-22 06:44:58', '2020-05-22 06:44:58'),
(13, 'Sarawak', '2020-05-22 06:44:58', '2020-05-22 06:44:58'),
(14, 'Kuala Lumpur', '2020-05-22 06:48:57', '2020-05-22 06:48:57'),
(15, 'Labuan', '2020-05-22 06:48:57', '2020-05-22 06:48:57'),
(16, 'Putrajaya', '2020-05-22 06:49:05', '2020-05-22 06:49:05');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'random user id',
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `tac` varchar(255) DEFAULT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `fb_id` varchar(255) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `account_status` varchar(255) DEFAULT NULL,
  `receiver_name` varchar(255) DEFAULT NULL,
  `receiver_contact_no` varchar(255) DEFAULT NULL,
  `shipping_state` varchar(255) DEFAULT NULL,
  `shipping_area` varchar(255) DEFAULT NULL,
  `shipping_postal_code` varchar(255) DEFAULT NULL,
  `shipping_address` text DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `name_on_card` varchar(255) DEFAULT NULL,
  `card_no` varchar(255) DEFAULT NULL,
  `card_type` varchar(255) DEFAULT NULL,
  `expiry_date` varchar(255) DEFAULT NULL,
  `ccv` int(255) DEFAULT NULL,
  `postal_code` int(255) DEFAULT NULL,
  `billing_address` varchar(255) DEFAULT NULL,
  `profile_pic` varchar(255) DEFAULT 'profile-pic.jpg',
  `points` varchar(255) DEFAULT NULL,
  `favorite_puppy` text DEFAULT NULL,
  `favorite_kitten` text DEFAULT NULL,
  `favorite_reptile` text DEFAULT NULL,
  `favorite_product` text DEFAULT NULL,
  `email_verification_code` varchar(10) DEFAULT NULL,
  `is_email_verified` tinyint(1) NOT NULL DEFAULT 1,
  `is_phone_verified` tinyint(1) NOT NULL DEFAULT 0,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `name`, `email`, `country`, `phone_no`, `tac`, `password`, `salt`, `user_type`, `fb_id`, `birthday`, `gender`, `account_status`, `receiver_name`, `receiver_contact_no`, `shipping_state`, `shipping_area`, `shipping_postal_code`, `shipping_address`, `bank_name`, `bank_account_holder`, `bank_account_no`, `name_on_card`, `card_no`, `card_type`, `expiry_date`, `ccv`, `postal_code`, `billing_address`, `profile_pic`, `points`, `favorite_puppy`, `favorite_kitten`, `favorite_reptile`, `favorite_product`, `email_verification_code`, `is_email_verified`, `is_phone_verified`, `date_created`, `date_updated`) VALUES
(1, 'd156938c9b63fd253e9cddf89eba10e7', 'admin', 'admin@gmail.com', 'Malaysia', '123321', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-04-28 04:05:16', '2020-11-20 03:10:39'),
(2, '9349aede685bae49c49b0b895e465f6d', 'user', 'user@gmail.com', 'Malaysia', '123321', NULL, 'fb1343bf3ac191ede70b6d323ac4a19b10905f49ad1df13338c5b6a40ca33f54', 'e70e7e71ece4a3469028b5a4b2cf316bbb448d70', 1, NULL, NULL, NULL, NULL, 'asd', '0126012', 'cvb', 'asd zxc', '123321', '123,asd asd', 'HSBC', 'Oliver Queen', '1234455667', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1605842197.png', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-04-28 04:05:55', '2020-11-20 03:16:37'),
(3, 'c2e4a20007e868502ea2857e094af93b', 'user2', 'user2@gmail.com', 'Singapore', '123321', NULL, 'fa0a1acac2e0844b2a9996cf8d57d86db163b04f7e93cdd157056992962273e4', '84d6588f07b2dd352d4e413a2dfe53fad13ff245', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-04-28 04:06:21', '2020-11-20 03:16:05'),
(4, 'c61de2ff2583ec2c9f8ce06db86a7a97', 'Wiskey', 'wiskey100@gmail.com', NULL, '0121234567', NULL, 'a231741b222e96c7c77589785a7260cb883a3272a982da2f71eb91321c8f06e0', '326342e1735583a8891a99442c2461bc7368babc', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-11-20 03:20:39', '2020-11-27 03:28:11'),
(5, '96efa305ea0de6aad93f3fe561f14bcc', 'Win Win Pet House', 'win125@gmail.com', NULL, '0107418520', NULL, 'bb2395b4c027ecf3d4b88f2c049bab6b1a3a41e401bd64202a63a20cead944a2', '6e97333a6ebb3c81dc0f07f501ed0853fe771945', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-11-20 03:43:46', '2020-11-27 03:28:22'),
(6, 'a55f9ab3c32ebbc3e0baaa00ba07303b', 'Durain Sam Gee Kann', 'duriansgk150@gmail.com', NULL, '0107419963', NULL, '0f7d3195f42127c7258c35b6fdebbb78a5749b7e5caa0ab52ca56db8fe952218', '48caa9c825eaf7236c615a9d5d19bea0c0029a13', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-11-20 03:49:14', '2020-11-27 03:28:36'),
(7, 'bd53170fbcc524c73c53d25755f9dbac', 'Happy Paws Paradise Trading', 'happyppt175@gmail.com', NULL, '0108520885', NULL, 'd7ded5b0b0f75df5dd4e98bdcccac835a4b25add8c1a631da2b27fd37950a8e1', '738bb9fca4213585f68487885b6df8d77eb6f528', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-11-20 03:51:48', '2020-11-27 03:28:47'),
(8, 'b0353df94aade4ebd438b30eb3b3415c', 'JPuppies Corner', 'jpuppiescorner@gmail.com', NULL, '0107788997', NULL, 'b12d14c1b3bd4ae1c0da027a0305340b120db315181c4b42ca6b33e0c3b81cf6', '67ea74dd56d6369b130f1dbc0dc03d500e87c514', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-11-20 04:00:06', '2020-11-27 03:29:01');

-- --------------------------------------------------------

--
-- Table structure for table `vaccination`
--

CREATE TABLE `vaccination` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vaccination`
--

INSERT INTO `vaccination` (`id`, `name`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, '1st Vaccination Done', 'Available', 1, '2020-06-25 01:40:17', '2020-06-25 01:40:17'),
(2, '2nd Vaccination Done', 'Available', 1, '2020-06-25 01:40:17', '2020-06-25 01:40:17'),
(3, '3rd Vaccination Done', 'Available', 1, '2020-06-25 01:40:45', '2020-06-25 01:40:45'),
(4, 'No', 'Available', 1, '2020-06-25 01:40:45', '2020-06-25 01:40:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `breed`
--
ALTER TABLE `breed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_card`
--
ALTER TABLE `credit_card`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorite`
--
ALTER TABLE `favorite`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fb_login`
--
ALTER TABLE `fb_login`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uid` (`uid`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kitten`
--
ALTER TABLE `kitten`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pet_details`
--
ALTER TABLE `pet_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `puppy`
--
ALTER TABLE `puppy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reported_article`
--
ALTER TABLE `reported_article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reported_reviews`
--
ALTER TABLE `reported_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reptile`
--
ALTER TABLE `reptile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews_respond`
--
ALTER TABLE `reviews_respond`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller`
--
ALTER TABLE `seller`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`name`);

--
-- Indexes for table `vaccination`
--
ALTER TABLE `vaccination`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `breed`
--
ALTER TABLE `breed`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `color`
--
ALTER TABLE `color`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `credit_card`
--
ALTER TABLE `credit_card`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `favorite`
--
ALTER TABLE `favorite`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fb_login`
--
ALTER TABLE `fb_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kitten`
--
ALTER TABLE `kitten`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pet_details`
--
ALTER TABLE `pet_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_orders`
--
ALTER TABLE `product_orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `puppy`
--
ALTER TABLE `puppy`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `reported_article`
--
ALTER TABLE `reported_article`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reported_reviews`
--
ALTER TABLE `reported_reviews`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reptile`
--
ALTER TABLE `reptile`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `reviews_respond`
--
ALTER TABLE `reviews_respond`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `seller`
--
ALTER TABLE `seller`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `vaccination`
--
ALTER TABLE `vaccination`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
