<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon2.php';

// require_once dirname(__FILE__) . '/classes/Kitten.php';
require_once dirname(__FILE__) . '/classes/Variation.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pets Products  Mypetslibrary" />
<title>Pets Products | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, buy kitten, buy cat, buy pet online, Malaysia, buy pet in Malaysia, 马来西亚,马来西亚宠物店, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<script src="js/jquery-2.2.0.min.js"></script>
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="fix-filter width100 small-padding3 overflow some-margin-top">
    <h1 class="green-text user-title left-align-title">Products</h1>
    <div class="filter-div">
        <a class="open-filter2 filter-a green-a ow-open-filter">Filter</a>
    </div>
</div>

<!-- Filter Modal -->
<div id="filter-modal2" class="modal-css ow-show-modal">

    <!-- Modal content -->
    <div class="modal-content-css big-filter-margin">

        <span class="close-css close-filter2">&times;</span>

        <div class="clear"></div>

        <h2 class="green-text h2-title">Filter</h2>
        
        <div class="green-border filter-border"></div>

        <div class="filter-div-section">
            <p class="filter-label filter-label3 green-filter">Category</p>
            <?php
            $query = "
            SELECT DISTINCT (category) FROM variation WHERE status = 'Available' 
            ";
            $statement = $connect->prepare($query);
            $statement->execute();
            $result = $statement->fetchAll();
            foreach($result as $row)
            {
            ?>
                <div class="filter-option">
                <label  class="filter-label filter-label2">
                        <input type="checkbox" class="common_selector filter-option category" value="<?php echo $row['category']; ?>" > <?php echo $row['category']; ?><span class="checkmark"></span>
                </label>  
                </div>                        
            <?php    
            }
            ?>
        </div>

        <div class="filter-div-section">
            <p class="filter-label filter-label3 green-filter">Rating</p>
            <?php
            $query = "
            SELECT DISTINCT (rating) FROM variation WHERE status = 'Available' ORDER BY rating ASC
            ";
            $statement = $connect->prepare($query);
            $statement->execute();
            $result = $statement->fetchAll();
            foreach($result as $row)
            {
            ?>
                <div class="filter-option">
                <label  class="filter-label filter-label2">
                        <input type="checkbox" class="common_selector filter-option rating" value="<?php echo $row['rating']; ?>" > <?php echo $row['rating']; ?><span class="checkmark"></span>
                </label>  
                </div>                        
            <?php    
            }
            ?>
        </div>

        <div class="filter-div-section">
            <p class="filter-label filter-label3 green-filter">Price</p>
                <div class="inline50-div overflow">
                    <p class="label-text">Min</p>
                    <select id="minPrice" class="line-select" name="minimum_price">
                        <!-- <option value="500">500</option>
                        <option value="1000">1K</option>
                        <option value="20000">20K</option> -->
                        <option value="10">RM10</option>
                        <option value="100">RM100</option>
                        <option value="500">RM500</option>
                        <option value="1000">RM1000</option>
                        <option value="2000">RM2000</option>
                        <option value="3000">RM3000</option>
                        <option value="4000">RM4000</option>
                        <option value="5000">RM5000</option>
                        <option value="6000">RM6000</option>
                        <option value="7000">RM7000</option>
                        <option value="8000">RM8000</option>
                        <option value="9000">RM9000</option>
                        <option value="10000">RM10000</option>
                        <option value="15000">RM15000</option>
                        <option value="20000">RM20000</option>
                    </select>
                </div>
                <div class="inline50-div second-inline50 overflow">
                    <p class="label-text">Max</p>
                    <select id="maxPrice" class="line-select" name="maximum_price">
                        <!-- <option value=""></option> -->
                        <option value=""></option>
                        <option value="10">RM10</option>
                        <option value="100">RM100</option>
                        <option value="500">RM500</option>
                        <option value="1000">RM1000</option>
                        <option value="2000">RM2000</option>
                        <option value="3000">RM3000</option>
                        <option value="4000">RM4000</option>
                        <option value="5000">RM5000</option>
                        <option value="6000">RM6000</option>
                        <option value="7000">RM7000</option>
                        <option value="8000">RM8000</option>
                        <option value="9000">RM9000</option>
                        <option value="10000">RM10000</option>
                        <option value="15000">RM15000</option>
                        <option value="20000">RM20000</option>
                    </select>
                </div>
        </div>

        <div class="green-button mid-btn-width clean mid-btn-margin close-filter2">Apply</div>

    </div>

</div>

</div>

<div class="width100 small-padding overflow min-height-with-filter filter-distance ow-filter-pet-data">
    <div class="width103">
        <div class="filter_data"></div>
    </div>
</div>

<div class="clear"></div>

    <style>
        .animated.slideUp{
            animation:none !important;}
        .animated{
            animation:none !important;}
        .kitten-a .hover1a{
            display:none !important;}
        .kitten-a .hover1b{
            display:inline-block !important;}
    </style>
    <script>
    $(document).ready(function(){

        filter_data();

        function filter_data()
        {
            $('.filter_data').html('<div id="loading" style="" ></div>');
            var action = 'filterdataProducts';
            // var minimum_price = $('#hidden_minimum_price').val();
            // var maximum_price = $('#hidden_maximum_price').val();
            var minimum_price = $('#minPrice').val();
            var maximum_price = $('#maxPrice').val();
            var category = get_filter('category');
            var rating = get_filter('rating');
            // var type = get_filter('type');
            // var gender = get_filter('gender');
            // var vaccinated = get_filter('vaccinated');
            // var breed = get_filter('breed');
            // var category = get_filter('category');
            $.ajax({
            url:"filterdataProducts.php",
            method:"POST",
            // data:{action:action, minimum_price:minimum_price, maximum_price:maximum_price, type:type, gender:gender, vaccinated:vaccinated,
            //     breed:breed, color:color },
            data:{action:action, minimum_price:minimum_price, maximum_price:maximum_price, category:category, rating:rating },
            success:function(data){
                    $('.filter_data').html(data);
            }
            });
        }

        function get_filter(class_name)
        {
            var filter = [];
            $('.'+class_name+':checked').each(function(){
                filter.push($(this).val());
            });
            return filter;
        }

        $('.common_selector').click(function(){
            filter_data();
        });
        $("#minPrice,#maxPrice").on('change',function(){
          filter_data();
        });

    });
    </script>

<script>
$("#show_hide").click(function(){
    $("#display").toggle();
	$("#hide1").toggle();
    if($(this).text() == "Show More"){
       $(this).text("Hide");
    }else{
       $(this).text("Show More");
    }
});

</script>

<script>
$("#show_hide2").click(function(){
    $("#display2").toggle();
	 $("#hide2").toggle();
    if($(this).text() == "Show More"){
       $(this).text("Hide");
    }else{
       $(this).text("Show More");
    }
});
</script>

    <?php include 'js.php'; ?>
    <?php include 'stickyDistance.php'; ?>
    <?php include 'stickyFooter.php'; ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            // $messageType = "Register Succesfully !!";
            $messageType = "Welcome to Mypetslibrary!<br>Your registration is successful.";
        }
        else if($_GET['type'] == 2)
        {
            // $messageType = "There are no referrer with this email ! Please register again";
            $messageType = "No such username ! Please register again";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "User does not exist! Try to login again";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>
