<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Kitten.php';
require_once dirname(__FILE__) . '/classes/Pets.php';
require_once dirname(__FILE__) . '/classes/Puppy.php';
require_once dirname(__FILE__) . '/classes/Reptile.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $puppies = getPuppy($conn, "WHERE featured_seller = 'Yes' ORDER BY date_created DESC ");
// $kittens = getKitten($conn, "WHERE featured_seller = 'Yes' ORDER BY date_created DESC ");

$puppies = getPuppy($conn, "WHERE featured_seller = 'Yes' AND status != 'Deleted' ORDER BY date_created DESC ");
$kittens = getKitten($conn, "WHERE featured_seller = 'Yes' AND status != 'Deleted' ORDER BY date_created DESC ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Featured Pets | Mypetslibrary" />
<title>Featured Pets | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance">

	<div class="width100">
		<div class="left-h1-div featured-left">
        <h1 class="green-text h1-title">Featured Puppies</h1>
        <div class="green-border"></div>
    </div>
    
    <div class="right-add-div featured-right">
      <a href="addFeaturedPuppies.php"><div class="green-button white-text">Add</div></a>
    </div>        

  </div>

  <div class="clear"></div>

	<div class="width103 border-separation">

    <?php
    $conn = connDB();
    if($puppies)
    {
      for($cnt = 0;$cnt < count($puppies) ;$cnt++)
      {
      ?>
          <div class="four-box-size"> 	
            <div class="shadow-white-box">
             <div class="square">
              <div class="width100 white-bg content">
                <img src="uploads/<?php echo $puppies[$cnt]->getDefaultImage();?>" alt="<?php echo $puppies[$cnt]->getBreed();?>" 
                title="<?php echo $puppies[$cnt]->getBreed();?>" class="width100 two-border-radius">
              </div>
             </div>
                <!-- Display none or add class hidden if the dog not yet sold -->
                <!--	<div class="sold-label sold-label3">Sold</div>    -->
                     
                  <?php 
                      $status = $puppies[$cnt]->getStatus();
                      if($status == 'Sold')
                      {
                      ?>
                          <div class="sold-label sold-label3">Sold</div>
                      <?php
                      }
                      else
                      {}
                  ?>

          <?php 
            $statusA = $puppies[$cnt]->getStatus();
            if($statusA == 'Sold')
            {
            ?>
                <div class="sold-label sold-label3">Sold</div> 
            <?php
            }
            else
            {}
          ?>

              <div class="width100 product-details-div">

                  <?php 
                    $dogGender = $puppies[$cnt]->getGender();
                    if($dogGender == 'Female')
                    {
                          $puppyGender = 'F';
                    }
                    elseif($dogGender == 'Male')
                    {
                          $puppyGender = 'M';
                    }
                  ?>

                <p class="width100 text-overflow slider-product-name"><?php echo $puppies[$cnt]->getBreed();?> | <?php echo $puppyGender ;?></p>
                <p class="width100 text-overflow slider-product-name">
                  <!-- <?php //echo $puppies[$cnt]->getSku();?> -->
                  <?php 
                    $dogSku = $puppies[$cnt]->getSku();
                    if($dogSku != "")
                    {
                      echo $dogSku;
                    }
                    else
                    {
                      echo "-";
                    }
                  ?>
                </p>
              </div>
            </div>        
              
            <form method="POST" action="utilities/adminDeleteFeaturedPuppyFunction.php">
                <button class="clean red-btn featured-same-button open-confirm" type="submit" name="pets_uid" value="<?php echo $puppies[$cnt]->getUid();?>">
                    Delete
                </button> 
            </form>

          </div>
      <?php
      }
      ?>
    <?php
    }
    $conn->close();
    ?>

  </div>

  <div class="clear"></div>
    
 	<div class="width100 border-separation">

    <div class="left-h1-div featured-left">
      <h1 class="green-text h1-title">Featured Kittens</h1>
      <div class="green-border"></div>
    </div>
    
    <div class="right-add-div featured-right">
      <a href="addFeaturedKittens.php"><div class="green-button white-text">Add</div></a>
    </div>        
          
    </div>

    <div class="clear"></div>

    <div class="width103 border-separation">

    <?php
    $conn = connDB();
    if($kittens)
    {
      for($cntAA = 0;$cntAA < count($kittens) ;$cntAA++)
      {
      ?>
          <div class="four-box-size"> 	
            <div class="shadow-white-box">
             <div class="square">
              <div class="width100 white-bg content">
                <img src="uploads/<?php echo $kittens[$cntAA]->getDefaultImage();?>" alt="<?php echo $kittens[$cntAA]->getBreed();?>" 
                title="<?php echo $kittens[$cntAA]->getBreed();?>" class="width100 two-border-radius">
              </div>
             </div>
                    <!-- Display none or add class hidden if the dog not yet sold -->
                    <!--	<div class="sold-label sold-label3">Sold</div>    -->
                     
                    <?php 
                      $statusA = $kittens[$cntAA]->getStatus();
                      if($statusA == 'Sold')
                      {
                      ?>
                          <div class="sold-label sold-label3">Sold</div>
                      <?php
                      }
                      else
                      {}
                    ?>

          <?php 
            $statusB = $kittens[$cntAA]->getStatus();
            if($statusB == 'Sold')
            {
            ?>
                <div class="sold-label sold-label3 all-pets-sold">Sold</div> 
            <?php
            }
            else
            {}
          ?>
                     
              <div class="width100 product-details-div">

                  <?php 
                    $catGender = $kittens[$cntAA]->getGender();
                    if($catGender == 'Female')
                    {
                          $kittenGender = 'F';
                    }
                    elseif($catGender == 'Male')
                    {
                          $kittenGender = 'M';
                    }
                  ?>

                <p class="width100 text-overflow slider-product-name"><?php echo $kittens[$cntAA]->getBreed();?> | <?php echo $kittenGender;?></p>
                <p class="width100 text-overflow slider-product-name">
                  <!-- <?php //echo $kittens[$cntAA]->getSku();?> -->
                  <?php 
                    $catSku = $kittens[$cntAA]->getSku();
                    if($catSku != "")
                    {
                      echo $catSku;
                    }
                    else
                    {
                      echo "-";
                    }
                  ?>
                </p>
              </div>
            </div> 

            <form method="POST" action="utilities/adminDeleteFeaturedKittenFunction.php">
                <button class="clean red-btn featured-same-button open-confirm" type="submit" name="pets_uid" value="<?php echo $kittens[$cntAA]->getUid();?>">
                    Delete
                </button> 
            </form>
           
          </div>
      <?php
      }
      ?>
    <?php
    }
    $conn->close();
    ?>

    </div>

  <div class="clear"></div>       
                
</div>        

<div class="clear"></div>
        
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Featured Puppy Added !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to add featured puppy !! ";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !! ";
        }
        else if($_GET['type'] == 4)
        {
          $messageType = "Featured Puppy Deleted !"; 
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Fail to delete puppy !! ";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "ERROR <br> Please Retry !! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Featured Kitten Added !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to add featured kitten !! ";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !! ";
        }
        else if($_GET['type'] == 4)
        {
          $messageType = "Featured Kitten Deleted !"; 
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Fail to delete kitten !! ";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "ERROR <br> Please Retry !! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

}
?>

</body>
</html>