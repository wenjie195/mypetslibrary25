<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $messageDetails = getMessage($conn);
$messageDetails = getMessage($conn," ORDER BY date_created DESC LIMIT 50 ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/liveChat.php" />
<meta property="og:title" content="Live Chat | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>Live Chat | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<link rel="canonical" href="https://gmvec.com/liveChat.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<div class="width100 same-padding overflow min-height100vh">

    <h2 class="h1-title left-title">Live Chat</h2><input type="button" name="btnClose" value="X" onclick="parent.$.colorbox.close()" class="close-button clean opacity-hover" /> 
	
    <div class="clear"></div>

	<div class="chat-section">
    	<div id="divLiveMessage"></div>
	</div>

	 <div class="clear"></div>

    <!-- <form action="utilities/sentMessageFunction.php" method="POST">
        <div class="width100 bottom-send-div">
            <input class="send-message clean"  type="text" placeholder="Your Message" id="message_details" name="message_details" required>  
        
            <button class="clean send-button" type="submit" name="submit">
                SENT
            </button>
        </div>
    </form> -->

    <?php
    if(isset($_SESSION['uid']))
    {
    ?>
        <form action="utilities/sentMessageFunction.php" method="POST">
            <div class="width100 bottom-send-div">
                <input class="send-message clean"  type="text" placeholder="Your Message" id="message_details" name="message_details" required>  
            
                <button class="clean send-button" type="submit" name="submit">
                    SENT
                </button>
            </div>
        </form>
    <?php
    }
    else
    {
    ?>
            <div class="width100 bottom-send-div open-userlogin">
                <input class="send-message clean"  type="text" placeholder="Your Message" >  
            
                <button class="clean send-button">
                    SENT
                </button>
            </div>	
	<!--
        Please Login 
        <a href="login.php">Here</a>-->
    <?php
    }
    ?>

	<div class="clear"></div>
	
</div>

<div class="clear"></div>
<style>
	.footer-div{
	display: none;}		
</style>
<?php include 'js.php'; ?>


<script type="text/javascript">
    $(document).ready(function()
    {
        $("#divLiveMessage").load("liveMessage.php");
    setInterval(function()
    {
        $("#divLiveMessage").load("liveMessage.php");
    }, 5000);
    });
</script>

</body>
</html>