<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Color.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
$userType = $_SESSION['usertype'];

$conn = connDB();

$colorDetails = getColor($conn," WHERE type = ? ",array("type"),array(1),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Puppy Color | Mypetslibrary" />
<title>Puppy Color | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance" id="myTable">
	<div class="width100">
        <div class="left-h1-div">
            <h1 class="green-text h1-title">Puppy Color</h1>
            <div class="green-border"></div>
        </div>
        <div class="mid-search-div">
        <form>
            <input class="line-input clean" type="text" id="myInput" onkeyup="myFunction()" placeholder="Search">
                <button class="search-btn hover1 clean">
                    <img src="img/search.png" class="visible-img hover1a" alt="Search" title="Search">
                    <img src="img/search2.png" class="visible-img hover1b" alt="Search" title="Search">
                </button>
            </form>
        </div>
        <div class="right-add-div">
        	<a href="addPuppyColor.php"><div class="green-button white-text">Add Color</div></a>
        </div>
        
    </div>
    <div class="clear"></div>
	<div class="width100 scroll-div border-separation">
    	<table class="green-table width100">
        	<thead>
            	<tr>
                	<th class="first-column">No.</th>
                    <th>Color</th>
                    <!--<th>Total Puppies Registered</th>-->
                    <th>Added On</th>
                    <!--<th>Sold</th>-->
                    <th>Status</th>

                    <?php 
                        if($userType == '0')
                        {
                        ?>
                            <th>Details</th>
                            <th>Delete</th>
                        <?php
                        }
                        else
                        {
                        ?>
                        <?php
                        }
                    ?> 

                    <!-- <th>Details</th>
                    <th>Delete</th> -->
                </tr>
            </thead>
            <tbody>
            <?php
                if($colorDetails)
                {
                    
                    for($cnt = 0;$cnt < count($colorDetails) ;$cnt++)
                    {?>
                        
                        <tr class="link-to-details">
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $colorDetails[$cnt]->getName();?></td>
                            <!-- <td>120</td> -->
                            <!--<td></td>-->
                            <td>
                                <?php echo $date = date("d-m-Y",strtotime($colorDetails[$cnt]->getDateCreated()));?>
                            </td>
                            <!-- <td>1000</td> -->
                            <!--<td></td>-->
                            <td><?php echo $colorDetails[$cnt]->getStatus();?></td>

                            <?php 
                                if($userType == '0')
                                {
                                ?>

                                <td>
                                    <form action="editColor.php" method="POST" class="hover1">
                                        <button class="clean hover1 transparent-button pointer" type="submit" name="color_id" value="<?php echo $colorDetails[$cnt]->getId();?>">
                                            <img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                                            <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                                        </button>
                                    </form> 
                                </td>

                                <td>
                                    <form method="POST" action="utilities/deleteColorFunction.php" class="hover1">
                                        <input type="hidden" value="<?php echo $colorDetails[$cnt]->getType();?>" name="color_type" id="color_type">
                                        <button class="clean hover1 transparent-button pointer" type="submit" name="color_id" value="<?php echo $colorDetails[$cnt]->getId();?>">
                                            <img src="img/delete1a.png" class="edit-icon1 hover1a" alt="Delete" title="Delete">
                                            <img src="img/delete3a.png" class="edit-icon1 hover1b" alt="Delete" title="Delete">
                                        </button>
                                    </form> 
                                </td>

                                <?php
                                }
                                else
                                {
                                ?>
                                <?php
                                }
                            ?> 

                        </tr>
                        <?php
                    }
                }
                ?>                                
            </tbody>
        </table>
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>
<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Colour Updated !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update colour !!";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }

        else if($_GET['type'] == 4)
        {
            $messageType = "The colour already existing !!";
        }

        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Colour Deleted !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to delete colour !!";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

}
?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>