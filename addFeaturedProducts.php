<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $products = getProduct($conn, "WHERE status = 'Available' ORDER BY date_created DESC");

// if (isset($_GET['search'])) {
//   $products = getProduct($conn," WHERE status = 'Available' AND name= ?",array("name"),array($_GET['search']), "s");
//   $featuredProduct = getProduct($conn," WHERE featured_product = 'Yes' AND status = 'Available' AND name = ?",array("name"),array($_GET['search']), "s");
// }else {
//   $products = getProduct($conn," WHERE status = 'Available' ");
//   $featuredProduct = getProduct($conn," WHERE featured_product = 'Yes' AND status = 'Available' ");
// }

if (isset($_GET['search'])) 
{
      $products = getProduct($conn," WHERE status = 'Available' AND name= ?",array("name"),array($_GET['search']), "s");
      $featuredProduct = getProduct($conn," WHERE feature = 'Yes' AND status = 'Available' AND name = ?",array("name"),array($_GET['search']), "s");
}
else 
{
      $products = getProduct($conn," WHERE status = 'Available' ");
      $featuredProduct = getProduct($conn," WHERE feature = 'Yes' AND status = 'Available' ");
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add Featured Products | Mypetslibrary" />
<title>Add Featured Products | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance  padding-bottom30">

      <div class="width100">
            <div class="left-h1-div featured-left">
                  <a href="featuredProducts.php"><h1 class="green-text h1-title opacity-hover"><img src="img/back2.png" class="back-png"> Add Featured Products</h1></a>
                  <div class="green-border"></div>
            </div>        
      </div>

      <div class="clear"></div>

  <!-- <div class="width100">
    <form>
    <input class="line-input clean" type="text" placeholder="Search Product Name or SKU">
    <button class="search-btn hover1 clean">
    <img src="img/search.png" class="visible-img hover1a" alt="Search" title="Search">
    <img src="img/search2.png" class="visible-img hover1b" alt="Search" title="Search">
    </button>
    </form>
  </div> -->

      <div class="width100">
            <form action="search/searchFunction.php" method="post">
                  <?php
                  if (isset($_GET['search'])) {
                  ?>
                  <input class="line-input clean" type="text" value="<?php echo $_GET['search'] ?>" name="search" placeholder="Search">
                  <?php
                  }else {
                  ?>
                  <input class="line-input clean" type="text" name="search" placeholder="Search">
                  <?php
                  }
                  ?>
                  <!-- SKU and pet name-->
                  <input type="hidden" name="location" value="<?php echo $_SERVER["PHP_SELF"] ?>">
                  <button class="search-btn hover1 clean" type="submit">
                        <img src="img/search.png" class="visible-img hover1a" alt="Search" title="Search">
                        <img src="img/search2.png" class="visible-img hover1b" alt="Search" title="Search">
                  </button>
            </form>
      </div>

      <div class="width103 border-separation" id="app">
            <?php
            if($featuredProduct)
            {
            $totalFeaturedProduct = count($featuredProduct);
            }
            else
            {   $totalFeaturedProduct = 0;   }
            ?>

            <?php
            if($totalFeaturedProduct >= 10)
            {

            if($products)
            {
                  for($cntAA = 0;$cntAA < count($products) ;$cntAA++)
                  {
                  ?>
                        <div class="four-box-size">
                              <div class="shadow-white-box">
                              	<div class="square">
                                    <div class="width100 white-bg content progressive">
                                    
                                          <img data-src="productImage/<?php echo $products[$cntAA]->getImageOne();?>" src="img/pet-load300.jpg" class="preview width100 two-border-radius lazy" alt="<?php echo $products[$cntAA]->getName();?>"
                                          title="<?php echo $products[$cntAA]->getName();?>" />
                                    
                                    </div>
								</div>
                              <div class="width100 product-details-div">
                                    <p class="width100 text-overflow slider-product-name"><?php echo $products[$cntAA]->getName();?></p>
                                    <p class="width100 text-overflow slider-product-price"><?php //echo $products[$cntAA]->getVariationOneprice();?></p>
                              </div>
                        </div>

                        <form method="POST" action="utilities/adminDeleteFeaturedProductFunction.php">
                              <button class="clean red-btn featured-same-button open-confirm" type="submit" name="product_uid" value="<?php echo $products[$cntAA]->getUid();?>">
                                    Delete
                              </button>
                        </form>

                  </div>
                  <?php
                  }
                  ?>
            <?php
            }

            }
            elseif($totalFeaturedProduct < 10)
            {

            if($products)
            {
                  for($cntAA = 0;$cntAA < count($products) ;$cntAA++)
                  {
                  ?>
                        <div class="four-box-size">
                              <div class="shadow-white-box">
                              	<div class="square">
                                    <div class="width100 white-bg content progressive">
                                    
                                          <img data-src="productImage/<?php echo $products[$cntAA]->getImageOne();?>" src="img/pet-load300.jpg" class="preview width100 two-border-radius lazy" alt="<?php echo $products[$cntAA]->getName();?>"
                                          title="<?php echo $products[$cntAA]->getName();?>" />
                                    </div>
								</div>
                              <div class="width100 product-details-div">
                                    <p class="width100 text-overflow slider-product-name"><?php echo $products[$cntAA]->getName();?></p>
                                    <p class="width100 text-overflow slider-product-price"><?php //echo $products[$cntAA]->getVariationOneprice();?></p>
                              </div>
                        </div>

                        <?php
                        $status = $products[$cntAA]->getFeature();
                        if($status == 'Yes')
                        {
                        ?>
                              <form method="POST" action="utilities/adminDeleteFeaturedProductFunction.php">
                                    <button class="clean red-btn featured-same-button open-confirm" type="submit" name="product_uid" value="<?php echo $products[$cntAA]->getUid();?>">
                                          Delete
                                    </button>
                              </form>
                        <?php
                        }
                        else
                        {
                        ?>
                              <form method="POST" action="utilities/adminAddFeaturedProductFunction.php">
                                    <button class="clean green-button featured-same-button" type="submit" name="product_uid" value="<?php echo $products[$cntAA]->getUid();?>">
                                          Add
                                    </button>
                              </form>
                        <?php
                        }
                        ?>

                  </div>
                  <?php
                  }
                  ?>
            <?php
            }

            }
            ?>
      </div>

  <div class="clear"></div>

</div>    

<div class="clear"></div>
        
<?php include 'js.php'; ?>

</body>
</html>