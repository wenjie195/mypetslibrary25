<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/lib/API.php';
require_once dirname(__FILE__) . '/lib/Connect.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/configuration.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

use Billplz\Minisite\API;
use Billplz\Minisite\Connect;

$conn = connDB();

$parameter = array(
    'collection_id' => !empty($collection_id) ? $collection_id : $_REQUEST['collection_id'],
    'email'=> isset($_REQUEST['email']) ? $_REQUEST['email'] : '',
    'mobile'=> isset($_REQUEST['mobile']) ? $_REQUEST['mobile'] : '',
    'name'=> isset($_REQUEST['name']) ? $_REQUEST['name'] : 'No Name',
    'amount'=> !empty($amount) ? $amount : $_REQUEST['amount'],
    'callback_url'=> $websiteurl . '/callback.php',
    'description'=> !empty($description) ? $description : $_REQUEST['description']
);

$optional = array(
    'redirect_url' => $websiteurl . '/redirect.php',
    'reference_1_label' => isset($reference_1_label) ? $reference_1_label : $_REQUEST['reference_1_label'],
    'reference_1' => isset($_REQUEST['reference_1']) ? $_REQUEST['reference_1'] : '',
    'reference_2_label' => isset($reference_2_label) ? $reference_2_label : $_REQUEST['reference_2_label'],
    'reference_2' => isset($_REQUEST['reference_2']) ? $_REQUEST['reference_2'] : '',
    'deliver' => 'false'
);

if (empty($parameter['mobile']) && empty($parameter['email'])) {
    $parameter['email'] = 'noreply@billplz.com';
}

if (!filter_var($parameter['email'], FILTER_VALIDATE_EMAIL)) {
    $parameter['email'] = 'noreply@billplz.com';
}

$connnect = (new Connect($api_key))->detectMode();
$billplz = new API($connnect);
list ($rheader, $rbody) = $billplz->toArray($billplz->createBill($parameter, $optional));
/***********************************************/
// Include tracking code here
/***********************************************/
if ($rheader !== 200) {
    if (defined('DEBUG')) {
        echo '<pre>'.print_r($rbody, true).'</pre>';
    }
    if (!empty($fallbackurl)) {
        header('Location: ' . $fallbackurl);
    }
}
// header('Location: ' . $rbody['url']);
if (isset($rbody['url']))
{
    // $billId = str_replace('https://www.billplz.com/bills/','',$rbody['url']);
    // $billId = str_replace('https://www.billplz-sandbox.com/bills/','',$rbody['url']);
    $billId = str_replace('https://billplz-staging.herokuapp.com/bills/','',$rbody['url']);
    $paymentStatus = 'PENDING';
    $shippingStatus = 'TBC';
    // $billId = str_replace('https://billplz-staging.herokuapp.com/bills/','',$rbody['url']); // $sandbox
    // updateDynamicData($conn,'orders','WHERE id =?', array('payment_bankreference'),array($billId,$_REQUEST['reference_2']), "ss");
    // updateDynamicData($conn,'orders','WHERE id =?', array('payment_bankreference','payment_status'),array($billId,$paymentStatus,$_REQUEST['reference_2']), "sss");
    updateDynamicData($conn,'orders','WHERE id =?', array('payment_bankreference','payment_status','shipping_status'),array($billId,$paymentStatus,$shippingStatus,$_REQUEST['reference_2']), "ssss");
    header('Location: ' . $rbody['url']);
}
else
{}