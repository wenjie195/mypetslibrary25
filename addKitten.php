<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Kitten.php';
require_once dirname(__FILE__) . '/classes/Color.php';
require_once dirname(__FILE__) . '/classes/Breed.php';
require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $sellerDetails = getSeller($conn);
// $sellerDetails = getSeller($conn," WHERE account_status = 'Active' ");
$sellerDetails = getSeller($conn," WHERE account_status = 'Active' and credit > 1");

$colorDetails = getColor($conn," WHERE type = ? ",array("type"),array(2),"s");
$breedDetails = getBreed($conn," WHERE type = ? ",array("type"),array(2),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add a Kitten | Mypetslibrary" />
<title>Add a Kitten | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Add a Kitten</h1>
            <div class="green-border"></div>
   </div>
   <div class="border-separation">
        <div class="clear"></div>
        <form action="utilities/adminRegisterKittenFunction.php" method="POST" enctype="multipart/form-data">
         <div class="dual-input">
            <p class="input-top-p admin-top-p">Seller*</p>
        	<select class="clean input-name admin-input" value="<?php echo $sellerDetails[0]->getCompanyName();?>"name="register_seller" id="register_seller" required>
                <option value="">Please Select a Seller</option>
                <?php
                for ($cntPro=0; $cntPro <count($sellerDetails) ; $cntPro++)
                {
                ?>
                    <option value="<?php echo $sellerDetails[$cntPro]->getCompanyName(); ?>"> 
                        <?php echo $sellerDetails[$cntPro]->getCompanyName(); ?>
                    </option>
                <?php
                }
                ?>
            </select>   
        </div>        
        
         <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Pet Color <a href="kittenColor.php" class="green-a">(Add New Color Here)*</a></p>
        	<select class="clean input-name admin-input" value="<?php echo $colorDetails[0]->getName();?>" name="register_color" id="register_color" required>
                <option value="">Please Select a Color</option>
                <?php
                for ($cntPro=0; $cntPro <count($colorDetails) ; $cntPro++)
                {
                ?>
                    <option value="<?php echo $colorDetails[$cntPro]->getName(); ?>"> 
                        <?php echo $colorDetails[$cntPro]->getName(); ?>
                    </option>
                <?php
                }
                ?>
            </select>       
        </div>    
        <div class="clear"></div>    
         <div class="dual-input">
        	<p class="input-top-p admin-top-p">Pet Breed <a href="kittenBreed.php" class="green-a">(Add New Breed Here)*</a></p>
        	<select class="clean input-name admin-input" value="<?php echo $breedDetails[0]->getName();?>" name="register_breed" id="register_breed" required>
                <option value="">Please Select a Breed</option>
                <?php
                for ($cntPro=0; $cntPro <count($breedDetails) ; $cntPro++)
                {
                ?>
                    <option value="<?php echo $breedDetails[$cntPro]->getName(); ?>"> 
                        <?php echo $breedDetails[$cntPro]->getName(); ?>
                    </option>
                <?php
                }
                ?>
            </select>       
        </div>

        
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">SKU*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="SKU" name="register_sku" id="register_sku" required> 
        </div>
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Pet Price (RM)*</p>
            <input class="input-name clean input-textarea admin-input" type="number" placeholder="Pet Price (RM)"  name="register_price" id="register_price" required>
        </div>        
        
 
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Pet Age*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Pet Age" name="register_age" id="register_age"  required>      
        </div>
		<div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Vaccinated Status*</p>
            <select class="clean input-name admin-input" type="text" name="register_vaccinated" id="register_vaccinated" required>
                <option value="1st Vaccination Done" name="First Vaccination Done">1st Vaccination Done</option>
                <option value="2nd Vaccination Done" name="Second Vaccination Done">2nd Vaccination Done</option>
                <option value="3rd Vaccination Done" name="Third Vaccination Done">3rd Vaccination Done</option>
                <option value="No" name="No">No</option>
            </select> 
        </div>         

        
        
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Dewormed Status*</p>
            <select class="clean input-name admin-input" type="text" name="register_dewormed" id="register_dewormed" required>
                <option value="Yes" name="Yes">Yes</option>
                <option value="No" name="No">No</option>
            </select>    
        </div>
		<div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Gender*</p>
        	<select class="clean input-name admin-input" name="register_gender" id="register_gender" required>
                <option value="" name=" ">Select a Gender</option>
                <option value="Male" name="Male">Male</option>
                <option value="Female" name="Female">Female</option>
            </select>   
        </div>  
        
    

        <input class="input-name clean input-textarea admin-input" type="hidden" value="Pending"  name="register_status" id="register_status" readonly>  



        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Pet Size*</p>
        	<select class="clean input-name admin-input" name="register_size" id="register_size" required>
                <option value="" name=" ">Select a Size</option>
                <option value="Small" name="Small">Small</option>
                <option value="Medium" name="Medium">Medium</option>
                <option value="Large" name="Large">Large</option>
            </select>   
        </div>  


        <div class="clear"></div>

        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Details</p>
            <input class="input-name clean input-textarea admin-input" type="text" placeholder="Details 1" name="register_details" id="register_details">    
            <input class="input-name clean input-textarea admin-input" type="text" placeholder="Details 2" name="register_details_two" id="register_details_two">    
            <input class="input-name clean input-textarea admin-input" type="text" placeholder="Details 3" name="register_details_three" id="register_details_three">    
            <input class="input-name clean input-textarea admin-input" type="text" placeholder="Details 4" name="register_details_four" id="register_details_four">       
        </div>

        <div class="clear"></div>

        <div class="width100 overflow">
        	<img src="img/vimeo-tutorial.png" class="vimeo-tutorial" alt="Tutorial" title="Tutorial">
        	<p class="input-top-p admin-top-p">Vimeo Video Link* (Copy the Highlighted Part Only) <!--<img src="img/attention3.png" class="attention-png opacity-hover open-vimeo" alt="Click Me!" title="Click Me!">--></p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="379922769" name="register_link" id="register_link" required>           
        </div>

        <div class="clear"></div> 

        <div class="width100 overflow text-center">     
            <button class="green-button white-text clean2 edit-1-btn margin-auto" type="submit" value="submit">Next</button>
        </div>

        </form>
	</div>
</div>
<div class="clear"></div>

<?php include 'js.php'; ?>
<?php unset($_SESSION['puppy_uid']);unset($_SESSION['newPetsUid']);unset($_SESSION['image']); ?>
<?php

if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully registered new Kitten!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to register for Pets !";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Fail to register for Kitten !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Kitten name or SKU has been used !! <br> Please enter a new name or SKU !";
        } 
        else if($_GET['type'] == 5)
        {
            $messageType = "Insufficient Credit For Seller !!";
        } 
        else if($_GET['type'] == 6)
        {
            $messageType = "unable to update seller's credit !!";
        } 
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>