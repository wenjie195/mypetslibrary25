<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Article.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$articles = getArticles($conn,"WHERE article_link = ? AND display = 'YES' ",array("article_link"),array($_GET['id']), "s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php 
    // Program to display URL of current page. 
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
    $link = "https"; 
    else
    $link = "http"; 

    // Here append the common URL characters. 
    $link .= "://"; 

    // Append the host(domain name, ip) to the URL. 
    $link .= $_SERVER['HTTP_HOST']; 

    // Append the requested resource location to the URL 
    $link .= $_SERVER['REQUEST_URI']; 

    // Print the link 
    // echo $link; 
?>
<?php
if(isset($_GET['id']))
{
$conn = connDB();
$articlesDetails = getArticles($conn,"WHERE article_link = ? ", array("article_link") ,array($_GET['id']),"s");
?>

        <?php
        if($articlesDetails)
        {
            for($cnt = 0;$cnt < count($articlesDetails) ;$cnt++)
            {
            ?>
<meta property="og:image" content="https://qlianmeng.asia/mypetslibrary/uploadsArticle/<?php echo $articlesDetails[$cnt]->getTitleCover();?>" />  
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://qlianmeng.asia/mypetslibrary/pet-blog.php?id=<?php echo $articles[$cnt]->getArticleLink();?>" />
<link rel="canonical" href="https://qlianmeng.asia/mypetslibrary/pet-blog.php?id=<?php echo $articles[$cnt]->getArticleLink();?>" />
<meta property="og:title" content="<?php echo $articlesDetails[$cnt]->getTitle();?> | Mypetslibrary" />
<title><?php echo $articlesDetails[$cnt]->getTitle();?> | Mypetslibrary</title>
<meta property="og:description" content="<?php echo $articlesDetails[$cnt]->getKeywordOne();?>" />
<meta name="description" content="<?php echo $articlesDetails[$cnt]->getKeywordOne();?>" />
<meta name="keywords" content="<?php echo $articlesDetails[$cnt]->getKeywordTwo();?>, Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, malaysia, online pet store, etc">

<meta property="og:image" content="https://qlianmeng.asia/mypetslibrary/uploadsArticle/<?php echo $articlesDetails[$cnt]->getTitleCover();?>" />  
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'header.php'; ?>
 



<div class="width100 blog-big-div overflow min-height menu-distance2">
	<div class="blog-inner-div">
    	<div class="blog-content">
                <img src="uploadsArticle/<?php echo $articlesDetails[$cnt]->getTitleCover();?>" class="cover-photo" alt="<?php echo $articlesDetails[$cnt]->getTitle();?>" title="<?php echo $articlesDetails[$cnt]->getTitle();?>"> 
                <h1 class="green-text user-title ow-margin-bottom-0"><?php echo $articlesDetails[$cnt]->getTitle();?></h1>       
                <?php $currentUser = $articlesDetails[$cnt]->getAuthorUid();?>  
                <?php
                if($uid == $currentUser)
                {   }
                elseif($uid != "")
                {
                ?>

                    <button class="clean transparent-button article-report pointer opacity-hover open-report">
                            <img src="img/flag.png" class="hover1a" alt="Report" title="Report">
                            <img src="img/flag2.png" class="hover1b" alt="Report" title="Report">
                    </button>

                    <!-- Report Modal -->
                    <div id="report-modal" class="modal-css">
                        <!-- Modal content -->
                        <div class="modal-content-css forgot-modal-content login-modal-content report-modal-margin">
                            <span class="close-css close-report">&times;</span>
                            <h2 class="green-text h2-title">Report </h2>
                            <div class="green-border"></div>
                            <div class="clear"></div>
                            <div class="width100 mini-height30"></div>
                            <div class="clear"></div>
                            <form method="POST" action="utilities/reportArticleFunction.php">
                                <p class="input-top-p">Reason</p>
                                <input class="input-name clean" type="text" placeholder="Reason" name="report_reason" id="report_reason" required>   
                                <input class="input-name clean" type="hidden" value="<?php echo $articlesDetails[$cnt]->getUid();?>" name="article_uid" id="article_uid" required readonly>   
                                <div class="clear"></div>
                                <button class="green-button white-text width100 clean2" name="submit">Submit</button>
                            </form>    
                        </div>
                    </div>

                <?php
                }
                else
                {
                ?>

                    <button class="clean transparent-button article-report pointer opacity-hover open-login">
                            <img src="img/flag.png" class="hover1a" alt="Report" title="Report">
                            <img src="img/flag2.png" class="hover1b" alt="Report" title="Report">
                    </button>

                <?php
                }
                ?>

                
                <!-- <p class="author-p">Author Name</p> -->
                <p class="author-p">
                    <!-- <?php //echo $articlesDetails[$cnt]->getAuthorName();?> -->

                    <?php $reviewWriter = $articlesDetails[$cnt]->getAuthorName();
                        if($reviewWriter == 'admin')
                        {
                            echo "Mypetslibrary";
                        }
                        else
                        {
                            echo $reviewWriter;
                        }
                    ?>

                </p>
                <p class="small-blog-date">
                    <?php echo $date = date("d-m-Y",strtotime($articlesDetails[$cnt]->getDateCreated()));?>
                </p>
                <p class="article-paragraph">
                    <?php echo $articlesDetails[$cnt]->getParagraphOne();?>
                </p>
                <?php $currentUid =  $articlesDetails[$cnt]->getUid();?>
            <?php
            }
        }
        ?>
		</div>
        <div class="border-new-div share-div">
            <h1 class="green-text user-title">Share:</h1>
            
                <div class="clear"></div>

            	<script async src="https://static.addtoany.com/menu/page.js"></script>

                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                    <a class="a2a_button_copy_link"></a>
                    <a class="a2a_button_facebook"></a>
                    <a class="a2a_button_twitter"></a>
                    <a class="a2a_button_linkedin"></a>
                    <a class="a2a_button_blogger"></a>
                    <a class="a2a_button_facebook_messenger"></a>
                    <a class="a2a_button_whatsapp"></a>
                    <a class="a2a_button_wechat"></a>
                    <a class="a2a_button_line"></a>
                    <a class="a2a_button_telegram"></a>
                    <!--<a class="a2a_button_print"></a>-->
                </div>         
        </div>

        <div class="border-new-div share-div">
        	<h1 class="green-text user-title">Recommended for You:</h1>      
        	<div class="width100 recommend">
 
                <?php
                // $conn = connDB();
                // $allArticles = getArticles($conn);
                $allArticles = getArticles($conn," WHERE uid != '$currentUid' AND display = 'YES' ORDER BY date_created DESC LIMIT 3");

                if($allArticles)
                {   
                    for($cntAA = 0;$cntAA < count($allArticles) ;$cntAA++)
                    {
                    ?>
                        
                    <a href='pet-blog.php?id=<?php echo $allArticles[$cntAA]->getArticleLink();?>' class="opacity-hover">
                        <div class="shadow-white-box width100 blog-box opacity-hover">
                            <div class="left-img-div2">
                                <img src="uploadsArticle/<?php echo $allArticles[$cntAA]->getTitleCover();?>" class="width100" alt="Blog Title" title="Blog Title">
                            </div>
                            <div class="right-content-div3">
                                <h3 class="article-title text-overflow">
                                    <?php echo $allArticles[$cntAA]->getTitle();?>
                                </h3>
                                <p class="date-p">
                                    <?php echo $allArticles[$cntAA]->getDateCreated();?>
                                </p>
                                <p class="right-content-p">
                                    <?php echo $allArticles[$cntAA]->getKeywordOne();?>
                                </p>
                            </div>
                        </div>
                    </a>

                    <?php
                    }
                    ?>
                <?php
                }
                ?>

            </div>

        </div>

<?php
}
?>
                      
	</div>
</div>


<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>