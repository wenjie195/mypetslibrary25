<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Services.php';
require_once dirname(__FILE__) . '/classes/States.php';
require_once dirname(__FILE__) . '/classes/Seller.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$states = getStates($conn);

$petriRate = getServices($conn);
$serviceDetails = getServices($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add Seller | Mypetslibrary" />
<title>Add Seller | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Add Sellers</h1>
            <div class="green-border"></div>
   </div>
   <div class="border-separation">
        <div class="clear"></div>
         <!-- <form action="utilities/registerSellerFunction.php" method="POST"> -->
        <form method="POST" action="utilities/registerSellerFunction.php" enctype="multipart/form-data">
            <div class="dual-input">
                <p class="input-top-p admin-top-p important-text">Company Name*</p>
                <input class="input-name clean input-textarea admin-input" type="text" placeholder="Company Name" required name="register_name" id="register_name" required>      
            </div>
            
            <div class="dual-input second-dual-input important-text">
                <p class="input-top-p admin-top-p important-text">Contact Person Name*</p>
                <input class="input-name clean input-textarea admin-input" type="text" placeholder="Contact Person Name" name="register_contact_person" id="register_contact_person" required>         
            </div>        

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p admin-top-p important-text">Contact Person Phone No.*</p>
                                <input class="input-name clean input-textarea admin-input" type="text" placeholder="Contact No." name="register_contact_no" id="register_contact_no"> 
                <!--<input class="input-name clean input-textarea admin-input" type="text" placeholder="Phone No." name="register_contact_personNo" id="register_contact_personNo" required>-->      
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p important-text">Company State*</p>
                <select class="clean input-name admin-input" type="text" name="register_state" id="register_state" required>
                    <option value="">Company State</option>
                    <?php
                    for ($cnt=0; $cnt <count($states) ; $cnt++)
                    {
                    ?>
                        <option value="<?php echo $states[$cnt]->getStateName(); ?>"> 
                            <?php echo $states[$cnt]->getStateName(); ?>
                        </option>
                    <?php
                    }
                    ?>
                </select>   
            </div>         

            <div class="clear"></div>        
            <div class="dual-input">
                <p class="input-top-p admin-top-p important-text">Credits*</p>
                <input class="input-name clean input-textarea admin-input" type="text" placeholder="1" name="register_credit" id="register_credit" required>      
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Company Registered No.</p>
                <input class="input-name clean input-textarea admin-input" type="text" placeholder="Company Registered No." name="register_registration_no" id="register_registration_no">      
            </div>

      

            <div class="clear"></div>   
           <!-- <div class="dual-input">
                <p class="input-top-p admin-top-p">Company Contact</p>
   
            </div>  -->
            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Company Address</p>
                <textarea class="input-name clean input-textarea address-textarea admin-address-textarea" type="text" placeholder="Company Address" name="register_address" id="register_address"></textarea>
            </div>

            <div class="clear"></div>
            
            <div class="dual-input">
                <p class="input-top-p admin-top-p">Years of Experience</p>
                <input class="input-name clean input-textarea admin-input" type="text" placeholder="20"  name="register_experience" id="register_experience">      
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Cert</p>
                <input class="input-name clean input-textarea admin-input" type="text" placeholder="Cert"  name="register_cert" id="register_cert">    
            </div>    

            <div class="clear"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Services</p>

                    <?php
                    if ($petriRate)
                    {
                        for ($i=0; $i <count($petriRate) ; $i++)
                        {
                        ?>
                            <div class="filter-option">
                                <label for="<?php echo $petriRate[$i]->getServiceType() ?>" class="filter-label filter-label2 services-label"><?php echo $petriRate[$i]->getServiceType();?>
                                <input type="checkbox" name="register_services[]" id="<?php echo $petriRate[$i]->getServiceType() ?>" value="<?php echo $petriRate[$i]->getId();?>" class="filter-option" />
                                <span class="checkmark"></span>
                                </label>
                            </div>
                        <?php
                        }
                    }
                    ?>
            </div>

            <div class="clear"></div>

            <div class="width100 overflow padding-top30">
                <p class="input-top-p admin-top-p">Type of Breed</p>
                <input class="input-name clean input-textarea admin-input" type="text" placeholder="Type of Breed"  name="register_breed" id="register_breed">           			
            </div>

            <div class="clear"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Other Info</p>
                <!-- <input class="input-name clean input-textarea admin-input" type="text" placeholder="Other Info Like FB, Insta Link"  name="register_info" id="register_info">    -->
                <input class="input-name clean input-textarea admin-input" type="text" placeholder="Info 1"  name="register_info" id="register_info"> 
                <input class="input-name clean input-textarea admin-input" type="text" placeholder="Info 2"  name="register_info_two" id="register_info_two">      			
                <input class="input-name clean input-textarea admin-input" type="text" placeholder="Info 3"  name="register_info_three" id="register_info_three"> 
                <input class="input-name clean input-textarea admin-input" type="text" placeholder="Info 4"  name="register_info_four" id="register_info_four"> 
            </div>

            <div class="clear"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Email</p>
                <input class="input-name clean input-textarea admin-input" type="email" placeholder="Email"  name="register_email" id="register_email" >        			
            </div>       

            <div class="clear"></div>

            <div class="width100 overflow text-center">     
                <button class="green-button white-text clean2 edit-1-btn margin-auto">Next</button>
            </div>
        </form>
	</div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

<?php

if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully registered new seller !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Registration of new seller failed!";
        } 
        else if($_GET['type'] == 6)
        {
            $messageType = "Registration of new seller as an user !";
        } 
        else if($_GET['type'] == 7)
        {
            $messageType = "Registration data has been taken ! <br> Please enter a new data.";
        } 
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>