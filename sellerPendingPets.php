<?php
require_once dirname(__FILE__) . '/sellerAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Pets.php';
require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];
// $sellerName = $userData->getName();
$sellerName = $userData->getUid();


$userAmount = getUser($conn," WHERE user_type = 1 ");
$sellerAmount = getSeller($conn);

if (isset($_GET['search'])) {
$sellerAllPets = getPetsDetails($conn, "WHERE seller =? AND status ='Pending' AND name =?",array("seller,name"),array($sellerName,$_GET['search']),"ss");
}else {
$sellerAllPets = getPetsDetails($conn, "WHERE seller =? AND status ='Pending' ",array("seller"),array($sellerName),"s");
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pending Pets | Mypetslibrary" />
<title>Pending Pets | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance">
	<div class="width100">
        <div class="dual-left-div">
            <h1 class="green-text h1-title">Pending Pets</h1>
            <div class="green-border"></div>
        </div>
        <div class="dual-search-div">
          <?php //echo $_SERVER["PHP_SELF"] ?>
            <form action="search/searchFunction.php" method="post">
                    <?php
                    if (isset($_GET['search'])) {
                      ?>
                      <input class="line-input clean" type="text" value="<?php echo $_GET['search'] ?>" name="search" placeholder="Search">
                      <?php
                    }else {
                      ?>
                      <input class="line-input clean" type="text" name="search" placeholder="Search">
                      <?php
                    }
                     ?>
                     <!-- SKU and pet name-->
                     <input type="hidden" name="location" value="<?php echo $_SERVER["PHP_SELF"] ?>">
                     <button class="search-btn hover1 clean" type="submit">
                           <img src="img/search.png" class="visible-img hover1a" alt="Search" title="Search">
                           <img src="img/search2.png" class="visible-img hover1b" alt="Search" title="Search">
                     </button>
              </form>
        </div>


    </div>


    <div class="clear"></div>
	<div class="width100 scroll-div border-separation">
    	<table class="green-table width100" id="myTable">
        	<thead>
            	<tr>
                	<th class="first-column">No.</th>
                   
                    <th>Type</th>
                    <th>SKU</th>
                    <th>Seller</th>
                    <th>Added On</th>
                    <th>Status</th>
                    <th>Details</th>
                    <th>Delete</th>
                </tr>
            </thead>

            <tbody>
                <?php
                if($sellerAllPets)
                {

                    for($cnt = 0;$cnt < count($sellerAllPets) ;$cnt++)
                    {?>

                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            
                            <td><?php echo $sellerAllPets[$cnt]->getType();?></td>
                            <td><?php echo $sellerAllPets[$cnt]->getSKU();?></td>

                            <td>
                                <?php
                                    $sellerUid = $sellerAllPets[$cnt]->getSeller();

                                    $conn = connDB();
                                    $compDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($sellerUid),"s");
                                    $compData = $compDetails[0];
                                    echo $reviewHeader = $compData->getName();
                                    $conn->close();
                                ?>
                            </td>

                            <td>
                                <!-- <?php //echo $sellerAllPets[$cnt]->getDateCreated();?> -->
                                <?php echo $date = date("d-m-Y",strtotime($sellerAllPets[$cnt]->getDateCreated()));?>
                            </td>

                            <td><?php echo $sellerAllPets[$cnt]->getStatus();?></td>
                            <td>
                                <!-- <form action="#" method="POST" class="hover1"> -->
                                <!-- <form method="POST" action="editPuppy.php" class="hover1"> -->
                                <form method="POST" action="sellerEditPets.php" class="hover1">
                                    <button class="clean hover1 img-btn" type="submit" name="pet_uid" value="<?php echo $sellerAllPets[$cnt]->getUid();?>">
                                        <img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                                        <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                                    </button>
                                </form>
                            </td>
                            <td>
                                <!-- <form method="POST" action="#" class="hover1"> -->
                                <!-- <form method="POST" action="utilities/deletePetsFunction.php" class="hover1"> -->
                                <form method="POST" action="utilities/sellerDeletePetsFunction.php" class="hover1">
                                    <input class="input-name clean" type="hidden" value="<?php echo $sellerAllPets[$cnt]->getType();?>"  name="pets_type" id="pets_type" readonly>

                                    <button class="clean hover1 img-btn" type="submit" name="pets_uid" value="<?php echo $sellerAllPets[$cnt]->getUid();?>">
                                        <img src="img/delete1a.png" class="edit-icon1 hover1a" alt="Delete" title="Delete">
                                        <img src="img/delete3a.png" class="edit-icon1 hover1b" alt="Delete" title="Delete">
                                    </button>
                                </form>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>

            <!-- <tbody>
            	<tr>
                	<td class="first-column">1.</td>
                    <td>Akita</td>
                    <td>Puppy</td>
                    <td>JAN-PP-01</td>
                    <td>Fur</td>
                    <td>1/12/2019</td>
                    <td>Pending</td>
                    <td>
                    	<a href="editPuppy.php" class="hover1">
                        	<img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                            <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                        </a>
                    </td>
                    <td>
                    	<form>
                            <a class="hover1 open-confirm pointer">
                                <img src="img/delete1a.png" class="edit-icon1 hover1a" alt="Delete" title="Delete">
                                <img src="img/delete3a.png" class="edit-icon1 hover1b" alt="Delete" title="Delete">
                            </a>


                            <div id="confirm-modal" class="modal-css">


                              <div class="modal-content-css confirm-modal-margin">
                                <span class="close-css close-confirm">&times;</span>
                                <div class="clear"></div>
                                <h2 class="green-text h2-title confirm-title">Confirm Delete?</h2>
                                <div class="clean cancel-btn close-confirm">Cancel</div>
                                <button class="clean red-btn delete-btn2">Delete</button>
                                <div class="clear"></div>


                              </div>

                            </div>

                        </form>
                    </td>
                </tr>
            	<tr>
                	<td class="first-column">2.</td>
                    <td>Cat Name</td>
                    <td>Kitten</td>
                    <td>JAN-PP-02</td>
                    <td>Fur</td>
                    <td>1/12/2019</td>
                    <td>Pending</td>
                    <td>
                    	<a href="editPuppy.php" class="hover1">
                        	<img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                            <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                        </a>
                    </td>
                    <td>
                            <a class="hover1 open-confirm pointer">
                                <img src="img/delete1a.png" class="edit-icon1 hover1a" alt="Delete" title="Delete">
                                <img src="img/delete3a.png" class="edit-icon1 hover1b" alt="Delete" title="Delete">
                            </a>
                    </td>
                </tr>
            	<tr>
                	<td class="first-column">3.</td>
                    <td>Reptile Name</td>
                    <td>Reptile</td>
                    <td>JAN-PP-03</td>
                    <td>Fur</td>
                    <td>1/12/2019</td>
                    <td>Pending</td>
                    <td>
                    	<a href="editPuppy.php" class="hover1">
                        	<img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                            <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                        </a>
                    </td>
                    <td>
                            <a class="hover1 open-confirm pointer">
                                <img src="img/delete1a.png" class="edit-icon1 hover1a" alt="Delete" title="Delete">
                                <img src="img/delete3a.png" class="edit-icon1 hover1b" alt="Delete" title="Delete">
                            </a>
                    </td>
                </tr>
            </tbody> -->
        </table>
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>
<div class="clear"></div>

<?php include 'js.php'; ?>

<?php

if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Pet Added ! <br> Waitng Approval from Admin !";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to register for Pet on Pets Details table !";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Fail to register for Pet !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Pet's name or SKU has been used !! <br> Please enter a new name or SKU !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

    elseif($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Pets as Available To Sell";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Update Pets as Rejected";
        }

        else if($_GET['type'] == 3)
        {
            $messageType = "Pet Deleted !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Unable to delete pet !!";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "ERROR !! Lvl 2 !!";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "unknow species !!";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "Unable to update on pet details !!";
        }
        else if($_GET['type'] == 8)
        {
            $messageType = "ERROR !!";
        }

        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

}
?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

</body>
</html>
