<?php
  $tz = 'Asia/Kuala_Lumpur';
  $timestamp = time();
  $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
  $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
  $time = $dt->format('Y');
?>

<div class="width100 same-padding green-footer">
	<p class="footer-p white-text">© <?php echo $time ;?> Mypetslibrary, All Rights Reserved.</p>
</div>

<!-- Sign Up Modal -->
<div id="signup-modal" class="modal-css">

    <!-- Modal content -->
    <div class="modal-content-css signup-modal-content forgot-modal-content login-modal-content">
      <span class="close-css close-signup upper-close">&times;</span>
      <h2 class="green-text h2-title text-center">Sign Up</h2>
      <form class="login-form" action="utilities/registerFunction.php" method="POST">
        <!-- <div class="fb-button-container">
        <div class="fb-login-button" data-width="" data-size="large" data-button-type="login_with" data-auto-logout-link="false" data-use-continue-as="false"></div>
        </div> -->
        <!-- <div class="grey-border"></div> -->
        <div class="grey-border extra-spacing-bottom"></div>
        <!-- <p class="hint-p">Email or phone number must key in either one or both.</p> -->
        <!-- <p class="input-top-p">Name</p>
        <input class="input-name clean" type="text" placeholder="Name" required id="register_name" name="register_name"> -->
        <p class="input-top-p">Username</p>
        <input class="input-name clean" type="text" placeholder="Username" id="register_name" name="register_name" required>

        <p class="input-top-p">Email</p>
        <input class="input-name clean" type="email" placeholder="Email" id="register_email_user" name="register_email_user">

        <!-- <button class="orange-button white-text width100 clean2 bottom-distance">Verify Email</button> -->

        <!-- <p class="input-top-p">Country</p>
        <select class="clean input-name" type="text" name="register_country" id="register_country" required>
          <option value="" name=" ">Please choose a country</option>
          <option value="Malaysia" name="Malaysia">Malaysia</option>
          <option value="Singapore" name="Singapore">Singapore</option>
        </select>  -->

        <p class="input-top-p">Phone No.</p>
        <input class="input-name clean" type="number" placeholder="Phone Number" id="register_phone" name="register_phone" required>
        <!-- <button class="orange-button white-text width100 clean2 bottom-distance">Request TAC</button> -->
        <!-- <p class="input-top-p">TAC</p>
        <input class="input-name clean" type="number" placeholder="TAC" id="register_tac" name="register_tac">   -->

        <!-- <p class="input-top-p">Password</p>
        <div class="edit-password-input-div">
          <input class="input-name clean input-password edit-password-input" type="Password" placeholder="Password" id="register_password" name="register_password" required>
          <p class="edit-p-password"><img src="img/visible.png" class="hover1a edit-password-img" alt="View Password" onclick="myFunctionA()" title="View Password"><img src="img/visible2.png" class="hover1b edit-password-img" alt="View Password" title="View Password"></p>
        </div>


        <p class="input-top-p">Retype Password</p>
        <div class="edit-password-input-div">
          <input class="input-name clean input-password edit-password-input" type="Password"  placeholder="Retype Password" id="register_retype_password" name="register_retype_password" required>
          <p class="edit-p-password"><img src="img/visible.png" class="hover1a edit-password-img" alt="View Password" onclick="myFunctionB()" title="View Password"><img src="img/visible2.png" class="hover1b edit-password-img" alt="View Password" title="View Password"></p>
        </div>          -->

        <div class="clear"></div>
        <p class="hint-p">By clicking “Sign Up”, I agree to Mypetslibrary <a href="termsPrivacy.php" class="light-green-a" target="_blank">terms & privacy</a>.</p>

        <button class="green-button white-text width100 clean2" name="submit">Sign Up</button>

        <p class="text-center signup-spacing"><a class="modal-a-size light-green-a open-login">Login</a></p>

      </form>
    </div>

</div>
<!-- Login Modal -->
<div id="login-modal" class="modal-css">

<!-- Modal content -->
<div class="modal-content-css signup-modal-content forgot-modal-content login-modal-content">
  <span class="close-css close-login upper-close">&times;</span>
  <h2 class="green-text h2-title text-center">Login</h2>

	<a href=<?php echo $facebook_login_url ?>>
    	<button class="green-button white-text width100 clean2 ow-fb-button" ><img src="img/Facebook-icon.png" class="fb-icon1" alt="Facebook" title="Facebook">Facebook</button>
	</a>
   <h2 class="horizontal-h2"><span class="horizontal-span">Or</span></h2>
  <form class="login-form" action="utilities/loginFunction.php" method="POST">
    <!-- <div class="fb-button-container">
    <div class="fb-login-button" data-width="" data-size="large" data-button-type="login_with" data-auto-logout-link="false" data-use-continue-as="false"></div>
    </div>
    <div class="grey-border extra-spacing-bottom"></div> -->
    <!-- <p class="input-top-p">Email or Phone No.</p>
    <input class="input-name clean" type="text" placeholder="Email or Phone No." id="register_name" name="register_name"> -->
    <p class="input-top-p">Username</p>
    <input class="input-name clean" type="text" placeholder="Username" id="register_name" name="register_name" required>

    <p class="input-top-p">Phone No.</p>
    <input class="input-name clean"   type="text" placeholder="Phone Number" id="phone_no" name="phone_no" required>

    <!-- <p class="input-top-p">Password</p>
    <div class="edit-password-input-div">
      <input class="input-name clean input-password edit-password-input"   type="Password" placeholder="Password" id="password" name="password" required>
      <p class="edit-p-password"><img src="img/visible.png" class="hover1a edit-password-img" alt="View Password" onclick="myFunctionC()" title="View Password"><img src="img/visible2.png" class="hover1b edit-password-img" alt="View Password" onclick="myFunctionC()" title="View Password"></p>
    </div>            -->

    <button class="green-button white-text width100 clean2" name="loginButton">Login</button>

    <p class="text-center signup-spacing"><a class="light-green-a open-signup modal-a-size">Sign Up</a></p>

    <p class="text-center"><a class="light-green-a open-forgot2 modal-a-size">Forgot Login Details</a></p>

  </form>





</div>

</div>

<!-- User Forgot Password Modal -->
<div id="forgot2-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css signup-modal-content forgot-modal-content login-modal-content">
    <span class="close-css close-forgot2 upper-close">&times;</span>
    <h2 class="green-text h2-title text-center forgot-h2">Forgot Login Details</h2>
    <div class="clear"></div>
    <form class="login-form" >
   <!-- 	<div class="fb-button-container">
        	<div class="fb-login-button" data-width="" data-size="large" data-button-type="login_with" data-auto-logout-link="false" data-use-continue-as="false"></div>
        </div>
        <div class="grey-border bottom-distance"></div>-->
        <p class="input-top-p">Email or Phone No.</p>
        <input class="input-name clean" type="text" placeholder="Email Address or Phone Number" required name="">
        <div class="clear"></div>
        <button class="green-button white-text width100 clean2">Submit</button>
        <p class="text-center signup-spacing"><a class="modal-a-size light-green-a open-login">Login</a></p>
        <p class="text-center"><a class="modal-a-size light-green-a open-signup">Sign Up</a></p>
    </form>
  </div>

</div>
<!-- Forgot Password Modal -->
<div id="forgot-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css forgot-modal-content login-modal-content">
    <span class="close-css close-forgot">&times;</span>
    <h2 class="green-text h2-title">Forgot Password</h2>
    <div class="green-border"></div>
    <form class="login-form" >

		<input class="clean line-input" type="email" placeholder="Email" required name="forgotPassword_email">

        <div class="clear"></div>
        <button class="green-button white-text width100 clean2">Submit</button>

    </form>
  </div>

</div>

<!-- Filter Modal -->
<!-- <div id="filter-modal" class="modal-css">

  <div class="modal-content-css filter-modal">
    <span class="close-css close-filter">&times;</span>
    <h2 class="green-text h2-title">Filter</h2>
    <div class="green-border filter-border"></div>
    <form>
    	<div class="filter-div-section">
            <label for="breedGroup" class="filter-label filter-label3">Breed Group
                <input type="checkbox" name="breedGroup" id="breedGroup" value="0" class="filter-input" />
                <span class="checkmark"></span>
            </label>
            <div id="breedGroupDiv" style="display:none">
                <input type="text" placeholder="Search" class="filter-search clean">
                <button class="transparent-button filter-search-btn">
                    <img src="img/search.png" class="filter-search-img opacity-hover" alt="Search" title="Search">
                </button>
                <div class="clear"></div>
                <div class="filter-option">
                    <label for="breedGroup1" class="filter-label filter-label2">Akita
                        <input type="checkbox" name="breedGroup1" id="breedGroup1" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="breedGroup2" class="filter-label filter-label2">Alaskan Husky
                        <input type="checkbox" name="breedGroup2" id="breedGroup2" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="breedGroup3" class="filter-label filter-label2">Alaskan Malamute
                        <input type="checkbox" name="breedGroup3" id="breedGroup3" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="breedGroup4" class="filter-label filter-label2">American Bully
                        <input type="checkbox" name="breedGroup4" id="breedGroup4" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="breedGroup5" class="filter-label filter-label2">Bassett Hound
                        <input type="checkbox" name="breedGroup5" id="breedGroup5" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
        </div>
    	<div class="filter-div-section">
            <label for="gender" class="filter-label filter-label3">Gender
                <input type="checkbox" name="gender" id="gender" value="0" class="filter-input" />
                <span class="checkmark"></span>
            </label>
            <div id="genderDiv" style="display:none">

                <input class="hidden radio-label" type="radio" name="male" id="male"/>
                <label class="button-label" for="male">
                  Male
                </label>
                <input class="hidden radio-label" type="radio" name="female" id="female"/>
                <label class="button-label" for="female">
                  Female
                </label>
        	</div>
        </div>
    	<div class="filter-div-section">
            <label for="color" class="filter-label filter-label3">Color
                <input type="checkbox" name="color" id="color" value="0" class="filter-input" />
                <span class="checkmark"></span>
            </label>
            <div id="colorDiv" style="display:none">
                <div class="clear"></div>
                <div class="filter-option">
                    <label for="color1" class="filter-label filter-label2">Beige
                        <input type="checkbox" name="color1" id="color1" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="color2" class="filter-label filter-label2">Black
                        <input type="checkbox" name="color2" id="color2" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="color3" class="filter-label filter-label2">Brown
                        <input type="checkbox" name="color3" id="color3" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="color4" class="filter-label filter-label2">Grey
                        <input type="checkbox" name="color4" id="color4" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="color5" class="filter-label filter-label2">White
                        <input type="checkbox" name="color5" id="color4" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
        </div>
    	<div class="filter-div-section">
            <label for="state" class="filter-label filter-label3">Location
                <input type="checkbox" name="state" id="state" value="0" class="filter-input" />
                <span class="checkmark"></span>
            </label>
            <div id="stateDiv" style="display:none">
                <input type="text" placeholder="Search" class="filter-search clean">
                <button class="transparent-button filter-search-btn">
                    <img src="img/search.png" class="filter-search-img opacity-hover" alt="Search" title="Search">
                </button>
                <div class="clear"></div>
                <div class="filter-option">
                    <label for="Johor" class="filter-label filter-label2">Johor
                        <input type="checkbox" name="Johor" id="Johor" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Kedah" class="filter-label filter-label2">Kedah
                        <input type="checkbox" name="Kedah" id="Kedah" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Kelantan" class="filter-label filter-label2">Kelantan
                        <input type="checkbox" name="Kelantan" id="Kelantan" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="KualaLumpur" class="filter-label filter-label2">Kuala Lumpur
                        <input type="checkbox" name="KualaLumpur" id="KualaLumpur" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Labuan" class="filter-label filter-label2">Labuan
                        <input type="checkbox" name="Labuan" id="Labuan" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Melaka" class="filter-label filter-label2">Melaka
                        <input type="checkbox" name="Melaka" id="Melaka" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="NegeriSembilan" class="filter-label filter-label2">Negeri Sembilan
                        <input type="checkbox" name="NegeriSembilan" id="NegeriSembilan" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Pahang" class="filter-label filter-label2">Pahang
                        <input type="checkbox" name="Pahang" id="Pahang" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Penang" class="filter-label filter-label2">Penang
                        <input type="checkbox" name="Penang" id="Penang" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Perak" class="filter-label filter-label2">Perak
                        <input type="checkbox" name="Perak" id="Perak" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Perlis" class="filter-label filter-label2">Perlis
                        <input type="checkbox" name="Perlis" id="Perlis" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Putrajaya" class="filter-label filter-label2">Putrajaya
                        <input type="checkbox" name="Putrajaya" id="Putrajaya" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Sabah" class="filter-label filter-label2">Sabah
                        <input type="checkbox" name="Sabah" id="Sabah" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Sarawak" class="filter-label filter-label2">Sarawak
                        <input type="checkbox" name="Sarawak" id="Sarawak" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Selangor" class="filter-label filter-label2">Selangor
                        <input type="checkbox" name="Selangor" id="Selangor" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Terengganu" class="filter-label filter-label2">Terengganu
                        <input type="checkbox" name="Terengganu" id="Terengganu" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
        </div>

    	<div class="filter-div-section">
            <label for="price" class="filter-label filter-label3">Price Range: <span id="amount"></span>
                <input type="checkbox" name="price" id="price" value="0" class="filter-input" />
                <span class="checkmark"></span>
            </label>
            <div id="priceDiv" style="display:none" class="distance-slider">
            	  <div id="slider-range" class="clean"></div>

                  <form method="post" action="get_items.php">
                    <input type="hidden" id="amount1" class="clean">
                    <input type="hidden" id="amount2" class="clean">

                  </form>
            </div>
        </div>
        <div class="clear"></div>
        <button class="green-button mid-btn-width clean mid-btn-margin">Submit</button>
    </form>
  </div>

</div> -->


<!-- Filter Modal -->
<div id="partnerfilter-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css filter-modal">
    <span class="close-css close-partnerfilter">&times;</span>
    <h2 class="green-text h2-title">Filter</h2>
    <div class="green-border filter-border"></div>
    <form>
     	<div class="filter-div-section">
            <label for="partnerState" class="filter-label filter-label3">Location
                <input type="checkbox" name="state" id="partnerState" value="0" class="filter-input" />
                <span class="checkmark"></span>
            </label>
            <div id="partnerStateDiv" style="display:none">
                <input type="text" placeholder="Search" class="filter-search clean">
                <button class="transparent-button filter-search-btn clean">
                    <img src="img/search.png" class="filter-search-img opacity-hover" alt="Search" title="Search">
                </button>
                <div class="clear"></div>
                <div class="filter-option">
                    <label for="Johor" class="filter-label filter-label2">Johor
                        <input type="checkbox" name="Johor" id="Johor" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Kedah" class="filter-label filter-label2">Kedah
                        <input type="checkbox" name="Kedah" id="Kedah" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Kelantan" class="filter-label filter-label2">Kelantan
                        <input type="checkbox" name="Kelantan" id="Kelantan" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="KualaLumpur" class="filter-label filter-label2">Kuala Lumpur
                        <input type="checkbox" name="KualaLumpur" id="KualaLumpur" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Labuan" class="filter-label filter-label2">Labuan
                        <input type="checkbox" name="Labuan" id="Labuan" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Melaka" class="filter-label filter-label2">Melaka
                        <input type="checkbox" name="Melaka" id="Melaka" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="NegeriSembilan" class="filter-label filter-label2">Negeri Sembilan
                        <input type="checkbox" name="NegeriSembilan" id="NegeriSembilan" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Pahang" class="filter-label filter-label2">Pahang
                        <input type="checkbox" name="Pahang" id="Pahang" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Penang" class="filter-label filter-label2">Penang
                        <input type="checkbox" name="Penang" id="Penang" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Perak" class="filter-label filter-label2">Perak
                        <input type="checkbox" name="Perak" id="Perak" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Perlis" class="filter-label filter-label2">Perlis
                        <input type="checkbox" name="Perlis" id="Perlis" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Putrajaya" class="filter-label filter-label2">Putrajaya
                        <input type="checkbox" name="Putrajaya" id="Putrajaya" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Sabah" class="filter-label filter-label2">Sabah
                        <input type="checkbox" name="Sabah" id="Sabah" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Sarawak" class="filter-label filter-label2">Sarawak
                        <input type="checkbox" name="Sarawak" id="Sarawak" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Selangor" class="filter-label filter-label2">Selangor
                        <input type="checkbox" name="Selangor" id="Selangor" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="Terengganu" class="filter-label filter-label2">Terengganu
                        <input type="checkbox" name="Terengganu" id="Terengganu" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
        </div>
    	<div class="filter-div-section">
            <label for="rating" class="filter-label filter-label3">Rating
                <input type="checkbox" name="rating" id="rating" value="0" class="filter-input" />
                <span class="checkmark"></span>
            </label>
            <div id="ratingDiv" style="display:none">

                <input class="hidden radio-label" type="radio" name="hightolow" id="hightolow"/>
                <label class="button-label" for="hightolow">
                  High to Low
                </label>
                <input class="hidden radio-label" type="radio" name="lowtohigh" id="lowtohigh"/>
                <label class="button-label" for="lowtohigh">
                  Low to High
                </label>
        	</div>
        </div>
    	<div class="filter-div-section">
            <label for="services" class="filter-label filter-label3">Services
                <input type="checkbox" name="services" id="services" value="0" class="filter-input" />
                <span class="checkmark"></span>
            </label>
            <div id="servicesDiv" style="display:none">
                <div class="clear"></div>
                <div class="filter-option">
                    <label for="puppySellers" class="filter-label filter-label2">Puppy Sellers
                        <input type="checkbox" name="puppySellers" id="puppySellers" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="petHotel" class="filter-label filter-label2">Pet Hotel
                        <input type="checkbox" name="petHotel" id="petHotel" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="kittenSellers" class="filter-label filter-label2">Kitten Sellers
                        <input type="checkbox" name="kittenSellers" id="kittenSellers" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="vet" class="filter-label filter-label2">Vet
                        <input type="checkbox" name="vet" id="vet" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="reptileSellers" class="filter-label filter-label2">Reptile Sellers
                        <input type="checkbox" name="reptileSellers" id="reptileSellers" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="grooming" class="filter-label filter-label2">Grooming
                        <input type="checkbox" name="grooming" id="grooming" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="deliveryServices" class="filter-label filter-label2">Delivery Services
                        <input type="checkbox" name="deliveryServices" id="deliveryServices" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="restaurant" class="filter-label filter-label2">Restaurant
                        <input type="checkbox" name="restaurant" id="restaurant" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="others" class="filter-label filter-label2">Others
                        <input type="checkbox" name="others" id="others" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>

            </div>
        </div>

        <div class="clear"></div>
        <button class="green-button mid-btn-width clean mid-btn-margin">Submit</button>
    </form>
  </div>

</div>


<!-- Social Media Share Modal -->
<div id="social-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css forgot-modal-content login-modal-content">
    <span class="close-css close-social">&times;</span>
    <h2 class="green-text h2-title">Share</h2>
    <div class="green-border"></div>
    <div class="clear"></div>
    <div class="width100 mini-height30"></div>
    <div class="clear"></div>
		<div class="a2a_kit a2a_kit_size_32 a2a_default_style">
        					<a class="a2a_button_whatsapp"></a>
                            <a class="a2a_button_facebook social-a"></a>
                            <a class="a2a_button_twitter social-a"></a>
                            <a class="a2a_button_facebook_messenger social-a"></a>
                            <a class="a2a_button_wechat social-a"></a>
                            <a class="a2a_button_copy_link social-a"></a>
                            
                            
		</div>
        <script async src="https://static.addtoany.com/menu/page.js"></script>
  </div>

</div>




<!-- Product Filter Modal -->
<div id="productfilter-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css filter-modal">
    <span class="close-css close-productfilter">&times;</span>
    <h2 class="green-text h2-title">Filter</h2>
    <div class="green-border filter-border"></div>
    <form>
    	<div class="filter-div-section">
            <label for="animalType" class="filter-label filter-label3">For Animal Type
                <input type="checkbox" name="animalType" id="animalType" value="0" class="filter-input" />
                <span class="checkmark"></span>
            </label>
            <div id="animalTypeDiv" style="display:none">
                <div class="filter-option">
                    <label for="dog" class="filter-label filter-label2">Dog
                        <input type="checkbox" name="dog" id="dog" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="cat" class="filter-label filter-label2">Cat
                        <input type="checkbox" name="cat" id="cat" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="reptile" class="filter-label filter-label2">Reptile
                        <input type="checkbox" name="reptile" id="reptile" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
        </div>
    	<div class="filter-div-section">
            <label for="brand" class="filter-label filter-label3">Brand
                <input type="checkbox" name="brand" id="brand" value="0" class="filter-input" />
                <span class="checkmark"></span>
            </label>
            <div id="brandDiv" style="display:none">
                <input type="text" placeholder="Search" class="filter-search clean">
                <button class="transparent-button filter-search-btn clean">
                    <img src="img/search.png" class="filter-search-img opacity-hover" alt="Search" title="Search">
                </button>
                <div class="clear"></div>
                <div class="filter-option">
                    <label for="brand1" class="filter-label filter-label2">Canine Caviar
                        <input type="checkbox" name="brand1" id="brand1" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="brand2" class="filter-label filter-label2">Hill's Prescription
                        <input type="checkbox" name="brand2" id="brand2" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="brand3" class="filter-label filter-label2">Merrick
                        <input type="checkbox" name="brand3" id="brand3" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="brand4" class="filter-label filter-label2">Orijen
                        <input type="checkbox" name="brand4" id="brand4" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="brand5" class="filter-label filter-label2">Pedigree
                        <input type="checkbox" name="brand5" id="brand5" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="brand6" class="filter-label filter-label2">Royal Canin
                        <input type="checkbox" name="brand6" id="brand6" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="brand7" class="filter-label filter-label2">Taste of the Wild
                        <input type="checkbox" name="brand7" id="brand7" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="brand8" class="filter-label filter-label2">Zignature
                        <input type="checkbox" name="brand8" id="brand8" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
        </div>


    	<div class="filter-div-section">
            <label for="category" class="filter-label filter-label3">Category
                <input type="checkbox" name="category" id="category" value="0" class="filter-input" />
                <span class="checkmark"></span>
            </label>
            <div id="categoryDiv" style="display:none">
                <div class="clear"></div>
                <div class="filter-option">
                    <label for="accessory" class="filter-label filter-label2">Accessory
                        <input type="checkbox" name="accessory" id="accessory" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="food" class="filter-label filter-label2">Food
                        <input type="checkbox" name="food" id="food" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="grooming" class="filter-label filter-label2">Grooming
                        <input type="checkbox" name="grooming" id="grooming" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="health" class="filter-label filter-label2">Health
                        <input type="checkbox" name="health" id="health" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="supplies" class="filter-label filter-label2">Supplies
                        <input type="checkbox" name="supplies" id="supplies" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="filter-option">
                    <label for="toys" class="filter-label filter-label2">Toys
                        <input type="checkbox" name="toys" id="toys" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
        </div>

    	<div class="filter-div-section">
            <label for="productprice" class="filter-label filter-label3">Price Range: <span id="amounta"></span>
                <input type="checkbox" name="productprice" id="productprice" value="0" class="filter-input" />
                <span class="checkmark"></span>
            </label>
            <div id="productpriceDiv" style="display:none" class="distance-slider">
            	  <div id="slider-range2" class="clean"></div>

                  <form method="post" action="get_items.php">
                    <input type="hidden" id="amount3" class="clean">
                    <input type="hidden" id="amount4" class="clean">

                  </form>
            </div>
        </div>
        <div class="clear"></div>
        <button class="green-button mid-btn-width clean mid-btn-margin">Submit</button>
    </form>
  </div>

</div>
<!-- Product Filter Modal -->
<div id="variation-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css filter-modal variation-modal-css">
    <span class="close-css close-variation">&times;</span>
    <h2 class="green-text h2-title">Amount and Variation</h2>
    <div class="green-border filter-border"></div>
	<div class="clear"></div>
    <div class="variation-left-div">
        <div class="variation-product-preview">
        <div  id="Variation1" class="tabcontent block2">
            <img src="img/product-1.jpg" class="variation-img" alt="Product Name" title="Product Name"  />
        </div>
        <div  id="Variation2" class="tabcontent">
            <img src="img/product-2.jpg" class="variation-img" alt="Product Name" title="Product Name"  />
        </div>
        <div  id="Variation3" class="tabcontent">
            <img src="img/product-3.jpg" class="variation-img" alt="Product Name" title="Product Name"  />
        </div>
        <div  id="Variation4" class="tabcontent">
            <img src="img/product-4.jpg" class="variation-img" alt="Product Name" title="Product Name"  />
        </div>

        </div>
    </div>
    <div class="variation-right-div">
            <div class="tab variation-tab">

                <button class="tablinks active variation-btn" onclick="openTab(event, 'Variation1')">
                    <label for="variation1" class="filter-label filter-label3 variation-label">Variation 1
                        <input type="checkbox" name="variation1" id="variation1" class="filter-input" value="0"/>
                        <span class="checkmark"></span>
                    </label>
                </button>
                <div id="variation1Div" class="variation-hidden-div" style="display:none">
                	<p class="left-stock-p">Stock: 200</p>
                    <p class="right-price-p">RM28.00</p>
                    <div class="clear"></div>
                    <!--
                    <p class="quantity-p">Quantity</p>
                    <p class="right-amount-p">0</p>
                    -->
                    <p class="quantity-p">Quantity</p>
                    <div class="numbers-row numbers-row-css">



                    </div>

                </div>
                <button class="tablinks variation-btn" onclick="openTab(event, 'Variation2')">
                    <label for="variation2" class="filter-label filter-label3 variation-label">Variation 2
                        <input type="checkbox" name="variation2" id="variation2" class="filter-input" value="0"/>
                        <span class="checkmark"></span>
                    </label>
                </button>
                <div id="variation2Div" class="variation-hidden-div" style="display:none">
                	<p class="left-stock-p">Stock: 200</p>
                    <p class="right-price-p">RM28.00</p>
                    <div class="clear"></div>
                    <!--
                    <p class="quantity-p">Quantity</p>
                    <p class="right-amount-p">0</p>
                    -->
                    <p class="quantity-p">Quantity</p>
                    <div class="numbers-row2 numbers-row-css">



                    </div>

                </div>
                <button class="tablinks variation-btn" onclick="openTab(event, 'Variation3')">
                    <label for="variation3" class="filter-label filter-label3 variation-label">Variation 3
                        <input type="checkbox" name="variation3" id="variation3" class="filter-input" value="0"/>
                        <span class="checkmark"></span>
                    </label>
                </button>
                <div id="variation3Div" class="variation-hidden-div" style="display:none">
                	<p class="left-stock-p">Stock: 200</p>
                    <p class="right-price-p">RM28.00</p>
                    <div class="clear"></div>
                    <!--
                    <p class="quantity-p">Quantity</p>
                    <p class="right-amount-p">0</p>
                    -->
                    <p class="quantity-p">Quantity</p>
                    <div class="numbers-row3 numbers-row-css">



                    </div>

                </div>
                <button class="tablinks variation-btn" onclick="openTab(event, 'Variation4')">
                    <label for="variation4" class="filter-label filter-label3 variation-label">Variation 4
                        <input type="checkbox" name="variation4" id="variation4" class="filter-input" value="0"/>
                        <span class="checkmark"></span>
                    </label>
                </button>
                <div id="variation4Div" class="variation-hidden-div" style="display:none">
                	<p class="left-stock-p">Stock: 200</p>
                    <p class="right-price-p">RM28.00</p>
                    <div class="clear"></div>
                    <!--
                    <p class="quantity-p">Quantity</p>
                    <p class="right-amount-p">0</p>
                    -->
                    <p class="quantity-p">Quantity</p>
                    <div class="numbers-row4 numbers-row-css">



                    </div>

                </div>
                <div class="clear"></div>

            </div>

   		</div>
		<div class="clear"></div>
		<div class="purchase-bottom-div overflow">
                <div class="width100 variation-bottom-div">
                    <p class="left-total-p">Total</p>
                    <p class="right-total-price">RM0.00</p>

                </div>
        	<button class="left-orange-div clean orange-button">Add to Cart</button>
            <button class="right-red-div clean red-btn">Buy Now</button>
        </div>
    </div>

</div>


<!-- CCV Modal -->
<div id="ccv-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css ccv-modal-margin">
    <span class="close-css close-ccv">&times;</span>

    <div class="clear"></div>
    <img src="img/ccv-sample.png" class="width100" alt="CCV Sample" title="CCV Sample">

  </div>

</div>
<!-- URL Modal -->
<div id="url-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css ccv-modal-margin">
    <span class="close-css close-url">&times;</span>

    <div class="clear"></div>
    <img src="img/url-sample.jpg" class="width100" alt="URL Sample" title="URL Sample">


  </div>

</div>
<!-- URL Modal -->
<div id="desc-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css ccv-modal-margin">
    <span class="close-css close-desc">&times;</span>

    <div class="clear"></div>
    <img src="img/description-sample.jpg" class="width100" alt="Description Sample" title="Description Sample">


  </div>

</div>
<!-- Tutorial Modal -->
<div id="tutorial-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css ccv-modal-margin">
    <span class="close-css close-tutorial">&times;</span>

    <div class="clear"></div>
    <h2 class="tutorial-title ow-no-margin-top">Tutorial of Placing Image</h2>
    <p class="tutorial-p">Click the image icon. Please don't directly copy paste image inside.</p>
    <img src="img/t1.jpg" class="width100" alt="Tutorial" title="Tutorial">
    <p class="tutorial-p">Right click the image and choose copy image address.</p>
    <img src="img/t2.jpg" class="width100" alt="Tutorial" title="Tutorial">
    <p class="tutorial-p">Paste the image address inside and click ok.</p>
    <img src="img/t3.jpg" class="width100" alt="Tutorial" title="Tutorial">
    <h2 class="tutorial-title">Tutorial of Embed Youtube Video</h2>
    <p class="tutorial-p">Click iframe (the Earth icon) from the tool list.</p>
    <img src="img/t4.jpg" class="width100" alt="Tutorial" title="Tutorial">
    <p class="tutorial-p">Click share below the Youtube video.</p>
    <img src="img/t5.jpg" class="width100" alt="Tutorial" title="Tutorial">
    <p class="tutorial-p">Click embed.</p>
    <img src="img/t6.jpg" class="width100" alt="Tutorial" title="Tutorial">
    <p class="tutorial-p">Copy the link ONLY, start from https</p>
    <img src="img/t7.jpg" class="width100" alt="Tutorial" title="Tutorial">
    <p class="tutorial-p">Paste the link and click ok.</p>
    <img src="img/t8.jpg" class="width100" alt="Tutorial" title="Tutorial">
    <h2 class="tutorial-title">Tutorial of Insert Link</h2>
    <p class="tutorial-p">Click the link/chain icon.</p>
    <img src="img/t9.jpg" class="width100" alt="Tutorial" title="Tutorial">
    <p class="tutorial-p">Type the display text for the link. Paste the link inside URL column.</p>
    <img src="img/t10.jpg" class="width100" alt="Tutorial" title="Tutorial">
        <div class="width100 overflow text-center">
            <div class="green-button white-text clean2 edit-1-btn margin-auto close-tutorial">Close</div>
        </div>


  </div>

</div>
<!-- Vimeo Modal -->
<div id="vimeo-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css ccv-modal-margin">
    <span class="close-css close-vimeo">&times;</span>

    <div class="clear"></div>
    <h2 class="tutorial-title ow-no-margin-top">Tutorial of Embed Vimeo Video</h2>
    <p class="tutorial-p">Click Share</p>
    <img src="img/vmeo1.png" class="width100" alt="Tutorial" title="Tutorial">
    <p class="tutorial-p">Only copy the link highlighted, don't copy the ""</p>
    <img src="img/vmeo2.png" class="width100" alt="Tutorial" title="Tutorial">
    <h2 class="tutorial-title">Tutorial of Embed Youtube Video</h2>
    <p class="tutorial-p">Click iframe (the Earth icon) from the tool list.</p>
    <img src="img/t4.jpg" class="width100" alt="Tutorial" title="Tutorial">
    <p class="tutorial-p">Click share below the Youtube video.</p>
    <img src="img/t5.jpg" class="width100" alt="Tutorial" title="Tutorial">
    <p class="tutorial-p">Click embed.</p>
    <img src="img/t6.jpg" class="width100" alt="Tutorial" title="Tutorial">
    <p class="tutorial-p">Copy the link ONLY, start from https, don't copy the ""</p>
    <img src="img/t7.jpg" class="width100" alt="Tutorial" title="Tutorial">
    <h2 class="tutorial-title">Tutorial of Embed FB Video</h2>
    <p class="tutorial-p">Click ... and select Embed</p>
    <img src="img/fb1.png" class="width100" alt="Tutorial" title="Tutorial">
    <p class="tutorial-p">Only copy the link inside src="" start from https, don't copy the ""</p>
    <img src="img/fb2.png" class="width100" alt="Tutorial" title="Tutorial">


        <div class="width100 overflow text-center">
            <div class="green-button white-text clean2 edit-1-btn margin-auto close-vimeo">Close</div>
        </div>


  </div>

</div>
<script type="text/javascript" src="js/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="js/jquery-2.2.0.min.js"></script>
<script src="js/jquery.fancybox.js" type="text/javascript"></script>
<script src="js/jquery.colorbox-min.js" type="text/javascript"></script>
		<script>
			$(document).ready(function(){
				//Examples of how to assign the Colorbox event to elements

				$(".iframe").colorbox({iframe:true, width:"400px", height:"490px"});
				$(".inline").colorbox({inline:true, width:"50%"});
				$(".callbacks").colorbox({
					onOpen:function(){ alert('onOpen: colorbox is about to open'); },
					onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
					onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
					onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
					onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
				});


				//Example of preserving a JavaScript event for inline calls.
				$("#click").click(function(){
					$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
					return false;
				});

				if($(window).width()<=500)
				{
					$(".iframe").colorbox({iframe:true, width:"280px", height:"440px"});
				}

			});
		</script>



<!-- filter dropdown script -->
<script type="text/javascript">
$('#breedGroup').change(function() {
    $('#breedGroupDiv').toggle();
});
$('#color').change(function() {
    $('#colorDiv').toggle();
});
$('#gender').change(function() {
    $('#genderDiv').toggle();
});
$('#gender1').change(function() {
    $('#genderDiv1').toggle();
});
$('#state').change(function() {
    $('#stateDiv').toggle();
});
$('#partnerState').change(function() {
    $('#partnerStateDiv').toggle();
});
$('#price').change(function() {
    $('#priceDiv').toggle();
});
$('#rating').change(function() {
    $('#ratingDiv').toggle();
});
$('#services').change(function() {
    $('#servicesDiv').toggle();
});
$('#animalType').change(function() {
    $('#animalTypeDiv').toggle();
});
$('#brand').change(function() {
    $('#brandDiv').toggle();
});
$('#category').change(function() {
    $('#categoryDiv').toggle();
});
$('#productprice').change(function() {
    $('#productpriceDiv').toggle();
});
$('#variation1').change(function() {
    $('#variation1Div').toggle();
});
$('#variation2').change(function() {
    $('#variation2Div').toggle();
});
$('#variation3').change(function() {
    $('#variation3Div').toggle();
});
$('#variation4').change(function() {
    $('#variation4Div').toggle();
});
$('#voucher').change(function() {
    $('#voucherDiv').toggle();
});
$('#price1').change(function() {
    $('#priceDiv1').toggle();
});
</script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
  <script type="text/javascript">

  $(function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 10000,
      // max: 1000000,
      values: [ 1000, 5000 ],
      slide: function( event, ui ) {
        $( "#amount" ).html( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
		$( "#amount1" ).val(ui.values[ 0 ]);
		$( "#amount2" ).val(ui.values[ 1 ]);
      }
    });
    $( "#amount" ).html( "$" + $( "#slider-range" ).slider( "values", 0 ) +
     " - $" + $( "#slider-range" ).slider( "values", 1 ) );
  });
  </script>
  <script type="text/javascript">

  $(function() {
    $( "#slider-range2" ).slider({
      range: true,
      min: 0,
      max: 10000,
      values: [ 0, 10000 ],
      slide: function( event, ui ) {
        $( "#amounta" ).html( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
		$( "#amount3" ).val(ui.values[ 0 ]);
		$( "#amount4" ).val(ui.values[ 1 ]);
      }
    });
    $( "#amounta" ).html( "$" + $( "#slider-range2" ).slider( "values", 0 ) +
     " - $" + $( "#slider-range2" ).slider( "values", 1 ) );
  });
  </script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/headroom.js"></script>

<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();

    }());
</script>

	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });

    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });

    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;

       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";

    });
    </script>
<script type="text/javascript" src="js/lightslider.js"></script>
    <script>
    	 $(document).ready(function() {
			$("#content-slider").lightSlider({
                loop:false,
                keyPress:true
            });
            $('#image-gallery').lightSlider({
                gallery:true,
                item:1,
                thumbItem:9,
                slideMargin: 0,
                speed:500,
                auto:false,
                loop:true,
                onSliderLoad: function() {
                    $('#image-gallery').removeClass('cS-hidden');
                }
            });
		});
    </script>
<!--- Modal Box --->
<script>
var forgotmodal = document.getElementById("forgot-modal");
var signupmodal = document.getElementById("signup-modal");
var loginmodal = document.getElementById("login-modal");
var forgot2modal = document.getElementById("forgot2-modal");
var filtermodal = document.getElementById("filter-modal");
var filtermodal2 = document.getElementById("filter-modal2");
var socialmodal = document.getElementById("social-modal");
var reportmodal = document.getElementById("report-modal");
var reviewmodal = document.getElementById("review-modal");
var partnerfiltermodal = document.getElementById("partnerfilter-modal");
var productfiltermodal = document.getElementById("productfilter-modal");
var variationmodal = document.getElementById("variation-modal");
var ccvmodal = document.getElementById("ccv-modal");
var urlmodal = document.getElementById("url-modal");
var descmodal = document.getElementById("desc-modal");
var tutorialmodal = document.getElementById("tutorial-modal");
var vimeomodal = document.getElementById("vimeo-modal");
var confirmmodal = document.getElementById("confirm-modal");


var openforgot = document.getElementsByClassName("open-forgot")[0];
var openforgot2 = document.getElementsByClassName("open-forgot2")[0];
var opensignup = document.getElementsByClassName("open-signup")[0];
var opensignup1 = document.getElementsByClassName("open-signup")[1];
var opensignup2 = document.getElementsByClassName("open-signup")[2];
var opensignup3 = document.getElementsByClassName("open-signup")[3];
var openlogin = document.getElementsByClassName("open-login")[0];
var openlogin1 = document.getElementsByClassName("open-login")[1];
var openlogin2 = document.getElementsByClassName("open-login")[2];
var openlogin3 = document.getElementsByClassName("open-login")[3];
var openlogin4 = document.getElementsByClassName("open-login")[4];
var openlogin5 = document.getElementsByClassName("open-login")[5];
var openlogin6 = document.getElementsByClassName("open-login")[6];
var openlogin7 = document.getElementsByClassName("open-login")[7];
var openlogin8 = document.getElementsByClassName("open-login")[8];
var openlogin9 = document.getElementsByClassName("open-login")[9];
var openfilter = document.getElementsByClassName("open-filter")[0];
var openfilter2 = document.getElementsByClassName("open-filter2")[0];
var opensocial = document.getElementsByClassName("open-social")[0];
var openreport = document.getElementsByClassName("open-report")[0];
var openreport1 = document.getElementsByClassName("open-report")[1];
var openreport2 = document.getElementsByClassName("open-report")[2];
var openreview = document.getElementsByClassName("open-review")[0];
var openreview1 = document.getElementsByClassName("open-review")[1];
var openpartnerfilter = document.getElementsByClassName("open-partnerfilter")[0];
var openproductfilter = document.getElementsByClassName("open-productfilter")[0];
var openvariation = document.getElementsByClassName("open-variation")[0];
var openccv = document.getElementsByClassName("open-ccv")[0];
var openurl = document.getElementsByClassName("open-url")[0];
var opendesc = document.getElementsByClassName("open-desc")[0];
var opentutorial = document.getElementsByClassName("open-tutorial")[0];
var openvimeo = document.getElementsByClassName("open-vimeo")[0];
var openconfirm = document.getElementsByClassName("open-confirm")[0];
var openconfirm1 = document.getElementsByClassName("open-confirm")[1];
var openconfirm2 = document.getElementsByClassName("open-confirm")[2];
var openconfirm3 = document.getElementsByClassName("open-confirm")[3];



var closeforgot = document.getElementsByClassName("close-forgot")[0];
var closeforgot2 = document.getElementsByClassName("close-forgot2")[0];
var closesignup = document.getElementsByClassName("close-signup")[0];
var closelogin = document.getElementsByClassName("close-login")[0];
var closefilter = document.getElementsByClassName("close-filter")[0];
var closefilter2 = document.getElementsByClassName("close-filter2")[0];
var closefilter2a = document.getElementsByClassName("close-filter2")[1];
var closesocial = document.getElementsByClassName("close-social")[0];
var closereport = document.getElementsByClassName("close-report")[0];
var closereview = document.getElementsByClassName("close-review")[0];
var closepartnerfilter = document.getElementsByClassName("close-partnerfilter")[0];
var closeproductfilter = document.getElementsByClassName("close-productfilter")[0];
var closevariation = document.getElementsByClassName("close-variation")[0];
var closeccv = document.getElementsByClassName("close-ccv")[0];
var closeurl = document.getElementsByClassName("close-url")[0];
var closedesc = document.getElementsByClassName("close-desc")[0];
var closetutorial = document.getElementsByClassName("close-tutorial")[0];
var closetutorial1 = document.getElementsByClassName("close-tutorial")[1];
var closevimeo = document.getElementsByClassName("close-vimeo")[0];
var closevimeo1 = document.getElementsByClassName("close-vimeo")[1];
var closeconfirm = document.getElementsByClassName("close-confirm")[0];
var closeconfirm2 = document.getElementsByClassName("close-confirm")[1];

if(openforgot){
openforgot.onclick = function() {
  forgotmodal.style.display = "block";
}
}
if(opensignup){
opensignup.onclick = function() {
  signupmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(opensignup1){
opensignup1.onclick = function() {
  signupmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(opensignup2){
opensignup2.onclick = function() {
  signupmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(opensignup3){
opensignup3.onclick = function() {
  signupmodal.style.display = "block";
  forgot2modal.style.display = "none";
  loginmodal.style.display = "none";
}
}
if(openlogin){
openlogin.onclick = function() {
  loginmodal.style.display = "block";
  signupmodal.style.display = "none";
  forgot2modal.style.display = "none";
}
}
if(openlogin1){
openlogin1.onclick = function() {
  loginmodal.style.display = "block";
  signupmodal.style.display = "none";
  forgot2modal.style.display = "none";

}
}
if(openlogin2){
openlogin2.onclick = function() {
  loginmodal.style.display = "block";
  signupmodal.style.display = "none";
  forgot2modal.style.display = "none";

}
}
if(openlogin3){
openlogin3.onclick = function() {
  loginmodal.style.display = "block";
	signupmodal.style.display = "none";
	forgot2modal.style.display = "none";
}
}
if(openlogin4){
openlogin4.onclick = function() {
  loginmodal.style.display = "block";
	forgot2modal.style.display = "none";
	signupmodal.style.display = "none";
	forgot2modal.style.display = "none";
}
}
if(openlogin5){
openlogin5.onclick = function() {
  loginmodal.style.display = "block";
  signupmodal.style.display = "none";
  forgot2modal.style.display = "none";

}
}
if(openlogin6){
openlogin6.onclick = function() {
  loginmodal.style.display = "block";
  signupmodal.style.display = "none";
  forgot2modal.style.display = "none";

}
}
if(openlogin7){
openlogin7.onclick = function() {
  loginmodal.style.display = "block";
  signupmodal.style.display = "none";
  forgot2modal.style.display = "none";

}
}
if(openlogin8){
openlogin8.onclick = function() {
  loginmodal.style.display = "block";
  signupmodal.style.display = "none";
  forgot2modal.style.display = "none";

}
}
if(openlogin9){
openlogin9.onclick = function() {
  loginmodal.style.display = "block";
  signupmodal.style.display = "none";
  forgot2modal.style.display = "none";

}
}
if(openforgot2){
openforgot2.onclick = function() {
  loginmodal.style.display = "none";
	forgot2modal.style.display = "block";
}
}
if(openfilter){
openfilter.onclick = function() {

	filtermodal.style.display = "block";
}
}
if(openfilter2){
openfilter2.onclick = function() {

	filtermodal2.style.display = "block";
}
}
if(opensocial){
opensocial.onclick = function() {
  socialmodal.style.display = "none";
	socialmodal.style.display = "block";
}
}
if(openreport){
openreport.onclick = function() {
  reportmodal.style.display = "block";
}
}
if(openreport1){
openreport1.onclick = function() {
  reportmodal.style.display = "block";
}
}
if(openreport2){
openreport2.onclick = function() {
  reportmodal.style.display = "block";
}
}
if(openreview){
openreview.onclick = function() {
  reviewmodal.style.display = "block";
}
}
if(openreview1){
openreview1.onclick = function() {
  reviewmodal.style.display = "block";
}
}
if(openpartnerfilter){
openpartnerfilter.onclick = function() {
  partnerfiltermodal.style.display = "block";
}
}
if(openproductfilter){
openproductfilter.onclick = function() {
  productfiltermodal.style.display = "block";
}
}
if(openvariation){
openvariation.onclick = function() {
  variationmodal.style.display = "block";
}
}
if(openccv){
openccv.onclick = function() {
  ccvmodal.style.display = "block";
}
}
if(openurl){
openurl.onclick = function() {
  urlmodal.style.display = "block";
}
}
if(opendesc){
opendesc.onclick = function() {
  descmodal.style.display = "block";
}
}
if(opentutorial){
opentutorial.onclick = function() {
  tutorialmodal.style.display = "block";
}
}
if(openvimeo){
openvimeo.onclick = function() {
  vimeomodal.style.display = "block";
}
}
if(openconfirm){
openconfirm.onclick = function() {
  confirmmodal.style.display = "block";
}
}
if(openconfirm1){
openconfirm1.onclick = function() {
  confirmmodal.style.display = "block";
}
}
if(openconfirm2){
openconfirm2.onclick = function() {
  confirmmodal.style.display = "block";
}
}
if(openconfirm3){
openconfirm3.onclick = function() {
  confirmmodal.style.display = "block";
}
}


if(closeforgot){
closeforgot.onclick = function() {
  forgotmodal.style.display = "none";
}
}
if(closeforgot2){
closeforgot2.onclick = function() {
  forgot2modal.style.display = "none";
}
}
if(closesignup){
closesignup.onclick = function() {
  signupmodal.style.display = "none";
}
}
if(closelogin){
closelogin.onclick = function() {
  loginmodal.style.display = "none";
}
}
if(closefilter){
closefilter.onclick = function() {
  filtermodal.style.display = "none";
}
}
if(closefilter2){
closefilter2.onclick = function() {
  filtermodal2.style.display = "none";
}
}
if(closefilter2a){
closefilter2a.onclick = function() {
  filtermodal2.style.display = "none";
}
}
if(closesocial){
closesocial.onclick = function() {
  socialmodal.style.display = "none";
}
}
if(closereport){
closereport.onclick = function() {
  reportmodal.style.display = "none";
}
}
if(closereview){
closereview.onclick = function() {
  reviewmodal.style.display = "none";
}
}
if(closepartnerfilter){
closepartnerfilter.onclick = function() {
  partnerfiltermodal.style.display = "none";
}
}
if(closeproductfilter){
closeproductfilter.onclick = function() {
  productfiltermodal.style.display = "none";
}
}
if(closevariation){
closevariation.onclick = function() {
  variationmodal.style.display = "none";
}
}
if(closeccv){
closeccv.onclick = function() {
  ccvmodal.style.display = "none";
}
}
if(closeurl){
closeurl.onclick = function() {
  urlmodal.style.display = "none";
}
}
if(closedesc){
closedesc.onclick = function() {
  descmodal.style.display = "none";
}
}
if(closetutorial){
closetutorial.onclick = function() {
  tutorialmodal.style.display = "none";
}
}
if(closetutorial1){
closetutorial1.onclick = function() {
  tutorialmodal.style.display = "none";
}
}
if(closevimeo){
closevimeo.onclick = function() {
  vimeomodal.style.display = "none";
}
}
if(closevimeo1){
closevimeo1.onclick = function() {
  vimeomodal.style.display = "none";
}
}
if(closeconfirm){
closeconfirm.onclick = function() {
  confirmmodal.style.display = "none";
}
}
if(closeconfirm2){
closeconfirm2.onclick = function() {
  confirmmodal.style.display = "none";
}
}

window.onclick = function(event) {
  if (event.target == forgotmodal) {
    forgotmodal.style.display = "none";
  }
  if (event.target == signupmodal) {
    signupmodal.style.display = "none";
  }
  if (event.target == loginmodal) {
    loginmodal.style.display = "none";
  }
  if (event.target == forgot2modal) {
    forgot2modal.style.display = "none";
  }
  if (event.target == filtermodal) {
    filtermodal.style.display = "none";
  }
  if (event.target == filtermodal2) {
    filtermodal2.style.display = "none";
  }
  if (event.target == socialmodal) {
    socialmodal.style.display = "none";
  }
  if (event.target == reportmodal) {
    reportmodal.style.display = "none";
  }
  if (event.target == reviewmodal) {
    reviewmodal.style.display = "none";
  }
  if (event.target == partnerfiltermodal) {
    partnerfiltermodal.style.display = "none";
  }
  if (event.target == productfiltermodal) {
    productfiltermodal.style.display = "none";
  }
  if (event.target == variationmodal) {
    variationmodal.style.display = "none";
  }
  if (event.target == ccvmodal) {
    ccvmodal.style.display = "none";
  }
  if (event.target == urlmodal) {
    urlmodal.style.display = "none";
  }
  if (event.target == descmodal) {
    descmodal.style.display = "none";
  }
  if (event.target == tutorialmodal) {
    tutorialmodal.style.display = "none";
  }
  if (event.target == vimeomodal) {
    vimeomodal.style.display = "none";
  }
  if (event.target == confirmmodal) {
    confirmmodal.style.display = "none";
  }
}
</script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>
<script>
function goBack() {
  window.history.back();
}
</script>

<script src="js/jssor.slider.min.js" type="text/javascript"></script>
<script type="text/javascript">

        jssor_1_slider_init = function() {

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 3000;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>


    <script type="text/javascript">jssor_1_slider_init();</script>
    <!-- #endregion Jssor Slider End -->
<!--
  <script src="./slick/slick.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
    $(document).on('ready', function() {
      $(".variable").slick({
        dots: false,
        infinite: true,
        variableWidth: true
      });
	  $(".regular").slick({
        dots: true,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1
      });
    });
  </script>-->
<script>
function openTab(evt, tabName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
<script>
$(function(){$(".numbers-row1").append('<div class="dec button2 hover1 button"><img src="img/minus.png" class="hover1a add-minus" alt="Minus" title="Minus"><img src="img/minus2.png" class="hover1b add-minus" alt="Minus" title="Minus"></div><label for="name" class="quantity-label"></label><input type="text" name="variation1-input" id="variation1-input" value="0" class="quantity-input"><div class="inc button2 hover1 button"><span class="display-none">+</span><img src="img/add.png" class="hover1a add-minus" alt="Add" title="Add"><img src="img/add2.png" class="hover1b add-minus" alt="Add" title="Add"></div>');$(".button").on("click",function(){var $button=$(this);var oldValue=$button.parent().find("input").val();if($button.text()=="+"){var newVal=parseFloat(oldValue)+1;}else{if(oldValue>0){var newVal=parseFloat(oldValue)-1;}else{newVal=0;}}
$button.parent().find("input").val(newVal);});});

</script>
<script>
$(function(){$(".numbers-row2").append('<div class="dec button2 hover1 button"><img src="img/minus.png" class="hover1a add-minus" alt="Minus" title="Minus"><img src="img/minus2.png" class="hover1b add-minus" alt="Minus" title="Minus"></div><label for="name" class="quantity-label"></label><input type="text" name="variation1-input" id="variation1-input" value="0" class="quantity-input"><div class="inc button2 hover1 button"><span class="display-none">+</span><img src="img/add.png" class="hover1a add-minus" alt="Add" title="Add"><img src="img/add2.png" class="hover1b add-minus" alt="Add" title="Add"></div>');$(".button").on("click",function(){var $button=$(this);var oldValue=$button.parent().find("input").val();if($button.text()=="+"){var newVal=parseFloat(oldValue)+1;}else{if(oldValue>0){var newVal=parseFloat(oldValue)-1;}else{newVal=0;}}
$button.parent().find("input").val(newVal);});});

</script>
<script>
$(function(){$(".numbers-row3").append('<div class="dec button2 hover1 button"><img src="img/minus.png" class="hover1a add-minus" alt="Minus" title="Minus"><img src="img/minus2.png" class="hover1b add-minus" alt="Minus" title="Minus"></div><label for="name" class="quantity-label"></label><input type="text" name="variation1-input" id="variation1-input" value="0" class="quantity-input"><div class="inc button2 hover1 button"><span class="display-none">+</span><img src="img/add.png" class="hover1a add-minus" alt="Add" title="Add"><img src="img/add2.png" class="hover1b add-minus" alt="Add" title="Add"></div>');$(".button").on("click",function(){var $button=$(this);var oldValue=$button.parent().find("input").val();if($button.text()=="+"){var newVal=parseFloat(oldValue)+1;}else{if(oldValue>0){var newVal=parseFloat(oldValue)-1;}else{newVal=0;}}
$button.parent().find("input").val(newVal);});});

</script>
<script>
$(function(){$(".numbers-row4").append('<div class="dec button2 hover1 button"><img src="img/minus.png" class="hover1a add-minus" alt="Minus" title="Minus"><img src="img/minus2.png" class="hover1b add-minus" alt="Minus" title="Minus"></div><label for="name" class="quantity-label"></label><input type="text" name="variation1-input" id="variation1-input" value="0" class="quantity-input"><div class="inc button2 hover1 button"><span class="display-none">+</span><img src="img/add.png" class="hover1a add-minus" alt="Add" title="Add"><img src="img/add2.png" class="hover1b add-minus" alt="Add" title="Add"></div>');$(".button").on("click",function(){var $button=$(this);var oldValue=$button.parent().find("input").val();if($button.text()=="+"){var newVal=parseFloat(oldValue)+1;}else{if(oldValue>0){var newVal=parseFloat(oldValue)-1;}else{newVal=0;}}
$button.parent().find("input").val(newVal);});});

</script>
<script>
$(function(){$(".numbers-row5").append('<div class="dec button2 hover1 button"><img src="img/minus.png" class="hover1a add-minus" alt="Minus" title="Minus"><img src="img/minus2.png" class="hover1b add-minus" alt="Minus" title="Minus"></div><label for="name" class="quantity-label"></label><input type="text" name="variation1-input" id="variation1-input" value="0" class="quantity-input"><div class="inc button2 hover1 button"><span class="display-none">+</span><img src="img/add.png" class="hover1a add-minus" alt="Add" title="Add"><img src="img/add2.png" class="hover1b add-minus" alt="Add" title="Add"></div>');$(".button").on("click",function(){var $button=$(this);var oldValue=$button.parent().find("input").val();if($button.text()=="+"){var newVal=parseFloat(oldValue)+1;}else{if(oldValue>0){var newVal=parseFloat(oldValue)-1;}else{newVal=0;}}
$button.parent().find("input").val(newVal);});});

</script>
<script>
// File Upload
//
function ekUpload(){
  function Init() {

    console.log("Upload Initialised");

    var fileSelect    = document.getElementById('file-upload'),
        fileDrag      = document.getElementById('file-drag'),
        submitButton  = document.getElementById('submit-button');

    fileSelect.addEventListener('change', fileSelectHandler, false);

    // Is XHR2 available?
    var xhr = new XMLHttpRequest();
    if (xhr.upload) {
      // File Drop
      fileDrag.addEventListener('dragover', fileDragHover, false);
      fileDrag.addEventListener('dragleave', fileDragHover, false);
      fileDrag.addEventListener('drop', fileSelectHandler, false);
    }
  }

  function fileDragHover(e) {
    var fileDrag = document.getElementById('file-drag');

    e.stopPropagation();
    e.preventDefault();

    fileDrag.className = (e.type === 'dragover' ? 'hover' : 'modal-body file-upload');
  }

  function fileSelectHandler(e) {
    // Fetch FileList object
    var files = e.target.files || e.dataTransfer.files;

    // Cancel event and hover styling
    fileDragHover(e);

    // Process all File objects
    for (var i = 0, f; f = files[i]; i++) {
      parseFile(f);
      uploadFile(f);
    }
  }

  // Output
  function output(msg) {
    // Response
    var m = document.getElementById('messages');
    m.innerHTML = msg;
  }

  function parseFile(file) {

    console.log(file.name);
    output(
      '<strong>' + encodeURI(file.name) + '</strong>'
    );

    // var fileType = file.type;
    // console.log(fileType);
    var imageName = file.name;

    var isGood = (/\.(?=gif|jpg|png|jpeg)/gi).test(imageName);
    if (isGood) {
      document.getElementById('start').classList.add("hidden");
      document.getElementById('response').classList.remove("hidden");
      document.getElementById('notimage').classList.add("hidden");
      // Thumbnail Preview
      document.getElementById('file-image').classList.remove("hidden");
      document.getElementById('file-image').src = URL.createObjectURL(file);
    }
    else {
      document.getElementById('file-image').classList.add("hidden");
      document.getElementById('notimage').classList.remove("hidden");
      document.getElementById('start').classList.remove("hidden");
      document.getElementById('response').classList.add("hidden");
      document.getElementById("file-upload-form").reset();
    }
  }

  function setProgressMaxValue(e) {
    var pBar = document.getElementById('file-progress');

    if (e.lengthComputable) {
      pBar.max = e.total;
    }
  }

  function updateFileProgress(e) {
    var pBar = document.getElementById('file-progress');

    if (e.lengthComputable) {
      pBar.value = e.loaded;
    }
  }

  function uploadFile(file) {

    var xhr = new XMLHttpRequest(),
      fileInput = document.getElementById('class-roster-file'),
      pBar = document.getElementById('file-progress'),
      fileSizeLimit = 1024; // In MB
    if (xhr.upload) {
      // Check if file is less than x MB
      if (file.size <= fileSizeLimit * 1024 * 1024) {
        // Progress bar
        pBar.style.display = 'inline';
        xhr.upload.addEventListener('loadstart', setProgressMaxValue, false);
        xhr.upload.addEventListener('progress', updateFileProgress, false);

        // File received / failed
        xhr.onreadystatechange = function(e) {
          if (xhr.readyState == 4) {
            // Everything is good!

            // progress.className = (xhr.status == 200 ? "success" : "failure");
            // document.location.reload(true);
          }
        };

        // Start upload
        xhr.open('POST', document.getElementById('file-upload-form').action, true);
        xhr.setRequestHeader('X-File-Name', file.name);
        xhr.setRequestHeader('X-File-Size', file.size);
        xhr.setRequestHeader('Content-Type', 'multipart/form-data');
        xhr.send(file);
      } else {
        output('Please upload a smaller file (< ' + fileSizeLimit + ' MB).');
      }
    }
  }

  // Check for the various File API support.
  if (window.File && window.FileList && window.FileReader) {
    Init();
  } else {
    document.getElementById('file-drag').style.display = 'none';
  }
}
ekUpload();
</script>
<script>
function goBack() {
  window.history.back();
}
</script>
<script>
$("#copy-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          putNoticeJavascript("Copied!! ","");
      });
      $("#invest-now-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          putNoticeJavascript("Copied!! ","");
      });
</script>
<script type="text/javascript" src="js/main.js?version=1.0.1"></script>

<script>
function myFunctionA()
{
    var x = document.getElementById("register_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("register_retype_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionC()
{
    var x = document.getElementById("password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>
<script>
function showVariation() {
  var x = document.getElementById("variationDiv");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function myShow() {
  var x = document.getElementById("show-hide-p-div");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<!--<script src="js/progressive-image.js" type="text/javascript"></script>-->
  <script src="js/index2.js"></script>
  <script>
    (function(){
      new Progressive({
        el: '#app',
        lazyClass: 'lazy',
        removePreview: true,
        scale: true
      }).fire()
    })()
  </script>
<!-- Notice Modal -->
<div id="notice-modal" class="modal-css notice-modal-ow">

  <!-- Modal content -->
  <div class="modal-content-css notice-modal-content">
    <span class="close-css close-notice" id="close-notice">&times;</span>
    <div class="clear"></div>
    <h1 class="menu-h1" id="noticeTitle">Title Here</h1>
	<div class="menu-link-container" id="noticeMessage">Message Here</div>
  </div>

</div>
