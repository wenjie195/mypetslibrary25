<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/States.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$states = getStates($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Update Company Logo | Mypetslibrary" />
<title>Update Company Logo | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Update Company Logo</h1>
            <div class="green-border"></div>
   </div>
   <div class="border-separation">
        <div class="clear"></div>
            <div class="width100 overflow">

                <?php
                if(isset($_POST['seller_uid']))
                {
                    $conn = connDB();
                    $sellerDetails = getSeller($conn,"WHERE uid = ? ", array("uid") ,array($_POST['seller_uid']),"s");
                    ?>

                        <p class="input-top-p admin-top-p">Seller Company Logo</p>

                        <div class="four-div-box1">
                            <img src="sellerProfile/<?php echo $sellerDetails[0]->getCompanyLogo();?>" class="pet-photo-preview">
                        </div>

                        <div class="four-div-box1 left-four-div1">

                            <?php 
                            if ($sellerDetails[0]->getCompanyPic()) 
                            {
                            ?>
                                <img src="sellerProfile/<?php echo $sellerDetails[0]->getCompanyPic();?>" class="pet-photo-preview">
                            <?php
                            }
                            else
                            {   }
                            ?>
                        </div>

                        <div class="clear"></div>

                        <div class="four-div-box1">
                            <form method="POST" action="updateCompanyLogo1.php" class="hover1">
                                <button class="clean green-button pointer width100" type="submit" name="seller_uid" value="<?php echo $sellerDetails[0]->getUid();?>">
                                Update Logo
                                </button>
                            </form>
                        </div>  

                        <div class="four-div-box1 left-four-div1">
                            <form method="POST" action="updateCompanyLogo2.php" class="hover1">
                                <button class="clean green-button pointer width100" type="submit" name="seller_uid" value="<?php echo $sellerDetails[0]->getUid();?>">
                                Update Profile
                                </button>
                            </form>               

                            <?php 
                                $compPic = $sellerDetails[0]->getCompanyPic();
                                if($compPic != '')
                                {
                                ?>
                                    <button class="clean green-button pointer width100 ow-peach-bg-hover" id="deletePic" type="button" name="button" value="<?php echo $sellerDetails[0]->getuid();?>">
                                        Delete
                                    </button>  
                                <?php
                                }
                                else
                                {}
                            ?>
             
                        </div> 

                    <?php
                }
                ?>

                
                             



        </div>
  </div>  
</div>      
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>
<script>
  $("#deletePic").click(function(){
    var companyPic = $(this).val();
    // alert(companyPic);
    $.ajax({
      url: 'utilities/deleteCompanyPic.php',
      data: {pic:companyPic},
      type: 'post',
      dataType: 'json',
      success:function(response){
        var success = response[0]['picDelete'];
        alert(success);
        window.location.reload();
      },
      error:function(response){
        alert("Error Occur");
      }
    });
  });
</script>
