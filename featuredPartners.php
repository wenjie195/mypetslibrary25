<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $seller = getSeller($conn," WHERE account_status = 'Active' ");
$seller = getSeller($conn," WHERE featured_seller = 'Yes' AND account_status = 'Active' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Featured Sellers | Mypetslibrary" />
<title>Featured Sellers | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance">

  <div class="width100">
    <div class="left-h1-div featured-left">
      <h1 class="green-text h1-title">Featured Sellers</h1>
      <div class="green-border"></div>
    </div>
    <div class="right-add-div featured-right">
      <a href="addFeaturedPartners.php"><div class="green-button white-text">Add</div></a>
      <!-- <a href="addSeller.php"><div class="green-button white-text">Add</div></a> -->
    </div>        
  </div>
    
  <div class="clear"></div>

	<div class="width103 border-separation">
    <?php
    $conn = connDB();
    if($seller)
    {
      for($cntAA = 0;$cntAA < count($seller) ;$cntAA++)
      {
      ?>
          <div class="four-box-size"> 	
            <div class="shadow-white-box">
              <div class="width100 white-bg">
                <!-- <img  src="img/pet-seller.jpg" alt="Partner Name" title="Partner Name" class="width100 two-border-radius"> -->
                <img  src="uploads/<?php echo $seller[$cntAA]->getCompanyLogo();?>" alt="<?php echo $seller[$cntAA]->getCompanyName();?>" 
                      title="<?php echo $seller[$cntAA]->getCompanyName();?>" class="width100 two-border-radius">
              </div>
              <div class="width100 product-details-div">
                <!-- <p class="width100 text-overflow slider-product-name">Partner Name</p>
                <p class="width100 text-overflow slider-product-price">Penang</p> -->
                <p class="width100 text-overflow slider-product-name"><?php echo $seller[$cntAA]->getCompanyName();?></p>
                <p class="width100 text-overflow slider-product-name"><?php echo $seller[$cntAA]->getState();?></p>
              </div>
            </div>
            <!-- <div class="clean red-btn featured-same-button open-confirm">Delete</div> -->

            <!-- <form method="POST" action="utilities/deleteFeaturedPartnerFunction.php" >
                <button class="clean red-btn featured-same-button open-confirm" type="submit" name="seller_uid" value="<?php echo $seller[$cntAA]->getUid();?>">
                    Delete
                </button>
            </form> -->

            <form method="POST" action="utilities/adminDeleteFeaturedPartnerFunction.php">
                <button class="clean red-btn featured-same-button open-confirm" type="submit" name="seller_uid" value="<?php echo $seller[$cntAA]->getUid();?>">
                    Delete
                </button> 
            </form>

          </div>
      <?php
      }
      ?>
    <?php
    }
    $conn->close();
    ?>
  </div>

        <!-- <form>
            <div class="four-box-size"> 	
                <div class="shadow-white-box featured">
                      <div class="width100 white-bg">
                        <img  src="img/pet-seller.jpg" alt="Partner Name" title="Partner Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Partner Name</p>
                            <p class="width100 text-overflow slider-product-price">Penang</p>
                      </div>
                </div>

                <div class="clean red-btn featured-same-button open-confirm">Delete</div>

                            <div id="confirm-modal" class="modal-css">
                        
                              <div class="modal-content-css confirm-modal-margin">
                                <span class="close-css close-confirm">&times;</span>
                                <div class="clear"></div>
                                <h2 class="green-text h2-title confirm-title">Confirm Delete?</h2>
                                <div class="clean cancel-btn close-confirm">Cancel</div>
                                <button class="clean red-btn delete-btn2">Delete</button>
                                <div class="clear"></div>
                              </div>
                            
                            </div>                 
            </div>
        </form>

        <form>
            <div class="four-box-size"> 	
                <div class="shadow-white-box featured">
                      <div class="width100 white-bg">
                        <img  src="img/pet-seller.jpg" alt="Partner Name" title="Partner Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Partner Name</p>
                            <p class="width100 text-overflow slider-product-price">Penang</p>
                      </div>
                </div>

                <div class="clean red-btn featured-same-button open-confirm">Delete</div>
                
            </div>
        </form>

        <form>
            <div class="four-box-size"> 	
                <div class="shadow-white-box featured">
                      <div class="width100 white-bg">
                        <img  src="img/pet-seller.jpg" alt="Partner Name" title="Partner Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Partner Name</p>
                            <p class="width100 text-overflow slider-product-price">Penang</p>
                      </div>
                </div>

                <div class="clean red-btn featured-same-button open-confirm">Delete</div>
                
            </div>
        </form>

        <form>
            <div class="four-box-size"> 	
                <div class="shadow-white-box featured">
                      <div class="width100 white-bg">
                        <img  src="img/pet-seller.jpg" alt="Partner Name" title="Partner Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Partner Name</p>
                            <p class="width100 text-overflow slider-product-price">Penang</p>
                      </div>
                </div>

                <div class="clean red-btn featured-same-button open-confirm">Delete</div>
                
            </div>
        </form>
          
        </div>

    <div class="clear"></div> -->
    
</div>      
 
<div class="clear"></div>
        
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Seller Deleted!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to delete seller! ";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !! ";
        }

        else if($_GET['type'] == 4)
        {
            $messageType = "Featured Seller Added !! ";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Fail to add featured sellers !! ";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "ERROR <br> Please Retry !! ";
        }

        else if($_GET['type'] == 7)
        {
            $messageType = "Featured Seller Deleted !! ";
        }
        else if($_GET['type'] == 8)
        {
            $messageType = "Fail to delete featured sellers !! ";
        }

        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>