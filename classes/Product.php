<?php  
class Product {
    /* Member variables */
    var $id,$uid,$category,$brand,$name,$sku,$slug,$price,$diamond,$minPrice,$midPrice,$maxPrice,$feature,$status,$quantity,$description,$descriptionTwo,$descriptionThree,
            $descriptionFour,$descriptionFive,$descriptionSix,$rating,$animalType,$keywordOne,$link,$imageOne,$imageTwo,$imageThree,$imageFour,$imageFive,$imageSix,
                $defaultImage,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getDiamond()
    {
        return $this->diamond;
    }

    /**
     * @param mixed $diamond
     */
    public function setDiamond($diamond)
    {
        $this->diamond = $diamond;
    }

        /**
     * @return mixed
     */
    public function getMinPrice()
    {
        return $this->minPrice;
    }

    /**
     * @param mixed $minPrice
     */
    public function setMinPrice($minPrice)
    {
        $this->minPrice = $minPrice;
    }

        /**
     * @return mixed
     */
    public function getMidPrice()
    {
        return $this->midPrice;
    }

    /**
     * @param mixed $midPrice
     */
    public function setMidPrice($midPrice)
    {
        $this->midPrice = $midPrice;
    }

        /**
     * @return mixed
     */
    public function getMaxPrice()
    {
        return $this->maxPrice;
    }

    /**
     * @param mixed $maxPrice
     */
    public function setMaxPrice($maxPrice)
    {
        $this->maxPrice = $maxPrice;
    }

    /**
     * @return mixed
     */
    public function getFeature()
    {
        return $this->feature;
    }

    /**
     * @param mixed $feature
     */
    public function setFeature($feature)
    {
        $this->feature = $feature;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDescriptionTwo()
    {
        return $this->descriptionTwo;
    }

    /**
     * @param mixed $descriptionTwo
     */
    public function setDescriptionTwo($descriptionTwo)
    {
        $this->descriptionTwo = $descriptionTwo;
    }

    /**
     * @return mixed
     */
    public function getDescriptionThree()
    {
        return $this->descriptionThree;
    }

    /**
     * @param mixed $descriptionThree
     */
    public function setDescriptionThree($descriptionThree)
    {
        $this->descriptionThree = $descriptionThree;
    }

    /**
     * @return mixed
     */
    public function getDescriptionFour()
    {
        return $this->descriptionFour;
    }

    /**
     * @param mixed $descriptionFour
     */
    public function setDescriptionFour($descriptionFour)
    {
        $this->descriptionFour = $descriptionFour;
    }

    /**
     * @return mixed
     */
    public function getDescriptionFive()
    {
        return $this->descriptionFive;
    }

    /**
     * @param mixed $descriptionFive
     */
    public function setDescriptionFive($descriptionFive)
    {
        $this->descriptionFive = $descriptionFive;
    }

    /**
     * @return mixed
     */
    public function getDescriptionSix()
    {
        return $this->descriptionSix;
    }

    /**
     * @param mixed $descriptionSix
     */
    public function setDescriptionSix($descriptionSix)
    {
        $this->descriptionSix = $descriptionSix;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return mixed
     */
    public function getAnimalType()
    {
        return $this->animalType;
    }

    /**
     * @param mixed $animalType
     */
    public function setAnimalType($animalType)
    {
        $this->animalType = $animalType;
    }

    /**
     * @return mixed
     */
    public function getKeywordOne()
    {
        return $this->keywordOne;
    }

    /**
     * @param mixed $keywordOne
     */
    public function setKeywordOne($keywordOne)
    {
        $this->keywordOne = $keywordOne;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getImageOne()
    {
        return $this->imageOne;
    }

    /**
     * @param mixed $imageOne
     */
    public function setImageOne($imageOne)
    {
        $this->imageOne = $imageOne;
    }

    /**
     * @return mixed
     */
    public function getImageTwo()
    {
        return $this->imageTwo;
    }

    /**
     * @param mixed $imageTwo
     */
    public function setImageTwo($imageTwo)
    {
        $this->imageTwo = $imageTwo;
    }

    /**
     * @return mixed
     */
    public function getImageThree()
    {
        return $this->imageThree;
    }

    /**
     * @param mixed $imageThree
     */
    public function setImageThree($imageThree)
    {
        $this->imageThree = $imageThree;
    }

    /**
     * @return mixed
     */
    public function getImageFour()
    {
        return $this->imageFour;
    }

    /**
     * @param mixed $imageFour
     */
    public function setImageFour($imageFour)
    {
        $this->imageFour = $imageFour;
    }

    /**
     * @return mixed
     */
    public function getImageFive()
    {
        return $this->imageFive;
    }

    /**
     * @param mixed $imageFive
     */
    public function setImageFive($imageFive)
    {
        $this->imageFive = $imageFive;
    }

    /**
     * @return mixed
     */
    public function getImageSix()
    {
        return $this->imageSix;
    }

    /**
     * @param mixed $imageSix
     */
    public function setImageSix($imageSix)
    {
        $this->imageSix = $imageSix;
    }

    /**
     * @return mixed
     */
    public function getDefaultImage()
    {
        return $this->defaultImage;
    }

    /**
     * @param mixed $defaultImage
     */
    public function setDefaultImage($defaultImage)
    {
        $this->defaultImage = $defaultImage;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getProduct($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","category","brand","name","sku","slug","price","diamond","minPrice","midPrice","maxPrice","feature","status","quantity","description",
                                "description_two","description_three","description_four","description_five","description_six","rating","animal_type","keyword_one","link",
                                    "image_one","image_two","image_three","image_four","image_five","image_six","default_image","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"product");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$category,$brand,$name,$sku,$slug,$price,$diamond,$minPrice,$midPrice,$maxPrice,$feature,$status,$quantity,$description,$descriptionTwo,
                            $descriptionThree,$descriptionFour,$descriptionFive,$descriptionSix,$rating,$animalType,$keywordOne,$link,$imageOne,$imageTwo,$imageThree,
                                $imageFour,$imageFive,$imageSix,$defaultImage,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Product;
            $class->setId($id);
            $class->setUid($uid);
            $class->setCategory($category);
            $class->setBrand($brand);
            $class->setName($name);
            $class->setSku($sku);
            $class->setSlug($slug);
            $class->setPrice($price);
            $class->setDiamond($diamond);

            $class->setMinPrice($minPrice);
            $class->setMidPrice($midPrice);
            $class->setMaxPrice($maxPrice);

            $class->setFeature($feature);
            $class->setStatus($status);

            $class->setQuantity($quantity);
            $class->setDescription($description);
            $class->setDescriptionTwo($descriptionTwo);
            $class->setDescriptionThree($descriptionThree);
            $class->setDescriptionFour($descriptionFour);

            $class->setDescriptionFive($descriptionFive);
            $class->setDescriptionSix($descriptionSix);
            $class->setRating($rating);

            $class->setAnimalType($animalType);

            $class->setKeywordOne($keywordOne);
            $class->setLink($link);

            $class->setImageOne($imageOne);
            $class->setImageTwo($imageTwo);
            $class->setImageThree($imageThree);
            $class->setImageFour($imageFour);
            $class->setImageFive($imageFive);
            $class->setImageSix($imageSix);
            $class->setDefaultImage($defaultImage);

            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
          
            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function createProductList($products,$cartType = 1,$postQuantityRows = null,$isIncludeNotSelectedProductToo = true){
    /*
     * CART TYPE
     * 1 = normal product display cart
     * 2 = checkout cart
     */

    $productListHtml = "";

    if(!$products){
        return $productListHtml;
    }

    $index = 0;
    foreach ($products as $product){
        $quantity = 0;
        if($postQuantityRows){
            $quantity = $postQuantityRows[$index];
        }

        if($quantity <= 0 && !$isIncludeNotSelectedProductToo){
            $productListHtml .= '<div style="display: none;">';
        }else{
            $productListHtml .= '<div style="display: block;">';
        }

        $conn=connDB();
        //$productArray = getProduct($conn);
        $id  = $product->getName();

        $imageTwo  = $product->getImageTwo();
        $imageThree  = $product->getImageThree();
        $imageFour  = $product->getImageFour();
        $imageFive  = $product->getImageFive();
        $imageSix  = $product->getImageSix();

        $productLink  = $product->getLink();

        // Include the database configuration file

        // Get images from the database
        $query = $conn->query("SELECT image_one FROM product WHERE name = '$id'");

        if($query->num_rows > 0)
        {
            while($row = $query->fetch_assoc())
            {

                // if($imageSix == '')
                // {

                // }
                // elseif($imageFive == '')
                // {

                // }
                // elseif($imageFour == '')
                // {

                // }
                // elseif($imageThree == '')
                // {

                // }
                // elseif($imageTwo == '')
                // {

                // }

                // if($imageTwo != '')

                if($productLink == '')
                {
                    if($imageTwo == '')
                    {
                        $imageURL = './productImage/'.$row["image_one"];
                        $productListHtml .= '
                        <div class="shadow-white-box four-box-size opacity-hover pointer pointer-div" id="a'.$product->getUid().'2" onclick="myFunctiona'.$product->getUid().'()">
                            <div class="square">
                                <div class="width100 white-bg content progressive">
                                    <a>
                                        <img src="'.$imageURL.'" class="preview width100 two-border-radius opacity-hover pointer lazy" alt="'.$product->getName().'" title="'.$product->getName().'" />
                                    </a>                     
                                </div>
                            </div>
                            <p align="center" class="width100 text-overflow slider-product-name">'.$product->getName().'</p>
                            <p class="slider-product-name  text-overflow">RM '.$product->getPrice().'</p>
                        </div>  
                        <div class="fake-page-div product-details-all-div" style="display:none;" id="a'.$product->getUid().'">
                            <div class="big-content-div">
                                <div class="left-image-div">
                                    <div class="slider">        
                                         <div class="slides">
                                            <img src="'.$imageURL.'"  class="slides-img"  id="s'.$product->getUid().'1">
                                        </div>
                                    </div>  
                                </div> 
                                <div class="right-content-div2">
                                    <h1 class="green-text pet-name">'.$product->getName().'</h1>
                                    <p class="price-p2 ow-font-weight400">RM '.$product->getPrice().'</p>
                                    <div class="clear"></div>
                                    <div class="pet-details-div">
                                        <p class="pet-table-p">'.$product->getDescription().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionTwo().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionThree().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFour().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFive().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionSix().'</p>
                                        <input type="hidden" value="'.$product->getId().'" name="product-list-id-input['.$index.']">
                                    </div>
                                    <a href="productReview.php?id='.$product->getUid().'" target="_blank">
                                        <div class="view-review-div">
                                            <p class="light-green-a2 left-review-text">View Reviews</p>
                                            <p class="right-arrow2">></p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="input-quantity-div">
                                <p class="quantity-p2">Quantity:<br></p>
                                <div class="input-group center-div">
                                    <button type="button" class="product-button-minus button-minus math-button math-button2 clean opacity-hover">-</button>
                                    <input readonly type="number" step="1" max="" value="'.$quantity.'" name="product-list-quantity-input['.$index.']" class="quantity-field math-input clean text-center value-input-div">
                                    <button type="button" class="product-button-plus button-plus math-button math-button2 opacity-hover clean">+</button>
                                </div>
                                <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>	
                            </div>
                            <div class="clear"></div>
                            <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div x-back" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>
                            <div class="clear"></div>
                            <div class="bottom-height1"></div>	
                        </div> 
                        <script>
                            function myFunctiona'.$product->getUid().'()
                            {
                                var x = document.getElementById("a'.$product->getUid().'");
                                if (x.style.display === "none")
                                {
                                    x.style.display = "block";
                                } 
                                else
                                {
                                    x.style.display = "none";
                                }
                            }
                        </script>
                        ';
                    }
                    // elseif($imageThree != '')
                    elseif($imageThree == '')
                    {
                        $imageURL = './productImage/'.$row["image_one"];
                        $productListHtml .= '
                        <div class="shadow-white-box four-box-size opacity-hover pointer pointer-div" id="a'.$product->getUid().'2" onclick="myFunctiona'.$product->getUid().'()">
                            <div class="square">
                                <div class="width100 white-bg content progressive">
                                    <a>
                                        <img src="'.$imageURL.'" class="preview width100 two-border-radius opacity-hover pointer lazy" alt="'.$product->getName().'" title="'.$product->getName().'" />
                                    </a>                     
                                </div>
                            </div>
                            <p align="center" class="width100 text-overflow slider-product-name">'.$product->getName().'</p>
                            <p class="slider-product-name  text-overflow">RM '.$product->getPrice().'</p>
                        </div>  
                        <div class="fake-page-div product-details-all-div" style="display:none;" id="a'.$product->getUid().'">
                            <div class="big-content-div">
                                <div class="left-image-div">
                                    <div class="slider">        
                                        <div class="slides">
                                            <img src="'.$imageURL.'"  class="slides-img"  id="s'.$product->getUid().'1">
                                            <img src="./productImage/'.$product->getImageTwo().'"   class="slides-img" id="s'.$product->getUid().'2">
                                        </div>
                                        <div class="a-container">  
                                            <a href="#s'.$product->getUid().'1" class="small-a"><img src="'.$imageURL.'" class="link-small-img" ></a>
                                            <a href="#s'.$product->getUid().'2" class="small-a"><img src="./productImage/'.$product->getImageTwo().'" class="link-small-img" ></a>
                                        </div>  
                                    </div>
                                </div>  
                                <div class="right-content-div2">
                                    <h1 class="green-text pet-name">'.$product->getName().'</h1>
                                    <p class="price-p2 ow-font-weight400">RM '.$product->getPrice().'</p>
                                    <div class="clear"></div>
                                    <div class="pet-details-div">
                                        <p class="pet-table-p">'.$product->getDescription().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionTwo().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionThree().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFour().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFive().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionSix().'</p>
                                        <input type="hidden" value="'.$product->getId().'" name="product-list-id-input['.$index.']">
                                    </div>
                                    <a href="productReview.php?id='.$product->getUid().'" target="_blank">
                                        <div class="view-review-div">
                                            <p class="light-green-a2 left-review-text">View Reviews</p>
                                            <p class="right-arrow2">></p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="input-quantity-div">
                                <p class="quantity-p2">Quantity:<br></p>
                                <div class="input-group center-div">
                                    <button type="button" class="product-button-minus button-minus math-button math-button2 clean opacity-hover">-</button>
                                    <input readonly type="number" step="1" max="" value="'.$quantity.'" name="product-list-quantity-input['.$index.']" class="quantity-field math-input clean text-center value-input-div">
                                    <button type="button" class="product-button-plus button-plus math-button math-button2 opacity-hover clean">+</button>
                                </div>
                                <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>	
                            </div>
                            <div class="clear"></div>
                            <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div x-back" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>
                            <div class="clear"></div>
                            <div class="bottom-height1"></div>	
                        </div> 
                        <script>
                            function myFunctiona'.$product->getUid().'()
                            {
                                var x = document.getElementById("a'.$product->getUid().'");
                                if (x.style.display === "none")
                                {
                                    x.style.display = "block";
                                } 
                                else
                                {
                                    x.style.display = "none";
                                }
                            }
                        </script>
                        ';
                    }
                    // elseif($imageFour != '')
                    elseif($imageFour == '')
                    {
                        $imageURL = './productImage/'.$row["image_one"];
                        $productListHtml .= '
                        <div class="shadow-white-box four-box-size opacity-hover pointer pointer-div" id="a'.$product->getUid().'2" onclick="myFunctiona'.$product->getUid().'()">
                            <div class="square">
                                <div class="width100 white-bg content progressive">
                                    <a>
                                        <img src="'.$imageURL.'" class="preview width100 two-border-radius opacity-hover pointer lazy" alt="'.$product->getName().'" title="'.$product->getName().'" />
                                    </a>                     
                                </div>
                            </div>
                            <p align="center" class="width100 text-overflow slider-product-name">'.$product->getName().'</p>
                            <p class="slider-product-name  text-overflow">RM '.$product->getPrice().'</p>
                        </div>  
                        <div class="fake-page-div product-details-all-div" style="display:none;" id="a'.$product->getUid().'">
                            <div class="big-content-div">
                                <div class="left-image-div">
                                    <div class="slider">        
                                        <div class="slides">
                                            <img src="'.$imageURL.'"  class="slides-img"  id="s'.$product->getUid().'1">
                                            <img src="./productImage/'.$product->getImageTwo().'"   class="slides-img" id="s'.$product->getUid().'2">
                                            <img src="./productImage/'.$product->getImageThree().'"   class="slides-img" id="s'.$product->getUid().'3">
                                        </div>
                                        <div class="a-container">  
                                            <a href="#s'.$product->getUid().'1" class="small-a"><img src="'.$imageURL.'" class="link-small-img" ></a>
                                            <a href="#s'.$product->getUid().'2" class="small-a"><img src="./productImage/'.$product->getImageTwo().'" class="link-small-img" ></a>
                                            <a href="#s'.$product->getUid().'3" class="small-a"><img src="./productImage/'.$product->getImageThree().'" class="link-small-img" ></a>
                                        </div>  
                                    </div>
                                </div>  							
                                <div class="right-content-div2">
                                    <h1 class="green-text pet-name">'.$product->getName().'</h1>
                                    <p class="price-p2 ow-font-weight400">RM '.$product->getPrice().'</p>
                                    <div class="clear"></div>
                                    <div class="pet-details-div">
                                        <p class="pet-table-p">'.$product->getDescription().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionTwo().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionThree().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFour().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFive().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionSix().'</p>
                                        <input type="hidden" value="'.$product->getId().'" name="product-list-id-input['.$index.']">
                                    </div>
                                    <a href="productReview.php?id='.$product->getUid().'" target="_blank">
                                        <div class="view-review-div">
                                            <p class="light-green-a2 left-review-text">View Reviews</p>
                                            <p class="right-arrow2">></p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="input-quantity-div">
                                <p class="quantity-p2">Quantity:<br></p>
                                <div class="input-group center-div">
                                    <button type="button" class="product-button-minus button-minus math-button math-button2 clean opacity-hover">-</button>
                                    <input readonly type="number" step="1" max="" value="'.$quantity.'" name="product-list-quantity-input['.$index.']" class="quantity-field math-input clean text-center value-input-div">
                                    <button type="button" class="product-button-plus button-plus math-button math-button2 opacity-hover clean">+</button>
                                </div>
                                <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>	
                            </div>
                            <div class="clear"></div>
                            <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div x-back" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>
                            <div class="clear"></div>
                            <div class="bottom-height1"></div>	
                        </div> 
                        <script>
                            function myFunctiona'.$product->getUid().'()
                            {
                                var x = document.getElementById("a'.$product->getUid().'");
                                if (x.style.display === "none")
                                {
                                    x.style.display = "block";
                                } 
                                else
                                {
                                    x.style.display = "none";
                                }
                            }
                        </script>
                        ';
                    }
                    // elseif($imageFive != '')
                    elseif($imageFive == '')
                    {
                        $imageURL = './productImage/'.$row["image_one"];
                        $productListHtml .= '
                        <div class="shadow-white-box four-box-size opacity-hover pointer pointer-div" id="a'.$product->getUid().'2" onclick="myFunctiona'.$product->getUid().'()">
                            <div class="square">
                                <div class="width100 white-bg content progressive">
                                    <a>
                                        <img src="'.$imageURL.'" class="preview width100 two-border-radius opacity-hover pointer lazy" alt="'.$product->getName().'" title="'.$product->getName().'" />
                                    </a>                     
                                </div>
                            </div>
                            <p align="center" class="width100 text-overflow slider-product-name">'.$product->getName().'</p>
                            <p class="slider-product-name  text-overflow">RM '.$product->getPrice().'</p>
                        </div>  
                        <div class="fake-page-div product-details-all-div" style="display:none;" id="a'.$product->getUid().'">
                            <div class="big-content-div">
                                <div class="left-image-div">
                                    <div class="slider">        
                                        <div class="slides">
                                            <img src="'.$imageURL.'"  class="slides-img"  id="s'.$product->getUid().'1">
                                            <img src="./productImage/'.$product->getImageTwo().'"   class="slides-img" id="s'.$product->getUid().'2">
                                            <img src="./productImage/'.$product->getImageThree().'"   class="slides-img" id="s'.$product->getUid().'3">
                                            <img src="./productImage/'.$product->getImageFour().'"   class="slides-img" id="s'.$product->getUid().'4">
                                        </div>
                                        <div class="a-container">  
                                            <a href="#s'.$product->getUid().'1" class="small-a"><img src="'.$imageURL.'" class="link-small-img" ></a>
                                            <a href="#s'.$product->getUid().'2" class="small-a"><img src="./productImage/'.$product->getImageTwo().'" class="link-small-img" ></a>
                                            <a href="#s'.$product->getUid().'3" class="small-a"><img src="./productImage/'.$product->getImageThree().'" class="link-small-img" ></a>
                                            <a href="#s'.$product->getUid().'4" class="small-a"><img src="./productImage/'.$product->getImageFour().'" class="link-small-img" ></a>
                                        </div>  
                                    </div>
                                </div>  							
                                <div class="right-content-div2">
                                    <h1 class="green-text pet-name">'.$product->getName().'</h1>
                                    <p class="price-p2 ow-font-weight400">RM '.$product->getPrice().'</p>
                                    <div class="clear"></div>
                                    <div class="pet-details-div">
                                        <p class="pet-table-p">'.$product->getDescription().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionTwo().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionThree().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFour().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFive().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionSix().'</p>
                                        <input type="hidden" value="'.$product->getId().'" name="product-list-id-input['.$index.']">
                                    </div>
                                    <a href="productReview.php?id='.$product->getUid().'" target="_blank">
                                        <div class="view-review-div">
                                            <p class="light-green-a2 left-review-text">View Reviews</p>
                                            <p class="right-arrow2">></p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="input-quantity-div">
                                <p class="quantity-p2">Quantity:<br></p>
                                <div class="input-group center-div">
                                    <button type="button" class="product-button-minus button-minus math-button math-button2 clean opacity-hover">-</button>
                                    <input readonly type="number" step="1" max="" value="'.$quantity.'" name="product-list-quantity-input['.$index.']" class="quantity-field math-input clean text-center value-input-div">
                                    <button type="button" class="product-button-plus button-plus math-button math-button2 opacity-hover clean">+</button>
                                </div>
                                <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>	
                            </div>
                            <div class="clear"></div>
                            <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div x-back" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>
                            <div class="clear"></div>
                            <div class="bottom-height1"></div>	
                        </div> 
                        <script>
                            function myFunctiona'.$product->getUid().'()
                            {
                                var x = document.getElementById("a'.$product->getUid().'");
                                if (x.style.display === "none")
                                {
                                    x.style.display = "block";
                                } 
                                else
                                {
                                    x.style.display = "none";
                                }
                            }
                        </script>
                        ';
                    }
                    // elseif($imageSix != '')
                    elseif($imageSix == '')
                    {
                        $imageURL = './productImage/'.$row["image_one"];
                        $productListHtml .= '
                        <div class="shadow-white-box four-box-size opacity-hover pointer pointer-div" id="a'.$product->getUid().'2" onclick="myFunctiona'.$product->getUid().'()">
                            <div class="square">
                                <div class="width100 white-bg content progressive">
                                    <a>
                                        <img src="'.$imageURL.'" class="preview width100 two-border-radius opacity-hover pointer lazy" alt="'.$product->getName().'" title="'.$product->getName().'" />
                                    </a>                     
                                </div>
                            </div>
                            <p align="center" class="width100 text-overflow slider-product-name">'.$product->getName().'</p>
                            <p class="slider-product-name  text-overflow">RM '.$product->getPrice().'</p>
                        </div>  
                        <div class="fake-page-div product-details-all-div" style="display:none;" id="a'.$product->getUid().'">
                            <div class="big-content-div">
                                <div class="left-image-div">
                                    <div class="slider">        
                                        <div class="slides">
                                            <img src="'.$imageURL.'"  class="slides-img"  id="s'.$product->getUid().'1">
                                            <img src="./productImage/'.$product->getImageTwo().'"   class="slides-img" id="s'.$product->getUid().'2">
                                            <img src="./productImage/'.$product->getImageThree().'"   class="slides-img" id="s'.$product->getUid().'3">
                                            <img src="./productImage/'.$product->getImageFour().'"   class="slides-img" id="s'.$product->getUid().'4">
                                            <img src="./productImage/'.$product->getImageFive().'"   class="slides-img" id="s'.$product->getUid().'5">
                                        </div>
                                        <div class="a-container">  
                                            <a href="#s'.$product->getUid().'1" class="small-a"><img src="'.$imageURL.'" class="link-small-img" ></a>
                                            <a href="#s'.$product->getUid().'2" class="small-a"><img src="./productImage/'.$product->getImageTwo().'" class="link-small-img" ></a>
                                            <a href="#s'.$product->getUid().'3" class="small-a"><img src="./productImage/'.$product->getImageThree().'" class="link-small-img" ></a>
                                            <a href="#s'.$product->getUid().'4" class="small-a"><img src="./productImage/'.$product->getImageFour().'" class="link-small-img" ></a>
                                            <a href="#s'.$product->getUid().'5" class="small-a"><img src="./productImage/'.$product->getImageFive().'" class="link-small-img" ></a>
                                        </div>  
                                    </div>
                                </div>  								
                                <div class="right-content-div2">
                                    <h1 class="green-text pet-name">'.$product->getName().'</h1>
                                    <p class="price-p2 ow-font-weight400">RM '.$product->getPrice().'</p>
                                    <div class="clear"></div>
                                    <div class="pet-details-div">
                                        <p class="pet-table-p">'.$product->getDescription().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionTwo().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionThree().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFour().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFive().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionSix().'</p>
                                        <input type="hidden" value="'.$product->getId().'" name="product-list-id-input['.$index.']">
                                    </div>
                                    <a href="productReview.php?id='.$product->getUid().'" target="_blank">
                                        <div class="view-review-div">
                                            <p class="light-green-a2 left-review-text">View Reviews</p>
                                            <p class="right-arrow2">></p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="input-quantity-div">
                                <p class="quantity-p2">Quantity:<br></p>
                                <div class="input-group center-div">
                                    <button type="button" class="product-button-minus button-minus math-button math-button2 clean opacity-hover">-</button>
                                    <input readonly type="number" step="1" max="" value="'.$quantity.'" name="product-list-quantity-input['.$index.']" class="quantity-field math-input clean text-center value-input-div">
                                    <button type="button" class="product-button-plus button-plus math-button math-button2 opacity-hover clean">+</button>
                                </div>
                                <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>	
                            </div>
                            <div class="clear"></div>
                            <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div x-back" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>
                            <div class="clear"></div>
                            <div class="bottom-height1"></div>	
                        </div> 
                        <script>
                            function myFunctiona'.$product->getUid().'()
                            {
                                var x = document.getElementById("a'.$product->getUid().'");
                                if (x.style.display === "none")
                                {
                                    x.style.display = "block";
                                } 
                                else
                                {
                                    x.style.display = "none";
                                }
                            }
                        </script>
                        ';
                    }
                    else
                    {
                        $imageURL = './productImage/'.$row["image_one"];
                        $productListHtml .= '
                        <div class="shadow-white-box four-box-size opacity-hover pointer pointer-div" id="a'.$product->getUid().'2" onclick="myFunctiona'.$product->getUid().'()">
                            <div class="square">
                                <div class="width100 white-bg content progressive">
                                    <a>
                                        <img src="'.$imageURL.'" class="preview width100 two-border-radius opacity-hover pointer lazy" alt="'.$product->getName().'" title="'.$product->getName().'" />
                                    </a>                     
                                </div>
                            </div>
                            <p align="center" class="width100 text-overflow slider-product-name">'.$product->getName().'</p>
                            <p class="slider-product-name  text-overflow">RM '.$product->getPrice().'</p>
                        </div>  
                        <div class="fake-page-div product-details-all-div" style="display:none;" id="a'.$product->getUid().'">
                            <div class="big-content-div">
                                <div class="left-image-div">
                                    <div class="slider">        
                                        <div class="slides">
                                            <img src="'.$imageURL.'"  class="slides-img"  id="s'.$product->getUid().'1">
                                            <img src="./productImage/'.$product->getImageTwo().'"   class="slides-img" id="s'.$product->getUid().'2">
                                            <img src="./productImage/'.$product->getImageThree().'"   class="slides-img" id="s'.$product->getUid().'3">
                                            <img src="./productImage/'.$product->getImageFour().'"   class="slides-img" id="s'.$product->getUid().'4">
                                            <img src="./productImage/'.$product->getImageFive().'"   class="slides-img" id="s'.$product->getUid().'5">
                                            <img src="./productImage/'.$product->getImageSix().'"   class="slides-img" id="s'.$product->getUid().'6">
                                        </div>
                                        <div class="a-container">  
                                            <a href="#s'.$product->getUid().'1" class="small-a"><img src="'.$imageURL.'" class="link-small-img" ></a>
                                            <a href="#s'.$product->getUid().'2" class="small-a"><img src="./productImage/'.$product->getImageTwo().'" class="link-small-img" ></a>
                                            <a href="#s'.$product->getUid().'3" class="small-a"><img src="./productImage/'.$product->getImageThree().'" class="link-small-img" ></a>
                                            <a href="#s'.$product->getUid().'4" class="small-a"><img src="./productImage/'.$product->getImageFour().'" class="link-small-img" ></a>
                                            <a href="#s'.$product->getUid().'5" class="small-a"><img src="./productImage/'.$product->getImageFive().'" class="link-small-img" ></a>
                                            <a href="#s'.$product->getUid().'6" class="small-a"><img src="./productImage/'.$product->getImageSix().'" class="link-small-img" ></a>
                                        </div>  
                                    </div>
                                </div>  								
                                <div class="right-content-div2">
                                    <h1 class="green-text pet-name">'.$product->getName().'</h1>
                                    <p class="price-p2 ow-font-weight400">RM '.$product->getPrice().'</p>
                                    <div class="clear"></div>
                                    <div class="pet-details-div">
                                        <p class="pet-table-p">'.$product->getDescription().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionTwo().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionThree().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFour().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFive().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionSix().'</p>
                                        <input type="hidden" value="'.$product->getId().'" name="product-list-id-input['.$index.']">
                                    </div>
                                    <a href="productReview.php?id='.$product->getUid().'" target="_blank">
                                        <div class="view-review-div">
                                            <p class="light-green-a2 left-review-text">View Reviews</p>
                                            <p class="right-arrow2">></p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="input-quantity-div">
                                <p class="quantity-p2">Quantity:<br></p>
                                <div class="input-group center-div">
                                    <button type="button" class="product-button-minus button-minus math-button math-button2 clean opacity-hover">-</button>
                                    <input readonly type="number" step="1" max="" value="'.$quantity.'" name="product-list-quantity-input['.$index.']" class="quantity-field math-input clean text-center value-input-div">
                                    <button type="button" class="product-button-plus button-plus math-button math-button2 opacity-hover clean">+</button>
                                </div>
                                <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>	
                            </div>
                            <div class="clear"></div>
                            <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div x-back" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>
                            <div class="clear"></div>
                            <div class="bottom-height1"></div>	
                        </div> 
                        <script>
                            function myFunctiona'.$product->getUid().'()
                            {
                                var x = document.getElementById("a'.$product->getUid().'");
                                if (x.style.display === "none")
                                {
                                    x.style.display = "block";
                                } 
                                else
                                {
                                    x.style.display = "none";
                                }
                            }
                        </script>
                        ';
                    }
    
                }
                else
                {
                    if($imageTwo == '')
                    {
                        $imageURL = './productImage/'.$row["image_one"];
                        $productListHtml .= '
                        <div class="shadow-white-box four-box-size opacity-hover pointer pointer-div" id="a'.$product->getUid().'2" onclick="myFunctiona'.$product->getUid().'()">
                            <div class="square">
                                <div class="width100 white-bg content progressive">
                                    <a>
                                        <img src="'.$imageURL.'" class="preview width100 two-border-radius opacity-hover pointer lazy" alt="'.$product->getName().'" title="'.$product->getName().'" />
                                    </a>                     
                                </div>
                            </div>
                            <p align="center" class="width100 text-overflow slider-product-name">'.$product->getName().'</p>
                            <p class="slider-product-name  text-overflow">RM '.$product->getPrice().'</p>
                        </div>  
                        <div class="fake-page-div product-details-all-div" style="display:none;" id="a'.$product->getUid().'">
                            <div class="big-content-div">
                                <div class="left-image-div">
                                    <div class="slider">        
                                         <div class="slides">
                                            <img src="'.$imageURL.'"  class="slides-img"  id="s'.$product->getUid().'1">

                                            <iframe src="https://player.vimeo.com/video/'.$product->getLink().'" id="v'.$product->getUid().'" class="video-iframe slides-img" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                                
                                        </div>
										<div class="a-container">  
                                            <a href="#s'.$product->getUid().'1" class="small-a"><img src="'.$imageURL.'" class="link-small-img" ></a>
                                            <a href="#v'.$product->getUid().'" class="small-a"><img src="img/video.jpg" class="link-small-img" ></a>
										</div>
                                    </div>  
                                </div> 
                                <div class="right-content-div2">
                                    <h1 class="green-text pet-name">'.$product->getName().'</h1>
                                    <p class="price-p2 ow-font-weight400">RM '.$product->getPrice().'</p>
                                    <div class="clear"></div>
                                    <div class="pet-details-div">
                                        <p class="pet-table-p">'.$product->getDescription().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionTwo().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionThree().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFour().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFive().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionSix().'</p>
                                        <input type="hidden" value="'.$product->getId().'" name="product-list-id-input['.$index.']">
                                    </div>
                                    <a href="productReview.php?id='.$product->getUid().'" target="_blank">
                                        <div class="view-review-div">
                                            <p class="light-green-a2 left-review-text">View Reviews</p>
                                            <p class="right-arrow2">></p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="input-quantity-div">
                                <p class="quantity-p2">Quantity:<br></p>
                                <div class="input-group center-div">
                                    <button type="button" class="product-button-minus button-minus math-button math-button2 clean opacity-hover">-</button>
                                    <input readonly type="number" step="1" max="" value="'.$quantity.'" name="product-list-quantity-input['.$index.']" class="quantity-field math-input clean text-center value-input-div">
                                    <button type="button" class="product-button-plus button-plus math-button math-button2 opacity-hover clean">+</button>
                                </div>
                                <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>	
                            </div>
                            <div class="clear"></div>
                            <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div x-back" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>
                            <div class="clear"></div>
                            <div class="bottom-height1"></div>	
                        </div> 
                        <script>
                            function myFunctiona'.$product->getUid().'()
                            {
                                var x = document.getElementById("a'.$product->getUid().'");
                                if (x.style.display === "none")
                                {
                                    x.style.display = "block";
                                } 
                                else
                                {
                                    x.style.display = "none";
                                }
                            }
                        </script>
                        ';
                    }
                    // elseif($imageThree != '')
                    elseif($imageThree == '')
                    {
                        $imageURL = './productImage/'.$row["image_one"];
                        $productListHtml .= '
                        <div class="shadow-white-box four-box-size opacity-hover pointer pointer-div" id="a'.$product->getUid().'2" onclick="myFunctiona'.$product->getUid().'()">
                            <div class="square">
                                <div class="width100 white-bg content progressive">
                                    <a>
                                        <img src="'.$imageURL.'" class="preview width100 two-border-radius opacity-hover pointer lazy" alt="'.$product->getName().'" title="'.$product->getName().'" />
                                    </a>                     
                                </div>
                            </div>
                            <p align="center" class="width100 text-overflow slider-product-name">'.$product->getName().'</p>
                            <p class="slider-product-name  text-overflow">RM '.$product->getPrice().'</p>
                        </div>  
                        <div class="fake-page-div product-details-all-div" style="display:none;" id="a'.$product->getUid().'">
                            <div class="big-content-div">
                                <div class="left-image-div">
 									<div class="slider">        
                                         <div class="slides">
                                            <img src="'.$imageURL.'"  class="slides-img"  id="s'.$product->getUid().'1">
											<img src="./productImage/'.$product->getImageTwo().'"   class="slides-img" id="s'.$product->getUid().'2">
                                            <iframe src="https://player.vimeo.com/video/'.$product->getLink().'" id="v'.$product->getUid().'" class="video-iframe slides-img" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                                
                                        </div>
										<div class="a-container">  
                                            <a href="#s'.$product->getUid().'1" class="small-a"><img src="'.$imageURL.'" class="link-small-img" ></a>
											 <a href="#s'.$product->getUid().'2" class="small-a"><img src="./productImage/'.$product->getImageTwo().'" class="link-small-img" ></a>
                                            <a href="#v'.$product->getUid().'" class="small-a"><img src="img/video.jpg" class="link-small-img" ></a>
										</div>
                                    </div>  
                                </div> 								
								
								
								
								
								
								
								
								
 
                                <div class="right-content-div2">
                                    <h1 class="green-text pet-name">'.$product->getName().'</h1>
                                    <p class="price-p2 ow-font-weight400">RM '.$product->getPrice().'</p>
                                    <div class="clear"></div>
                                    <div class="pet-details-div">
                                        <p class="pet-table-p">'.$product->getDescription().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionTwo().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionThree().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFour().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFive().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionSix().'</p>
                                        <input type="hidden" value="'.$product->getId().'" name="product-list-id-input['.$index.']">
                                    </div>
                                    <a href="productReview.php?id='.$product->getUid().'" target="_blank">
                                        <div class="view-review-div">
                                            <p class="light-green-a2 left-review-text">View Reviews</p>
                                            <p class="right-arrow2">></p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="input-quantity-div">
                                <p class="quantity-p2">Quantity:<br></p>
                                <div class="input-group center-div">
                                    <button type="button" class="product-button-minus button-minus math-button math-button2 clean opacity-hover">-</button>
                                    <input readonly type="number" step="1" max="" value="'.$quantity.'" name="product-list-quantity-input['.$index.']" class="quantity-field math-input clean text-center value-input-div">
                                    <button type="button" class="product-button-plus button-plus math-button math-button2 opacity-hover clean">+</button>
                                </div>
                                <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>	
                            </div>
                            <div class="clear"></div>
                            <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div x-back" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>
                            <div class="clear"></div>
                            <div class="bottom-height1"></div>	
                        </div> 
                        <script>
                            function myFunctiona'.$product->getUid().'()
                            {
                                var x = document.getElementById("a'.$product->getUid().'");
                                if (x.style.display === "none")
                                {
                                    x.style.display = "block";
                                } 
                                else
                                {
                                    x.style.display = "none";
                                }
                            }
                        </script>
                        ';
                    }
                    // elseif($imageFour != '')
                    elseif($imageFour == '')
                    {
                        $imageURL = './productImage/'.$row["image_one"];
                        $productListHtml .= '
                        <div class="shadow-white-box four-box-size opacity-hover pointer pointer-div" id="a'.$product->getUid().'2" onclick="myFunctiona'.$product->getUid().'()">
                            <div class="square">
                                <div class="width100 white-bg content progressive">
                                    <a>
                                        <img src="'.$imageURL.'" class="preview width100 two-border-radius opacity-hover pointer lazy" alt="'.$product->getName().'" title="'.$product->getName().'" />
                                    </a>                     
                                </div>
                            </div>
                            <p align="center" class="width100 text-overflow slider-product-name">'.$product->getName().'</p>
                            <p class="slider-product-name  text-overflow">RM '.$product->getPrice().'</p>
                        </div>  
                        <div class="fake-page-div product-details-all-div" style="display:none;" id="a'.$product->getUid().'">
                            <div class="big-content-div">
                                <div class="left-image-div">
									<div class="slider">        
                                         <div class="slides">
                                            <img src="'.$imageURL.'"  class="slides-img"  id="s'.$product->getUid().'1">
											<img src="./productImage/'.$product->getImageTwo().'"   class="slides-img" id="s'.$product->getUid().'2">
											<img src="./productImage/'.$product->getImageThree().'"   class="slides-img" id="s'.$product->getUid().'3">
                                            <iframe src="https://player.vimeo.com/video/'.$product->getLink().'" id="v'.$product->getUid().'" class="video-iframe slides-img" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                                
                                        </div>
										<div class="a-container">  
                                            <a href="#s'.$product->getUid().'1" class="small-a"><img src="'.$imageURL.'" class="link-small-img" ></a>
											 <a href="#s'.$product->getUid().'2" class="small-a"><img src="./productImage/'.$product->getImageTwo().'" class="link-small-img" ></a>
											 <a href="#s'.$product->getUid().'3" class="small-a"><img src="./productImage/'.$product->getImageThree().'" class="link-small-img" ></a>
                                            <a href="#v'.$product->getUid().'" class="small-a"><img src="img/video.jpg" class="link-small-img" ></a>
										</div>
                                    </div>  
                                </div> 								
								
																
								
								
								
								
											
                                <div class="right-content-div2">
                                    <h1 class="green-text pet-name">'.$product->getName().'</h1>
                                    <p class="price-p2 ow-font-weight400">RM '.$product->getPrice().'</p>
                                    <div class="clear"></div>
                                    <div class="pet-details-div">
                                        <p class="pet-table-p">'.$product->getDescription().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionTwo().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionThree().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFour().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFive().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionSix().'</p>
                                        <input type="hidden" value="'.$product->getId().'" name="product-list-id-input['.$index.']">
                                    </div>
                                    <a href="productReview.php?id='.$product->getUid().'" target="_blank">
                                        <div class="view-review-div">
                                            <p class="light-green-a2 left-review-text">View Reviews</p>
                                            <p class="right-arrow2">></p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="input-quantity-div">
                                <p class="quantity-p2">Quantity:<br></p>
                                <div class="input-group center-div">
                                    <button type="button" class="product-button-minus button-minus math-button math-button2 clean opacity-hover">-</button>
                                    <input readonly type="number" step="1" max="" value="'.$quantity.'" name="product-list-quantity-input['.$index.']" class="quantity-field math-input clean text-center value-input-div">
                                    <button type="button" class="product-button-plus button-plus math-button math-button2 opacity-hover clean">+</button>
                                </div>
                                <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>	
                            </div>
                            <div class="clear"></div>
                            <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div x-back" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>
                            <div class="clear"></div>
                            <div class="bottom-height1"></div>	
                        </div> 
                        <script>
                            function myFunctiona'.$product->getUid().'()
                            {
                                var x = document.getElementById("a'.$product->getUid().'");
                                if (x.style.display === "none")
                                {
                                    x.style.display = "block";
                                } 
                                else
                                {
                                    x.style.display = "none";
                                }
                            }
                        </script>
                        ';
                    }
                    // elseif($imageFive != '')
                    elseif($imageFive == '')
                    {
                        $imageURL = './productImage/'.$row["image_one"];
                        $productListHtml .= '
                        <div class="shadow-white-box four-box-size opacity-hover pointer pointer-div" id="a'.$product->getUid().'2" onclick="myFunctiona'.$product->getUid().'()">
                            <div class="square">
                                <div class="width100 white-bg content progressive">
                                    <a>
                                        <img src="'.$imageURL.'" class="preview width100 two-border-radius opacity-hover pointer lazy" alt="'.$product->getName().'" title="'.$product->getName().'" />
                                    </a>                     
                                </div>
                            </div>
                            <p align="center" class="width100 text-overflow slider-product-name">'.$product->getName().'</p>
                            <p class="slider-product-name  text-overflow">RM '.$product->getPrice().'</p>
                        </div>  
                        <div class="fake-page-div product-details-all-div" style="display:none;" id="a'.$product->getUid().'">
                            <div class="big-content-div">
                                <div class="left-image-div">
									<div class="slider">        
                                         <div class="slides">
                                            <img src="'.$imageURL.'"  class="slides-img"  id="s'.$product->getUid().'1">
											<img src="./productImage/'.$product->getImageTwo().'"   class="slides-img" id="s'.$product->getUid().'2">
											<img src="./productImage/'.$product->getImageThree().'"   class="slides-img" id="s'.$product->getUid().'3">
											 <img src="./productImage/'.$product->getImageFour().'"   class="slides-img" id="s'.$product->getUid().'4">
                                            <iframe src="https://player.vimeo.com/video/'.$product->getLink().'" id="v'.$product->getUid().'" class="video-iframe slides-img" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                                
                                        </div>
										<div class="a-container">  
                                            <a href="#s'.$product->getUid().'1" class="small-a"><img src="'.$imageURL.'" class="link-small-img" ></a>
											 <a href="#s'.$product->getUid().'2" class="small-a"><img src="./productImage/'.$product->getImageTwo().'" class="link-small-img" ></a>
											 <a href="#s'.$product->getUid().'3" class="small-a"><img src="./productImage/'.$product->getImageThree().'" class="link-small-img" ></a>
											 <a href="#s'.$product->getUid().'4" class="small-a"><img src="./productImage/'.$product->getImageFour().'" class="link-small-img" ></a>
                                            <a href="#v'.$product->getUid().'" class="small-a"><img src="img/video.jpg" class="link-small-img" ></a>
										</div>
                                    </div>  
                                </div>								
								
													
                                <div class="right-content-div2">
                                    <h1 class="green-text pet-name">'.$product->getName().'</h1>
                                    <p class="price-p2 ow-font-weight400">RM '.$product->getPrice().'</p>
                                    <div class="clear"></div>
                                    <div class="pet-details-div">
                                        <p class="pet-table-p">'.$product->getDescription().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionTwo().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionThree().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFour().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFive().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionSix().'</p>
                                        <input type="hidden" value="'.$product->getId().'" name="product-list-id-input['.$index.']">
                                    </div>
                                    <a href="productReview.php?id='.$product->getUid().'" target="_blank">
                                        <div class="view-review-div">
                                            <p class="light-green-a2 left-review-text">View Reviews</p>
                                            <p class="right-arrow2">></p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="input-quantity-div">
                                <p class="quantity-p2">Quantity:<br></p>
                                <div class="input-group center-div">
                                    <button type="button" class="product-button-minus button-minus math-button math-button2 clean opacity-hover">-</button>
                                    <input readonly type="number" step="1" max="" value="'.$quantity.'" name="product-list-quantity-input['.$index.']" class="quantity-field math-input clean text-center value-input-div">
                                    <button type="button" class="product-button-plus button-plus math-button math-button2 opacity-hover clean">+</button>
                                </div>
                                <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>	
                            </div>
                            <div class="clear"></div>
                            <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div x-back" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>
                            <div class="clear"></div>
                            <div class="bottom-height1"></div>	
                        </div> 
                        <script>
                            function myFunctiona'.$product->getUid().'()
                            {
                                var x = document.getElementById("a'.$product->getUid().'");
                                if (x.style.display === "none")
                                {
                                    x.style.display = "block";
                                } 
                                else
                                {
                                    x.style.display = "none";
                                }
                            }
                        </script>
                        ';
                    }
                    // elseif($imageSix != '')
                    elseif($imageSix == '')
                    {
                        $imageURL = './productImage/'.$row["image_one"];
                        $productListHtml .= '
                        <div class="shadow-white-box four-box-size opacity-hover pointer pointer-div" id="a'.$product->getUid().'2" onclick="myFunctiona'.$product->getUid().'()">
                            <div class="square">
                                <div class="width100 white-bg content progressive">
                                    <a>
                                        <img src="'.$imageURL.'" class="preview width100 two-border-radius opacity-hover pointer lazy" alt="'.$product->getName().'" title="'.$product->getName().'" />
                                    </a>                     
                                </div>
                            </div>
                            <p align="center" class="width100 text-overflow slider-product-name">'.$product->getName().'</p>
                            <p class="slider-product-name  text-overflow">RM '.$product->getPrice().'</p>
                        </div>  
                        <div class="fake-page-div product-details-all-div" style="display:none;" id="a'.$product->getUid().'">
                            <div class="big-content-div">
                                <div class="left-image-div">
									<div class="slider">        
                                         <div class="slides">
                                            <img src="'.$imageURL.'"  class="slides-img"  id="s'.$product->getUid().'1">
											<img src="./productImage/'.$product->getImageTwo().'"   class="slides-img" id="s'.$product->getUid().'2">
											<img src="./productImage/'.$product->getImageThree().'"   class="slides-img" id="s'.$product->getUid().'3">
											 <img src="./productImage/'.$product->getImageFour().'"   class="slides-img" id="s'.$product->getUid().'4">
											 <img src="./productImage/'.$product->getImageFive().'"   class="slides-img" id="s'.$product->getUid().'5">
                                            <iframe src="https://player.vimeo.com/video/'.$product->getLink().'" id="v'.$product->getUid().'" class="video-iframe slides-img" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                                
                                        </div>
										<div class="a-container">  
                                            <a href="#s'.$product->getUid().'1" class="small-a"><img src="'.$imageURL.'" class="link-small-img" ></a>
											 <a href="#s'.$product->getUid().'2" class="small-a"><img src="./productImage/'.$product->getImageTwo().'" class="link-small-img" ></a>
											 <a href="#s'.$product->getUid().'3" class="small-a"><img src="./productImage/'.$product->getImageThree().'" class="link-small-img" ></a>
											 <a href="#s'.$product->getUid().'4" class="small-a"><img src="./productImage/'.$product->getImageFour().'" class="link-small-img" ></a>
											 <a href="#s'.$product->getUid().'5" class="small-a"><img src="./productImage/'.$product->getImageFive().'" class="link-small-img" ></a>
                                            <a href="#v'.$product->getUid().'" class="small-a"><img src="img/video.jpg" class="link-small-img" ></a>
										</div>
                                    </div>  
                                </div>											
								
								
													
                                <div class="right-content-div2">
                                    <h1 class="green-text pet-name">'.$product->getName().'</h1>
                                    <p class="price-p2 ow-font-weight400">RM '.$product->getPrice().'</p>
                                    <div class="clear"></div>
                                    <div class="pet-details-div">
                                        <p class="pet-table-p">'.$product->getDescription().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionTwo().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionThree().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFour().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFive().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionSix().'</p>
                                        <input type="hidden" value="'.$product->getId().'" name="product-list-id-input['.$index.']">
                                    </div>
                                    <a href="productReview.php?id='.$product->getUid().'" target="_blank">
                                        <div class="view-review-div">
                                            <p class="light-green-a2 left-review-text">View Reviews</p>
                                            <p class="right-arrow2">></p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="input-quantity-div">
                                <p class="quantity-p2">Quantity:<br></p>
                                <div class="input-group center-div">
                                    <button type="button" class="product-button-minus button-minus math-button math-button2 clean opacity-hover">-</button>
                                    <input readonly type="number" step="1" max="" value="'.$quantity.'" name="product-list-quantity-input['.$index.']" class="quantity-field math-input clean text-center value-input-div">
                                    <button type="button" class="product-button-plus button-plus math-button math-button2 opacity-hover clean">+</button>
                                </div>
                                <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>	
                            </div>
                            <div class="clear"></div>
                            <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div x-back" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>
                            <div class="clear"></div>
                            <div class="bottom-height1"></div>	
                        </div> 
                        <script>
                            function myFunctiona'.$product->getUid().'()
                            {
                                var x = document.getElementById("a'.$product->getUid().'");
                                if (x.style.display === "none")
                                {
                                    x.style.display = "block";
                                } 
                                else
                                {
                                    x.style.display = "none";
                                }
                            }
                        </script>
                        ';
                    }
                    else
                    {
                        $imageURL = './productImage/'.$row["image_one"];
                        $productListHtml .= '
                        <div class="shadow-white-box four-box-size opacity-hover pointer pointer-div" id="a'.$product->getUid().'2" onclick="myFunctiona'.$product->getUid().'()">
                            <div class="square">
                                <div class="width100 white-bg content progressive">
                                    <a>
                                        <img src="'.$imageURL.'" class="preview width100 two-border-radius opacity-hover pointer lazy" alt="'.$product->getName().'" title="'.$product->getName().'" />
                                    </a>                     
                                </div>
                            </div>
                            <p align="center" class="width100 text-overflow slider-product-name">'.$product->getName().'</p>
                            <p class="slider-product-name  text-overflow">RM '.$product->getPrice().'</p>
                        </div>  
                        <div class="fake-page-div product-details-all-div" style="display:none;" id="a'.$product->getUid().'">
                            <div class="big-content-div">
                                <div class="left-image-div">
									<div class="slider">        
                                         <div class="slides">
                                            <img src="'.$imageURL.'"  class="slides-img"  id="s'.$product->getUid().'1">
											<img src="./productImage/'.$product->getImageTwo().'"   class="slides-img" id="s'.$product->getUid().'2">
											<img src="./productImage/'.$product->getImageThree().'"   class="slides-img" id="s'.$product->getUid().'3">
											 <img src="./productImage/'.$product->getImageFour().'"   class="slides-img" id="s'.$product->getUid().'4">
											 <img src="./productImage/'.$product->getImageFive().'"   class="slides-img" id="s'.$product->getUid().'5">
											 <img src="./productImage/'.$product->getImageSix().'"   class="slides-img" id="s'.$product->getUid().'6">
                                            <iframe src="https://player.vimeo.com/video/'.$product->getLink().'" id="v'.$product->getUid().'" class="video-iframe slides-img" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                                
                                        </div>
										<div class="a-container">  
                                            <a href="#s'.$product->getUid().'1" class="small-a"><img src="'.$imageURL.'" class="link-small-img" ></a>
											 <a href="#s'.$product->getUid().'2" class="small-a"><img src="./productImage/'.$product->getImageTwo().'" class="link-small-img" ></a>
											 <a href="#s'.$product->getUid().'3" class="small-a"><img src="./productImage/'.$product->getImageThree().'" class="link-small-img" ></a>
											 <a href="#s'.$product->getUid().'4" class="small-a"><img src="./productImage/'.$product->getImageFour().'" class="link-small-img" ></a>
											 <a href="#s'.$product->getUid().'5" class="small-a"><img src="./productImage/'.$product->getImageFive().'" class="link-small-img" ></a>
											 <a href="#s'.$product->getUid().'6" class="small-a"><img src="./productImage/'.$product->getImageSix().'" class="link-small-img" ></a>
                                            <a href="#v'.$product->getUid().'" class="small-a"><img src="img/video.jpg" class="link-small-img" ></a>
										</div>
                                    </div>  
                                </div>											
								
								
								
								
													
                                <div class="right-content-div2">
                                    <h1 class="green-text pet-name">'.$product->getName().'</h1>
                                    <p class="price-p2 ow-font-weight400">RM '.$product->getPrice().'</p>
                                    <div class="clear"></div>
                                    <div class="pet-details-div">
                                        <p class="pet-table-p">'.$product->getDescription().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionTwo().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionThree().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFour().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionFive().'</p>
                                        <p class="pet-table-p">'.$product->getDescriptionSix().'</p>
                                        <input type="hidden" value="'.$product->getId().'" name="product-list-id-input['.$index.']">
                                    </div>
                                    <a href="productReview.php?id='.$product->getUid().'" target="_blank">
                                        <div class="view-review-div">
                                            <p class="light-green-a2 left-review-text">View Reviews</p>
                                            <p class="right-arrow2">></p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="input-quantity-div">
                                <p class="quantity-p2">Quantity:<br></p>
                                <div class="input-group center-div">
                                    <button type="button" class="product-button-minus button-minus math-button math-button2 clean opacity-hover">-</button>
                                    <input readonly type="number" step="1" max="" value="'.$quantity.'" name="product-list-quantity-input['.$index.']" class="quantity-field math-input clean text-center value-input-div">
                                    <button type="button" class="product-button-plus button-plus math-button math-button2 opacity-hover clean">+</button>
                                </div>
                                <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>	
                            </div>
                            <div class="clear"></div>
                            <div class="clean black-button add-to-cart-btn green-button checkout-btn close-fake purchase-btn center-div x-back" onclick="myFunctiona'.$product->getUid().'()">Go Back</div>
                            <div class="clear"></div>
                            <div class="bottom-height1"></div>	
                        </div> 
                        <script>
                            function myFunctiona'.$product->getUid().'()
                            {
                                var x = document.getElementById("a'.$product->getUid().'");
                                if (x.style.display === "none")
                                {
                                    x.style.display = "block";
                                } 
                                else
                                {
                                    x.style.display = "none";
                                }
                            }
                        </script>
                        ';
                    }
    
                }

                
            }
        }
        $index++;
    }

    $productListHtml .= '
    <script>
        $(".button-minus").on("click", function(e)
        {
            e.preventDefault();
            var $this = $(this);
            var $input = $this.closest("div").find("input");
            var value = parseInt($input.val());
            if (value > 1)
            {
                value = value - 1;
            } 
            else 
            {
                value = 0;
            }
            $input.val(value);
        });

        $(".button-plus").on("click", function(e)
        {
        e.preventDefault();
        var $this = $(this);
        var $input = $this.closest("div").find("input");
        var value = parseInt($input.val());
        if (value < 100)
        {
            value = value + 1;
        }
        else
        {
            value = 100;
        }
        $input.val(value);
        });
    </script>
    ';

    return $productListHtml;
}

function getProductPrice($conn,$productId){
    $price = 0;

    $productRows = getProduct($conn," WHERE id = ? ",array("id"),array($productId),"i");

    if($productRows){
        $price = $productRows[0]->getDiamond();
    }

    return $price;
}

function addToCart(){
    $shoppingCart = array();

    $totalProductCount = count($_POST['product-list-id-input']);
    for($i = 0; $i < $totalProductCount; $i++){
        $productId = $_POST['product-list-id-input'][$i];
        $quantity = $_POST['product-list-quantity-input'][$i];

    //    if($quantity > 0){
        $thisOrder = array();
        $thisOrder['productId'] = $productId;
        $thisOrder['quantity'] = $quantity;
        array_push($shoppingCart,$thisOrder);
    //    }
    }

    if(count($shoppingCart) > 0) {
        $_SESSION['shoppingCart'] = $shoppingCart;
    }

}

function clearCart(){
    unset ($_SESSION["shoppingCart"]);
}

//when checkout only create order
function createOrder($conn,$uid){
    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'] && count($_SESSION['shoppingCart']) > 0){
        $shoppingCart = $_SESSION['shoppingCart'];

        $order_Id = md5(uniqid());

        $orderId = insertDynamicData($conn,"orders",array("uid","order_id"),array($uid,$order_Id),"ss");
        //$orderId = insertDynamicData($conn,"orders",array("uid","username","full_name"),array($uid,$username,$fullName),"sss");

        if($orderId){
            $totalPrice = 0;

            for($index = 0; $index < count($shoppingCart); $index++){
                $thisCart = $shoppingCart[$index];
                $productId = $thisCart['productId'];
                $quantity = $thisCart['quantity'];
                $originalPrice = getProductPrice($conn,$productId);
                $totalPrice += ($originalPrice * $quantity);

                if(!insertDynamicData($conn,"product_orders",array("product_id","order_id","quantity","final_price","original_price","discount_given"),
                    array($productId,$orderId,$quantity,$originalPrice,$originalPrice,0),"iiiddd")){
                    promptError("error creating order for product : $productId");
                }
            }

        }else{
            promptError("error creating order");
        }

    }
}

function getShoppingCart($conn,$cartType = 2,$isIncludeNotSelectedProductToo = false){
    $productListHtml = "";

    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
        $products = array();
        $quantities = array();
        for($index = 0; $index < count($_SESSION['shoppingCart']); $index++){
            $thisCart = $_SESSION['shoppingCart'][$index];

            $tempProductRows = getProduct($conn," WHERE id = ? ",array("id"),array($thisCart['productId']),"i");
            if($tempProductRows){
                array_push($products,$tempProductRows[0]);
                array_push($quantities,$thisCart['quantity']);
            }
        }

        if(count($products) > 0 && count($quantities) > 0 && count($products) == count($quantities)){
            $productListHtml = createProductList($products,$cartType,$quantities,$isIncludeNotSelectedProductToo);
        }
    }

    return $productListHtml;
}
