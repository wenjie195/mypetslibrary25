<?php
class OrderList {
    /* Member variables */
    // var $id,$userUid,$productUid,$productName,$orderId,$quantity,$originalPrice,$finalPrice,$discount,$totalPrice,$status,$dateCreated,$dateUpdated;
    var $id,$userUid,$productUid,$productName,$mainProductUid,$orderId,$quantity,$originalPrice,$finalPrice,$discount,$totalPrice,$status,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserUid()
    {
        return $this->userUid;
    }

    /**
     * @param mixed $userUid
     */
    public function setUserUid($userUid)
    {
        $this->userUid = $userUid;
    }

    /**
     * @return mixed
     */
    public function getProductUid()
    {
        return $this->productUid;
    }

    /**
     * @param mixed $productUid
     */
    public function setProductUid($productUid)
    {
        $this->productUid = $productUid;
    }

    /**
     * @return mixed
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * @param mixed $productName
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;
    }

    /**
     * @return mixed
     */
    public function getMainProductUid()
    {
        return $this->mainProductUid;
    }

    /**
     * @param mixed $mainProductUid
     */
    public function setMainProductUid($mainProductUid)
    {
        $this->mainProductUid = $mainProductUid;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param mixed $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getOriginalPrice()
    {
        return $this->originalPrice;
    }

    /**
     * @param mixed $originalPrice
     */
    public function setOriginalPrice($originalPrice)
    {
        $this->originalPrice = $originalPrice;
    }

    /**
     * @return mixed
     */
    public function getFinalPrice()
    {
        return $this->finalPrice;
    }

    /**
     * @param mixed $finalPrice
     */
    public function setFinalPrice($finalPrice)
    {
        $this->finalPrice = $finalPrice;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return mixed
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * @param mixed $totalPrice
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;
    }

        /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getOrderList($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    // $dbColumnNames = array("id","user_uid","product_uid","product_name","order_id","quantity","original_price","final_price","discount","totalPrice","status",
    //                             "date_created","date_updated");
    $dbColumnNames = array("id","user_uid","product_uid","product_name","main_product_uid","order_id","quantity","original_price","final_price","discount","totalPrice",
                                "status","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"order_list");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        // $stmt->bind_result($id,$userUid,$productUid,$productName,$orderId,$quantity,$originalPrice,$finalPrice,$discount,$totalPrice,$status,$dateCreated,$dateUpdated);
        $stmt->bind_result($id,$userUid,$productUid,$productName,$mainProductUid,$orderId,$quantity,$originalPrice,$finalPrice,$discount,$totalPrice,$status,
                                $dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new OrderList();
            $class->setId($id);
            $class->setUserUid($userUid);
            $class->setProductUid($productUid);
            $class->setProductName($productName);
            $class->setMainProductUid($mainProductUid);
            $class->setOrderId($orderId);
            $class->setQuantity($quantity);
            $class->setOriginalPrice($originalPrice);
            $class->setFinalPrice($finalPrice);
            $class->setDiscount($discount);
            $class->setTotalPrice($totalPrice);
            $class->setStatus($status);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}