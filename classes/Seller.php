<?php  
class Seller {
    /* Member variables */
    var $id,$uid,$companyName,$slug,$registrationNo,$contactNo,$email,$address,$state,$country,$accountStatus,$featuredSeller,$contactPerson,$contactPersonNo,
            $secContactPerson,$secContactPersonNo,$experience,$cert,$services,$breedType,$otherInfo,$infoTwo,$infoThree,$infoFour,$companyLogo,$companyPic,$rating,
                $credit,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getRegistrationNo()
    {
        return $this->registrationNo;
    }

    /**
     * @param mixed $registrationNo
     */
    public function setRegistrationNo($registrationNo)
    {
        $this->registrationNo = $registrationNo;
    }

    /**
     * @return mixed
     */
    public function getContactNo()
    {
        return $this->contactNo;
    }

    /**
     * @param mixed $contactNo
     */
    public function setContactNo($contactNo)
    {
        $this->contactNo = $contactNo;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getAccountStatus()
    {
        return $this->accountStatus;
    }

    /**
     * @param mixed $accountStatus
     */
    public function setAccountStatus($accountStatus)
    {
        $this->accountStatus = $accountStatus;
    }

    /**
     * @return mixed
     */
    public function getFeaturedSeller()
    {
        return $this->featuredSeller;
    }

    /**
     * @param mixed $featuredSeller
     */
    public function setFeaturedSeller($featuredSeller)
    {
        $this->featuredSeller = $featuredSeller;
    }

    /**
     * @return mixed
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * @param mixed $contactPerson
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;
    }

    /**
     * @return mixed
     */
    public function getContactPersonNo()
    {
        return $this->contactPersonNo;
    }

    /**
     * @param mixed $contactPersonNo
     */
    public function setContactPersonNo($contactPersonNo)
    {
        $this->contactPersonNo = $contactPersonNo;
    }

    /**
     * @return mixed
     */
    public function getSecContactPerson()
    {
        return $this->secContactPerson;
    }

    /**
     * @param mixed $secContactPerson
     */
    public function setSecContactPerson($secContactPerson)
    {
        $this->secContactPerson = $secContactPerson;
    }

    /**
     * @return mixed
     */
    public function getSecContactPersonNo()
    {
        return $this->secContactPersonNo;
    }

    /**
     * @param mixed $secContactPersonNo
     */
    public function setSecContactPersonNo($secContactPersonNo)
    {
        $this->secContactPersonNo = $secContactPersonNo;
    }

    /**
     * @return mixed
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * @param mixed $experience
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;
    }

    /**
     * @return mixed
     */
    public function getCert()
    {
        return $this->cert;
    }

    /**
     * @param mixed $cert
     */
    public function setCert($cert)
    {
        $this->cert = $cert;
    }

    /**
     * @return mixed
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @param mixed $services
     */
    public function setServices($services)
    {
        $this->services = $services;
    }

     /**
     * @return mixed
     */
    public function getBreedType()
    {
        return $this->breedType;
    }

    /**
     * @param mixed $breedType
     */
    public function setBreedType($breedType)
    {
        $this->breedType = $breedType;
    }

    /**
     * @return mixed
     */
    public function getOtherInfo()
    {
        return $this->otherInfo;
    }

    /**
     * @param mixed $otherInfo
     */
    public function setOtherInfo($otherInfo)
    {
        $this->otherInfo = $otherInfo;
    }

    /**
     * @return mixed
     */
    public function getInfoTwo()
    {
        return $this->infoTwo;
    }

    /**
     * @param mixed $infoTwo
     */
    public function setInfoTwo($infoTwo)
    {
        $this->infoTwo = $infoTwo;
    }

    /**
     * @return mixed
     */
    public function getInfoThree()
    {
        return $this->infoThree;
    }

    /**
     * @param mixed $infoThree
     */
    public function setInfoThree($infoThree)
    {
        $this->infoThree = $infoThree;
    }

    /**
     * @return mixed
     */
    public function getInfoFour()
    {
        return $this->infoFour;
    }

    /**
     * @param mixed $infoFour
     */
    public function setInfoFour($infoFour)
    {
        $this->infoFour = $infoFour;
    }

    /**
     * @return mixed
     */
    public function getCompanyLogo()
    {
        return $this->companyLogo;
    }

    /**
     * @param mixed $companyLogo
     */
    public function setCompanyLogo($companyLogo)
    {
        $this->companyLogo = $companyLogo;
    }

    /**
     * @return mixed
     */
    public function getCompanyPic()
    {
        return $this->companyPic;
    }

    /**
     * @param mixed $companyPic
     */
    public function setCompanyPic($companyPic)
    {
        $this->companyPic = $companyPic;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return mixed
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * @param mixed $credit
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getSeller($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","company_name","slug","registration_no","contact_no","email","address","state","country","account_status","featured_seller",
        "contact_person","contact_person_no","sec_contact_person","sec_contact_person_no","experience","cert","services","breed_type","other_info","info_two",
            "info_three","info_four","company_logo","company_pic","rating","credit","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"seller"); //database name
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$companyName,$slug,$registrationNo,$contactNo,$email,$address,$state,$country,$accountStatus,$featuredSeller,$contactPerson,
                    $contactPersonNo,$secContactPerson,$secContactPersonNo,$experience,$cert,$services,$breedType,$otherInfo,$infoTwo,$infoThree,$infoFour,$companyLogo,
                        $companyPic,$rating,$credit,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $user = new Seller;
            $user->setId($id);
            $user->setUid($uid);
            $user->setCompanyName($companyName);
            $user->setSlug($slug);
            $user->setRegistrationNo($registrationNo);
            $user->setContactNo($contactNo);
            $user->setEmail($email);
            $user->setAddress($address);
            $user->setState($state);
            $user->setCountry($country);
            $user->setAccountStatus($accountStatus);
            $user->setFeaturedSeller($featuredSeller);
            $user->setContactPerson($contactPerson);
            $user->setContactPersonNo($contactPersonNo);
            $user->setSecContactPerson($secContactPerson);
            $user->setSecContactPersonNo($secContactPersonNo);
            $user->setExperience($experience);
            $user->setCert($cert);
            $user->setServices($services);
            $user->setBreedType($breedType);

            $user->setOtherInfo($otherInfo);
            $user->setInfoTwo($infoTwo);
            $user->setInfoThree($infoThree);
            $user->setInfoFour($infoFour);

            $user->setCompanyLogo($companyLogo);
            $user->setCompanyPic($companyPic);
            $user->setRating($rating);

            $user->setCredit($credit);

            $user->setDateCreated($dateCreated);
            $user->setDateUpdated($dateUpdated);
          
            array_push($resultRows,$user);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
