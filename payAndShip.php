<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Shipping.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
$orderUid = $_SESSION['order_uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

// $orderDetails = getOrders($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1 ",array("uid"),array($uid),"s");
// $orderDetails = getOrders($conn," WHERE order_id = ?  ",array("order_id"),array($orderUid),"s");
$orderDetails = getOrders($conn," WHERE order_id = ? ORDER BY date_created DESC LIMIT 1 ",array("order_id"),array($orderUid),"s");
$orderID = $orderDetails[0]->getId();
$orderUID = $orderDetails[0]->getOrderId();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Payment Details | Mypetslibrary" />
<title>Payment Details | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

  
    <div class="width100 same-padding  menu-distance">

    <div class="width100 overflow">
        <h1 class="green-text payment-title text-center-important">Payment Details</h1>
    </div>

    <form method="post" action="billplzpost.php">

        <div class="width100 overflow"> 

        <div class="width100  text-center-important">
            <p class="input-top-p ow-input-top-p">Name</p>
            <input class="input-name clean no-input" type="text" id="name" name="name" value="<?php echo $userDetails[0]->getName();?>" readonly>
        </div>
		<div class="clear"></div>
        <div class="width100  text-center-important">
            <p class="input-top-p ow-input-top-p">Phone</p>
            <input class="input-name clean no-input" type="text" id="mobile" name="mobile" value="<?php echo $orderDetails[0]->getContactNo();?>" readonly> 
        </div>

        <div class="clear"></div>

        <div class="width100  text-center-important">
            <p class="input-top-p ow-input-top-p">Amount</p>
            <input class="input-name clean no-input" type="text" value="RM <?php echo $orderDetails[0]->getSubtotal();?>" readonly> 
        </div>

        <div class="clear"></div>

        <?php  
            $subtotal = $orderDetails[0]->getSubtotal();
            $adjustTotal = ($subtotal * 100);
        ?>

        <!-- <input class="input-name clean" type="hidden" id="email" name="email" value="abc@gmail.com" readonly>  -->
        <input class="input-name clean" type="hidden" id="email" name="email"> 
        <input class="input-name clean" type="hidden" id="amount" name="amount" value="<?php echo $adjustTotal; ?>" readonly> 
        <input class="input-name clean" type="hidden" id="reference_1_label" name="reference_1_label" value="Order UID" readonly> 
        <input class="input-name clean" type="hidden" id="reference_1" name="reference_1" value="<?php echo $orderUID; ?>" readonly>
        <input class="input-name clean" type="hidden" id="reference_2_label" name="reference_2_label" value="Order ID" readonly> 
        <input class="input-name clean" type="hidden" id="reference_2" name="reference_2" value="<?php echo $orderID; ?>" readonly> 

        <div class="clear"></div> 

        <div class="width100 text-center extra-spacing-up-down">
            <button class="green-button checkout-btn clean" name="submit">Proceed To Payment</button>
        </div> 

        <div class="clear"></div> 

        </div> 
                

    </form>

    </div>
</div>



<style>
	.animated.slideUp{
		animation:none !important;}
	.animated{
		animation:none !important;}
	.green-footer{
		display:none;}
</style>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>
</body>
</html>