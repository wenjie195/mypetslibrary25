<?php
// DB SQL Connection
function connDB(){
    // Create connection
    // $conn = new mysqli("localhost", "root", "","vidatech_mypetslib");//for localhost
    $conn = new mysqli("localhost", "root", "","vidatech_mypetslib2");//for localhost
    // $conn = new mysqli("localhost", "qlianmen_mpluser", "DID89XFHfqrU","qlianmen_mypetslibrary");//for qlianmeng server
    // $conn = new mysqli("localhost", "vidatech_mpluser", "rjJ1qEVrshCp","vidatech_mypetlibrary");//for vidatech server
    // $conn = new mysqli("localhost", "ichibang_mpluser", "62lnqb914ETf","ichibang_mypetlib");//for ichibangame server
    // $conn = new mysqli("localhost", "ichibang_mpuser2", "R5XOUFuAOjYP","ichibang_mypets");//for ichibangame server merge
    // $conn = new mysqli("localhost", "qlianmen_mpuser", "KlsF17fUuRk6","qlianmen_mypets");//for qlianmeng server

    mysqli_set_charset($conn,'UTF8');//because cant show chinese characters so need to include this to show
    //for when u insert chinese characters inside, because if dont include this then it will become unknown characters in the database
    mysqli_query($conn,"SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'")or die(mysqli_error($conn));

// Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    return $conn;
}
