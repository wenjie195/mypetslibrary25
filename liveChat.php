<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Variation.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $messageDetails = getMessage($conn," ORDER BY date_created DESC LIMIT 50 ");
$messageDetails = getMessage($conn," WHERE uid = ? ORDER BY date_created ASC LIMIT 50 ",array("uid"),array($uid),"s");

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Chat | Mypetslibrary" />
<title>Chat | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>
<body class="body">

<?php include 'header.php'; ?>
<div class="width100 small-padding overflow min-height menu-distance2">

    <h2 class="h1-title left-title">Chat With Mypetslibrary</h2> 
	
    <div class="clear"></div>

	<div class="chat-section">
    	<div id="divLiveMessage"></div>
	</div>

    <?php
    if(isset($_GET['id']))
    {
    $conn = connDB();
    // echo $_GET['id'];
    // $productDetails = getProduct($conn,"WHERE uid = ? AND status = 'Available' ", array("uid") ,array($_GET['id']),"s");
    // $productVariation = getVariation($conn,"WHERE product_uid = ? AND status = 'Available' ", array("product_uid") ,array($_GET['id']),"s");
    ?>

            <form action="utilities/sentMessageFunction.php" method="POST">
                <div class="width100 bottom-send-div">
                    <input class="send-message clean"  type="text" placeholder="Your Message" id="message_details" name="message_details" required>  
                    <input class="send-message clean"  type="hidden" value="<?php echo $_GET['id'];?>" id="product_name" name="product_name" readonly>  
                    <button class="clean send-button" type="submit" name="submit">
                        SENT
                    </button>
                </div>
            </form>

    <?php
    }
    else
    {
    ?>
    
    <form action="utilities/sentMessageFunction.php" method="POST">
        <div class="width100 bottom-send-div">
            <input class="send-message clean"  type="text" placeholder="Your Message" id="message_details" name="message_details" required>  
        
            <button class="clean send-button" type="submit" name="submit">
                SENT
            </button>
        </div>
    </form>
    
    <?php
    }
    ?>

    <!-- <form action="utilities/sentMessageFunction.php" method="POST">
        <div class="width100 bottom-send-div">
            <input class="send-message clean"  type="text" placeholder="Your Message" id="message_details" name="message_details" required>  
        
            <button class="clean send-button" type="submit" name="submit">
                SENT
            </button>
        </div>
    </form> -->

</div>
<div class="clear"></div>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>
<script type="text/javascript">
    $(document).ready(function()
    {
        $("#divLiveMessage").load("liveMessage.php");
    setInterval(function()
    {
        $("#divLiveMessage").load("liveMessage.php");
    }, 5000);
    });
</script>

</body>
</html>