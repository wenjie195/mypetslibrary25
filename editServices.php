<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Services.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Services | Mypetslibrary" />
<title>Edit Services | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
        <h1 class="green-text h1-title">Edit Services</h1>
        <div class="green-border"></div>
   </div>

   <div class="border-separation">
        <div class="clear"></div>

        <form method="POST" action="utilities/editServicesFunction.php">

            <?php
            if(isset($_POST['services_id']))
            {
                $conn = connDB();
                $servicesDetails = getServices($conn,"WHERE id = ? ", array("id") ,array($_POST['services_id']),"i");
            ?>
                <div class="width100">
                    <p class="input-top-p admin-top-p">Services*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $servicesDetails[0]->getServiceType();?>" name="update_services" id="update_services" required>       
                </div>
                
                <div class="clear"></div>

                <input type="hidden" value="<?php echo $servicesDetails[0]->getId();?>" name="services_id" id="services_id" readonly>   

            <?php
            }
            ?>
    	
            <div class="clear"></div>  

            <div class="width100 overflow text-center">     
                <button class="green-button white-text clean2 edit-1-btn margin-auto" type="submit" id ="submit" name ="submit">Submit</button>
            </div>

        </form>

	</div>
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>