<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $messageDetails = getMessage($conn, " WHERE admin_status = 'GET' ORDER BY date_created DESC  ");
// $messageDetails = getUser($conn);
// $messageDetails = getUser($conn, " WHERE message != ' ' ");
$messageDetails = getUser($conn, " WHERE message = 'YES' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Message | Mypetslibrary" />
<title>Message | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance" id="myTable">
	<div class="width100">
        <div class="width100 ship-top-div">
            <h1 class="green-text h1-title">New Messages | <a href="adminMessageAll.php" class="green-a">All Message</a> </h1>
            <div class="green-border"></div>
        </div>
    </div>

    <div class="clear"></div>

	<div class="width100 scroll-div border-separation">
    	<table class="green-table width100">
        	<thead>
            	<tr>
                	<th class="first-column">No.</th>
                    <th>Name</th>
                    <th>Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($messageDetails)
                    {
                        
                        for($cnt = 0;$cnt < count($messageDetails) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $messageDetails[$cnt]->getName();?></td>
                                <td><?php echo $messageDetails[$cnt]->getDateCreated();?></td>
                                <td>
                                    <!-- <form method="POST" action="adminMessageView.php" class="hover1"> -->
                                    <!-- <a href="adminMessageView.php"> -->
                                    <a href='adminMessageView.php?id=<?php echo $messageDetails[$cnt]->getUid();?>'>
                                        <button class="clean hover1 transparent-button pointer" type="submit" name="message_uid" value="<?php echo $messageDetails[$cnt]->getUid();?>">
                                             <img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Reply" title="Reply">
                                        <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Reply" title="Reply">
                                        </button>
                                    </a>
                                    <!-- </form> -->
                                </td>
                            </tr>
                        <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>
<div class="clear"></div>

<?php include 'js.php'; ?>
<?php unset($_SESSION['url']);?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "User Deleted !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to delete user !! ";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>