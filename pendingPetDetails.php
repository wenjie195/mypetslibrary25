<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Pets.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $userAmount = getUser($conn," WHERE user_type = 1 ");

$pendingPets = getPetsDetails($conn," WHERE status = 'Pending' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pending Pet Details | Mypetslibrary" />
<title>Pending Pet Details | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">

    <div class="width100">
    <!-- Please Add Pet Type: Puppy/Kitten/Reptile-->
    <!--<h1 class="green-text h1-title">Pending Pet Details - Puppy</h1>-->
        <h1 class="green-text h1-title">Pending Pet Details</h1>
        <div class="green-border"></div>
    </div>

    <div class="border-separation">
        <?php
        if(isset($_POST['pets_uid']))
        {
        $conn = connDB();
        $petsData = getPetsDetails($conn,"WHERE uid = ? ", array("uid") ,array($_POST['pets_uid']),"s");
        $petsDetails = $petsData[0];
        ?>

        <div class="clear"></div>
        <!-- <form> -->
        <!-- <form action="utilities/registerPuppyFunction.php" method="POST" enctype="multipart/form-data"> -->

        <form method="POST" id="petsVerificationForm" onsubmit="doPreview(this.submited); return false;">

        <div class="dual-input">
            <p class="input-top-p admin-top-p">Pet Name*</p>
            <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $petsDetails->getName();?>" readonly>   
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p admin-top-p">SKU*</p>
            <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $petsDetails->getSKU();?>" readonly>     
        </div>       

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p admin-top-p slug-p">Pet Slug (or URL, can't repeat,  Avoid Spacing and Symbol Specially"',.) Can Use - <img src="img/attention2.png" class="attention-png opacity-hover open-url" alt="Click Me!" title="Click Me!"></p>
            <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $petsDetails->getSlug();?>" readonly>    
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p admin-top-p slug-p">Pet Price (RM)*</p>
            <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $petsDetails->getPrice();?>" readonly>    
        </div> 

        <div class="clear"></div>  

        <div class="dual-input">
            <p class="input-top-p admin-top-p">Pet Age*</p>
            <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $petsDetails->getAge();?>" readonly>     
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p admin-top-p">Vaccinated Status*</p>
            <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $petsDetails->getVaccinated();?>" readonly>   
        </div>         

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p admin-top-p">Dewormed Status*</p>
            <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $petsDetails->getDewormed();?>" readonly>        
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p admin-top-p">Gender*</p>
            <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $petsDetails->getGender();?>" readonly>   
        </div>  

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p admin-top-p">Pet Color*</p>
            <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $petsDetails->getColor();?>" readonly>      
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p admin-top-p">Pet Size</p>
            <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $petsDetails->getSize();?>" readonly>    
        </div>  

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p admin-top-p">Status*</p>
            <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $petsDetails->getStatus();?>" readonly>       
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p admin-top-p">Feature</p>
            <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $petsDetails->getFeature();?>" readonly>  
        </div>        

        <div class="clear"></div>       
        <div class="dual-input">
            <p class="input-top-p admin-top-p">Pet Breed*</p>
            <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $petsDetails->getBreed();?>" readonly>      
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p admin-top-p">Seller*</p>
            <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $petsDetails->getSeller();?>" readonly>  
        </div>         

        <div class="clear"></div>

        <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Details (Avoid "'')</p>
            <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $petsDetails->getDetails();?>" readonly>    
        </div>

        <div class="clear"></div>
        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Google Search Keyword  (Use Coma , to Separate Each Keyword, Avoid"')</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="dog, pet, cute, for sale, Husky, Penang," name="" id="">
        </div>
        <div class="clear"></div>   
        <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Video Link*</p>
            <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $petsDetails->getLink();?>" readonly>     
        </div>

        <input class="input-name clean input-textarea admin-input" type="hidden" value="<?php echo $petsDetails->getUid();?>" name="pets_uid" id="pets_uid" readonly>    

        <input class="input-name clean input-textarea admin-input" type="hidden" value="<?php echo $petsDetails->getType();?>" name="pets_type" id="pets_type" readonly>    

        <div class="clear"></div>  

        <div class="four-div-box1">
        <img src="uploads/<?php echo $petsDetails->getImageOne();?>" class="pet-photo-preview">
        <!-- <p><input class="upload-file" type="file"></p> -->
        </div>
        <div class="four-div-box1 left-four-div1">
        <img src="uploads/<?php echo $petsDetails->getImageTwo();?>" class="pet-photo-preview">
        <!-- <p><input class="upload-file" type="file"></p> -->
        </div>
        <div class="four-div-box1 right-four-div1">               
        <img src="uploads/<?php echo $petsDetails->getImageThree();?>" class="pet-photo-preview">
        <!-- <p><input class="upload-file" type="file"></p> -->
        </div>                    
        <div class="four-div-box1">
        <img src="uploads/<?php echo $petsDetails->getImageFour();?>" class="pet-photo-preview">
        <!-- <p><input class="upload-file" type="file"></p> -->
        </div>

        <div class="clear"></div>

        <!-- <div class="width100 overflow text-center">     
        <button class="green-button white-text clean2 edit-1-btn margin-auto">Approve</button>
        <br>
        <button class="red-btn white-text clean2 edit-1-btn margin-auto bottom-reject">Reject</button>
        </div> -->

        <div class="width100 overflow text-center">   
            <input onclick="this.form.submited=this.value;"  type="submit" name="Approve" value="Approve" class="green-button white-text clean2 edit-1-btn margin-auto">
            <br>
            <input onclick="this.form.submited=this.value;"  type="submit" name="Reject" value="Reject" class="red-btn white-text clean2 edit-1-btn margin-auto bottom-reject">
        </div>

        </form>

        <?php
        }
        ?>
    </div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<?php

if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Pets as Available To Sell"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update on Pet Details table !!";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Error 1 !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Fail to update on Puppy table !!";
        } 
        else if($_GET['type'] == 5)
        {
            $messageType = "Error 2 !";
        } 
        else if($_GET['type'] == 6)
        {
            $messageType = "Update Pets as Rejected"; 
        } 
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script type="text/javascript">
    function doPreview(buttonType)
    {
        switch(buttonType){
            case 'Approve':
                form=document.getElementById('petsVerificationForm');
                // form.action='shippingRefund.php';
                form.action='utilities/adminApprovePets.php';
                form.submit();
            break;
            case 'Reject':
                form=document.getElementById('petsVerificationForm');
                form.action='utilities/adminRejectPets.php';
                form.submit();
            break;
        }

    }
</script>

</body>
</html>