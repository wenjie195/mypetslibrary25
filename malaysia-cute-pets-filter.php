<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Breed.php';
require_once dirname(__FILE__) . '/classes/Color.php';
require_once dirname(__FILE__) . '/classes/Kitten.php';
require_once dirname(__FILE__) . '/classes/Pets.php';
require_once dirname(__FILE__) . '/classes/Puppy.php';
require_once dirname(__FILE__) . '/classes/Reptile.php';
require_once dirname(__FILE__) . '/classes/States.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $breedDetails = getBreed($conn," WHERE type = ? ",array("type"),array(1),"s");
// $colorDetails = getColor($conn," WHERE type = ? ",array("type"),array(1),"s");

$breedDetails = getBreed($conn);
$colorDetails = getColor($conn);
$states = getStates($conn);

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
      echo $petType = rewrite($_POST["pet_type"]);
      echo $breedType = rewrite($_POST["breed_type"]);
      echo $colorType = rewrite($_POST["color_type"]);
      echo $gender = rewrite($_POST["gender"]);
      echo $location = rewrite($_POST["state"]);

      echo $priceRange = rewrite($_POST["price_range"]);
      echo $priceValue = $_POST["price_value"];

      // echo $amountOne = rewrite($_POST["amount1"]);
      // echo $amountTwo = rewrite($_POST["amount2"]);


      if($priceRange == 'low')
      // if($priceRange != "")
      {

            if($petType == 'Puppy')
            {
                  // $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND breed = '$breedType' AND color = '$colorType' AND gender = '$gender' AND location = '$location'
                  //       AND status = 'Available' ORDER BY date_created DESC ");

                  $petsDet = getPetsDetails($conn, "WHERE type = '.$petType.' AND breed = '.$breedType.' AND color = '.$colorType.' AND gender = '.$gender.' AND location = '.$location.'
                        AND status = 'Available' AND price >=  0 AND price <= $priceValue ORDER BY date_created DESC ");

                  // $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND price <= 2000 ORDER BY date_created DESC ");

            }
            elseif($petType == 'Kitten')
            {
                  $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND breed = '$breedType' AND color = '$colorType' AND gender = '$gender' AND location = '$location'
                        AND status = 'Available' AND price <= '$priceValue' ORDER BY date_created DESC ");
            }
            elseif($petType == 'Reptile')
            {
                  $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND breed = '$breedType' AND color = '$colorType' AND gender = '$gender' AND location = '$location'
                        AND  status = 'Available' AND price <= '$priceValue' ORDER BY date_created DESC ");
            }

      }
      elseif($priceRange == 'higher')
      // else
      {

            if($petType == 'Puppy')
            {
                  $petsDet = getPetsDetails($conn, "WHERE type = '.$petType.' AND breed = '.$breedType.' AND color = '.$colorType.' AND gender = '.$gender.' AND location = '.$location.'
                        AND status = 'Available' AND price >= $priceValue ORDER BY date_created DESC ");

                  // $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND price >= 2000 ORDER BY date_created DESC ");

            }
            elseif($petType == 'Kitten')
            {
                  $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND breed = '$breedType' AND color = '$colorType' AND gender = '$gender' AND location = '$location'
                        AND status = 'Available' AND price >= '$priceValue' ORDER BY date_created DESC ");
            }
            elseif($petType == 'Reptile')
            {
                  $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND breed = '$breedType' AND color = '$colorType' AND gender = '$gender' AND location = '$location'
                        AND  status = 'Available' AND price >= '$priceValue' ORDER BY date_created DESC ");
            }

      }


      // if($petType == 'Puppy')
      // {
      //       // $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND breed = '$breedType' AND color = '$colorType' AND gender = '$gender' AND location = '$location'
      //       //       AND price between '100' AND '5000' AND status = 'Available' ORDER BY date_created DESC ");
      //       $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND breed = '$breedType' AND color = '$colorType' AND gender = '$gender' AND location = '$location'
      //             AND status = 'Available' ORDER BY date_created DESC ");
      // }
      // elseif($petType == 'Kitten')
      // {
      //       $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND breed = '$breedType' AND color = '$colorType' AND gender = '$gender' AND location = '$location'
      //             AND status = 'Available' ORDER BY date_created DESC ");
      // }
      // elseif($petType == 'Reptile')
      // {
      //       $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND breed = '$breedType' AND color = '$colorType' AND gender = '$gender' AND location = '$location'
      //             AND  status = 'Available' ORDER BY date_created DESC ");
      // }

}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Filter On Sale in Malaysia | Mypetslibrary" />
<title>Filter On Sale in Malaysia | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

      <div class="fix-filter width100 small-padding overflow">
            <h1 class="green-text user-title left-align-title">Filter</h1>

            <div class="filter-div">
                  <form method="POST" action="malaysia-cute-pets-filter.php">
                        <input class="clean pop-input no-bg-input" type="hidden" value="<?php echo $petType?>" name="pet_type" id="pet_type" readonly>
                        <button class="transparent-button filter-btn clean opacity-hover" value="Male" name="search_data" id="search_data">
                              <img src="img/male.png" alt="Male" title="Male" class="filter-icon">
                        </button>
                  </form>
                  <form method="POST" action="malaysia-cute-pets-filter.php">
                        <input class="clean pop-input no-bg-input" type="hidden" value="<?php echo $petType?>" name="pet_type" id="pet_type" readonly>
                        <button class="transparent-button filter-btn clean opacity-hover" value="Female" name="search_data" id="search_data">
                              <img src="img/female.png" alt="Female" title="Female" class="filter-icon">
                        </button>
                  </form>
                  <form method="POST" action="malaysia-cute-pets-filter.php">
                        <input class="clean pop-input no-bg-input" type="hidden" value="<?php echo $petType?>" name="pet_type" id="pet_type" readonly>
                        <button class="transparent-button filter-btn clean opacity-hover" value="Both" name="search_data" id="search_data">
                              <img src="img/both-gender.png" alt="Both Gender" title="Both Gender" class="filter-icon">
                        </button>
                  </form>

                  <form method="POST" action="malaysia-cute-pets-filter.php">
                        <input class="clean pop-input no-bg-input" type="hidden" value="<?php echo $petType?>" name="pet_type" id="pet_type" readonly>
                        <button class="transparent-button filter-btn clean opacity-hover" value="PriceLTH" name="search_data" id="search_data">
                              <img src="img/price-low.png" alt="Start From The Lowest Price" title="Start From The Lowest Price" class="filter-icon">
                        </button>
                  </form>

                  <form method="POST" action="malaysia-cute-pets-filter.php">
                        <input class="clean pop-input no-bg-input" type="hidden" value="<?php echo $petType?>" name="pet_type" id="pet_type" readonly>
                        <button class="transparent-button filter-btn clean opacity-hover" value="PriceHTL" name="search_data" id="search_data">
                              <img src="img/price-high.png" alt="Start From The Highest Price" title="Start From The Highest Price" class="filter-icon">
                        </button>
                  </form>

                  <a class="open-filter filter-a green-a">Filter</a>
            </div>
      </div>

      <div class="clear"></div>

<div class="width100 small-padding overflow min-height-with-filter filter-distance">

	<div class="width103">

      <?php
      $conn = connDB();
      if($petsDet)
      {
        for($cnt = 0;$cnt < count($petsDet) ;$cnt++)
        {
        ?>
            <a href='puppyDetails.php?id=<?php echo $petsDet[$cnt]->getUid();?>'>

                  <div class="shadow-white-box featured four-box-size">
                        <div class="width100 white-bg">
                              <img src="uploads/<?php echo $petsDet[$cnt]->getImageOne();?>" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                        </div>
                        <div class="width100 product-details-div">
                              <p class="width100 text-overflow slider-product-name"><?php echo $petsDet[$cnt]->getName();?></p>
                              <p class="slider-product-price">

                                    RM<?php $length = strlen($petsDet[$cnt]->getPrice());?>
                                    <?php echo substr($petsDet[$cnt]->getPrice(),0,1);?>
                                    <?php
                                    if($length == 2)
                                    {
                                    echo "X";
                                    }
                                    elseif($length == 3)
                                    {
                                    echo "XX";
                                    }
                                    elseif($length == 4)
                                    {
                                    echo "XXX";
                                    }
                                    elseif($length == 5)
                                    {
                                    echo "XXXX";
                                    }
                                    elseif($length == 6)
                                    {
                                    echo "XXXXX";
                                    }
                                    elseif($length == 7)
                                    {
                                    echo "XXXXXX";
                                    }
                                    ?>

                              </p>
                              <p class="width100 text-overflow slider-location"><?php echo $petsDet[$cnt]->getGender();?></p>
                        </div>
                  </div>
            </a>

        <?php
        }
        ?>
      <?php
      }
      $conn->close();
      ?>

    </div>



      <div id="filter-modal" class="modal-css">
            <div class="modal-content-css filter-modal">
                  <span class="close-css close-filter">&times;</span>
                  <h2 class="green-text h2-title">Filter</h2>
                  <div class="green-border filter-border"></div>
                  <form method="POST" action="malaysia-cute-pets-filter.php">
                        <div class="filter-div-section">
                              <label for="breedGroup" class="filter-label filter-label3">Breed Group
                                    <input type="checkbox" name="breedGroup" id="breedGroup" class="filter-input"/>
                                    <span class="checkmark"></span>
                              </label>
                              <div id="breedGroupDiv" style="display:none">
                                    <!-- <input type="text" placeholder="Search" class="filter-search clean">
                                    <button class="transparent-button filter-search-btn">
                                          <img src="img/search.png" class="filter-search-img opacity-hover" alt="Search" title="Search">
                                    </button> -->
                                    <div class="clear"></div>
                                    <?php
                                    for ($cnt=0; $cnt <count($breedDetails) ; $cnt++)
                                    {
                                    ?>
                                          <div class="filter-option">
                                                <label class="filter-label filter-label2"><?php echo $breedDetails[$cnt]->getName();?>
                                                      <input type="checkbox" name="breed_type" id="breed_type"
                                                            value="<?php echo $breedDetails[$cnt]->getName(); ?>" class="filter-option"/>
                                                      <span class="checkmark"></span>
                                                </label>
                                          </div>
                                    <?php
                                    }
                                    ?>
                              </div>
                        </div>

                        <div class="filter-div-section">
                              <label for="gender" class="filter-label filter-label3">Gender
                              </label>

                              <select class="clean input-name admin-input" type="text" name="gender" id="gender">
                                    <option value="" name=" ">Select a Gender</option>
                                    <option value="Male" name="Male">Male</option>
                                    <option value="Female" name="Female">Female</option>
                              </select>
                        </div>

                        <div class="filter-div-section">
                              <label for="color" class="filter-label filter-label3">Color
                                    <input type="checkbox" name="color" id="color" class="filter-input" />
                                    <span class="checkmark"></span>
                              </label>
                              <div id="colorDiv" style="display:none">
                              <div class="clear"></div>
                                    <?php
                                    for ($cnt=0; $cnt <count($colorDetails) ; $cnt++)
                                    {
                                    ?>
                                          <div class="filter-option">
                                                <label class="filter-label filter-label2"><?php echo $colorDetails[$cnt]->getName();?>
                                                <input type="checkbox" name="color_type" id="color_type"
                                                      value="<?php echo $colorDetails[$cnt]->getName();?>" class="filter-option"/>
                                                <span class="checkmark"></span>
                                                </label>
                                          </div>
                                    <?php
                                    }
                                    ?>
                              </div>
                        </div>

                        <div class="filter-div-section">
                              <label for="state" class="filter-label filter-label3">Location
                              <input type="checkbox" name="state" id="state" value="0" class="filter-input" />
                              <span class="checkmark"></span>
                              </label>
                              <div id="stateDiv" style="display:none">
                                    <!-- <input type="text" placeholder="Search" class="filter-search clean">
                                    <button class="transparent-button filter-search-btn">
                                    <img src="img/search.png" class="filter-search-img opacity-hover" alt="Search" title="Search">
                                    </button> -->
                              <div class="clear"></div>
                                    <?php
                                    for ($cnt=0; $cnt <count($states) ; $cnt++)
                                    {
                                    ?>
                                          <div class="filter-option">
                                                <label class="filter-label filter-label2"><?php echo $states[$cnt]->getStateName();?>
                                                <input type="checkbox" name="state" id="state"
                                                      value="<?php echo $states[$cnt]->getStateName();?>" class="filter-option"/>
                                                <span class="checkmark"></span>
                                                </label>
                                          </div>
                                    <?php
                                    }
                                    ?>
                              </div>
                        </div>

                        <input class="clean pop-input no-bg-input" type="hidden" value="<?php echo $petType;?>" name="pet_type" id="pet_type" readonly>

                        <div class="clear"></div>

                        <button class="green-button mid-btn-width clean mid-btn-margin">Submit</button>
                  </form>
            </div>
      </div>



</div>

<div class="clear"></div>

<style>
	.animated.slideUp{
		animation:none !important;}
	.animated{
		animation:none !important;}
	.puppy-a .hover1a{
		display:none !important;}
	.puppy-a .hover1b{
		display:inline-block !important;}
</style>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>
