<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Seller.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

unset($_SESSION['register_uid']);

//$userRows = getSeller($conn,"WHERE id = ?",array("id"),array($id),"s");
$sellerDetails = getSeller($conn," WHERE account_status = ? ",array("account_status"),array('Active'),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="All Sellers | Mypetslibrary" />
<title>All Sellers | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance" id="myTable">
	<div class="width100">
        <div class="left-h1-div">
            <h1 class="green-text h1-title">All Sellers | <a href="partnerServices.php" class="green-a">Services</a></h1>
            <div class="green-border"></div>
        </div>
        <div class="mid-search-div">
        	<form>
            <input class="line-input clean" type="text" id="myInput" onkeyup="myFunction()" placeholder="Search">
                <button class="search-btn hover1 clean">
                        <img src="img/search.png" class="visible-img hover1a" alt="Search" title="Search">
                        <img src="img/search2.png" class="visible-img hover1b" alt="Search" title="Search">
                </button>
            </form>
        </div>
        <div class="right-add-div">
        	<a href="addSeller.php"><div class="green-button white-text">Add Seller</div></a>
        </div>
        
    </div>

    <div class="clear"></div>
	<div class="width100 scroll-div border-separation">
    	<table class="green-table width100">
        	<thead>
            	<tr>
                	<th class="first-column">No.</th>
                    <th>Company</th>
                    <th>Contact</th>
                    <th>Joined Date</th>
                    <th>Status</th>
                    <th>Details</th>
                    <th>Logo</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
            <?php
                if($sellerDetails)
                {
                    
                    for($cnt = 0;$cnt < count($sellerDetails) ;$cnt++)
                    {?>
                        
                        <tr class="link-to-details">
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $sellerDetails[$cnt]->getCompanyName();?></td>
                            <td>
                                <!-- <?php //echo $sellerDetails[$cnt]->getContactNo();?> -->

                                <?php
                                    $sellerContact = $sellerDetails[$cnt]->getContactNo();
                                    $str1 = $sellerContact;
                                    echo $sellerPhone = str_replace( 'https://api.whatsapp.com/send?phone=', '', $str1);
                                ?>

                            </td>
                            <td>
                                <!-- <?php //echo $sellerDetails[$cnt]->getDateCreated();?> -->
                                <?php echo $date = date("d-m-Y",strtotime($sellerDetails[$cnt]->getDateCreated()));?>
                            </td>
                            <td><?php echo $sellerDetails[$cnt]->getAccountStatus();?></td>
                            <!-- <td>
                                <form action="editSeller.php" method="POST" class="hover1">
                                    <button class="clean hover1 transparent-button pointer" type="submit" name="user_id" value="<?php echo $sellerDetails[$cnt]->getId();?>">
                                        <img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                                        <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                                    </button>
                                </form>
                            </td> -->

                            <td>
                                <form action="editSeller.php" method="POST" class="hover1">
                                    <button class="clean hover1 transparent-button pointer" type="submit" name="seller_uid" value="<?php echo $sellerDetails[$cnt]->getUid();?>">
                                        <img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                                        <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                                    </button>
                                </form>
                            </td>

                            <td>
                                <form action="updateCompanyLogo.php" method="POST" class="hover1">
                                    <button class="clean hover1 transparent-button pointer" type="submit" name="seller_uid" value="<?php echo $sellerDetails[$cnt]->getUid();?>">
                                        <img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                                        <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                                    </button>
                                </form>                            
                            </td>
                            <td>
                                    <!-- <a class="hover1 open-confirm pointer">
                                        <img src="img/delete1a.png" class="edit-icon1 hover1a" alt="Delete" title="Delete">
                                        <img src="img/delete3a.png" class="edit-icon1 hover1b" alt="Delete" title="Delete">
                                    </a>         -->
                                    
                                <!-- <form action="editSeller.php" method="POST" class="hover1"> -->
                                <form method="POST" action="utilities/deleteSellerFunction.php" class="hover1">
                                    <button class="clean hover1 transparent-button pointer" type="submit" name="seller_uid" value="<?php echo $sellerDetails[$cnt]->getUid();?>">
                                        <img src="img/delete1a.png" class="edit-icon1 hover1a" alt="Delete" title="Delete">
                                        <img src="img/delete3a.png" class="edit-icon1 hover1b" alt="Delete" title="Delete">
                                    </button>
                                </form>
                                    
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>
<div class="clear"></div>



<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Seller Deleted !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to delete seller !! ";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Seller Details Updated !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update user details !! ";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Fail to update seller details !! ";
        }

        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

}
?>

<script>
    function myFunction() 
    {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++)
        {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) 
            {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) 
                {
                    tr[i].style.display = "";
                } 
                else 
                {
                    tr[i].style.display = "none";
                }
            }       
        }
    }
</script>

</body>
</html>