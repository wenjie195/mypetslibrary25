<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Slider.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Update Slider | Mypetslibrary" />
<title>Update Slider | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<script src="jquery.min.js"></script>
<script src="bootstrap.min.js"></script>
<script src="croppie.js"></script>
<link rel="stylesheet" href="bootstrap.min.css" />
<link rel="stylesheet" href="croppie.css" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<!-- <?php //include 'userHeaderAfterLogin.php'; ?> -->
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
    <div class="width100">
        <h1 class="green-text h1-title">Update Slider</h1>
        <div class="green-border"></div>
    </div>

    <div class="border-separation">
        <div class="clear"></div>
        <!-- <form method="POST" action="utilities/updateSliderFunction.php" enctype="multipart/form-data"> -->
        <?php
            if(isset($_POST['slider_uid']))
            {
                $conn = connDB();
                $sliderDetails = getSlider($conn,"WHERE uid = ? ", array("uid") ,array($_POST['slider_uid']),"s");
                $sliderUid = $sliderDetails[0]->getUid();
            ?>

                <form method="POST" action="utilities/updateSliderFunction.php">
                
                  <div class="width100 overflow">
                  <p class="input-top-p admin-top-p">Link to</p>
                  <input class="input-name clean input-textarea admin-input" value="<?php echo $sliderDetails[0]->getLink() ?>" type="text" placeholder="https://mypetslibrary.com/" name="update_link" id="updateLink">
                  <input type="hidden" name="sliderUid" id="sliderUid" value="<?php echo $sliderUid ?>">
                  </div>

                  <div class="clear"></div>

                  <div class="width100 overflow text-center">
                    <button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
                  </div>
                  
                </form>

                <div class="clear"></div>

                <!-- <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Upload Photo</p>
                    <div class="width100 overflow">
                        <img src="uploadsSlider/<//?php echo $sliderDetails[0]->getImgName();?>" class="width100">
                    </div>

                    <p><input id="file-upload" type="file" name="update_image_one" id="update_image_one" accept="image/*" required></p>

                    <input type="hidden" id="old_img" name="old_img" value="<//?php echo $sliderDetails[0]->getImgName() ?>" readonly>

                    <input type="hidden" id="slider_uid" name="slider_uid" value="<//?php echo $sliderDetails[0]->getUid() ?>" readonly>

                </div> -->

                <div class="width100 overflow">
                    <div class="width100 overflow">
                        <!-- <img src="img/slider.jpg" class="width100">  -->
                        <p>Current Photo</p>
                        <img src="uploadsSlider/<?php echo $sliderDetails[0]->getImgName();?>" class="width100">
                    </div>
                    <div class="clear"></div>
                    <p class="update-photo-p">Update Photo</p>
                    <div class="panel-body slider-panel-body">
                        <!-- <input id="file-upload" type="file" name="image_one" id="image_one" accept="image/*" /> -->
                        <input type="file" name="update_image_one" id="update_image_one" accept="image/*" required>

                        <div id="uploaded_image"></div>
                    </div>

                    <input type="hidden" id="old_img" name="old_img" value="<?php echo $sliderDetails[0]->getImgName() ?>" readonly>

                    <input type="hidden" id="slider_uid" name="slider_uid" value="<?php echo $sliderDetails[0]->getUid() ?>" readonly>
                </div>

                <div class="clear"></div>

                <!-- <div class="width100 overflow text-center">
                    <button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
                </div> -->

            <?php
            }
        ?>
        <!-- </form> -->
	</div>
</div>
<div class="clear"></div>

<!-- <//?php include 'js.php'; ?> -->

<div class="width100 same-padding green-footer">
	<p class="footer-p white-text">© <?php echo $time;?> Mypetslibrary, All Rights Reserved.</p>
</div>


<div id="uploadimageModal" class="modal slider-upload-big-div" role="dialog">
	<div class="modal-dialog slider-second-div">
		<div class="modal-content slider-third-div">
      		<div class="modal-header slider-header">
        		<button type="button" class="close clean" data-dismiss="modal">&times;</button>
        		<h4 class="modal-title">Upload & Crop Image</h4>
      		</div>
      		<div class="modal-body slider-body">


						  <div id="image_demo" class="slider-cropping-div"></div>
						  <p class="recommend-p"><b>Recommended Size:</b> 1920px (width) X 738px (height)</p>
						  <div class="width100 overflow text-center">
						  	<button class="green-button mid-btn-width btn-success crop_image cropping-btn clean">Crop Image</button>
						  </div>

      		</div>

    	</div>
    </div>
</div>



<script>

$(document).ready(function(){

	$image_crop = $('#image_demo').croppie({

		enableExif: true,
		viewport:{
			width:'default',
			height:'default',
			type:'square' //circle
		},
		boundary:{
		width:'default',
		height:'default',
		}
	});

  $('#update_image_one').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });

  $('.crop_image').click(function(event){
  var updateLink = $("#updateLink").val();
  var sliderUid = $("#sliderUid").val();
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'original'
    }).then(function(response){
      $.ajax({
        url:"uploaded.php",
        type: "POST",
        data:{"image": response,updateLink:updateLink,sliderUid:sliderUid},
        success:function(data)
        {
          $('#uploadimageModal').modal('hide');
          $('#uploaded_image').html(data);
        }
      });
    })
  });

});

</script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>

</body>
</html>