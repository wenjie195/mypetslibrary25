<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/Slider.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$slider = getSlider($conn," WHERE status = 'Show' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Slider | Mypetslibrary" />
<title>Slider | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance">
	<div class="width100">
		<div class="left-h1-div two-left-h1-div">
            <h1 class="green-text h1-title">Slider</h1>
            <div class="green-border"></div>
		</div>
        <div class="right-add-div two-right-add-div">
        	<a href="addSlider.php"><div class="green-button white-text">Add Slider</div></a>
        </div>        
    </div>

    <div class="clear"></div>


    <div class="width100 border-separation">

            <?php
            $conn = connDB();
            if($slider)
            {
                for($cnt = 0;$cnt < count($slider) ;$cnt++)
                {
                ?>
                <div class="width100 overflow margin-bottom-25">
                    <div class="clear"></div>                
                
                    <p class="review-product-name left-slider">Slider <?php echo ($cnt+1)?></p>

                    <!-- <div class="right-slider">
                        <div class="clean red-btn delete-btn2 open-confirm slider-delete">Delete</div>
                        <a href="updateSlider.php"><div class="clean green-button close-confirm slider-update">Update</div></a>
                    </div> -->

                    <div class="right-slider">
                        <form method="POST" action="utilities/deleteSliderFunction.php">
                            <button class="clean red-btn delete-btn2 open-confirm slider-delete" type="submit" name="slider_uid" value="<?php echo $slider[$cnt]->getUid();?>">
                                Delete
                            </button>
                        </form>

                        <form method="POST" action="updateSlider.php">
                            <button class="clean green-button close-confirm slider-update" type="submit" name="slider_uid" value="<?php echo $slider[$cnt]->getUid();?>">
                                Update
                            </button>
                        </form>
                    </div>

                    <div class="clear"></div>

                    <div class="width100 overflow">
                        <img src="uploadsSlider/<?php echo $slider[$cnt]->getImgName();?>" class="width100"> 
                    </div>
                    
                    <div class="clear"></div>
                </div>                    
                    
                <?php
                }
                ?>
            <?php
            }
            $conn->close();
            ?>


    </div>


	<!-- <div class="width100 border-separation">
 		<div class="width100 overflow margin-bottom-25">
        <form>
        	<p class="review-product-name left-slider">Slider 1</p>
            <div class="right-slider">
					<div class="clean red-btn delete-btn2 open-confirm slider-delete">Delete</div>
                    <a href="updateSlider.php"><div class="clean green-button close-confirm slider-update">Update</div></a>
            </div>
            <div class="clear"></div>
            <div class="width100 overflow">
            	<img src="img/slider.jpg" class="width100"> 
            </div>
            
            

                            <div id="confirm-modal" class="modal-css">
                              <div class="modal-content-css confirm-modal-margin">
                                <span class="close-css close-confirm">&times;</span>
                                <div class="clear"></div>
                                <h2 class="green-text h2-title confirm-title">Confirm Delete?</h2>
                                <div class="clean cancel-btn close-confirm">Cancel</div>
                                <button class="clean red-btn delete-btn2">Delete</button>
                                <div class="clear"></div>
                              </div>
                            </div>             
        </form>    
        </div>
        <div class="clear"></div>
 		<div class="width100 overflow margin-bottom-25">
   
        	<p class="review-product-name left-slider">Slider 2</p>
            <div class="right-slider">
					<div class="clean red-btn delete-btn2 open-confirm slider-delete">Delete</div>
                    <a href="updateSlider.php"><div class="clean green-button close-confirm slider-update">Update</div></a>
            </div>
            <div class="clear"></div>
            <div class="width100 overflow">
            	<img src="img/slider.jpg" class="width100"> 
            </div>

        </div> 
        <div class="clear"></div>       
 		<div class="width100 overflow margin-bottom-25">

        	<p class="review-product-name left-slider">Slider 3</p>
            <div class="right-slider">
					<div class="clean red-btn delete-btn2 open-confirm slider-delete">Delete</div>
                    <a href="updateSlider.php"><div class="clean green-button close-confirm slider-update">Update</div></a>
            </div>
            <div class="clear"></div>
            <div class="width100 overflow">
            	<img src="img/slider.jpg" class="width100"> 
            </div>

        </div>        
        <div class="clear"></div>
        
        
    </div>
    <div class="clear"></div> -->

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Slider Added !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to add new slider !!";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Slider Deleted !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Fail to delete slider !";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "ERROR !!";
        }

        else if($_GET['type'] == 6)
        {
            $messageType = "Slider Details Updated !";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "Fail to update slider !";
        }
        else if($_GET['type'] == 8)
        {
            $messageType = "ERROR during update slider !!";
        }

        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>