<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

$timestamp = time();

include 'selectFilecss.php';

require_once('vendor/php-excel-reader/excel_reader2.php');
require_once('vendor/SpreadsheetReader.php');

if (isset($_POST["import"]))
{
  $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
  if(in_array($_FILES["file"]["type"],$allowedFileType))
  {
    $targetPath = 'uploads_old/'.$timestamp.$_FILES['file']['name'];
    move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
    $Reader = new SpreadsheetReader($targetPath);
    $sheetCount = count($Reader->sheets());
    for($i=0;$i<$sheetCount;$i++)
    {
      $Reader->ChangeSheet($i);
      foreach ($Reader as $Row)
      {


        $experience = "";
        if(isset($Row[0]))
        {
          $experience = mysqli_real_escape_string($conn,$Row[1]);
        }
        $services = "";
        if(isset($Row[0]))
        {
          $services = mysqli_real_escape_string($conn,$Row[2]);
        }
        $breed_type = "";
        if(isset($Row[0]))
        {
          $breed_type = mysqli_real_escape_string($conn,$Row[3]);
        }
        $contact_no = "";
        if(isset($Row[0]))
        {
          $contact_no = mysqli_real_escape_string($conn,$Row[4]);
        }
        $rating = "";
        if(isset($Row[0]))
        {
          $rating = mysqli_real_escape_string($conn,$Row[5]);
        }
        $state = "";
        if(isset($Row[0]))
        {
          $state = mysqli_real_escape_string($conn,$Row[6]);
        }
        $slug = "";
        if(isset($Row[0]))
        {
          $slug = mysqli_real_escape_string($conn,$Row[7]);
        }
        $contact_person = "";
        if(isset($Row[0]))
        {
          $contact_person = mysqli_real_escape_string($conn,$Row[8]);
        }
        $cert = "";
        if(isset($Row[0]))
        {
          $cert = mysqli_real_escape_string($conn,$Row[9]);
        }
        $contact_person_no = "";
        if(isset($Row[0]))
        {
          $contact_person_no = mysqli_real_escape_string($conn,$Row[10]);
        }
        $abcd = "";
        if(isset($Row[0]))
        {
          $abcd = mysqli_real_escape_string($conn,$Row[11]);
        }
        $aaaa = "";
        if(isset($Row[0]))
        {
          $aaaa = mysqli_real_escape_string($conn,$Row[12]);
        }
        $aabb = "";
        if(isset($Row[0]))
        {
          $aabb = mysqli_real_escape_string($conn,$Row[13]);
        }
        $aacc = "";
        if(isset($Row[0]))
        {
          $aacc = mysqli_real_escape_string($conn,$Row[14]);
        }
        $aadd = "";
        if(isset($Row[0]))
        {
          $aadd = mysqli_real_escape_string($conn,$Row[15]);
        }
        $aaee = "";
        if(isset($Row[0]))
        {
          $aaee = mysqli_real_escape_string($conn,$Row[16]);
        }
        

        // $uid = md5(uniqid());

        if (!empty($experience) || !empty($services) || !empty($breed_type) || !empty($contact_no) || !empty($rating) || !empty($state) || !empty($slug) || !empty($contact_person) || !empty($cert) || !empty($contact_person_no) || !empty($abcd) || !empty($aaaa) || !empty($aabb) || !empty($aacc) || !empty($aadd) || !empty($aaee))
        {
          $query = "INSERT INTO pets_data (name,new_price,sku,age,new_gender,new_vaccinated,new_dewormed,color,status,seller,type,breed,slug,video_url,new_size,more_information) 
                      VALUES ('".$experience."','".$services."','".$breed_type."','".$contact_no."','".$rating."','".$state."','".$slug."','".$contact_person."','".$cert."','".$contact_person_no."','".$abcd."','".$aaaa."','".$aabb."','".$aacc."','".$aadd."','".$aaee."') ";
          $result = mysqli_query($conn, $query);
          if (! empty($result))
          {
            echo "<script>alert('Excel File Uploaded !!');window.location='../mypetlibrary22/adminAddPetsExcel.php'</script>";
          }
          else 
          {
            echo "<script>alert('Fail to upload excel file !!');window.location='../mypetlibrary22/adminAddPetsExcel.php'</script>";
          }
        }
      }
    }
  }
  else
  {
    echo "<script>alert('Invalid File Type. Upload Excel File.');window.location='../mypetlibrary22/adminAddPetsExcel.php'</script>";
  }
}
?>

<!DOCTYPE html>
<html>
<head>
  <?php include 'meta.php'; ?>
  <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
  <meta property="og:title" content="Add Seller | MPL" />
  <title>Add Seller | MPL</title>
  <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
  <?php include 'css.php'; ?>
</head>


<body class="body">

<?php include 'header.php'; ?>
<div class="next-to-sidebar"> 

  <div class="width100 same-padding menu-distance min-height2 text-center">
  	<h1 class="h1-title green-text">Add Seller</h1>
    <form action="" method="post" name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
     <input type="file" name="file" id="file" class="choose-file-input" accept=".xls,.xlsx"><div class="clear"></div>
      <button type="submit" id="submit" name="import"  class="clean blue-gradient-button">Submit</button>
      <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>
    </form>
  </div>

<style>
.import-li{
color:#bf1b37;
background-color:white;}
.import-li .hover1a{
display:none;}
.import-li .hover1b{
display:block;}
</style>

<?php include 'js.php'; ?>

<script  src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
    </script>

</body>
</html>