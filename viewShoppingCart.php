<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/PreOrderList.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $products = getPreOrderList($conn, "WHERE status = 'Pending' ");
// $products = getPreOrderList($conn, "WHERE user_uid = ? ",array("user_uid"),array($uid),"s");
$products = getPreOrderList($conn, "WHERE user_uid = ? AND status = 'Pending' ",array("user_uid"),array($uid),"s");
// $products = getPreOrderList($conn, "WHERE user_uid = ? AND status != 'Delete' ",array("user_uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="View Cart | Mypetslibrary" />
<title>View Cart | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
                    
<div class="width100 same-padding menu-distance min-height big-black-text user-dash user-dash2 product-padding">
    <div class="width100 overflow">
        <h1 class="green-text user-title left-align-title">Cart</h1>
    </div> 

    <div class="clear"></div>

    <!-- <form method="POST" action="utilities/orderListFunction.php"> -->

        <div class="width100 scroll-div border-separation">
            <table class="green-table width100">
                <thead>
                    <tr>
                        <!-- <th><input type="checkbox" id="checkAll" name="" value=""> </th> -->
                        <th>No.</th>
                        <th>Product Name</th>
                        <th>Unit Price (RM)</th>
                        <th>Quantity&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th>Subtotal (RM)</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($products)
                        {
                            for($cnt = 0;$cnt < count($products) ;$cnt++)
                            {
                            ?>
                                <tr>
                                    <!-- <td>
                                        <input class="checkBox" name="arr[]" type="checkbox" value="<?php //echo $products[$cnt]->getId();?>">
                                    </td> -->
                                    <td><?php echo ($cnt+1)?></td>
                                    <td><?php echo $products[$cnt]->getProductName();?></td>
                                    <td><?php echo $products[$cnt]->getOriginalPrice();?></td>

                                    <!-- <td><?php echo $products[$cnt]->getQuantity();?></td> -->

                                    <td class="quantity-td">
                                    
                                        <form method="POST" action="utilities/preOrderSubtractFunction.php" class="oz-form">
                                            <input class="clean" type="hidden" value="<?php echo $products[$cnt]->getOriginalPrice();?>" id="product_price" name="product_price" readonly>
                                            <input class="clean" type="hidden" value="<?php echo $products[$cnt]->getQuantity();?>" id="product_quantity" name="product_quantity" readonly>
                                            <button class="clean left-minus oz-btn" type="submit" name="preorder_id" value="<?php echo $products[$cnt]->getId();?>">
                                                -
                                            </button>
                                        </form>

                                       
                                        <p class="oz-p"><?php echo $products[$cnt]->getQuantity();?></p>
                                       

                                        <form method="POST" action="utilities/preOrderAddFunction.php"  class="oz-form">
                                            <input class="clean" type="hidden" value="<?php echo $products[$cnt]->getOriginalPrice();?>" id="product_price" name="product_price" readonly>
                                            <input class="clean" type="hidden" value="<?php echo $products[$cnt]->getQuantity();?>" id="product_quantity" name="product_quantity" readonly>
                                            <button class="clean right-add oz-btn" type="submit" name="preorder_id" value="<?php echo $products[$cnt]->getId();?>">
                                                +
                                            </button>
                                        </form>
                                        
                                    </td>

                                    <td><?php echo $products[$cnt]->getTotalPrice();?></td>

                                    <td>
                                        <form method="POST" action="utilities/deletePreOrderFunction.php" class="hover1">
                                            <button class="clean hover1 img-btn" type="submit" name="preorder_id" value="<?php echo $products[$cnt]->getId();?>">
                                                <img src="img/delete1a.png" class="edit-icon1 hover1a" alt="Delete" title="Delete">
                                                <img src="img/delete3a.png" class="edit-icon1 hover1b" alt="Delete" title="Delete">
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                    ?>                                 
                </tbody>
            </table>

            <?php
                if($products)
                {
                ?>
                    <form method="POST" action="utilities/orderListFunction.php" >
                        <button class="green-button white-text clean2 width100" style="margin-top:50px;" type="submit" name="user_uid" value="<?php echo $products[0]->getUserUid();?>">Checkout</button>
                    </form>
                <?php
                }
            ?> 

            
                <p class="text-center" style="margin-top:20px;"><a style="font-size:18px;" href="malaysia-pets-products.php" class="green-a">Continue Shopping</a></p>
           

        </div>

        <!-- <button class="clean tele-btn" type="submit" id="submit" name="order_list" value="<?php //echo $products[$cnt]->getId(); ?>">Proceed To Checkout</button> -->

    <!-- </form> -->

</div>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

<script>
  $("#checkAll").on("click",function(){
    var check = $(this).is(':checked')?true:false;
      $(".checkBox").prop('checked',check);
  });
</script>

</body>
</html>