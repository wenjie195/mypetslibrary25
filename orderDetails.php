<?php
// require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
// require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Order Details | Mypetslibrary" />
<title>Order Details | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
        <h1 class="green-text h1-title">Order Details</h1>
        <div class="green-border"></div>
    </div>

    <div class="border-separation ow-invoice-css">

        <div class="clear"></div>

        <?php
            if(isset($_POST['order_id']))
            {
                $conn = connDB();
                // $ordersDetails = getOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"s");
                $ordersDetails = getOrders($conn,"WHERE order_id = ? ORDER BY date_created DESC LIMIT 1  ", array("order_id") ,array($_POST['order_id']),"s");
                $orderId = $ordersDetails[0]->getId();

                $productOrder = getOrderList($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"s");
            ?>

                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Order ID: <b>#<?php echo $orderId;?></b></p>
                </div>

                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Purchaser Name: <b><?php echo $ordersDetails[0]->getName();?></b></p>
                </div>

                <div class="clear"></div>

                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Contact: <b><?php echo $ordersDetails[0]->getContactNo();?></b></p>
                </div>

                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Address: <b><?php echo $ordersDetails[0]->getAddressLine1();?></b></p>
                </div> 

                <div class="clear"></div>

                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Payment References: <b><?php echo $ordersDetails[0]->getPaymentBankReference();?></b></p>
                </div> 
                <?php 
                    // $paymentStatus = $ordersDetails[0]->getShippingStatus();
                    $paymentStatus = $ordersDetails[0]->getPaymentStatus();
                    // if($shippingStatus = 'DELIVERED')
                    if($paymentStatus == 'ACCEPTED')
                    {
                    ?>
                        <div class="dual-input second-dual-input">
                            <p class="input-top-p admin-top-p">Status: <b>Shipped</b></p>
                        </div>          

                        <div class="clear"></div>       

                        <div class="dual-input">
                            <p class="input-top-p admin-top-p">Shipping Method: <b><?php echo $ordersDetails[0]->getShippingMethod();?></b></p>
                        </div>        
                        
                
                        <div class="dual-input second-dual-input">
                            <p class="input-top-p admin-top-p">Tracking Number: <b><?php echo $ordersDetails[0]->getTrackingNumber();?></b></p>   
                        </div>

                        <div class="clear"></div>

                        <div class="dual-input">
                            <p class="input-top-p admin-top-p">Shipped Date: <?php echo $ordersDetails[0]->getShippingDate();?></p>
                        </div> 
                    <?php
                    }
                    // elseif($shippingStatus = 'REJECTED')
                    elseif($paymentStatus == 'REJECTED')
                    {
                    ?>
                    <div class="dual-input second-dual-input">
                        <!-- <p class="input-top-p admin-top-p">Status: <b><?php //echo $paymentStatus;?></b></p> -->
                        <p class="input-top-p admin-top-p">Status: <b>Order Rejected</b></p>
                    </div>                        
                    <div class="clear"></div>           
                            <div class="dual-input">
                                <!-- <p class="input-top-p admin-top-p">Date: <b><?php //echo $ordersDetails[0]->getShippingDate();?></b></p> -->
                                <p class="input-top-p admin-top-p">Date: <b><?php echo $ordersDetails[0]->getDateCreated();?></b></p>
                            </div>                      
                        
                    <?php
                    }
                    elseif($paymentStatus == 'PENDING')
                    {
                    ?>
                    <div class="dual-input second-dual-input">
                        <!-- <p class="input-top-p admin-top-p">Status: <b><?php //echo $paymentStatus;?></b></p> -->
                        <p class="input-top-p admin-top-p">Status: <b>Order Received</b></p>
                    </div>                        
                    <div class="clear"></div>           
                            <div class="dual-input">
                                <!-- <p class="input-top-p admin-top-p">Date: <b><?php //echo $ordersDetails[0]->getShippingDate();?></b></p> -->
                                <p class="input-top-p admin-top-p">Date: <b><?php echo $ordersDetails[0]->getDateCreated();?></b></p>
                            </div>                      
                        
                    <?php
                    }
                    elseif($paymentStatus == 'WAITING')
                    {
                    ?>
                    <div class="dual-input second-dual-input">
                        <!-- <p class="input-top-p admin-top-p">Status: <b><?php //echo $paymentStatus;?></b></p> -->
                        <p class="input-top-p admin-top-p">Status: <b>Transaction Failed</b></p>
                    </div>                        
                    <div class="clear"></div>           
                            <div class="dual-input">
                                <!-- <p class="input-top-p admin-top-p">Date: <b><?php //echo $ordersDetails[0]->getShippingDate();?></b></p> -->
                                <p class="input-top-p admin-top-p">Date: <b><?php echo $ordersDetails[0]->getDateCreated();?></b></p>
                            </div>                      
                        
                    <?php
                    }
                    // else
                    // {}
                ?>

            <?php
            }
            ?>
                <div class="clear"></div>


                <!-- <div class="width100 scroll-div border-separation"> -->
                <div class="width100 border-separation">
                    <table class="green-table width100">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Item</th>
                                <th>Quantity</th>
                                <th>Total Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if($productOrder)
                                {
                                    
                                    for($cnt = 0;$cnt < count($productOrder) ;$cnt++)
                                    {?>
                                        
                                        <tr class="link-to-details">
                                            <td><?php echo ($cnt+1)?></td>
                                            <td><?php echo $productOrder[$cnt]->getProductName();?></td>
                                            <td><?php echo $productOrder[$cnt]->getQuantity();?></td>
                                            <td>RM<?php echo $productOrder[$cnt]->getTotalPrice();?></td>
                                            <!-- <td>RM 1000.00</td> -->
                                        </tr>
                                        <?php
                                    }
                                }
                            ?>                                 
                        </tbody>
                    </table>
                </div>


        
	</div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>
</body>
</html>