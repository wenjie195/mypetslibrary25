<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/fblogin.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Reviews.php';
require_once dirname(__FILE__) . '/classes/ReviewsRespond.php';
require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pet Seller Review | Mypetslibrary" />
<title>Pet Seller Review | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="review-big-div width100 same-padding overflow menu-distance min-height">

<?php
// Program to display URL of current page.
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
$link = "https";
else
$link = "http";

// Here append the common URL characters.
$link .= "://";

// Append the host(domain name, ip) to the URL.
$link .= $_SERVER['HTTP_HOST'];

// Append the requested resource location to the URL
$link .= $_SERVER['REQUEST_URI'];

if(isset($_GET['id']))
{
    $referrerUidLink = $_GET['id'];
    // echo $referrerUidLink;
}
else
{
    $referrerUidLink = "";
    // echo $referrerUidLink;
}
?>

        <?php
        if(isset($_GET['id']))
        {
            $conn = connDB();
            $reviewDetails = getReviews($conn,"WHERE company_uid = ? AND display = 'Yes' ", array("company_uid") ,array($_GET['id']),"s");
            // $reviewDetails = getReviews($conn);
            if($reviewDetails)
            {
                for($cnt = 0;$cnt < count($reviewDetails) ;$cnt++)
                {
                ?>
                    <div class="one-row-review">
                        <div class="left-review-profile">
                            <!-- <img src="img/pet-seller2.jpg" class="profile-pic-css"> -->

                            <?php
                            $reviewAuthorUid =  $reviewDetails[$cnt]->getAuthorUid();

                            $userRows = getUser($conn,"WHERE uid = ? ", array("uid") ,array($reviewAuthorUid),"s");
                            $userData = $userRows[0];
                            $userProPic = $userData->getProfilePic();
                            ?>

                            <?php
                            if($userProPic != "")
                            {
                            ?>
                                <img src="userProfilePic/<?php echo $userProPic;?>" class="profile-pic-css">
                            <?php
                            }
                            else
                            {}
                            ?>

                        </div>
                        <div class="left-review-data">
                            <p class="review-username-p">
                                <!-- <?php //echo $reviewDetails[$cnt]->getAuthorName();?> -->
                                <?php $reviewWriter = $reviewDetails[$cnt]->getAuthorName();
                                    if($reviewWriter == 'admin')
                                    {
                                        echo "Mypetslibrary";
                                    }
                                    else
                                    {
                                        echo $reviewWriter;
                                    }
                                ?>
                            </p>

                            <div class="review-star-div">
                                <?php $display = $reviewDetails[$cnt]->getRating();?>

                                <?php
                                    if ($display == 1)
                                    {
                                        echo '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">';
                                    }
                                    else if ($display == 2)
                                    {
                                        echo '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">';
                                    }
                                    else if ($display == 3)
                                    {
                                        echo '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">',
                                            '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">';
                                    }
                                    else if ($display == 4)
                                    {
                                        echo '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">',
                                            '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">';
                                    }
                                    else if ($display == 5)
                                    {
                                        echo '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">',
                                        '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">',
                                        '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">';
                                    }
                                ?>

                            </div>
                        </div>

                        <div class="right-review-div">


                            <?php
                            if($uid != "")
                            {
                                // $userUid = $userData->getUid();
                                $articleUid = $reviewDetails[$cnt]->getUid();
                                $userUid = $uid;
                                // $articleUid = $_GET['id'];
                                // echo $userUid = $uid;
                                // echo "<br>";
                                // echo $articleUid = $_GET['id'];

                                $reviewUidAA = getReveiewsRespond($conn," WHERE review_uid = ? ",array("review_uid"),array($articleUid),"s");
                                $userReviewUidAA = $reviewUidAA[0];

                                $uidAA = getReveiewsRespond($conn," WHERE review_uid = ? AND uid = ? ",array("review_uid", "uid"),array($articleUid,$userUid),"ss");
                                $userUidAA = $uidAA[0];

                                $uidlike = getReveiewsRespond($conn," WHERE review_uid = ? AND uid = ? AND like_amount = '1' ",array("review_uid", "uid"),array($articleUid,$userUid),"ss");
                                $userUidLike = $uidlike[0];

                                $uidDislike = getReveiewsRespond($conn," WHERE review_uid = ? AND uid = ? AND dislike_amount = '1' ",array("review_uid", "uid"),array($articleUid,$userUid),"ss");
                                $userUidDislike = $uidDislike[0];


                                if($uidlike)
                                {
                                    $totalUidLike = count($uidlike);
                                }
                                else
                                {    $totalUidLike = 0;   }

                                if($uidDislike)
                                {
                                    $totalUidDislike = count($uidDislike);
                                }
                                else
                                {    $totalUidDislike = 0;   }


                                // $uidAA = getReveiewsRespond($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");
                                // $userUidAA = $uidAA[0];

                                if($userReviewUidAA == "" && $userUidAA == "")
                                {
                                ?>

                                    <form method="POST" action="utilities/likesReviewFunction.php" class="float-left">
                                        <button class="clean transparent-button like-button hover1 pointer react-button float-left" value="<?php echo $reviewDetails[$cnt]->getUid();?>" name="review_uid" id="review_uid" readonly>
                                            <img src="img/like.png" class="hover1a" alt="Like" title="Like">
                                            <img src="img/like2.png" class="hover1b" alt="Like" title="Like">
                                            <p class="like-no-p"><?php echo $reviewDetails[$cnt]->getLikes();?></p>
                                        </button>
                                    </form>

              <!--                      <a href='viewReview.php?id=<?php echo $reviewDetails[$cnt]->getUid();?>'>
                                        <input class="input-name clean float-left" type="hidden" value="<?php echo $reviewDetails[$cnt]->getUid();?>" name="review_uid"
                                            id="review_uid" readonly>
                                        <button class="clean transparent-button report-button hover1 pointer react-button open-report float-left">
                                            <img src="img/flag.png" class="hover1a" alt="Report" title="Report">
                                            <img src="img/flag2.png" class="hover1b" alt="Report" title="Report">
                                        </button>
                                    </a>
-->
                                <?php
                                }
                                // elseif($userReviewUidAA && $userUidAA != "")
                                elseif($totalUidLike == $totalUidDislike)
                                {
                                ?>

                                    <form method="POST" action="utilities/likesReviewFunction.php" class="float-left">
                                        <button class="clean transparent-button like-button hover1 pointer react-button float-left" value="<?php echo $reviewDetails[$cnt]->getUid();?>" name="review_uid" id="review_uid" readonly>
                                            <img src="img/like.png" class="hover1a" alt="Like" title="Like">
                                            <img src="img/like2.png" class="hover1b" alt="Like" title="Like">
                                            <p class="like-no-p"><?php echo $reviewDetails[$cnt]->getLikes();?></p>
                                        </button>
                                    </form>

<!--                                    <a href='viewReview.php?id=<?php echo $reviewDetails[$cnt]->getUid();?>'>
                                        <input class="input-name clean float-left" type="hidden" value="<?php echo $reviewDetails[$cnt]->getUid();?>" name="review_uid"
                                            id="review_uid" readonly>
                                        <button class="clean transparent-button report-button hover1 pointer react-button open-report float-left">
                                            <img src="img/flag.png" class="hover1a" alt="Report" title="Report">
                                            <img src="img/flag2.png" class="hover1b" alt="Report" title="Report">
                                        </button>
                                    </a>
-->
                                <?php
                                }

                                else
                                {
                                ?>

                                    <form method="POST" action="utilities/unlikesReviewFunction.php"  class="float-left">
                                        <button class="clean transparent-button like-button hover1 pointer react-button float-left" value="<?php echo $reviewDetails[$cnt]->getUid();?>" name="review_uid" id="review_uid" readonly>
                                            <img src="img/like.png" class="hover1a" alt="Like" title="Like">
                                            <img src="img/like2.png" class="hover1b" alt="Like" title="Like">
                                            <p class="like-no-p"><?php echo $reviewDetails[$cnt]->getLikes();?></p>
                                        </button>
                                    </form>
<!--
                                    <a href='viewReview.php?id=<?php echo $reviewDetails[$cnt]->getUid();?>'>
                                        <input class="input-name clean cloat-left" type="hidden" value="<?php echo $reviewDetails[$cnt]->getUid();?>" name="review_uid"
                                            id="review_uid" readonly>
                                        <button class="clean transparent-button report-button hover1 pointer react-button open-report float-left">
                                            <img src="img/flag.png" class="hover1a" alt="Report" title="Report">
                                            <img src="img/flag2.png" class="hover1b" alt="Report" title="Report">
                                        </button>
                                    </a>-->

                                <?php
                                }


                            }
                            else
                            {
                            ?>
                                <a href='userLogin.php'>
                                    <button class="clean transparent-button like-button hover1 pointer react-button float-left">
                                        <img src="img/like.png" class="hover1a" alt="Like" title="Like">
                                        <img src="img/like2.png" class="hover1b" alt="Like" title="Like">
                                        <p class="like-no-p"><?php echo $reviewDetails[$cnt]->getLikes();?></p>
                                    </button>
                                </a>
<!--
                                <a href='userLogin.php'>
                                    <button class="clean transparent-button article-report pointer opacity-hover float-left">
                                            <img src="img/flag.png" class="hover1a" alt="Report" title="Report">
                                            <img src="img/flag2.png" class="hover1b" alt="Report" title="Report">
                                    </button>
                                </a>-->
                            <?php
                            }
                            ?>

                        </div>

                        <div class="clear"></div>
                        <div class="width100 review-content">
                            <div class="table-review-comment">

                                <?php $reviewImange = $reviewDetails[$cnt]->getImage();?>
                                <?php
                                if($reviewImange != "")
                                {
                                ?>
                                     <a href="reviewImages/<?php echo $reviewImange;?>" data-fancybox="images-preview"  ><img src="reviewImages/<?php echo $reviewImange;?>" class="table-review-img opacity-hover"></a>
                                <?php
                                }
                                else
                                {}
                                ?>

                            </div>
                            <p class="review-content-p">
                                <?php echo $reviewDetails[$cnt]->getParagraphOne();?>
                            </p>
                        </div>
                        <p class="review-date"><?php echo $date = date("d-m-Y",strtotime($reviewDetails[$cnt]->getDateCreated()));?></p>

                    </div>

                <?php
                }
                ?>
            <?php
            }
            ?>
        <?php
        }
        ?>

        <div class="clear"></div>

        <?php
        if($uid == $referrerUidLink)
        {
        ?>
        <?php
        }
        elseif($uid !="")
        {
        ?>

            <div class="width100 text-center margin-top-bottom">
                <div class="green-button mid-btn-width open-review">Write a Review</div>
            </div>


            <!-- Review Modal -->
            <div id="review-modal" class="modal-css">
            <!-- Modal content -->
            <!-- <div class="modal-content-css forgot-modal-content login-modal-content review-modal-margin"> -->
            <div class="modal-content-css forgot-modal-content login-modal-content review-modal-margin star-review-modal">
                <span class="close-css close-review">&times;</span>
                <h2 class="green-text h2-title">Review</h2>
                <div class="green-border"></div>
                <div class="clear"></div>


                <!-- Upload  -->
                <!-- <form id="file-upload-form" class="uploader"> -->
                <form method="POST" action="utilities/submitReviewFunction.php" id="file-upload-form" class="uploader" enctype="multipart/form-data">
                <!-- <input id="file-upload" type="file" name="image_one" id="image_one" accept="image/*" class="uploader-input"/> -->

                <label for="file-upload" id="file-drag" class="uploader-label">
                    <img id="file-image" src="#" alt="Preview" class="hidden">
                    <div id="start" class="uploader-div">
                    <i class="fa fa-download" aria-hidden="true"></i>
                   <!-- <div>Select a file or drag here</div>-->
                    <div id="notimage" class="hidden">Please select an image</div>
                    <input id="file-upload" type="file" name="image_one" id="image_one" accept="image/*" class="uploader-input"/>
                    <!-- <span id="file-upload-btn" class="btn btn-primary">Upload Image</span> -->
                    </div>
                    <div id="response" class="hidden uploader-div">
                    <div id="messages"></div>
                    </div>
                </label>
                <!-- </form>    -->

                <div class="clear"></div>

                <fieldset class="rating">
                    <input name="rating" type="radio" id="rating5" value="5" on="change:rating.submit"/>
                    <label for="rating5" title="5 stars">☆</label>

                    <input name="rating" type="radio" id="rating4" value="4" on="change:rating.submit" />
                    <label for="rating4" title="4 stars">☆</label>

                    <input name="rating" type="radio" id="rating3" value="3" on="change:rating.submit" />
                    <label for="rating3" title="3 stars">☆</label>

                    <input name="rating" type="radio" id="rating2" value="2" on="change:rating.submit" />
                    <label for="rating2" title="2 stars">☆</label>

                    <input name="rating" type="radio" id="rating1" value="1" on="change:rating.submit" checked="checked" />
                    <label for="rating1" title="1 stars">☆</label>
                </fieldset>

                <div class="clear"></div>

                <?php
                $conn = connDB();
                $compDetails = getSeller($conn,"WHERE uid = ? ", array("uid") ,array($_GET['id']),"s");
                $compData = $compDetails[0];
                $reviewHeader = $compData->getCompanyName();
                $conn->close();
                ?>

                <input class="input-name clean input-textarea admin-input" type="hidden" value="<?php echo $reviewHeader; ?>"  name="review_title" id="review_title" readonly>

                <input class="input-name clean input-textarea admin-input" type="hidden" value="<?php echo $referrerUidLink; ?>"  name="company_uid" id="company_uid" readonly>

                <p class="input-top-p">Review</p>
                <textarea class="input-name clean review-textarea" type="text" placeholder="Write Your Review" name="review_details" name="review_details" required></textarea>
                <div class="clear"></div>
                <button class="green-button white-text width100 clean2" name="submit">Submit</button>
                </form>
            </div>
            </div>



        <?php
        }
        else
        {
        ?>
            <!-- <div class="width100 text-center margin-top-bottom"></div> -->

            <!-- <div class="width100 text-center margin-top-bottom open-login">
                <div class="green-button mid-btn-width open-login">Write a Review</div>
            </div> -->

            <a href='userLogin.php'>
                <div class="width100 text-center margin-top-bottom">
                    <div class="green-button mid-btn-width">Write a Review</div>
                </div>
            </a>

        <?php
        }
        ?>

    </div>

<div class="clear"></div>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Review submitted ! <br> Waiting approval from admin !";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to submit review !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Review has been reported and submitted to admin !!";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Unable to report the review !!";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Fail to report review !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

}
?>

</body>
</html>
