<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Reptile.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $reptileDetails = getReptile($conn," Order By date_created DESC ");
$reptileDetails = getReptile($conn," WHERE status != 'Deleted' Order By date_created DESC ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="All Reptiles | Mypetslibrary" />
<title>All Reptiles | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance" id="myTable">
	<div class="width100">
        <div class="left-h1-div">
            <h1 class="green-text h1-title">All Reptiles</h1>
            <div class="green-border"></div>
        </div>
        <div class="mid-search-div">
        <form>
            <input class="line-input clean" type="text" id="myInput" onkeyup="myFunction()" placeholder="Search">
                <button class="search-btn hover1 clean">
                    <img src="img/search.png" class="visible-img hover1a" alt="Search" title="Search">
                    <img src="img/search2.png" class="visible-img hover1b" alt="Search" title="Search">
                </button>
            </form>
        </div>
        <div class="right-add-div puppy-right-add-div">
        	<a href="addReptile.php"><div class="green-button white-text puppy-button">Add a Reptile</div></a>
        </div>
        <div class="puppy-button-div">
            <a href="reptileBreed.php"><div class="green-button white-text breed-button puppy-button mid-breeed-button">Breed List</div></a>	
            <a href="reptileColor.php"><div class="green-button white-text breed-button color-button puppy-button">Color List</div></a>
        </div>        
    </div>


    <div class="clear"></div>
	<div class="width100 scroll-div border-separation">
    	<table class="green-table width100" id="myTable">
        	<thead>
            	<tr>
                	<th class="first-column">No.</th>
                    <!--<th>Name</th>-->
                    <th>SKU</th>
                    <th>Seller</th>
                    <th>Added On</th>
                    <th>Status</th>
                    
                    <th>Mark As Sold</th>
                    <th>Details</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
            <?php
            if($reptileDetails)
                {
                    
                    for($cnt = 0;$cnt < count($reptileDetails) ;$cnt++)
                    {?>
                        
                        <tr class="link-to-details">
                            <td><?php echo ($cnt+1)?></td>
                            <!--<td><?php echo $reptileDetails[$cnt]->getBreed();?></td>-->
                            <td><?php echo $reptileDetails[$cnt]->getSKU();?></td>
                            <td>
                                <?php 
                                    $sellerUid = $reptileDetails[$cnt]->getSeller();

                                    $conn = connDB();
                                    $compDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($sellerUid),"s");
                                    $compData = $compDetails[0];
                                    echo $reviewHeader = $compData->getName();
                                    $conn->close();
                                ?>
                            </td>
                            <td>
                                <?php echo $date = date("d-m-Y",strtotime($reptileDetails[$cnt]->getDateCreated()));?>
                            </td>
                            <td>
                                <?php 
                                    $status = $reptileDetails[$cnt]->getStatus();
                                    if($status == 'Sold')
                                    {
                                        echo "Sold";
                                    }
                                    else
                                    {
                                        echo "For Sale";
                                    }
                                ?>
                            </td>
                            <td>
                                <?php 
                                    $petStatus = $reptileDetails[$cnt]->getStatus();
                                    if($petStatus == 'Sold')
                                    {
                                        echo "Sold";
                                    }
                                    else
                                    {
                                    ?>
                                        <form method="POST" action="utilities/adminUpdateSoldPets.php" class="hover1">
                                            <input class="input-name clean" type="hidden" value="Reptile"  name="pets_type" id="pets_type" readonly>
                                            <button class="clean hover1 transparent-button pointer" type="submit" name="pets_uid" value="<?php echo $reptileDetails[$cnt]->getUid();?>">
                                                <img src="img/tick1.png" class="edit-icon1 hover1a" alt="Mark As Sold" title="Mark As Sold">
                                        		<img src="img/tick.png" class="edit-icon1 hover1b" alt="Mark As Sold" title="Mark As Sold">
                                            </button>
                                        </form>
                                    <?php
                                    }
                                ?>
                            </td>
                            <td>
                                <form action="editReptile.php" method="POST" class="hover1">
                                    <button class="clean hover1 transparent-button pointer" type="submit" name="user_id" value="<?php echo $reptileDetails[$cnt]->getId();?>">
                                        <img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                                        <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                                    </button>
                                </form>                    
                            </td>
                            <td>
                                <form method="POST" action="utilities/adminDeletePets.php" class="hover1">
                                    <input class="input-name clean" type="hidden" value="Reptile"  name="pets_type" id="pets_type" readonly>
                                    <button class="clean hover1 transparent-button pointer" type="submit" name="pets_uid" value="<?php echo $reptileDetails[$cnt]->getUid();?>">
                                        <img src="img/delete1a.png" class="edit-icon1 hover1a" alt="Delete" title="Delete">
                                        <img src="img/delete3a.png" class="edit-icon1 hover1b" alt="Delete" title="Delete">
                                    </button>
                                </form>               
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>                               
            </tbody>
        </table>
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>
<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Pets as Sold"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update !!"; 
        }

        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Reptile Deleted !!"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>