<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';
require_once dirname(__FILE__) . '/classes/Kitten.php';
require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/Pets.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Puppy.php';
require_once dirname(__FILE__) . '/classes/Reptile.php';
require_once dirname(__FILE__) . '/classes/Reviews.php';
require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$userAmount = getUser($conn," WHERE user_type = 1 ");
// $sellerAmount = getSeller($conn);
$sellerAmount = getSeller($conn," WHERE account_status = 'Active' ");

// $kittenAmount = getKitten($conn);
// $puppyAmount = getPuppy($conn);
// $reptileAmount = getReptile($conn);

$kittenAmount = getKitten($conn," WHERE status = 'Available' ");;
$puppyAmount = getPuppy($conn," WHERE status = 'Available' ");;
$reptileAmount = getReptile($conn," WHERE status = 'Available' ");;

// $petAmount = getPetsDetails($conn);
$petAmount = getPetsDetails($conn," WHERE status = 'Available' ");
$pendingPet = getPetsDetails($conn," WHERE status = 'Pending' ");

$articles = getArticles($conn, " WHERE display = 'Pending' ");
$reviews = getReviews($conn, " WHERE display = 'Pending' ");
// $shippingRequest = getOrders($conn, " WHERE payment_status = 'ACCEPTED' AND shipping_status = 'PENDING' ");
$products = getProduct($conn, "WHERE status = 'Available' ");

$orderRequest = getOrders($conn, " WHERE payment_status = 'PENDING' AND shipping_status = 'TBC' ");

$messageDetails = getUser($conn, " WHERE message = 'YES' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Admin Dashboard | Mypetslibrary" />
<title>Admin Dashboard | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance eight-div-box">
	<h1 class="green-text h1-title">Dashboard</h1>
	<div class="green-border"></div>
    <div class="clear"></div>

        <div class="width100 border-separation">
        
            <div class="white-dropshadow-box four-div-box">
                <img src="img/partner.png" alt="Seller" title="Seller" class="four-div-img">
                <p class="four-div-p">Seller</p>

                <?php
                if($sellerAmount)
                {   
                    $totalSeller = count($sellerAmount);
                }
                else
                {   $totalSeller = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalSeller;?></b></p>
                <a href="addSeller.php"><p class="dashboard-p"><b>Add</b></p></a>
                <a href="seller.php"><p class="dashboard-p dashboard-p2"><b>View</b></p></a>
               
            </div> 


        
            <div class="white-dropshadow-box four-div-box second-four-div-box left-four-div">
                <img src="img/cute-dog.png" alt="Puppy" title="Puppy" class="four-div-img">
                <p class="four-div-p">Puppy</p>
                <?php
                if($puppyAmount)
                {   
                    $totalPuppyAmount = count($puppyAmount);
                }
                else
                {   $totalPuppyAmount = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalPuppyAmount;?></b></p>                
                <a href="addPuppy.php"><p class="dashboard-p"><b>Add</b></p></a>
                <a href="allPuppies.php"><p class="dashboard-p dashboard-p2"><b>View</b></p></a>
            </div>  
    
            <div class="white-dropshadow-box four-div-box right-four-div">
                <img src="img/kitten.png" alt="Kitten" title="Kitten" class="four-div-img">
                <p class="four-div-p">Kitten</p>
                <?php
                if($kittenAmount)
                {   
                    $totalKittenAmount = count($kittenAmount);
                }
                else
                {   $totalKittenAmount = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalKittenAmount;?></b></p>                
                <a href="addKitten.php"><p class="dashboard-p"><b>Add</b></p></a>
                <a href="allKittens.php"><p class="dashboard-p dashboard-p2"><b>View</b></p></a>
            </div>  
    	
            <div class="white-dropshadow-box four-div-box second-four-div-box forth-div  last-three-div">
                <img src="img/reptile.png" alt="Reptile" title="Reptile" class="four-div-img">
                <p class="four-div-p last-three-p">Reptile</p>
                <?php
                if($reptileAmount)
                {   
                    $totalReptileAmount = count($reptileAmount);
                }
                else
                {   $totalReptileAmount = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalReptileAmount;?></b></p>                
                <a href="addReptile.php"><p class="dashboard-p"><b>Add</b></p></a>
                <a href="allReptiles.php"><p class="dashboard-p dashboard-p2"><b>View</b></p></a>
            </div>
                    
            <div class="white-dropshadow-box four-div-box last-three-div">
                <img src="img/dog-food.png" alt="Products" title="Products" class="four-div-img">
                <p class="four-div-p second-row-p mm-p">Products</p>
                <?php
                if($products)
                {   
                    $totalProducts = count($products);
                }
                else
                {   $totalProducts = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalProducts;?></b></p>                
                <a href="addProduct.php"><p class="dashboard-p"><b>Add</b></p></a>
                <a href="allProducts.php"><p class="dashboard-p dashboard-p2"><b>View</b></p></a>
            </div>  
       
            <!-- <a href="paymentVerification.php" class="opacity-hover">        -->
            <a href="adminOrdersPending.php" class="opacity-hover">    
                <div class="white-dropshadow-box four-div-box second-four-div-box left-four-div six-box last-three-div">
                    <img src="img/delivery.png" alt="Shipping Requests" title="Shipping Requests" class="four-div-img">
                    <!-- <p class="four-div-p second-row-p">Shipping Requests</p> -->
                    <p class="four-div-p second-row-p mm-p">Pending Order Requests</p>

                    <?php
                    if($orderRequest)
                    {   
                        $totalShippingRequest = count($orderRequest);
                    }
                    else
                    {   $totalShippingRequest = 0;   }
                    ?>

                    <p class="four-div-amount-p"><b><?php echo $totalShippingRequest;?></b></p>
                    <p class="dashboard-p"><b>&nbsp;</b></p>
                    <p class="dashboard-p dashboard-p2"><b>&nbsp;</b></p>
                    
                </div>  
            </a>        
            
            <a  href="pendingReview.php"class="opacity-hover">
                <div class="white-dropshadow-box four-div-box right-four-div">
                    <img src="img/review-pet.png" alt="Pending Reviews" title="Pending Reviews" class="four-div-img">
                    <p class="four-div-p last-three-p last-two-p second-row-p mm-p">Pending Reviews</p>
                    <?php
                    if($reviews)
                    {   
                        $totalPendingReviews = count($reviews);
                    }
                    else
                    {   $totalPendingReviews = 0;   }
                    ?>
                    <p class="four-div-amount-p"><b><?php echo $totalPendingReviews;?></b></p>
                    <p class="dashboard-p"><b>&nbsp;</b></p>
                    <p class="dashboard-p dashboard-p2"><b>&nbsp;</b></p>				
                </div> 
            </a>  

            <a href="allUsers.php" class="opacity-hover">
                <div class="white-dropshadow-box four-div-box second-four-div-box forth-div three-mid-div">
                    <img src="img/pet-sellers.png" alt="Total Users" title="Total Users" class="four-div-img">
                    <p class="four-div-p last-three-p last-two-p second-row-p mm-p">Total Users</p>

                    <?php
                    if($userAmount)
                    {   
                        $totalUser = count($userAmount);
                    }
                    else
                    {   $totalUser = 0;   }
                    ?>
                    <p class="four-div-amount-p"><b><?php echo $totalUser;?></b></p>
                    <p class="dashboard-p"><b>&nbsp;</b></p>
                    <p class="dashboard-p dashboard-p2"><b>&nbsp;</b></p>				
                </div> 
            </a>
            <a href="adminMessageNew.php">
             <div class="white-dropshadow-box four-div-box last-boxx">
                <img src="img/chat-request.png" alt="Chat" title="Chat" class="four-div-img">
                <p class="four-div-p">Chat</p>

                <?php
                if($messageDetails)
                {   
                    $totalMessage = count($messageDetails);
                }
                else
                {   $totalMessage = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalMessage;?></b></p>
               
            </div> 
            </a>            
        </div>

        <div class="clear"></div>
</div>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Password Successfully"; 
        }
        else if($_GET['type'] == 2)
        {
            // $messageType = "Fail to update password !! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>