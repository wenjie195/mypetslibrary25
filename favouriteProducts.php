<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Favorite.php';
// require_once dirname(__FILE__) . '/classes/Pets.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
$_SESSION['url'] = $_SERVER['REQUEST_URI'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$allFavorite = getFavorite($conn, "WHERE uid =? AND status = 'Yes' ",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Favourite Pets | Mypetslibrary" />
<title>Favourite Pets | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

        <div class="width100 same-padding overflow min-height menu-distance2">
        
            <h1 class="green-text seller-h1">My Favourite Products</h1>
        
            <div class="clear"></div>  
     		
            <div class="width103">

            <?php
                if($allFavorite)
                {
                    for($cnt = 0;$cnt < count($allFavorite) ;$cnt++)
                    {
                    ?>
                        <?php $petsUid = $allFavorite[$cnt]->getItemUid();?>

                        <?php
                        $conn = connDB();
                        $favoriteProduct = getProduct($conn,"WHERE uid = ? ", array("uid") ,array($petsUid),"s");
                        if($favoriteProduct)
                        {
                            for($cntAA = 0;$cntAA < count($favoriteProduct) ;$cntAA++)
                            {
                            ?>

                                <?php
                                    $productType = $allFavorite[$cnt]->getType();
                                    if($productType == 'Product')
                                    {
                                    ?>
                                        <a href='malaysia-pets-products-details.php?id=<?php echo $petsUid;?>'>
                                    <?php
                                    }
                                    else
                                    {   }
                                ?>

                                    <div class="shadow-white-box four-box-size opacity-hover">
                                    <div class="square">     
                                        <div class="width100 white-bg content">
                                            <img src="productImage/<?php echo $favoriteProduct[$cntAA]->getImageOne();?>" alt="" title="" class="width100 two-border-radius">
                                        </div>
                                    </div> 

                                    <div class="width100 product-details-div">

                                    <p class="width100 text-overflow slider-product-name"><?php echo $favoriteProduct[$cntAA]->getName();?></p>
                                    <p class="slider-product-name"><?php echo $favoriteProduct[$cntAA]->getPrice();?></p>
                                    </div>

                                    </div>
                                </a> 

                            <?php
                            }
                            ?>
                        <?php
                        }
                        ?>
                        



                    <?php
                    }
                }
            ?>  
            </div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>