<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/fblogin.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
$_SESSION['url'] = $_SERVER['REQUEST_URI'];

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    addToCart();
    header('Location: ./viewCart.php');
}

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$products = getProduct($conn, "WHERE status = 'Available' ORDER BY date_created DESC ");

$productListHtml = "";

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,1,true);
}else{
    if(isset($_POST['product-list-quantity-input'])){
        $productListHtml = createProductList($products,1,$_POST['product-list-quantity-input']);
    }else{
        $productListHtml = createProductList($products);
    }
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pet Food, Toys and Etc | Mypetslibrary" />
<title>Pet Food, Toys and Etc | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
                    
<div class="min-height overflow width100">

    <div class="width100 small-padding overflow min-height menu-distance2">
        <!-- <p class="review-product-name">All Products</p>    -->
        <p class="review-product-name">All Product</p>  
        <div class="width103 product-big-div product-big-div2">

                    <?php
                    if($uid != "")
                    {
                    ?>
					<style>
                    .x-back{
						display:none !important;}
                    </style>
                        <form method="POST">
                            <?php echo $productListHtml; ?>
                            <div class="clear"></div>
                                                            </div>
                            </div>
                            
                        </div> <div class="clear"></div>
                            <div class="width100 text-center stay-bottom-add"> 
                                <button class="ow-peach-bg clean black-button add-to-cart-btn green-button checkout-btn purchase-btn">View Cart</button>
                            </div>    
                            <div class="stay-bottom-height"></div>
                        </form>

                    <?php
                    }
                    else
                    {
                    ?>

                        <?php echo $productListHtml; ?>
                        <style>
						.input-quantity-div, .bottom-height1{
							display:none;}
			
						</style>
                        <div class="clear"></div>
                                </div>
                            </div>
                            
                        </div> <div class="clear"></div>

                        <div class="width100 text-center stay-bottom-add"> 
                            <button class="clean black-button add-to-cart-btn green-button checkout-btn open-login purchase-btn">Login to Purchase</button>
                            <!-- <button class="clean black-button add-to-cart-btn green-button open-login">Add To Cart</button> -->
                        </div>    
                        <div class="stay-bottom-height"></div>

                    <?php
                    }
                    ?>

            <!-- <form method="POST">
                <?php //echo $productListHtml; ?>
                <div class="clear"></div>
                <div class="width100 text-center stay-bottom-add"> 
                    <button class="clean black-button add-to-cart-btn green-button checkout-btn">Add To Cart</button>
                </div>    
                <div class="stay-bottom-height"></div>
            </form> -->

<div class="clear"></div>

<?php include 'js.php'; ?>

<style>
.green-footer{
	display:none;}
</style>
<script>
function goBack() {
  window.history.back();
}
</script>

<script>
    $(".button-minus").on("click", function(e)
    {
        e.preventDefault();
        var $this = $(this);
        var $input = $this.closest("div").find("input");
        var value = parseInt($input.val());
        if (value > 1)
        {
            value = value - 1;
        } 
        else 
        {
            value = 0;
        }
        $input.val(value);
    });

    $(".button-plus").on("click", function(e)
    {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest("div").find("input");
    var value = parseInt($input.val());
    if (value < 100)
    {
        value = value + 1;
    }
    else
    {
        value = 100;
    }
    $input.val(value);
    });
</script>

</body>
</html>